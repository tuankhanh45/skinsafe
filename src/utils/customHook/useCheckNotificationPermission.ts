import { useEffect, useState } from 'react';
import notifee, { AndroidImportance, AuthorizationStatus, AndroidVisibility } from '@notifee/react-native';

export const useCheckNotificationPermission = () => {
  const [isAuthorized, setIsAuthorized] = useState<boolean | null>(null);

  useEffect(() => {
    checkPermission();
  }, []);

  const checkPermission = async () => {
    try {
      const settings = await notifee.requestPermission({ sound: true, badge: true, announcement: true });

      await notifee.createChannel({
        id: 'default',
        vibration: true,
        name: 'Default Channel',
        vibrationPattern: [300, 500],
        importance: AndroidImportance.HIGH,
        visibility: AndroidVisibility.PUBLIC,
      });

      if (settings.authorizationStatus >= AuthorizationStatus.AUTHORIZED) {
        setIsAuthorized(true);
      } else {
        setIsAuthorized(false);
      }
    } catch (error) {
      setIsAuthorized(false);
    }
  };

  return isAuthorized;
};
