import dayjs from 'dayjs';
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

import {
  Block,
  Text,
  Touchable,
  ListEmpty,
  HeaderBar,
  SafeWrapper,
  CustomFlatlist,
  HeaderInformationPet,
  NotificationBadgeBtn,
} from '@components';
import styles from './styles';
import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { RenderItemProps } from '@configs';
import { IconHospital2, IconLocation2, translate } from '@assets';

interface IInjectionHistoryProps {
  route: any;
  navigation: any;
}

const InjectionHistoryComponent: React.FC<IInjectionHistoryProps> = (props) => {
  const { route, navigation } = props;
  const selectedPet = route?.params?.selectedPet;

  const renderRight = () => <NotificationBadgeBtn />;

  const renderItem = ({ item }: RenderItemProps) => {
    return <Item data={item} />;
  };

  const ListHeaderComponent = () => (
    <Text size="size18" fFamily="BRANDING" mgBottom={8}>
      {translate('injection_history.list')}
    </Text>
  );

  const onPressInfo = () => {
    navigation.pop();
    navigation.navigate(SCREENS.UTILITIES_MEDICAL);
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('injection_history.title')} renderRight={renderRight} />
      <Block bgColor={Colors.COLOR_1} flex>
        <HeaderInformationPet pet={selectedPet} onPressInfo={onPressInfo} />

        <CustomFlatlist
          data={[1, 1, 2, 2]}
          style={styles.list}
          renderItem={renderItem}
          ListEmptyComponent={ListEmpty}
          ListHeaderComponent={ListHeaderComponent}
          contentContainerStyle={styles.contentContainer}
        />
      </Block>
    </SafeWrapper>
  );
};

export const InjectionHistoryScreen = memo(InjectionHistoryComponent, isEqual);

const Item = memo((props: any) => {
  const { data } = props;

  return (
    <Touchable style={styles.item}>
      <Block style={styles.calendarBox}>
        <Text size="size48" fWeight="700">
          {dayjs().format('DD')}
        </Text>
        <Text size="size18" fWeight="500" mgTop={10}>
          {dayjs().format('MM/YYYY')}
        </Text>
      </Block>

      <Block justify="center" flex pdLeft={15}>
        <Block direction="row" align="center">
          <IconHospital2 />
          <Block pdLeft={9} flex>
            <Text fWeight="500">Tiêm phòng bệnh dại</Text>
            <Text mgTop={3}>Vaccin của Đức</Text>
          </Block>
        </Block>
        <Block direction="row" align="center" mgTop={5}>
          <IconLocation2 color={Colors.COLOR_9} />
          <Block pdLeft={9} flex>
            <Text numberOfLines={2} fWeight="500">
              Nơi tiêm: Số 1 A nguyễn văn trỗi, Hà Đông Hà Nội
            </Text>
          </Block>
        </Block>
      </Block>
    </Touchable>
  );
});
