import { Colors, FontSize } from '@themes';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  tabBarItem: {
    backgroundColor: Colors.WHITE,
  },
  labelItem: {
    fontWeight: '500',
    color: Colors.BLACK,
    fontSize: FontSize.size12,
    textTransform: 'capitalize',
  },
});

export default styles;
