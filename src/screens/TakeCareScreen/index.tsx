import isEqual from 'react-fast-compare';
import React, { memo, useCallback, useState } from 'react';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';

import styles from './styles';
import { device } from '@utils';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { IconSearch, translate } from '@assets';
import { Follow, ListProduct } from './components';
import { HeaderBar, SafeWrapper, Touchable } from '@components';

interface ITakeCareProps {
  route: any;
  navigation: any;
}

const ROUTES = [
  { key: 'first', title: 'follow' },
  { key: 'second', title: 'like' },
  { key: 'third', title: 'bought' },
  { key: 'fourth', title: 'viewed' },
];

const TakeCareComponent: React.FC<ITakeCareProps> = (props) => {
  const { route, navigation } = props;
  const status = route?.params?.status;
  const user = useAppStore((x) => x.account.user);

  const initTab = +status ? status - 1 : 0;
  const [indexTab, setIndexTab] = useState<number>(initTab);

  const renderRight = () => {
    return (
      <Touchable pd={8} onPress={() => navigation.navigate(SCREENS.SEARCH)}>
        <IconSearch color={Colors.WHITE} width={24} height={24} />
      </Touchable>
    );
  };

  const renderScene = SceneMap({
    first: useCallback(() => <Follow navigation={navigation} userId={user?.id} />, []),
    second: useCallback(() => <ListProduct navigation={navigation} type={1} />, []),
    third: useCallback(() => <ListProduct navigation={navigation} type={2} />, []),
    fourth: useCallback(() => <ListProduct navigation={navigation} type={3} userId={user?.id} />, []),
  });

  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      style={styles.tabBarItem}
      activeColor={Colors.COLOR_1}
      labelStyle={styles.labelItem}
      getLabelText={(e: any) => translate(`take_care.${e.route?.title}`)}
      indicatorStyle={{ backgroundColor: Colors.COLOR_1 }}
    />
  );

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('take_care.title')} renderRight={renderRight} />

      <TabView
        lazy
        renderScene={renderScene}
        onIndexChange={setIndexTab}
        renderTabBar={renderTabBar}
        keyboardDismissMode={'on-drag'}
        initialLayout={{ width: device.width }}
        navigationState={{ index: indexTab, routes: ROUTES }}
      />
    </SafeWrapper>
  );
};

export const TakeCareScreen = memo(TakeCareComponent, isEqual);
