import { useRequest } from 'ahooks';
import { Divider } from 'native-base';
import debounce from 'lodash/debounce';
import isEqual from 'react-fast-compare';
import { Modalize } from 'react-native-modalize';
import { LayoutAnimation, StyleSheet } from 'react-native';
import React, { memo, useEffect, useMemo, useRef, useState } from 'react';

import { Colors } from '@themes';
import { ShopItem } from './ShopItem';
import { IconCancel, IconSearch, translate } from '@assets';
import { Block, CustomFlatlist, CustomModalize, Input, Loading, Text, Touchable } from '@components';
import { handleResponse, RenderItemProps, requestFollowShop, requestGetListShop } from '@configs';
import { checkType, moderateScale, scale, showToastError, showToastSuccess, vScale } from '@utils';

interface IFollowProps {
  navigation: any;
  userId: string;
}

const FollowComponent: React.FC<IFollowProps> = (props) => {
  const { navigation, userId } = props;
  const refModalize = useRef<Modalize>(null);

  const [page, setPage] = useState<number>(1);
  const [listShop, setListShop] = useState<any[]>([]);
  const [searchText, setSearchText] = useState<string>('');
  const [selectedItem, setSelectedItem] = useState<any>(null);
  const [canLoadMore, setCanLoadMore] = useState<boolean>(true);
  const [isRefreshing, setIsRefreshing] = useState<boolean>(false);
  const [listSuggestions, setListSuggestions] = useState<any[]>([]);

  useEffect(() => {
    runGetList({ page, per_page: 10, user_id: userId, follow: 1 });
  }, [page]);
  useEffect(() => {
    if (searchText) {
      setPage(1);
      runGetList({ page: 1, per_page: 10, keyword: searchText });
    } else {
      runGetList({ page, per_page: 10, user_id: userId, follow: 1 });
    }
  }, [searchText]);
  useEffect(() => {
    runGetListSuggest({ page, per_page: 100, user_id: userId, follow: 2 });
  }, []);

  const debounceSearch = debounce((val) => setSearchText(val), 350);

  const { run: runGetList, loading: loadingGetList } = useRequest(requestGetListShop, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (res?.last_page === res?.current_page) {
        setCanLoadMore(false);
      }
      if (Array.isArray(res?.data)) {
        if (res?.current_page === 1) {
          setListShop(res?.data);
        } else {
          setListShop((prev) => [...prev, ...res?.data]);
        }
      }
    },
    onFinally: () => isRefreshing && setIsRefreshing(false),
  });
  const { run: runGetListSuggest, loading: loadingGetListSuggest } = useRequest(requestGetListShop, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (Array.isArray(res?.data)) {
        setListSuggestions(res?.data);
      }
    },
    onFinally: () => isRefreshing && setIsRefreshing(false),
  });
  const { run: runFollow, loading: loadingFollow } = useRequest(requestFollowShop, {
    manual: true,
    onSuccess: (data) => {
      const { res, err } = handleResponse(data);
      if (res) {
        const { type, ...rest } = selectedItem;
        const msg = checkType(res, 'string') ? res : translate('cm.success');
        if (selectedItem?.type === 'unFollow') {
          setListShop((prev) => prev.filter((x) => x?.id !== selectedItem?.id));
          setListSuggestions((prev) => [rest, ...prev]);
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        } else {
          setListShop((prev) => [...prev, rest]);
          setListSuggestions((prev) => prev.filter((x) => x?.id !== selectedItem?.id));
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        }
        showToastSuccess(msg);
      } else {
        const msg = err?.message && checkType(err?.message, 'string') ? err.message : translate('cm.error');
        showToastError(msg);
      }
    },
    onFinally: () => setSelectedItem(null),
  });

  const onRefresh = () => {
    setIsRefreshing(true);
    setCanLoadMore(true);
    if (page === 1) {
      runGetList({ page: 1, per_page: 10 });
    } else {
      setPage(1);
    }
  };

  const onEndReached = () => {
    if (canLoadMore && !loadingGetList) {
      setPage((prev) => prev + 1);
    }
  };

  const onPressClose = () => {
    setSelectedItem(null);
    refModalize?.current?.close();
  };
  const onBackButtonPress = () => {
    setSelectedItem(null);
    return false;
  };
  const onPressMore = (item: any) => {
    setSelectedItem({ ...item, type: 'unFollow' });
    refModalize?.current?.open();
  };
  const onPressMoreFollow = (item: any) => {
    setSelectedItem({ ...item, type: 'follow' });
    refModalize?.current?.open();
  };
  const onPressFollow = () => {
    runFollow({ shop_id: selectedItem?.id });
    refModalize?.current?.close();
  };

  const renderItem = ({ item }: RenderItemProps) => {
    return <ShopItem data={item} navigation={navigation} onPressMore={() => onPressMore(item)} />;
  };

  const memoLoading = useMemo(() => {
    return (loadingGetList && !listShop.length) || loadingFollow;
  }, [loadingGetList, listShop, loadingFollow]);

  return (
    <>
      {memoLoading && <Loading />}
      <CustomFlatlist
        data={listShop}
        onRefresh={onRefresh}
        renderItem={renderItem}
        refreshing={isRefreshing}
        ListHeaderComponent={
          <Block pdHorizontal={15}>
            <Input
              iconLeft={IconSearch}
              onChangeText={debounceSearch}
              styleWrapInput={styles.input}
              placeholder={translate('cm.placeholder_search')}
            />
          </Block>
        }
        onEndReached={onEndReached}
        onEndReachedThreshold={0.3}
        ListFooterComponent={
          <ListFooterComponent listSuggestions={listSuggestions} onPressFollow={onPressMoreFollow} />
        }
      />
      <CustomModalize
        ref={refModalize}
        onOverlayPress={onBackButtonPress}
        onBackButtonPress={onBackButtonPress}
        HeaderComponent={<HeaderComponent onPressClose={onPressClose} />}>
        <Touchable pd={15} onPress={onPressFollow}>
          {selectedItem?.name ? (
            <Text fWeight="500">
              {translate(selectedItem?.type === 'unFollow' ? 'take_care.unFollow' : 'take_care.follow')}{' '}
              {selectedItem?.name}
            </Text>
          ) : null}
        </Touchable>
      </CustomModalize>
    </>
  );
};

export const Follow = memo(FollowComponent, isEqual);

const styles = StyleSheet.create({
  headerWrapper: {
    padding: scale(15),
    borderColor: Colors.COLOR_9,
    borderBottomWidth: moderateScale(1),
  },
  footerWrapper: {
    paddingVertical: vScale(20),
    paddingHorizontal: scale(15),
  },
  input: {
    height: vScale(32),
    borderColor: Colors.COLOR_1,
    backgroundColor: Colors.COLOR_4,
  },
});

const HeaderComponent = memo(({ onPressClose }: any) => {
  return (
    <Block style={styles.headerWrapper}>
      <Touchable alignSelf="flex-end" pd={4} onPress={onPressClose}>
        <IconCancel />
      </Touchable>
    </Block>
  );
});

const ListFooterComponent = memo(({ listSuggestions, navigation, onPressFollow }: any) => {
  if (!listSuggestions?.length) return null;
  return (
    <>
      <Block style={styles.footerWrapper}>
        <Text fWeight="500" size="size16" mgBottom={10}>
          {translate('take_care.follow_suggestions')}
        </Text>
        <Divider />
      </Block>
      {Array.isArray(listSuggestions) &&
        listSuggestions.map((e, i) => {
          return <ShopItem data={e} key={i} navigation={navigation} onPressMore={() => onPressFollow(e)} follow />;
        })}
    </>
  );
});
