import { useRequest } from 'ahooks';
import { FlatList } from 'react-native';
import isEqual from 'react-fast-compare';
import { RefreshControl, StyleSheet } from 'react-native';
import React, { memo, useEffect, useMemo, useState } from 'react';

import {
  handleResponse,
  RenderItemProps,
  requestGetListBought,
  requestGetListWatched,
  requestGetListFavorite,
} from '@configs';
import { Colors } from '@themes';
import { fakeItemInOddArray, scale, vScale } from '@utils';
import { Block, ItemProduct, ListEmpty, Loading } from '@components';

interface IListProductProps {
  type: number;
  navigation: any;
  userId?: string;
}

const ListProductComponent: React.FC<IListProductProps> = (props) => {
  const { navigation, type, userId } = props;

  const [data, setData] = useState<any[]>([]);
  const [page, setPage] = useState<number>(1);
  const [canLoadMore, setCanLoadMore] = useState<boolean>(true);
  const [isRefreshing, setIsRefreshing] = useState<boolean>(false);

  const handleResponseRequest = (res: any) => {
    if (res?.current_page === res?.last_page) {
      setCanLoadMore(false);
    }
    if (Array.isArray(res?.data)) {
      if (res?.current_page === 1) {
        setData(res?.data);
      } else {
        setData((prev) => [...prev, ...res?.data]);
      }
    }
  };

  const { run: runGetFavorite, loading: loadingGetFavorite } = useRequest(requestGetListFavorite, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      handleResponseRequest(res);
    },
    onFinally: () => isRefreshing && setIsRefreshing(false),
  });
  const { run: runGetBought, loading: loadingGetBought } = useRequest(requestGetListBought, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      handleResponseRequest(res);
    },
    onFinally: () => isRefreshing && setIsRefreshing(false),
  });
  const { run: runGetWatched, loading: loadingGetWatched } = useRequest(requestGetListWatched, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      handleResponseRequest(res);
    },
    onFinally: () => isRefreshing && setIsRefreshing(false),
  });

  const request = type === 1 ? runGetFavorite : type === 2 ? runGetBought : runGetWatched;

  useEffect(() => {
    const params: any = { page, per_page: 10 };
    if (userId) {
      params.user_id = userId;
    }
    request(params);
  }, [page]);

  const keyExtractor = (item: any, index: number) => item?.id || index + '';
  const renderItem = ({ item }: RenderItemProps) => {
    if (item?.stt === 'empty') return <Block w={106} />;
    return <ItemProduct data={item} showAddCart />;
  };

  const onRefresh = () => {
    setIsRefreshing(true);
    setCanLoadMore(true);
    if (page === 1) {
      const params: any = { page, per_page: 10 };
      if (userId) {
        params.user_id = userId;
      }
      request(params);
    } else {
      setPage(1);
    }
  };

  const onEndReached = () => {
    if (!memoLoading && canLoadMore) {
      setPage((prev) => prev + 1);
    }
  };

  const memoLoading = useMemo(() => {
    return loadingGetFavorite || loadingGetWatched || loadingGetBought;
  }, [loadingGetFavorite || loadingGetWatched || loadingGetBought]);

  return (
    <>
      {memoLoading && !data.length && <Loading />}

      <FlatList
        numColumns={3}
        renderItem={renderItem}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.3}
        keyExtractor={keyExtractor}
        ListEmptyComponent={memoLoading ? undefined : ListEmpty}
        data={fakeItemInOddArray(data)}
        columnWrapperStyle={styles.columnWrapper}
        contentContainerStyle={styles.contentContainer}
        refreshControl={
          <RefreshControl
            onRefresh={onRefresh}
            refreshing={isRefreshing}
            colors={[Colors.PRIMARY]}
            tintColor={Colors.PRIMARY}
            progressBackgroundColor={'transparent'}
          />
        }
      />
    </>
  );
};

export const ListProduct = memo(ListProductComponent, isEqual);

const styles = StyleSheet.create({
  contentContainer: {
    paddingHorizontal: scale(15),
    paddingVertical: vScale(24),
  },
  columnWrapper: {
    marginBottom: vScale(28),
    justifyContent: 'space-between',
  },
});
