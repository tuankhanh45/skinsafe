import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { IconMore, translate } from '@assets';
import { device, moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, Text, Touchable } from '@components';

interface IShopItemProps {
  data: any;
  navigation: any;
  follow?: boolean;
  onPressMore: () => void;
}

const ShopItemComponent: React.FC<IShopItemProps> = (props) => {
  const { data, navigation, onPressMore, follow } = props;

  const onPressProduct = (product: any) => {
    navigation.navigate(SCREENS.DETAIL_PRODUCT, { id: product.id });
  };

  return (
    <Touchable style={styles.wrapper}>
      <Block direction="row">
        <ImageRemote source={data?.avatar} style={styles.avatar} resizeMode="cover" sizeIcon={16} />
        <Block flex pdHorizontal={16}>
          <Text fWeight="500" size="size16" numberOfLines={2}>
            {data?.name}
          </Text>
          <Text mgTop={15} size="size12" color="COLOR_15">
            Thương hiệu •{' '}
            <Text size="size12" color="TEXT_2">
              {data?.follow_count} người theo dõi
            </Text>
          </Text>
        </Block>
        {follow ? (
          <Touchable onPress={onPressMore} style={styles.followBtn}>
            <Text fWeight="500" size="size13" color="WHITE">
              +{translate('take_care.follow')}
            </Text>
          </Touchable>
        ) : (
          <Touchable onPress={onPressMore}>
            <IconMore />
          </Touchable>
        )}
      </Block>

      <Block style={styles.content}>
        {Array.isArray(data?.product) &&
          data?.product.map((e: any, i: number) => {
            return (
              <Touchable
                key={i}
                style={[styles.product, i === 0 && { marginLeft: 0 }]}
                onPress={() => onPressProduct(e)}>
                <ImageRemote source={e?.avatar} style={styles.imgProduct} />
                <LinearGradient colors={['#00000000', '#000000']} style={styles.linear}>
                  <Text size="size12" fWeight="500" color="WHITE" numberOfLines={1}>
                    {e?.product_detail?.title}
                  </Text>
                </LinearGradient>
              </Touchable>
            );
          })}
      </Block>
    </Touchable>
  );
};

export const ShopItem = memo(ShopItemComponent, isEqual);

const SIZE_ITEM = (device.width - scale(15 * 2 + 9 * 2)) / 3;

const styles = StyleSheet.create({
  wrapper: {
    borderColor: '#F4F3F9',
    paddingVertical: vScale(20),
    paddingHorizontal: scale(15),
    borderBottomWidth: moderateScale(5),
  },
  avatar: {
    width: scale(36),
    height: scale(36),
    borderRadius: moderateScale(36),
  },
  content: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingTop: vScale(21),
  },
  product: {
    width: SIZE_ITEM,
    height: SIZE_ITEM,
    marginLeft: scale(9),
    borderRadius: moderateScale(12),
  },
  imgProduct: {
    height: '100%',
    width: '100%',
  },
  linear: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    opacity: 0.7,
    position: 'absolute',
    paddingBottom: vScale(5),
    justifyContent: 'flex-end',
    paddingHorizontal: scale(10),
    borderRadius: moderateScale(12),
  },
  followBtn: {
    alignSelf: 'flex-start',
    paddingVertical: vScale(2),
    paddingHorizontal: scale(10),
    backgroundColor: Colors.COLOR_1,
    borderRadius: moderateScale(16),
  },
});
