import { StyleSheet } from 'react-native';
import { Colors, Fonts, FontSize } from '@themes';
import { device, moderateScale, scale, vScale } from '@utils';

const SIZE_ITEM = (device.width - scale(21 * 2 + 28 * 2)) / 3;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.COLOR_4,
  },
  coverAvatar: {
    width: scale(62),
    height: scale(62),
    borderRadius: scale(62),
    borderColor: Colors.WHITE,
    borderWidth: moderateScale(1),
  },
  avatar: {
    width: scale(60),
    height: scale(60),
    borderRadius: scale(60),
  },
  name: {
    color: Colors.BLACK,
    fontSize: FontSize.size14,
    fontFamily: Fonts.BRANDING,
  },
  coverBtn: {
    marginTop: scale(6),
    borderRadius: 500,
    backgroundColor: '#FF73C2',
    paddingVertical: scale(3),
    paddingHorizontal: scale(10),
  },
  txtBtn: {
    fontWeight: '400',
    color: Colors.WHITE,
    fontFamily: Fonts.MEDIUM,
    fontSize: FontSize.size12,
  },
  qr: {
    width: scale(60),
    height: scale(60),
    borderRadius: scale(5),
  },
  linear: {
    width: '100%',
    height: vScale(1),
    marginTop: vScale(19),
    marginBottom: vScale(16),
  },
  contentContainer: {
    paddingTop: vScale(16),
    paddingHorizontal: scale(15),
  },
  petItem: {
    width: SIZE_ITEM,
    height: SIZE_ITEM,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: SIZE_ITEM,
    backgroundColor: Colors.COLOR_11,
  },
  avatarPet: {
    width: scale(89),
    height: scale(89),
    borderRadius: moderateScale(89),
  },
  columnWrapper: {
    paddingHorizontal: scale(6),
    justifyContent: 'space-between',
    marginTop: vScale(16),
  },
});

export default styles;
