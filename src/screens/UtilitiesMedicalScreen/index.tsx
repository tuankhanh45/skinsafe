import { useRequest } from 'ahooks';
import { SCREENS } from '@navigation';
import isEqual from 'react-fast-compare';
import { RefreshControl, FlatList } from 'react-native';
import React, { memo, useEffect, useState } from 'react';
import LinearGradient from 'react-native-linear-gradient';

import styles from './styles';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { device, fakeItemInOddArray, scale } from '@utils';
import { handleResponse, RenderItemProps, requestGetListPet } from '@configs';
import { Block, ImageRemote, ListEmpty, Loading, Text, Touchable, WrapperImageBg } from '@components';

interface IUtilitiesMedicalProps {
  route: any;
  navigation: any;
}
const COLORS = ['#FFDE88', '#FF928BAF', '#FFC2BE55', '#62FFF6'];
const SIZE_ITEM = (device.width - scale(21 * 2 + 28 * 2)) / 3;

const UtilitiesMedicalComponent: React.FC<IUtilitiesMedicalProps> = (props) => {
  const { navigation } = props;

  const user = useAppStore((state) => state?.account?.user);
  const listPet = useAppStore((state) => state?.pet.listPet);
  const actSaveListPet = useAppStore((state) => state?.actSaveListPet);

  const [page, setPage] = useState<number>(1);
  const [refreshing, setRefreshing] = useState(false);
  const [loadMore, setLoadMore] = useState<boolean>(true);

  useEffect(() => {
    runGetListPet({ page, size: 20 });
  }, [page]);

  const { run: runGetListPet, loading } = useRequest(requestGetListPet, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (!res?.next_page_url) {
        setLoadMore(false);
      }
      if (Array.isArray(res?.data))
        if (res?.current_page === 1) {
          actSaveListPet(res?.data);
        } else {
          actSaveListPet([...listPet, ...res?.data]);
        }
    },
    onFinally: () => refreshing && setRefreshing(false),
  });

  const onPressDetail = (index: number) => {
    return navigation.navigate(SCREENS.PROFILE_MEDICAL, { index });
  };

  const keyExtractor = (item: any, index: number) => index + 'pet';
  const renderItem = ({ item, index }: RenderItemProps) => {
    if (item?.stt === 'empty') return <Block w={SIZE_ITEM} />;
    return (
      <Touchable center onPress={() => onPressDetail(index)}>
        <Block style={styles.petItem}>
          <ImageRemote source={item?.avatar} style={styles.avatarPet} resizeMode="cover" />
        </Block>
        <Text mgTop={10} fFamily="BRANDING" size="size15">
          {item?.name}
        </Text>
      </Touchable>
    );
  };

  const onRefresh = () => {
    setRefreshing(true);
    if (page === 1) {
      runGetListPet({ page: 1, size: 20 });
    } else {
      setPage(1);
    }
  };

  const onEndReached = () => {
    if (loadMore && !loading) {
      setPage((prev) => prev + 1);
    }
  };

  return (
    <WrapperImageBg style={styles.wrapper}>
      {loading && !listPet && <Loading />}

      <FlatList
        numColumns={3}
        refreshControl={
          <RefreshControl
            onRefresh={onRefresh}
            refreshing={refreshing}
            colors={[Colors.PRIMARY]}
            tintColor={Colors.PRIMARY}
            progressBackgroundColor={'transparent'}
          />
        }
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        onEndReached={onEndReached}
        ListEmptyComponent={ListEmpty}
        data={fakeItemInOddArray(listPet)}
        columnWrapperStyle={styles.columnWrapper}
        contentContainerStyle={styles.contentContainer}
        ListHeaderComponent={<ListHeader user={user} />}
      />
    </WrapperImageBg>
  );
};

export const UtilitiesMedicalScreen = memo(UtilitiesMedicalComponent, isEqual);

const ListHeader = memo((props: any) => {
  const { user } = props;
  return (
    <>
      <Block direction="row" align="center">
        <Block style={styles.coverAvatar}>
          <ImageRemote resizeMode={'cover'} source={user?.avatar} style={styles.avatar} />
        </Block>
        <Block pdHorizontal={10} flex>
          <Text text={user?.fullname} style={styles.name} />
        </Block>
        <ImageRemote resizeMode={'cover'} source={user?.qr} style={styles.qr} />
      </Block>

      <LinearGradient colors={COLORS} style={styles.linear} start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} />

      <Text fFamily="BRANDING" size="size15">
        Chọn Pet để sử dụng dịch vụ
      </Text>
    </>
  );
});
