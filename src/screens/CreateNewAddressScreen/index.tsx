import { useRequest } from 'ahooks';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'react-fast-compare';
import { useForm } from 'react-hook-form';
import { Modalize } from 'react-native-modalize';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import React, { memo, useEffect, useMemo, useRef, useState } from 'react';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import {
  handleResponse,
  requestGetWard,
  RenderItemProps,
  requestGetProvince,
  requestGetDistrict,
  requestUpdateAddress,
  requestAddNewAddress,
} from '@configs';
import styles from './styles';
import { Colors } from '@themes';
import * as Com from '@components';
import { FormAddress } from './components';
import { CustomModalize, Loading } from '@components';
import { IconRadioUncheck, IconSuccess, translate } from '@assets';
import { showToastError, showToastSuccess, useValidateForm } from '@utils';

const { Text, Block, Button, Touchable, HeaderBar, SafeWrapper, NotificationBadgeBtn } = Com;

interface ICreateNewAddressProps {
  route: any;
  navigation: any;
}

const CreateNewAddressComponent: React.FC<ICreateNewAddressProps> = (props) => {
  const { navigation, route } = props;
  const data = route?.params?.data;
  const insets = useSafeAreaInsets();

  const refProvince = useRef<Modalize>(null);
  const refDistrict = useRef<Modalize>(null);
  const refWard = useRef<Modalize>(null);
  let refSetInitProvince = useRef<boolean>(Boolean(data)).current; //if have params data, run get province & set init state province
  let refSetInitDistrict = useRef<boolean>(Boolean(data)).current;
  let refSetInitWard = useRef<boolean>(Boolean(data)).current;

  const [listWards, setListWards] = useState<any[]>([]);
  const [isDefault, setIsDefault] = useState<boolean>(data ? (data?.default ? true : false) : true);
  const [listProvinces, setListProvinces] = useState<any[]>([]);
  const [listDistricts, setListDistricts] = useState<any[]>([]);
  const [addressInfo, setAddressInfo] = useState<any>({
    province: null,
    district: null,
    ward: null,
    homeOrCompany: 'home', //home or company
  });

  useEffect(() => {
    runGetProvince();
    if (data) {
      runGetDistrict({ province_id: data?.province_id });
      runGetWard({ district_id: data?.district_id });
    }
  }, []);

  const renderRight = () => <NotificationBadgeBtn />;

  // form
  const formMethods = useForm({
    defaultValues: {
      full_name: data?.name,
      phone: data?.phone,
      address: data?.street,
    },
  });
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;

  const { run: runUpdate, loading: loadingUpdate } = useRequest(requestUpdateAddress, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        showToastError(err?.message || translate('create_new_address.update_failed'));
      } else {
        showToastSuccess(translate('create_new_address.update_success'));
        navigation.canGoBack() && navigation.goBack();
      }
    },
  });
  const { run: runAddAddress, loading: loadingAddAddress } = useRequest(requestAddNewAddress, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        showToastError(err?.message || translate('create_new_address.create_failed'));
      } else {
        showToastSuccess(translate('create_new_address.create_success'));
        navigation.canGoBack() && navigation.goBack();
      }
    },
  });
  const { run: runGetProvince, loading: loadingGetProvince } = useRequest(requestGetProvince, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (Array.isArray(res)) setListProvinces(res);
      if (refSetInitProvince) {
        const province = res.find((x: any) => +x?.id === +route?.params?.data?.province_id);
        setAddressInfo((prev: any) => ({ ...prev, province }));
        refSetInitProvince = false;
      }
    },
  });
  const { run: runGetDistrict, loading: loadingGetDistrict } = useRequest(requestGetDistrict, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (Array.isArray(res)) setListDistricts(res);
      if (refSetInitDistrict) {
        const district = res.find((x: any) => +x?.id === +route?.params?.data?.district_id);
        setAddressInfo((prev: any) => ({ ...prev, district }));
        refSetInitDistrict = false;
      }
    },
    onBefore: () => setListDistricts([]),
  });
  const { run: runGetWard, loading: loadingGetWard } = useRequest(requestGetWard, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (Array.isArray(res)) setListWards(res);
      if (refSetInitWard) {
        const ward = res.find((x: any) => +x?.id === +route?.params?.data?.ward_id);
        setAddressInfo((prev: any) => ({ ...prev, ward }));
        refSetInitWard = false;
      }
    },
  });

  const onErrors = (error: any) => {};
  const onSubmit = (form: any) => {
    if (!addressInfo.province) {
      return showToastError(translate('create_new_address.error_required_province'));
    }
    if (!addressInfo.district) {
      return showToastError(translate('create_new_address.error_required_district'));
    }
    if (!addressInfo.ward) {
      return showToastError(translate('create_new_address.error_required_ward'));
    }
    try {
      const params: any = {
        phone: form.phone,
        name: form.full_name,
        street: form.address,
        ward_id: addressInfo.ward.id,
        default: isDefault ? 1 : 0,
        address_full:
          form.address +
          ', ' +
          addressInfo.ward.name +
          ', ' +
          addressInfo.district.name +
          ', ' +
          addressInfo.province.name,
        district_id: addressInfo.district.id,
        province_id: addressInfo.province.id,
        home_company: addressInfo?.homeOrCompany === 'home' ? 1 : 2,
      };
      if (data) {
        runUpdate({ ...params, id: data?.id });
      } else {
        runAddAddress(params);
      }
    } catch (error) {
      if (data) {
        showToastError(translate('create_new_address.update_failed'));
      } else {
        showToastError(translate('create_new_address.create_failed'));
      }
    }
  };

  const keyExtractorProvince = (item: any, index: number) => index + 'Province_id';
  const keyExtractorDistrict = (item: any, index: number) => index + 'District_id';
  const keyExtractorWard = (item: any, index: number) => index + 'Ward_id';
  const renderItemProvince = ({ item, index }: RenderItemProps) => {
    const isSelected = item?.id === addressInfo.province?.id;
    return <ItemList data={item} isSelected={isSelected} onPress={() => onSelectProvince(item)} />;
  };
  const renderItemDistrict = ({ item, index }: RenderItemProps) => {
    const isSelected = item?.id === addressInfo.district?.id;
    return <ItemList data={item} isSelected={isSelected} onPress={() => onSelectDistrict(item)} />;
  };
  const renderItemWard = ({ item, index }: RenderItemProps) => {
    const isSelected = item?.id === addressInfo.ward?.id;
    return <ItemList data={item} isSelected={isSelected} onPress={() => onSelectWard(item)} />;
  };

  const onSelectProvince = (item: any) => {
    if (item?.id === addressInfo.province?.id) return refProvince.current?.close();
    setAddressInfo((prev: any) => ({ ...prev, province: item, district: null, ward: null }));
    runGetDistrict({ province_id: item?.id });
    refProvince.current?.close();
  };
  const onSelectDistrict = (item: any) => {
    if (item?.id === addressInfo.district?.id) return refDistrict.current?.close();
    setAddressInfo((prev: any) => ({ ...prev, district: item, ward: null }));
    runGetWard({ district_id: item?.id });
    refDistrict.current?.close();
  };
  const onSelectWard = (item: any) => {
    if (item?.id === addressInfo.ward?.id) return refWard.current?.close();
    setAddressInfo((prev: any) => ({ ...prev, ward: item }));
    refWard.current?.close();
  };

  const onPressDefault = () => setIsDefault((prev) => !prev);

  const onPressProvince = () => {
    refProvince.current?.open();
  };
  const onPressDistrict = () => {
    if (!addressInfo.province) return showToastError(translate('create_new_address.error_required_province'));
    if (listDistricts.length) refDistrict.current?.open();
  };
  const onPressWard = () => {
    if (!addressInfo.district) return showToastError(translate('create_new_address.error_required_district'));
    if (listWards.length) refWard.current?.open();
  };

  const memoLoading = useMemo(() => {
    return loadingGetProvince || loadingUpdate || loadingGetDistrict || loadingGetWard || loadingAddAddress;
  }, [loadingGetProvince, loadingUpdate, loadingGetDistrict, loadingGetWard, loadingAddAddress]);

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar
        highlightCenter
        renderRight={renderRight}
        title={translate(`create_new_address.${data ? 'title_update' : 'title'}`)}
      />
      <KeyboardAwareScrollView contentContainerStyle={styles.contentContainerStyle}>
        {memoLoading && <Loading />}
        <Text size={'size18'} center>
          {translate('create_new_address.please_confirm_info')}
        </Text>
        <FormAddress
          rules={rules}
          onSubmit={onSubmit}
          onErrors={onErrors}
          formMethods={formMethods}
          addressInfo={addressInfo}
          onPressWard={onPressWard}
          setAddressInfo={setAddressInfo}
          onPressProvince={onPressProvince}
          onPressDistrict={onPressDistrict}
        />
        <Touchable direction="row" center alignSelf="center" mgTop={100} mgBottom={30} onPress={onPressDefault}>
          <IconSuccess color={isDefault ? Colors.COLOR_1 : Colors.COLOR_15} />
          <Text size="size18" mgLeft={4}>
            {translate('create_new_address.set_default_address')}
          </Text>
        </Touchable>
      </KeyboardAwareScrollView>

      <Block pdHorizontal={15} pdBottom={49 - insets.bottom}>
        <Button
          style={styles.btn}
          isLoading={loadingUpdate || loadingAddAddress}
          onPress={formMethods.handleSubmit(onSubmit, onErrors)}
          disabled={!isEmpty(errors) || loadingUpdate || loadingAddAddress}>
          {translate('create_new_address.submit')}
        </Button>
      </Block>

      <CustomModalize
        ref={refProvince}
        title={translate('create_new_address.province')}
        flatListProps={{
          style: styles.list,
          data: listProvinces,
          renderItem: renderItemProvince,
          keyExtractor: keyExtractorProvince,
        }}
      />
      <CustomModalize
        ref={refDistrict}
        title={translate('create_new_address.district')}
        flatListProps={{
          style: styles.list,
          data: listDistricts,
          renderItem: renderItemDistrict,
          keyExtractor: keyExtractorDistrict,
        }}
      />
      <CustomModalize
        ref={refWard}
        title={translate('create_new_address.ward')}
        flatListProps={{
          style: styles.list,
          data: listWards,
          renderItem: renderItemWard,
          keyExtractor: keyExtractorWard,
        }}
      />
    </SafeWrapper>
  );
};

export const CreateNewAddressScreen = memo(CreateNewAddressComponent, isEqual);

const ItemList = memo((props: any) => {
  const { data, onPress, isSelected } = props;

  return (
    <Touchable style={styles.itemList} onPress={onPress}>
      <Text fWeight="500">{data?.name}</Text>
      {isSelected ? <IconSuccess color={Colors.COLOR_1} /> : <IconRadioUncheck />}
    </Touchable>
  );
});
