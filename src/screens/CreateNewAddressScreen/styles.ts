import { StyleSheet } from 'react-native';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  contentContainerStyle: {
    paddingTop: vScale(10),
    paddingHorizontal: scale(15),
  },
  input: {
    height: vScale(40),
    marginTop: vScale(30),
    borderRadius: moderateScale(50),
  },
  btn: {
    borderRadius: moderateScale(100),
  },
  itemList: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(4),
    paddingVertical: vScale(12),
    justifyContent: 'space-between',
    borderBottomWidth: moderateScale(0.8),
    borderColor: 'rgba(226, 226, 226, 0.38)',
  },
  list: {
    paddingHorizontal: scale(12),
  },
});

export default styles;
