import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, Form, ItemDropdown, Text, TextField, Touchable } from '@components';

interface IFormAddressProps {
  rules: any;
  onSubmit: any;
  onErrors: any;
  formMethods: any;
  addressInfo: any;
  onPressWard: () => void;
  onPressProvince: () => void;
  onPressDistrict: () => void;
  setAddressInfo: (e: any) => void;
}

const FormAddressComponent: React.FC<IFormAddressProps> = (props) => {
  const {
    formMethods,
    onSubmit,
    onErrors,
    rules,
    addressInfo,
    onPressProvince,
    onPressWard,
    onPressDistrict,
    setAddressInfo,
  } = props;

  return (
    <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
      <TextField
        name={'full_name'}
        returnKeyType={'done'}
        styleWrapInput={styles.input}
        placeholder={translate('placeholder.full_name')}
      />
      <TextField
        name={'phone'}
        returnKeyType={'done'}
        keyboardType={'phone-pad'}
        styleWrapInput={styles.input}
        placeholder={translate('placeholder.phone')}
      />
      <ItemDropdown
        isRequired
        onPress={onPressProvince}
        value={addressInfo?.province?.name}
        label={translate('create_new_address.province')}
        placeholder={translate('create_new_address.enter_province')}
      />
      <ItemDropdown
        isRequired
        onPress={onPressDistrict}
        value={addressInfo?.district?.name}
        label={translate('create_new_address.district')}
        placeholder={translate('create_new_address.enter_district')}
      />
      <ItemDropdown
        isRequired
        onPress={onPressWard}
        value={addressInfo?.ward?.name}
        label={translate('create_new_address.ward')}
        placeholder={translate('create_new_address.enter_ward')}
      />
      <TextField
        name={'address'}
        returnKeyType={'done'}
        styleWrapInput={styles.input}
        placeholder={translate('placeholder.address')}
      />

      <Block direction="row" mgTop={32}>
        {['home', 'company'].map((e, i) => {
          const isSelected = e === addressInfo?.homeOrCompany;
          return (
            <Touchable
              key={i}
              style={[styles.touch, isSelected && styles.touchSelected]}
              onPress={() => setAddressInfo((prev: any) => ({ ...prev, homeOrCompany: e }))}>
              <Text size="size12" color={isSelected ? 'WHITE' : 'COLOR_15'}>
                {translate(`create_new_address.${e}`)}
              </Text>
            </Touchable>
          );
        })}
      </Block>
    </Form>
  );
};

export const FormAddress = memo(FormAddressComponent, isEqual);

const styles = StyleSheet.create({
  input: {
    height: vScale(40),
    marginTop: vScale(30),
    borderRadius: moderateScale(50),
  },
  touch: {
    marginRight: scale(16),
    paddingVertical: vScale(8),
    borderColor: Colors.COLOR_9,
    paddingHorizontal: scale(20),
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(100),
  },
  touchSelected: {
    borderColor: Colors.COLOR_1,
    backgroundColor: Colors.COLOR_1,
  },
});
