import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingVertical: vScale(16),
    paddingHorizontal: scale(20),
  },
  input: {
    height: vScale(38),
    marginTop: vScale(10),
    borderRadius: moderateScale(5),
  },
  dropdown: {
    borderRadius: moderateScale(5),
  },
});

export default styles;
