import dayjs from 'dayjs';
import isEqual from 'react-fast-compare';
import { useForm } from 'react-hook-form';
import React, { memo, useRef, useState } from 'react';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import {
  Text,
  Form,
  Block,
  Button,
  HeaderBar,
  TextField,
  Touchable,
  SafeWrapper,
  ItemDropdown,
  CustomModalize,
  ModalSelectDate,
  NotificationBadgeBtn,
} from '@components';
import styles from './styles';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { useValidateForm } from '@utils';
import { RenderItemProps } from '@configs';
import { IconCalendar, IconCancel, translate } from '@assets';

interface IBookingInjectionProps {
  route: any;
  navigation: any;
}
const LIST_HOURS = [
  '8h30 - 9h30',
  '9h30 - 10h30',
  '10h30 - 11h30',
  '13h30 - 14h30',
  '14h30 - 15h30',
  '16h30 - 16h30',
  '17h30 - 18h30',
  '19h30 - 20h30',
];

const BookingInjectionComponent: React.FC<IBookingInjectionProps> = (props) => {
  const { navigation } = props;

  const refModalDate = useRef<any>();
  const refModalHour = useRef<any>();

  const user = useAppStore((state) => state?.account?.user);

  const formMethods = useForm({
    defaultValues: {
      full_name: user?.fullname,
      phone: user?.phone,
    },
  });
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;

  const [examinationInfo, setExaminationInfo] = useState<any>({
    date: new Date(),
    hour: null,
    service: null,
  });

  const onErrors = (errors: any) => {};
  const onSubmit = (form: any) => {
    navigation.navigate(SCREENS.FIND_NEAR_SHOP, { type: 'injection' });
  };

  const renderRight = () => <NotificationBadgeBtn />;

  const onPressDate = () => refModalDate.current.open();
  const onSelectDate = (date: Date) => {
    setExaminationInfo((prev: any) => ({ ...prev, date }));
  };

  const onPressHour = () => refModalHour.current?.open();
  const onSelect = (hour: any) => {
    refModalHour.current?.close();
    setExaminationInfo((prev: any) => ({ ...prev, hour }));
  };

  const keyExtractor = (_: any, index: number) => index + 'hour';
  const renderItem = ({ item }: RenderItemProps) => {
    return (
      <Touchable mgHorizontal={34} pdVertical={10} onPress={() => onSelect(item)}>
        <Text>{item}</Text>
      </Touchable>
    );
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('booking_injection.title')} renderRight={renderRight} />
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <Text size="size18" fFamily="BRANDING" mgBottom={20}>
          {translate('booking_injection.enter_information')}
        </Text>

        <Block pdHorizontal={7}>
          <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
            <Label label={translate('booking_injection.full_name')} isRequired />
            <TextField
              name={'full_name'}
              returnKeyType={'done'}
              styleWrapInput={styles.input}
              placeholder={translate('placeholder.full_name')}
            />

            <Label label={translate('booking_injection.date')} isRequired />
            <ItemDropdown
              mgTop={0}
              onPress={onPressDate}
              style={styles.dropdown}
              icon={<IconCalendar color={Colors.COLOR_9} />}
              placeholder={translate('register_remote_advise.dropdown_placeholder')}
              value={examinationInfo?.date ? dayjs(examinationInfo?.date).format('DD/MM/YYYY') : undefined}
            />

            <Label label={translate('booking_injection.hour')} isRequired />
            <ItemDropdown
              mgTop={0}
              onPress={onPressHour}
              style={styles.dropdown}
              value={examinationInfo?.hour ? examinationInfo?.hour : undefined}
            />

            <Label label={translate('booking_injection.service')} isRequired />
            <ItemDropdown mgTop={0} style={styles.dropdown} />

            <Label label={translate('booking_injection.from_country')} isRequired />
            <ItemDropdown mgTop={0} style={styles.dropdown} />

            <Label label={translate('booking_injection.doctor')} isRequired />
            <ItemDropdown mgTop={0} style={styles.dropdown} />
          </Form>
        </Block>
      </KeyboardAwareScrollView>

      <Block pdHorizontal={5} pdBottom={8}>
        <Button onPress={formMethods.handleSubmit(onSubmit, onErrors)}>{translate('booking_injection.submit')}</Button>
      </Block>

      <ModalSelectDate
        ref={refModalDate}
        minDate={new Date()}
        initDate={examinationInfo.date}
        setSelectedDate={onSelectDate}
      />
      <CustomModalize
        ref={refModalHour}
        HeaderComponent={<HeaderComponent label={'hour'} onPress={() => refModalHour.current?.close()} />}
        flatListProps={{
          data: LIST_HOURS,
          keyExtractor,
          renderItem,
        }}
      />
    </SafeWrapper>
  );
};

export const BookingInjectionScreen = memo(BookingInjectionComponent, isEqual);

const Label = memo((props: any) => {
  const { label, isRequired } = props;
  return (
    <Block mgTop={10}>
      <Text fWeight="700">
        {label}
        {isRequired && (
          <Text fWeight="700" color="COLOR_1">
            *
          </Text>
        )}
      </Text>
    </Block>
  );
});

const HeaderComponent = memo(({ label, onPress }: any) => {
  return (
    <Block direction="row" pd={16} justify="space-between">
      <Text fWeight="700">{translate(`booking_examination.${label}`)}</Text>
      <Touchable onPress={onPress}>
        <IconCancel />
      </Touchable>
    </Block>
  );
});
