import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  input: {
    height: vScale(38),
    marginTop: vScale(10),
    borderRadius: moderateScale(5),
  },
  dropdown: {
    borderRadius: moderateScale(5),
  },
  container: {
    paddingVertical: vScale(16),
    paddingHorizontal: scale(20),
  },
});

export default styles;
