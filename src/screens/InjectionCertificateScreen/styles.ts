import { Colors } from '@themes';
import { StyleSheet } from 'react-native';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ECFFF8',
  },
  bgCertificate: {
    height: vScale(349),
    marginHorizontal: scale(15),
  },
  qrImg: {
    width: scale(207),
    height: scale(207),
    borderRadius: moderateScale(8),
  },
  item: {
    flexDirection: 'row',
    marginBottom: vScale(10),
    paddingVertical: vScale(5),
    paddingHorizontal: scale(4),
    marginHorizontal: scale(15),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(8),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  calendarBox: {
    alignItems: 'center',
    borderColor: '#C8C8C8',
    justifyContent: 'center',
    paddingHorizontal: scale(5),
    paddingVertical: vScale(23),
    backgroundColor: Colors.WHITE,
    borderRightWidth: moderateScale(1),
  },
  note: {
    padding: scale(10),
    marginBottom: vScale(40),
    marginHorizontal: scale(15),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(8),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default styles;
