import dayjs from 'dayjs';
import chunk from 'lodash/chunk';
import isEqual from 'react-fast-compare';
import React, { memo, useMemo, useState } from 'react';
import { ImageBackground, ScrollView } from 'react-native';

import styles from './styles';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { ListPets } from './components';
import { PET_GENDER } from '@src/contants';
import { IconEdit, IconHospital2, IconLocation2, images, translate } from '@assets';
import { Block, HeaderBar, ImageRemote, NotificationBadgeBtn, SafeWrapper, Text, Touchable } from '@components';

interface IInjectionCertificateProps {
  route: any;
  navigation: any;
}
const QR =
  'https://www.investopedia.com/thmb/hJrIBjjMBGfx0oa_bHAgZ9AWyn0=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/qr-code-bc94057f452f4806af70fd34540f72ad.png';
const InjectionCertificateComponent: React.FC<IInjectionCertificateProps> = (props) => {
  const {} = props;

  const listPet = useAppStore((state) => state?.pet.listPet);
  const memoListPetSwipe = useMemo(() => {
    return chunk(listPet, 3);
  }, [listPet]);

  const [selectedPet, setSelectedPet] = useState<any>(listPet[0]);

  const renderRight = () => <NotificationBadgeBtn />;

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('injection_certificate.title')} renderRight={renderRight} />
      <ScrollView style={styles.container}>
        <ListPets listPets={memoListPetSwipe} selectedPet={selectedPet} setSelectedPet={setSelectedPet} />

        <ImageBackground source={images.bg_injection_certificate} style={styles.bgCertificate}>
          <Text mgHorizontal={20} mgTop={20} center color="WHITE" fWeight="500" size="size16">
            {translate('injection_certificate.certificate', { name: selectedPet?.name })}
          </Text>
          <Block flex center>
            <ImageRemote source={QR} style={styles.qrImg} />
          </Block>
        </ImageBackground>

        <Block pdHorizontal={10} pdVertical={8} bgColor={Colors.WHITE} mg={15} borderRadius={8}>
          <Text size="size16" fWeight="500">
            {translate('injection_certificate.information', { name: selectedPet?.name })}
          </Text>
          <Block mgTop={12} direction="row" align="center" justify="space-between">
            <Text>
              {translate('injection_certificate.gender')}: {PET_GENDER?.[selectedPet?.sex]}
            </Text>
            <Text>KL: {selectedPet.weight}kg </Text>
          </Block>
          <Text mgTop={3}>
            {translate('injection_certificate.des')}: {selectedPet?.description}
          </Text>
        </Block>

        <Text fWeight="500" size="size16" mgHorizontal={15} mgBottom={15}>
          Đã được tiêm Vắc-xin phòng bệnh
        </Text>
        {[1, 2, 3].map((e, i) => {
          return <Item data={e} key={i} numberInjection={i + 1} />;
        })}

        <Block style={styles.note}>
          <Text fWeight="500">{translate('injection_certificate.note_injection')}</Text>
          <Text mgTop={16} textAlign="justify">
            consectetur adipiscing elit. Dictum bibendum risus, pretium fames semper. Mauris elementum at massa sodales
            tellus sagittis, etiam in montes. Tortor fermentum, amet id ut vitae. Consectetur eu, condimentum porttitor
            nunc pellentesque sit accumsan. Vitae dis tellus tortor molestie nunc dapibus tincidunt. Congue iaculis
            elementum risus amet, eget lectus rutrum eget nisl. Odio morbi id tortor sit. Senectus ut non viverra porta
            a justo pulvinar. consectetur adipiscing elit. Dictum bibendum risus, pretium fames semper. Mauris elementum
            at massa sodales tellus sagittis, etiam in montes. Tortor fermentum, amet id ut vitae. Consectetur eu,
            condimentum porttitor nunc pellentesque sit accumsan. Vitae dis tellus tortor molestie nunc dapibus
            tincidunt. Congue iaculis elementum risus amet, eget lectus rutrum eget nisl. Odio morbi id tortor sit.
            Senectus ut non viverra porta a justo pulvinar. consectetur adipiscing elit. Dictum bibendum risus, pretium
            fames semper. Mauris elementum at massa sodales tellus sagittis, etiam in montes. Tortor fermentum, amet id
            ut vitae. Consectetur eu, condimentum porttitor nunc pellentesque sit accumsan. Vitae dis tellus tortor
            molestie nunc dapibus tincidunt. Congue iaculis elementum risus amet, eget lectus rutrum eget nisl. Odio
            morbi id tortor sit. Senectus ut non viverra porta a justo pulvinar.
          </Text>
        </Block>
      </ScrollView>
    </SafeWrapper>
  );
};

export const InjectionCertificateScreen = memo(InjectionCertificateComponent, isEqual);

const Item = memo((props: any) => {
  const { data, numberInjection } = props;

  return (
    <Touchable style={styles.item}>
      <Block style={styles.calendarBox}>
        <Text size="size40" fWeight="500">
          {dayjs().format('DD')}
        </Text>
        <Text size="size18" fWeight="500" mgTop={10}>
          {dayjs().format('MM/YYYY')}
        </Text>
      </Block>

      <Block justify="center" flex pdLeft={15}>
        <Block direction="row" align="center">
          <IconHospital2 />
          <Block pdLeft={9} flex>
            <Text fWeight="500">Tiêm phòng bệnh dại</Text>
            <Text mgTop={3}>Vaccin của Đức</Text>
          </Block>
        </Block>

        <Block direction="row" align="center" mgTop={5}>
          <IconLocation2 color={Colors.COLOR_9} />
          <Block pdLeft={9} flex>
            <Text numberOfLines={2} fWeight="500">
              Nơi tiêm: Số 1 A nguyễn văn trỗi, Hà Đông Hà Nội
            </Text>
          </Block>
        </Block>

        <Block direction="row" align="center" mgTop={5}>
          <IconEdit />
          <Block pdLeft={9} flex direction="row" align="center">
            <Text numberOfLines={2} fWeight="500">
              Mũi số
            </Text>
            <Block pdVertical={3} pdHorizontal={8} bgColor={Colors.COLOR_11} mgLeft={13} borderRadius={100}>
              <Text color="WHITE">{numberInjection}</Text>
            </Block>
          </Block>
        </Block>
      </Block>
    </Touchable>
  );
});
