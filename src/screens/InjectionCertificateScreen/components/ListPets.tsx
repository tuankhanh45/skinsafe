import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import Swiper from 'react-native-swiper';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { device, scale, vScale } from '@utils';
import { Block, ImageRemote, Text, Touchable } from '@components';

interface IListPetsProps {
  listPets: any[];
  selectedPet: any;
  setSelectedPet: (e: any) => void;
}
const SIZE = (device.width - scale(2 * 20 + 2 * 28)) / 3;

const ListPetsComponent: React.FC<IListPetsProps> = (props) => {
  const { listPets, selectedPet, setSelectedPet } = props;

  return (
    <Block pdVertical={16}>
      <Swiper
        loop={false}
        autoplay={false}
        dotColor={'#B2BAC7'}
        height={vScale(135)}
        activeDotColor={Colors.COLOR_1}
        paginationStyle={styles.dotContainer}>
        {listPets.map((e, i) => {
          return (
            <Block key={i} style={styles.listContainer}>
              {e.map((item: any, index: number) => {
                const isSelected = item === selectedPet;
                return (
                  <Touchable
                    w={SIZE}
                    key={index}
                    mgRight={index === 2 ? 0 : 28}
                    onPress={() => setSelectedPet(item)}
                    style={{ opacity: isSelected ? 1 : 0.3 }}>
                    <Block style={styles.avatarContainer}>
                      <ImageRemote style={styles.avatar} source={item?.avatar} resizeMode="cover" />
                    </Block>
                    <Text numberOfLines={2} mgTop={10} center>
                      {item?.name}
                    </Text>
                  </Touchable>
                );
              })}
            </Block>
          );
        })}
      </Swiper>
    </Block>
  );
};

export const ListPets = memo(ListPetsComponent, isEqual);

const styles = StyleSheet.create({
  listContainer: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingHorizontal: scale(20),
  },
  avatar: {
    width: SIZE - 4,
    height: SIZE - 4,
    borderRadius: SIZE,
  },
  avatarContainer: {
    width: SIZE,
    height: SIZE,
    borderRadius: SIZE,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.COLOR_11,
  },
  dotContainer: {
    bottom: -20,
  },
});
