import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import React, { memo, useRef } from 'react';

import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { moderateScale, scale, vScale } from '@utils';
import { IconHistory, IconLocation2, IconSearch, translate } from '@assets';
import { Block, Input, ModalSelectLocation, Text, Touchable } from '@components';

interface IHeaderProps {
  navigation: any;
  addressInfo: any;
  setAddressInfo: (e: any) => void;
}

const Header: React.FC<IHeaderProps> = (props) => {
  const { navigation, addressInfo, setAddressInfo } = props;

  const refModal = useRef<any>(null);

  const onPressHistory = () => navigation.navigate(SCREENS.HISTORY_ADVISE);

  const onOpenModal = () => refModal.current?.open();

  return (
    <Block mgBottom={16}>
      <Touchable direction="row" align="center" onPress={onPressHistory}>
        <IconHistory />
        <Text mgLeft={10} size="size15" fFamily="BRANDING">
          {translate('remote_advise.search_history')}
        </Text>
      </Touchable>

      <Block style={styles.inputContainer}>
        <Input
          iconLeft={IconSearch}
          styleContainer={{ flex: 1 }}
          styleWrapInput={styles.wrapInput}
          placeholder={translate('cm.search')}
        />
        <Touchable style={styles.selectLocationBtn} onPress={onOpenModal}>
          <IconLocation2 color={Colors.COLOR_1} />
          <Text size="size12" color="COLOR_9" numberOfLines={1}>
            {addressInfo?.province?.name}
          </Text>
        </Touchable>
      </Block>

      <ModalSelectLocation ref={refModal} addressInfo={addressInfo} setAddressInfo={setAddressInfo} />
    </Block>
  );
};

export const HeaderComponent = memo(Header, isEqual);

const styles = StyleSheet.create({
  wrapInput: {
    marginTop: 0,
    borderWidth: 0,
    height: vScale(32),
    paddingHorizontal: 0,
    backgroundColor: Colors.WHITE,
  },
  selectLocationBtn: {
    width: '20%',
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: scale(8),
    paddingLeft: scale(7),
    marginVertical: vScale(5),
    borderColor: Colors.COLOR_9,
    borderLeftWidth: moderateScale(1),
  },
  inputContainer: {
    flexDirection: 'row',
    marginTop: vScale(16),
    borderColor: '#E1E1E1',
    paddingHorizontal: scale(15),
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(100),
  },
});
