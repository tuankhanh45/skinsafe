import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, Text, Touchable } from '@components';
import { IconCalendar, IconCall, IconStar, translate } from '@assets';

interface IItemProps {
  data: any;
  navigation: any;
}
const LIST_CRITERIA = [{ label: 'booking' }, { label: 'advise' }, { label: 'rate' }];
const LIST_ACTIONS = [
  { label: 'book_advise', icon: <IconCall color={Colors.COLOR_7} /> },
  { label: 'book_examination', icon: <IconCalendar /> },
];

const ItemComponent: React.FC<IItemProps> = (props) => {
  const { data, navigation } = props;

  const onPressDetail = () => {
    return navigation.navigate(SCREENS.DETAIL_NEAR_SHOP, { data, type: 'remote_advise' });
  };

  return (
    <Touchable style={styles.wrapper} onPress={onPressDetail}>
      <Block pd={6} direction="row">
        <ImageRemote source={'https://picsum.photos/801'} style={styles.avatar} />
        <Block flex pdLeft={11}>
          <Text numberOfLines={1} size="size16" fWeight="500">
            GS: Đặng Trị Bệnh
          </Text>
          <Text numberOfLines={1} size="size12" fWeight="500" color="COLOR_15" mgTop={4}>
            Chuyên khoa: Hô hấp
          </Text>
        </Block>
      </Block>

      <Block style={styles.criteria}>
        {LIST_CRITERIA.map((e, i) => {
          return (
            <Block style={[styles.item, i === 2 && { borderRightWidth: 0 }]} key={i}>
              <Text size="size12" fWeight="700">
                {translate(`remote_advise.${e.label}`)}
              </Text>
              <Block direction="row" center mgTop={5}>
                {i === 2 && <IconStar />}
                <Text size="size12" color="COLOR_3" mgLeft={i === 2 ? 3 : 0}>
                  5000
                </Text>
              </Block>
            </Block>
          );
        })}
      </Block>

      <Block direction="row">
        {LIST_ACTIONS.map((e, i) => {
          return (
            <Touchable key={i} style={[styles.itemAction, i === 1 && { borderRightWidth: 0 }]}>
              {e.icon}
              <Text color={'COLOR_7'} fWeight="500" size="size16" mgLeft={7}>
                {translate(`remote_advise.${e.label}`)}
              </Text>
            </Touchable>
          );
        })}
      </Block>
    </Touchable>
  );
};

export const Item = memo(ItemComponent, isEqual);

const styles = StyleSheet.create({
  avatar: {
    width: scale(40),
    height: scale(40),
    borderRadius: moderateScale(40),
  },
  criteria: {
    flexDirection: 'row',
    marginTop: vScale(10),
  },
  wrapper: {
    marginBottom: vScale(10),
    borderColor: Colors.COLOR_9,
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(8),
    borderWidth: moderateScale(0.3),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  item: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Colors.COLOR_9,
    borderRightWidth: moderateScale(1),
  },
  itemAction: {
    flex: 1,
    flexDirection: 'row',
    marginTop: vScale(5),
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: vScale(10),
    borderColor: Colors.COLOR_9,
    borderTopWidth: moderateScale(1),
    borderRightWidth: moderateScale(1),
  },
});
