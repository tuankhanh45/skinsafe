import isEqual from 'react-fast-compare';
import React, { memo, useState } from 'react';
import { KeyboardAwareFlatList } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { translate } from '@assets';
import { RenderItemProps } from '@configs';
import { HeaderComponent, Item } from './components';
import { HeaderBar, ListEmpty, NotificationBadgeBtn, SafeWrapper } from '@components';

interface IRemoteAdviseProps {
  route: any;
  navigation: any;
}

const RemoteAdviseComponent: React.FC<IRemoteAdviseProps> = (props) => {
  const { navigation } = props;

  const [addressInfo, setAddressInfo] = useState<any>({
    province: null,
    district: null,
    ward: null,
  });

  const renderRight = () => <NotificationBadgeBtn />;

  const keyExtractor = (item: any, index: number) => index + '';
  const renderItem = ({ item }: RenderItemProps) => {
    return <Item data={item} navigation={navigation} />;
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('remote_advise.title')} renderRight={renderRight} />
      <KeyboardAwareFlatList
        data={[1, 2, 3]}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        ListEmptyComponent={ListEmpty}
        contentContainerStyle={styles.contentStyle}
        ListHeaderComponent={
          <HeaderComponent navigation={navigation} addressInfo={addressInfo} setAddressInfo={setAddressInfo} />
        }
      />
    </SafeWrapper>
  );
};

export const RemoteAdviseScreen = memo(RemoteAdviseComponent, isEqual);
