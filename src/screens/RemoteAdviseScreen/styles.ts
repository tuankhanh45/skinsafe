import { scale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  contentStyle: {
    padding: scale(16),
  },
});

export default styles;
