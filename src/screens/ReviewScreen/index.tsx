import React, { memo, useRef } from 'react';
import isEqual from 'react-fast-compare';
import { Block, HeaderBar, SafeWrapper, CartBadgeBtn, Touchable, NotificationBadgeBtn } from '@components';
import styles from './styles';
import { IconSearch, translate } from '@assets';
import { SCREENS } from '@navigation';
import { RenderItemProps } from '@configs';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Colors } from '@themes';
import { Qualify, Review, Statistic } from './components';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

interface IReviewProps {}

const ReviewComponent: React.FC<IReviewProps> = (props) => {
  const navigation = useNavigation<any>();
  const route = useRoute<any>();
  const service = route?.params.type === 'SERVICE';
  const renderRight = () => {
    return (
      <Block direction="row" align="center">
        <NotificationBadgeBtn />
      </Block>
    );
  };
  const onNavigate = (screen: string) => {
    navigation.navigate(screen);
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar
        title={translate(service ? 'review.service' : 'review.product')}
        renderRight={renderRight}
        highlightCenter
      />
      <KeyboardAwareScrollView>
        <Statistic data={{}} />
        <Qualify data={{}} />
        <Review service={service} />
      </KeyboardAwareScrollView>
    </SafeWrapper>
  );
};

export const ReviewScreen = memo(ReviewComponent, isEqual);
