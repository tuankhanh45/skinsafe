import dayjs from 'dayjs';
import size from 'lodash/size';
import React, { memo, useCallback, useState } from 'react';
import isEqual from 'react-fast-compare';
import { ImageBackground, StyleSheet } from 'react-native';
import { Colors } from '@themes';
import { IconArrowDown, images, translate } from '@assets';
import { currencyFormat, useValidateForm, moderateScale, scale, vScale } from '@utils';
import { Block, Button, Form, Image, StarRate, Text, TextField, Touchable } from '@components';
import { useForm } from 'react-hook-form';

const initRateGroup = [
  { rate: 5, title: 'Chất lượng dịch vụ', id: 1 },
  { rate: 5, title: 'Chất lượng phục vụ của Shop', id: 2 },
  { rate: 5, title: 'Thời gian phục vụ rất nhanh', id: 3 },
];
interface IReviewProps {
  service: boolean;
}
const ReviewComponent: React.FC<IReviewProps> = (props) => {
  const [rate, setRate] = useState<any>({ rate: 0, title: 'Vui lòng chọn mức độ hài lòng của bạn' });
  const [rateGroup, setRateGroup] = useState<any[]>(initRateGroup);
  const updateStar = (star: number) => {
    const txt =
      star === 1
        ? 'Rất tệ'
        : star === 2
        ? 'Trung Bình'
        : star === 3
        ? 'Tốt'
        : star === 4
        ? 'Khá hài lòng'
        : 'Tuyệt vời';
    setRate({ rate: star, title: txt });
  };
  const updateStarGroup = (item: any, star: number) => {
    let arr = [...rateGroup];
    arr.map((e) => {
      if (e?.id === item?.id) {
        e.rate = star;
      }

      return e;
    });

    setRateGroup(arr);
  };
  // form
  const formMethods = useForm();
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;
  const onErrors = (errors: any) => {};
  const onSubmit = (form: any) => {
    const params = {};
  };
  const pickerMedia = (type: string) => {};
  // header
  const Header = useCallback(
    () => (
      <Block>
        <Text
          text={translate(props.service ? 'review.service' : 'review.product')}
          size="size15"
          fFamily="BRANDING"
          color="TEXT"
        />
        <Block direction="row" align="center" mgTop={16} bgColor="#FDE8E8" pd={7}>
          <Image source="imgCoin" style={{ width: scale(20), height: vScale(20) }} />
          <Text text={`Đánh giá để nhận ${10} xu`} size="size12" color="COLOR_18" fWeight="600" mgLeft={14} />
        </Block>
        <Block direction="row" align="center" mgTop={11} style={styles.coverName}>
          <Image source="imgPNG" style={{ width: scale(30), height: vScale(36) }} />
          <Text text={'Dịch vụ Spa'} size="size16" fFamily="MEDIUM" color="TEXT" fWeight="500" mgLeft={11} />
        </Block>
      </Block>
    ),
    [],
  );
  // rate
  const StarReview = useCallback(
    () => (
      <Block align="center" mgTop={13}>
        <StarRate size={30} space={12} review rate={rate?.rate} updateStar={updateStar} disabled={false} />
        <Text
          text={rate?.title}
          size="size16"
          fFamily="MEDIUM"
          color="TEXT"
          fWeight={rate?.rate ? '500' : '400'}
          mgTop={11}
        />
      </Block>
    ),
    [rate],
  );
  // btn
  const ButtonView = useCallback(
    () => (
      <Block direction="row" align="center" mgTop={24} justify="space-between">
        <Touchable
          onPress={() => pickerMedia('img')}
          h={41}
          w={167}
          bgColor={Colors.COLOR_1}
          align="center"
          justify="center"
          borderRadius={8}
          direction="row">
          <Image source="imgCamera" style={{ width: scale(22), height: vScale(22) }} />
          <Text text={'Thêm hình ảnh'} size="size12" fFamily="MEDIUM" color="WHITE" fWeight={'500'} mgLeft={13} />
        </Touchable>
        <Touchable
          onPress={() => pickerMedia('video')}
          h={41}
          w={167}
          bgColor={Colors.COLOR_1}
          align="center"
          justify="center"
          borderRadius={8}
          direction="row">
          <Image source="imgVideo" style={{ width: scale(22), height: vScale(22) }} />
          <Text text={'Thêm Video'} size="size12" fFamily="MEDIUM" color="WHITE" fWeight={'500'} mgLeft={13} />
        </Touchable>
      </Block>
    ),
    [],
  );
  // input
  const DesInput = useCallback(
    () => (
      <Block>
        <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
          <TextField
            name={'description'}
            returnKeyType={'done'}
            multiline
            placeholder={translate('review.description')}
            styleWrapInput={{ height: vScale(193), backgroundColor: Colors.WHITE, borderWidth: moderateScale(1) }}
          />
        </Form>
      </Block>
    ),
    [],
  );
  // input
  const RateGroup = useCallback(() => {
    if (!rate?.rate) return <Block />;
    return (
      <Block mgTop={23}>
        {rateGroup.map((e: any, i: number) => {
          return (
            <Block key={i + 'rate'} direction="row" align="center" justify="space-between" mgTop={8}>
              <Block bgColor={Colors.COLOR_12} borderRadius={28} pdHorizontal={16} h={24} justify="center">
                <Text text={e?.title} size="size12" fFamily="MEDIUM" color="TEXT" fWeight={'400'} />
              </Block>
              <StarRate size={10} rate={e?.rate} updateStar={(star) => updateStarGroup(e, star)} disabled={false} />
            </Block>
          );
        })}
      </Block>
    );
  }, [rate]);
  // btn review
  const BtnReview = useCallback(
    () => (
      <Touchable
        onPress={() => {}}
        h={37}
        bgColor={Colors.COLOR_1}
        align="center"
        justify="center"
        borderRadius={8}
        mgTop={20}>
        <Text text={translate('review.send')} size="size18" fFamily="BRANDING" color="WHITE" />
      </Touchable>
    ),
    [],
  );

  return (
    <Block mgVertical={16} mgHorizontal={13}>
      <Header />
      <StarReview />
      <ButtonView />
      <DesInput />
      <RateGroup />
      <BtnReview />
    </Block>
  );
};

export const Review = memo(ReviewComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  coverName: {
    borderTopColor: '#C4C4C4',
    borderBottomColor: '#C4C4C4',
    borderBottomWidth: moderateScale(1),
    borderTopWidth: moderateScale(1),
    paddingVertical: vScale(11),
  },
});
