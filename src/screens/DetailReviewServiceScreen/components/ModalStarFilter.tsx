import { StyleSheet } from 'react-native';
import React, { useRef, useImperativeHandle } from 'react';

import { moderateScale, scale, vScale } from '@utils';
import { translate } from '@assets';
import { Colors } from '@themes';
import { Block, CustomModalize, Image, Text, Touchable } from '@components';
import times from 'lodash/times';
type Props = {};

export const ModalStarFilter = React.forwardRef((props: Props, ref) => {
  const {} = props;
  const refModal = useRef<any>(null);
  // action modal
  useImperativeHandle(ref, () => ({
    open: () => refModal.current?.open(),
    close: () => refModal.current?.close(),
  }));
  const press = (e: any) => {
    refModal.current?.close();
  };
  const renderContent = () => {
    return (
      <Block style={styles.modalContainer}>
        <Block mgTop={8}>
          {times(5, (e) => (
            <Touchable key={e} direction="row" align="center" mgLeft={30} mgTop={10} onPress={() => press(e)}>
              <Text fWeight="600" size="size14" fFamily="MEDIUM" color="TEXT" mgRight={4}>
                {5 - e}
              </Text>
              <Image source="imgStar" style={{ width: scale(14), height: vScale(14) }} />
            </Touchable>
          ))}
        </Block>
      </Block>
    );
  };
  return <CustomModalize ref={refModal}>{renderContent()}</CustomModalize>;
});

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: Colors.WHITE,
    width: scale(375),
    borderTopLeftRadius: scale(20),
    borderTopRightRadius: scale(20),
  },
});
