import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Block, ImageRemote, ProgressBar, StarRate, Text } from '@components';
import { moderateScale, scale, vScale } from '@utils';
import { Colors } from '@themes';
import { IconStar, translate } from '@assets';
import times from 'lodash/times';
interface IStatisticProps {
  data: any;
}
const StatisticComponent: React.FC<IStatisticProps> = (props) => {
  // const { data } = props;
  return (
    <Block style={styles.coverBox} mgTop={16} mgHorizontal={13}>
      <Text text={translate('review.statistic')} size="size18" fFamily="BRANDING" color="TEXT" textAlign="center" />
      <Block direction="row" justify="center" mgTop={20} mgBottom={8}>
        <StarRate rate={5} size={16} />
        <Text text={`4,85/5`} size="size15" fFamily="MEDIUM" color="TEXT" fWeight="500" mgLeft={17} />
      </Block>
      {times(5, (e) => (
        <Block key={e + 'star'} mgTop={12}>
          <Block direction="row" align="center" justify="space-between" mgBottom={8}>
            <StarRate rate={5 - e} size={16} />
            <Text text={`5`} size="size12" fFamily="MEDIUM" color="TEXT" fWeight="500" />
          </Block>
          <ProgressBar h={4} progress={5 / 25} />
        </Block>
      ))}
    </Block>
  );
};

export const Statistic = memo(StatisticComponent, isEqual);

const styles = StyleSheet.create({
  coverBox: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    padding: scale(10),
    paddingBottom: scale(13),
  },
});
