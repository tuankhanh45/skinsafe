import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Block, ImageRemote, ProgressBar, StarRate, Text } from '@components';
import { moderateScale, scale, vScale } from '@utils';
import { Colors } from '@themes';
import { IconStar, translate } from '@assets';
import times from 'lodash/times';
interface IQualifyProps {
  data: any;
}
const data = [
  { name: 'Chất lượng tuyệt vời', count: 5 },
  { name: 'Sản phẩm đóng gói chắc chắn', count: 10 },
  { name: 'Sản phẩm rất tốt', count: 15 },
];
const QualifyComponent: React.FC<IQualifyProps> = (props) => {
  // const { data } = props;
  return (
    <Block style={styles.coverBox} mgTop={16} mgHorizontal={13}>
      <Text text={translate('review.qualify')} size="size18" fFamily="BRANDING" color="TEXT" textAlign="center" />
      {data?.map((e: any, i: number) => (
        <Block key={i + 'star'} mgTop={12}>
          <Block direction="row" align="center" justify="space-between" mgBottom={8}>
            <Text text={e?.name} size="size12" fFamily="MEDIUM" color="TEXT" fWeight="500" />
            <Text text={e?.count} size="size12" fFamily="MEDIUM" color="TEXT" fWeight="500" />
          </Block>
          <ProgressBar h={4} progress={e?.count / 30} />
        </Block>
      ))}
    </Block>
  );
};

export const Qualify = memo(QualifyComponent, isEqual);

const styles = StyleSheet.create({
  coverBox: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    padding: scale(10),
    paddingBottom: scale(13),
  },
});
