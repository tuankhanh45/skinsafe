import dayjs from 'dayjs';
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { ImageBackground, StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { IconArrowDown, images, translate } from '@assets';
import { currencyFormat, device, moderateScale, scale, vScale } from '@utils';
import { Block, Button, ImageRemote, StarRate, Text, Touchable } from '@components';

interface IReviewProps {
  refModal: any;
}

const data = {
  price: 200000,
  review: {
    avatar: 'https://picsum.photos/801',
    name: 'thedozsd9909',
    rate: 5,
    title: 'Sản phẩm rất tốt',
    des: 'Rất hài lòng. Lần đầu mua của shop. Khá lo lắng nhưng khi nhận được giày thì rất chi là vừa ý. Hợp giá tiền. Đáng mua nha mọi người. Hộp hơi hơi móp nên hơi tiếc xíu.',
    rate_type: [
      { name: 'Chất lượng phục vụ của shop', rate: 5 },
      { name: 'Đóng gói', rate: 5 },
      { name: 'Rất đáng tiền', rate: 5 },
    ],
  },
  total_review: 5,
};
const ReviewComponent: React.FC<IReviewProps> = (props) => {
  const { refModal } = props;
  const openModalFilter = () => {
    console.log('press');
    refModal?.current?.open();
  };
  const pressAll = () => {};
  return (
    <Block mgVertical={16}>
      {/*  */}
      <Text text={translate('review.list')} size="size18" fFamily="BRANDING" color="TEXT" mgLeft={14} />
      {/* filter */}
      <Block
        direction="row"
        mgTop={16}
        pdHorizontal={8}
        pdVertical={13}
        bgColor={Colors.COLOR_1}
        justify="space-between">
        <Touchable direction="row" align="center" onPress={openModalFilter}>
          <Text
            text={translate('review.filter_star')}
            color="WHITE"
            fWeight="500"
            size={'size14'}
            fFamily="MEDIUM"
            mgRight={4}
          />
          <IconArrowDown color={'#fff'} />
        </Touchable>
        <Text
          onPress={pressAll}
          text={translate('review.all')}
          color="WHITE"
          fWeight="500"
          size={'size14'}
          fFamily="MEDIUM"
        />
      </Block>
      {/* review */}
      <Block style={styles.coverReview}>
        <ItemReview data={data?.review} />
      </Block>
    </Block>
  );
};

export const Review = memo(ReviewComponent, isEqual);
const DATA = [1, 2, 3, 4, 5, 6];
const ItemReview = memo((props: any) => {
  const { data } = props;
  return (
    <Block>
      <Block style={styles.itemWrapper}>
        <Block direction="row">
          <ImageRemote source={data?.avatar} style={styles.avatar} />
          <Block flex mgLeft={6}>
            <Block direction="row" align="center" justify="space-between">
              <Text fWeight="600" size="size12" color="PRIMARY" mgRight={10} flex>
                {data?.name}
              </Text>
              <StarRate rate={5} size={12} />
            </Block>
            <Block direction="row" align="center" justify="space-between" mgTop={4}>
              <StarRate rate={5} size={8} />
              <Text text={dayjs().format('DD-MM-YYYY hh:mm')} size={'size12'} color="TEXT_2" />
            </Block>
          </Block>
        </Block>
        <Text text={translate('review.cert')} size={'size8'} color="COLOR_2" fWeight="700" mgTop={8} mgLeft={20} />

        <Block style={styles.wrapperReview}>
          <Text text={translate('review.info')} size={'size14'} color="TEXT" fWeight="700" mgTop={8} mgLeft={2} />
          <Text text={data?.des} mgTop={10} size={'size10'} color="TEXT" mgHorizontal={6} />
        </Block>
      </Block>
      {/* img list */}
      <Block direction="row" mgVertical={8} style={{ flexWrap: 'wrap' }}>
        {DATA.map((e, i) => {
          const isLastItem = i === DATA.length - 1;
          return (
            <Touchable style={styles.imgBox} key={i}>
              <ImageRemote style={styles.img} source={'https://picsum.photos/801'} key={i} resizeMode={'cover'} />
              {isLastItem && (
                <Block style={styles.moreBox}>
                  <Text fWeight="600" size={'size18'} color={'COLOR_4'}>
                    +2
                  </Text>
                </Block>
              )}
            </Touchable>
          );
        })}
      </Block>
      {/*  */}
      <Block mgHorizontal={13}>
        <Text text={translate('review.good_info')} size={'size12'} color="TEXT" fWeight="500" mgTop={16} />
        <Block direction="row" align="center" mgTop={16}>
          <Touchable style={styles.coverBtnGood}>
            <Text text={'Có (10)'} size={'size12'} color="TEXT" fWeight="500" />
          </Touchable>
          <Touchable style={styles.coverBtnGood} mgLeft={14}>
            <Text text={'Không (2)'} size={'size12'} color="TEXT" fWeight="500" />
          </Touchable>
        </Block>
      </Block>
      {/*  */}
      <Touchable mgTop={16} bgColor={Colors.COLOR_1} pdVertical={9} onPress={() => {}}>
        <Text text={'1 Câu trả lời'} size={'size14'} color="WHITE" fWeight="500" textAlign="center" />
      </Touchable>
    </Block>
  );
});

const styles = StyleSheet.create({
  wrapper: {},
  coverBtn: {
    height: vScale(38),
    width: device.width - scale(30),
    alignItems: 'center',
    borderColor: Colors.COLOR_1,
    borderWidth: moderateScale(2),
    borderRadius: scale(8),
    justifyContent: 'center',
    marginTop: vScale(32),
  },
  coverReview: {
    marginTop: vScale(22),
    backgroundColor: '#F4F4F4',
    marginBottom: vScale(28),
  },
  itemWrapper: {
    padding: vScale(13),
  },
  avatar: {
    height: scale(31),
    width: scale(31),
    borderRadius: moderateScale(31),
  },
  imgBox: {
    width: scale(107),
    height: vScale(107),
    borderRadius: moderateScale(6),
    marginLeft: scale(13),
    marginTop: scale(12),
  },
  img: {
    height: '100%',
    width: '100%',
    borderRadius: moderateScale(6),
  },
  moreBox: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(6),
    backgroundColor: 'rgba(49, 49, 49, .7)',
  },
  wrapperReview: {
    marginTop: vScale(20),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    paddingBottom: 8,
  },
  coverBtnGood: {
    backgroundColor: '#E1D8D3',
    borderRadius: moderateScale(30),
    paddingHorizontal: scale(18),
    paddingVertical: scale(8),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
