import React, { memo, useRef } from 'react';
import isEqual from 'react-fast-compare';
import { Block, HeaderBar, SafeWrapper, CartBadgeBtn, Touchable, NotificationBadgeBtn } from '@components';
import styles from './styles';
import { IconSearch, translate } from '@assets';
import { SCREENS } from '@navigation';
import { RenderItemProps } from '@configs';
import { useNavigation } from '@react-navigation/native';
import { Colors } from '@themes';
import { ModalStarFilter, Qualify, Review, Statistic } from './components';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

interface IDetailReviewServiceProps {}

const DetailReviewServiceComponent: React.FC<IDetailReviewServiceProps> = (props) => {
  const navigation = useNavigation<any>();
  const refModal = useRef<any>(null);
  const renderRight = () => {
    return (
      <Block direction="row" align="center">
        <NotificationBadgeBtn />
      </Block>
    );
  };
  const onNavigate = (screen: string) => {
    navigation.navigate(screen);
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('review.service')} renderRight={renderRight} highlightCenter />
      <KeyboardAwareScrollView>
        <Statistic data={{}} />
        <Qualify data={{}} />
        <Review refModal={refModal} />
      </KeyboardAwareScrollView>
      <ModalStarFilter ref={refModal} />
    </SafeWrapper>
  );
};

export const DetailReviewServiceScreen = memo(DetailReviewServiceComponent, isEqual);
