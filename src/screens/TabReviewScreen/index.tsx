import React, { memo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { Block, HeaderBar, SafeWrapper, NotificationBadgeBtn } from '@components';
import { translate } from '@assets';
import { useNavigation, useRoute } from '@react-navigation/native';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';
import { Colors, Fonts, FontSize } from '@themes';
import { ListItem } from './components';
import styles from './styles';
import { device } from '@utils';
interface ITabReviewProps {}
const ROUTES = [
  { key: 'first', title: 'Chờ đánh giá' },
  { key: 'second', title: 'Đã đánh giá' },
];
const TabReviewComponent: React.FC<ITabReviewProps> = (props) => {
  const navigation = useNavigation<any>();
  const route = useRoute<any>();
  const isService = route?.params.type === 'SERVICE';
  const [indexTab, setIndexTab] = useState<number>(0);

  const renderScene = SceneMap({
    first: () => <ListItem isService={isService} type={'WAIT'} />,
    second: () => <ListItem isService={isService} type={'DONE'} />,
  });
  const renderTabBar = (prs: any) => {
    return (
      <TabBar
        {...prs}
        activeColor={Colors.COLOR_1}
        style={styles.tabBar}
        tabStyle={styles.tabStyle}
        labelStyle={styles.tabBarLabel}
        indicatorStyle={{ backgroundColor: Colors.COLOR_1 }}
      />
    );
  };
  const renderRight = () => {
    return (
      <Block direction="row" align="center">
        <NotificationBadgeBtn />
      </Block>
    );
  };
  const onNavigate = (screen: string) => {
    navigation.navigate(screen);
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar
        title={translate(isService ? 'review.service' : 'review.product')}
        renderRight={renderRight}
        highlightCenter
      />
      <TabView
        swipeEnabled={false}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={setIndexTab}
        keyboardDismissMode={'on-drag'}
        initialLayout={{ width: device.width }}
        navigationState={{ index: indexTab, routes: ROUTES }}
      />
    </SafeWrapper>
  );
};

export const TabReviewScreen = memo(TabReviewComponent, isEqual);
