import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Block, ImageRemote, ProgressBar, Touchable, Text } from '@components';
import { moderateScale, scale, vScale } from '@utils';
import { Colors } from '@themes';
import { IconStar, translate } from '@assets';
import times from 'lodash/times';
interface IEmptyProps {
  isService: boolean;
}
const EmptyComponent: React.FC<IEmptyProps> = (props) => {
  return (
    <Block mgTop={26} mgHorizontal={20}>
      <Block
        h={188}
        borderColor={'#F1C48D'}
        borderRadius={12}
        pdHorizontal={30}
        style={{ borderWidth: moderateScale(1) }}
        justify="flex-end">
        <Text
          text={translate('review.label')}
          size="size14"
          fFamily="MEDIUM"
          color="TEXT"
          textAlign="center"
          fWeight="500"
          mgBottom={20}
        />
      </Block>
      <Text
        text={translate(props.isService ? 'review.empty_service' : 'review.empty_product')}
        size="size12"
        fFamily="MEDIUM"
        color="TEXT"
        textAlign="center"
        mgTop={20}
        mgHorizontal={60}
      />
      <Touchable
        onPress={() => {}}
        h={37}
        bgColor={Colors.COLOR_1}
        align="center"
        justify="center"
        borderRadius={8}
        mgTop={24}>
        <Text
          text={translate(props.isService ? 'review.use_now' : 'review.buy_now')}
          size="size16"
          fFamily="MEDIUM"
          color="WHITE"
          fWeight="500"
        />
      </Touchable>
    </Block>
  );
};

export const Empty = memo(EmptyComponent, isEqual);

const styles = StyleSheet.create({
  container: {},
});
