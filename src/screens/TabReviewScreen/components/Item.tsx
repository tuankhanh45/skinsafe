import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Block, ImageRemote, Button, StarRate, Text } from '@components';
import { currencyFormat, moderateScale, scale, vScale } from '@utils';
import { Colors } from '@themes';
import { IconStar, translate } from '@assets';
import { useNavigation } from '@react-navigation/native';
import { SCREENS } from '@navigation';
interface IItemProps {
  data: any;
  isService?: boolean;
}
const data = {
  avatar: 'https://tikipetstore.com/wp-content/uploads/2021/07/me-o-ocean-fish-kitten-8-min.png',
  rate: 5,
  total_rate: 100,
  price: 2200000,
  name: 'Dịch vụ khách sạn Smart Power',
};

const ItemComponent: React.FC<IItemProps> = (props) => {
  const { isService } = props;
  const navigation = useNavigation<any>();
  return (
    <Block style={styles.container} pd={20}>
      <Block direction="row">
        <ImageRemote source={data?.avatar} resizeMode="cover" style={styles.avatar} />
        <Block mgLeft={24}>
          <Text text={data?.name} size="size14" fFamily="MEDIUM" color="TEXT" fWeight="500" w={140} />
          <Block mgTop={11} direction="row" align="center">
            <StarRate rate={data?.rate} />
            <Text
              text={`(${data?.total_rate})`}
              size="size10"
              fFamily="MEDIUM"
              color="COLOR_2"
              fWeight="700"
              mgLeft={4}
            />
          </Block>
          <Text
            text={currencyFormat(data?.price)}
            size="size14"
            fFamily="MEDIUM"
            color="COLOR_3"
            fWeight="500"
            mgTop={11}
          />
          <Block mgTop={16}>
            <Button
              onPress={() => navigation.native(SCREENS.REVIEW, { type: isService ? 'SERVICE' : 'PRODUCT' })}
              style={{ width: scale(123), height: scale(34) }}>
              {translate('review.review')}
            </Button>
          </Block>
        </Block>
      </Block>
    </Block>
  );
};

export const Item = memo(ItemComponent, isEqual);

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: moderateScale(1),
    borderBottomColor: '#EBEBEB',
  },
  avatar: { width: scale(70), height: vScale(70), borderRadius: moderateScale(4) },
});
