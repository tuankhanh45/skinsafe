import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Block, ImageRemote, ProgressBar, CustomFlatlist, Text } from '@components';
import { moderateScale, scale, vScale } from '@utils';
import { Colors } from '@themes';
import { IconStar, translate } from '@assets';
import times from 'lodash/times';
import { RenderItemProps } from '@configs';
import { Item } from './Item';
import { Empty } from './Empty';

interface IListItemProps {
  isService: boolean;
  type: string;
}

const ListItemComponent: React.FC<IListItemProps> = (props) => {
  const { isService, type } = props;
  const renderItem = ({ item, index }: RenderItemProps) => {
    return <Item data={{}} isService={isService} />;
  };
  return (
    <CustomFlatlist
      data={[1, 2, 3]}
      renderItem={renderItem}
      ListEmptyComponent={() => <Empty isService={isService} />}
    />
  );
};

export const ListItem = memo(ListItemComponent, isEqual);

const styles = StyleSheet.create({});
