import { StyleSheet } from 'react-native';
import { device, moderateScale, scale, vScale } from '@utils';
import { Colors, Fonts, FontSize } from '@themes';

const styles = StyleSheet.create({
  wrapper: {
    borderTopColor: '#F4F3F9',
    borderTopWidth: moderateScale(0.5),
  },
  tabBar: {
    backgroundColor: Colors.WHITE,
    height: scale(45),
  },
  tabBarLabel: {
    fontWeight: '500',
    color: Colors.TEXT,
    textTransform: 'none',
    fontSize: FontSize.size14,
    lineHeight: moderateScale(17),
    fontFamily: Fonts.MEDIUM,
  },
  tabStyle: {
    paddingVertical: vScale(13),
  },
});

export default styles;
