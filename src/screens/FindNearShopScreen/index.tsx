import isEqual from 'react-fast-compare';
import React, { memo, useEffect, useRef, useState } from 'react';
import { KeyboardAwareFlatList } from '@codler/react-native-keyboard-aware-scroll-view';

import {
  SpaItem,
  HotelItem,
  HeaderBar,
  SafeWrapper,
  ModalLocation,
  ModalSelectLocation,
  NotificationBadgeBtn,
} from '@components';
import styles from './styles';
import { translate } from '@assets';
import { RenderItemProps } from '@configs';
import { Header, ShopItem } from './components';
import { checkPermissionLocation } from '@utils';

interface IFindNearShopProps {
  route: any;
  navigation: any;
}

const FindNearShopComponent: React.FC<IFindNearShopProps> = (props) => {
  const { route, navigation } = props;
  const type = route?.params?.type;

  const refModal = useRef<any>(null);
  const refModalLocation = useRef<any>(null);

  const [addressInfo, setAddressInfo] = useState<any>({
    province: null,
    district: null,
    ward: null,
  });

  useEffect(() => {
    // checkLocation();
  }, []);

  const checkLocation = async () => {
    // const isApprove = await checkPermissionLocation();
    // if (!isApprove) {
    //   refModalLocation.current?.open();
    // }
  };

  const renderRight = () => <NotificationBadgeBtn />;

  const onOpenModal = () => refModal.current?.open();

  const keyExtractor = (item: any, index: number) => index + 'shop';
  const renderItem = ({ item, index }: RenderItemProps) => {
    if (type === 'hotel') {
      return <HotelItem data={item} type={type} navigation={navigation} />;
    }
    if (type === 'spa') {
      return <SpaItem data={item} type={type} navigation={navigation} />;
    }
    return <ShopItem data={item} type={type} navigation={navigation} />;
  };

  const title = type ? `find_near_shop.title_${type}` : 'find_near_shop.title';

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate(title)} renderRight={renderRight} highlightCenter />

      <KeyboardAwareFlatList
        data={[1, 2, 3]}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        contentContainerStyle={styles.contentContainerStyle}
        ListHeaderComponent={<Header onPressLocation={onOpenModal} />}
      />
      <ModalLocation ref={refModalLocation} />
      <ModalSelectLocation ref={refModal} addressInfo={addressInfo} setAddressInfo={setAddressInfo} />
    </SafeWrapper>
  );
};

export const FindNearShopScreen = memo(FindNearShopComponent, isEqual);
