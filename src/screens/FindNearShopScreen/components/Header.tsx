import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { Block, Input, Text, Touchable } from '@components';
import { IconLocation2, IconSearch, translate } from '@assets';

interface IHeaderProps {
  onPressLocation: () => void;
}

const HeaderComponent: React.FC<IHeaderProps> = (props) => {
  const { onPressLocation } = props;

  return (
    <Block pdBottom={16}>
      <Block style={styles.inputContainer}>
        <Input
          iconLeft={IconSearch}
          styleContainer={{ flex: 1 }}
          styleWrapInput={styles.wrapInput}
          placeholder={translate('cm.search')}
        />
        <Touchable style={styles.selectLocationBtn} onPress={onPressLocation}>
          <IconLocation2 color={Colors.COLOR_1} />
          <Text size="size12" color="COLOR_9">
            Ha Noi
          </Text>
        </Touchable>
      </Block>
      <Block h={213} w={'100%'} mgTop={16}>
        <MapView showsUserLocation style={StyleSheet.absoluteFillObject} provider={PROVIDER_GOOGLE} />
      </Block>
    </Block>
  );
};

export const Header = memo(HeaderComponent, isEqual);

const styles = StyleSheet.create({
  wrapInput: {
    marginTop: 0,
    borderWidth: 0,
    height: vScale(32),
    paddingHorizontal: 0,
    backgroundColor: Colors.WHITE,
  },
  selectLocationBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: scale(8),
    paddingLeft: scale(7),
    marginVertical: vScale(5),
    borderColor: Colors.COLOR_9,
    borderLeftWidth: moderateScale(1),
  },
  inputContainer: {
    flexDirection: 'row',
    marginTop: vScale(16),
    borderColor: '#E1E1E1',
    paddingHorizontal: scale(15),
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(100),
  },
});
