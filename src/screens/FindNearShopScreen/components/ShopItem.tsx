import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { IconHospital, IconLocation } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, Image, ImageRemote, Text, Touchable } from '@components';

interface IShopItemProps {
  data: any;
  type: string;
  navigation: any;
}

const ShopItemComponent: React.FC<IShopItemProps> = (props) => {
  const { data, type, navigation } = props;

  const onPressDetail = () => {
    if (type) {
      return navigation.navigate(SCREENS.DETAIL_NEAR_SHOP, { type, data });
    }
  };

  return (
    <Touchable style={styles.wrapper} onPress={onPressDetail}>
      <ImageRemote source={'https://picsum.photos/801'} style={styles.img} />
      <Block pdLeft={6} flex>
        <Block direction="row" align="center" flex>
          <Block style={styles.iconBox}>
            <IconHospital />
          </Block>
          <Block flex>
            <Block direction="row">
              <Text fWeight="700" flex numberOfLines={1}>
                day laf ten shop dai vclas jkdkalsjd
              </Text>
              <Block direction="row" align="center" mgLeft={8}>
                <IconLocation />
                <Text mgLeft={2} color={'COLOR_1'}>
                  0.2<Text> km</Text>
                </Text>
              </Block>
            </Block>
            <Text mgTop={3} fWeight="500" size="size12">
              Giờ mở cửa: 8h00p -22h00p
            </Text>
          </Block>
        </Block>

        <Block direction="row" align="center" flex pdRight={26}>
          <Block style={styles.iconBox}>
            <Image source="ic_location_pet" style={styles.imgLocation} />
          </Block>
          <Text fWeight="700" numberOfLines={2}>
            Số 1 A nguyễn văn trỗi, Hà Đông Hà Nội
          </Text>
        </Block>
      </Block>
    </Touchable>
  );
};

export const ShopItem = memo(ShopItemComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    paddingLeft: scale(3),
    flexDirection: 'row',
    paddingRight: scale(26),
    marginBottom: vScale(10),
    paddingVertical: vScale(5),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(9),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  img: {
    width: scale(76),
    height: scale(76),
    borderRadius: moderateScale(8),
  },
  imgLocation: {
    width: scale(9),
    height: vScale(13),
  },
  iconBox: {
    width: scale(20),
    height: scale(20),
    alignItems: 'center',
    marginLeft: scale(7),
    marginRight: scale(8),
    justifyContent: 'center',
    backgroundColor: '#FDE8E8',
    borderRadius: moderateScale(20),
  },
});
