import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  wrapInput: {
    marginTop: 0,
    borderWidth: 0,
    height: vScale(32),
    paddingHorizontal: 0,
    backgroundColor: Colors.WHITE,
  },
  selectLocationBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: scale(7),
    marginVertical: vScale(5),
    borderColor: Colors.COLOR_9,
    borderLeftWidth: moderateScale(1),
  },
  inputContainer: {
    borderColor: '#E1E1E1',
    marginTop: vScale(16),
    marginHorizontal: scale(15),
    paddingHorizontal: scale(15),
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(100),
  },
  contentContainerStyle: {
    paddingHorizontal: scale(15),
  },
});

export default styles;
