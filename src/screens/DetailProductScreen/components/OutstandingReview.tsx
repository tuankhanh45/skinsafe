import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, Text, Touchable } from '@components';

interface IOutstandingReviewProps {
  data: any;
}
const DATA = [1, 2, 3, 4];

const OutstandingReviewComponent: React.FC<IOutstandingReviewProps> = (props) => {
  const { data } = props;

  return (
    <Block style={styles.wrapper}>
      {DATA.map((e, i) => {
        const isLastItem = i === DATA.length - 1;
        return (
          <Touchable style={styles.imgBox} key={i}>
            <ImageRemote style={styles.img} source={'https://picsum.photos/801'} key={i} resizeMode={'cover'} />
            {isLastItem && (
              <Block style={styles.moreBox}>
                <Text fWeight="600" size={'size18'} color={'COLOR_4'}>
                  +20
                </Text>
              </Block>
            )}
          </Touchable>
        );
      })}
    </Block>
  );
};

export const OutstandingReview = memo(OutstandingReviewComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    paddingHorizontal: scale(15),
    justifyContent: 'space-between',
  },
  imgBox: {
    width: scale(80),
    height: vScale(71),
    borderRadius: moderateScale(6),
  },
  img: {
    height: '100%',
    width: '100%',
    borderRadius: moderateScale(6),
  },
  moreBox: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(6),
    backgroundColor: 'rgba(49, 49, 49, .7)',
  },
});
