import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ItemProduct, Text, Touchable } from '@components';

interface IRelatedProductsProps {
  data: any[];
  label: string;
  labelMore?: string;
  onPressMore?: () => void;
}

const RelatedProductsComponent: React.FC<IRelatedProductsProps> = (props) => {
  const { data, label, labelMore, onPressMore } = props;

  if (Array.isArray(data) && !data.length) return null;

  return (
    <Block style={styles.wrapper}>
      <Block direction="row" justify="space-between" mgBottom={20}>
        <Text size={'size15'} fFamily={'BRANDING'}>
          {translate(`detail_product.${label}`)}
        </Text>
        <Touchable onPress={onPressMore}>
          <Text fWeight="600" size="size11" color="COLOR_1">
            {translate(`detail_product.${labelMore}`)}
          </Text>
        </Touchable>
      </Block>

      <Block direction="row" justify="space-between">
        {Array.isArray(data) &&
          data.map((e, i) => {
            return <ItemProduct key={i} data={{}} />;
          })}
      </Block>
    </Block>
  );
};

export const RelatedProducts = memo(RelatedProductsComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    marginTop: vScale(16),
    paddingTop: vScale(10),
    paddingBottom: vScale(24),
    marginHorizontal: scale(8),
    paddingHorizontal: scale(5),
    backgroundColor: Colors.COLOR_4,
    borderRadius: moderateScale(10),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
