import dayjs from 'dayjs';
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { ImageBackground, StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { images, translate } from '@assets';
import { device, moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, StarRate, Text, Touchable } from '@components';

interface IReviewProductProps {}

const ReviewProductComponent: React.FC<IReviewProductProps> = (props) => {
  const {} = props;

  return (
    <Block style={styles.wrapper}>
      <ImageBackground source={images.bg_review_product} style={styles.bg} resizeMode={'cover'}>
        <Text fWeight="600">{translate('detail_product.review')}</Text>
        <Text fWeight="600" size="size11" color="COLOR_1">
          {translate('cm.view_more')}
        </Text>
      </ImageBackground>

      <ItemReview data={{}} />
      <ItemReview data={{}} />
      <Text textAlign="center" mgVertical={15} fWeight="500" size={'size12'}>
        Còn{' '}
        <Text fWeight="500" size={'size12'} color="COLOR_1" style={{ fontStyle: 'italic' }}>
          131 bình luận khác
        </Text>
        , Bấm vào để xem
      </Text>
    </Block>
  );
};

export const ReviewProduct = memo(ReviewProductComponent, isEqual);
const DATA = [1, 2, 3, 4];

const ItemReview = memo((props: any) => {
  const { data } = props;

  return (
    <Block style={styles.itemWrapper}>
      <Block direction="row">
        <ImageRemote source="https://picsum.photos/801" style={styles.avatar} />

        <Block flex align="flex-start" pdLeft={10}>
          <Block direction="row" mgBottom={4}>
            <Text fWeight="600" size="size12" color="PRIMARY" mgRight={10} flex>
              username
            </Text>
            <Text size="size8">{dayjs().format('DD/MM/YYYY')}</Text>
          </Block>
          <StarRate rate={4} />
        </Block>
      </Block>

      <Text mgTop={10} size={'size10'} color="TEXT">
        Rất hài lòng. Lần đầu mua của shop. Khá lo lắng nhưng khi nhận được giày thì rất chi là vừa ý. Hợp giá tiền.
        Đáng mua nha mọi người. Hộp hơi hơi móp nên hơi tiếc xíu.
      </Text>

      <Block direction="row" mgVertical={8}>
        {DATA.map((e, i) => {
          const isLastItem = i === DATA.length - 1;
          return (
            <Touchable style={styles.imgBox} key={i}>
              <ImageRemote style={styles.img} source={'https://picsum.photos/801'} key={i} resizeMode={'cover'} />
              {isLastItem && (
                <Block style={styles.moreBox}>
                  <Text fWeight="600" size={'size18'} color={'COLOR_4'}>
                    +20
                  </Text>
                </Block>
              )}
            </Touchable>
          );
        })}
      </Block>
    </Block>
  );
});

const styles = StyleSheet.create({
  wrapper: {
    marginTop: vScale(14),
    marginHorizontal: scale(8),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(15),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  bg: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: vScale(20),
    paddingBottom: vScale(8),
    paddingHorizontal: scale(8),
    justifyContent: 'space-between',
    borderTopLeftRadius: moderateScale(15),
    borderTopRightRadius: moderateScale(15),
  },
  avatar: {
    width: scale(32),
    height: scale(32),
    borderRadius: moderateScale(32),
  },
  imgBox: {
    width: scale(75),
    height: vScale(75),
    borderRadius: moderateScale(6),
    marginRight: (device.width - scale(8 * 2) - scale(15 * 2) - scale(4 * 75)) / 3,
  },
  img: {
    height: '100%',
    width: '100%',
    borderRadius: moderateScale(6),
  },
  moreBox: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(6),
    backgroundColor: 'rgba(49, 49, 49, .7)',
  },
  itemWrapper: {
    borderColor: '#D0D0D0',
    paddingVertical: vScale(15),
    marginHorizontal: scale(15),
    borderBottomWidth: moderateScale(1),
  },
});
