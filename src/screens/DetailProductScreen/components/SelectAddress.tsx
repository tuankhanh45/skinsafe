import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { IconLocation2, translate } from '@assets';
import { Block, Text, Touchable } from '@components';
import { moderateScale, scale, vScale } from '@utils';

interface ISelectAddressProps {
  navigation: any;
}

const COLORS = ['rgba(255, 200, 200, 0.27)', 'rgba(255, 96, 96, 0.21)', 'rgba(255, 200, 200, 0.26) '];

const SelectAddressComponent: React.FC<ISelectAddressProps> = (props) => {
  const { navigation } = props;

  const onPressSelect = () => {
    navigation.navigate(SCREENS.SELECT_ADDRESS);
  };

  return (
    <Block pdHorizontal={15}>
      <LinearGradient colors={COLORS} style={styles.linear}>
        <Block direction="row" align="center">
          <IconLocation2 />
          <Text fWeight="700" size="size10" mgLeft={3}>
            {translate('detail_product.select_address')}
          </Text>
        </Block>

        <Touchable onPress={onPressSelect}>
          <Text style={styles.more}>({translate('detail_product.select_address_now')})</Text>
        </Touchable>
      </LinearGradient>
    </Block>
  );
};

export const SelectAddress = memo(SelectAddressComponent, isEqual);

const styles = StyleSheet.create({
  linear: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: vScale(2),
    paddingHorizontal: scale(8),
    borderRadius: moderateScale(50),
    justifyContent: 'space-between',
  },
  more: {
    color: '#457BBB',
    fontWeight: '700',
    fontSize: FontSize.size10,
  },
});
