import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Block, Text } from '@components';

interface IDescProductProps {
  data: any;
}

const DescProductComponent: React.FC<IDescProductProps> = (props) => {
  const { data } = props;

  return (
    <Block pdHorizontal={15}>
      <Text size="size12" textAlign="justify">
        {data?.product_detail?.content}
      </Text>
    </Block>
  );
};

export const DescProduct = memo(DescProductComponent, isEqual);

const styles = StyleSheet.create({});
