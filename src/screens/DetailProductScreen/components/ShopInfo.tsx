import React, { memo } from 'react';
import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import LinearGradient from 'react-native-linear-gradient';
import { ImageBackground, StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { handleResponse, requestFollowShop } from '@configs';
import { Block, ImageRemote, Text, Touchable } from '@components';
import { checkType, moderateScale, scale, showToastSuccess, vScale } from '@utils';
import { IconLocation2, IconMedal2, IconSuccess, images, translate } from '@assets';

interface IShopInfoProps {
  data: any;
}

const COLORS = ['rgba(252, 11, 84, 0.57)', '#FF6A3C'];
const LIST_TARGET = [
  { label: 'satisfied' },
  { label: 'follower', field: 'follow_count' },
  { label: 'point_evaluation', field: 'rating_avg' },
  { label: 'share' },
];
//TODO: add field, change status follow
const ShopInfoComponent: React.FC<IShopInfoProps> = (props) => {
  const { data } = props;

  const { run, loading } = useRequest(requestFollowShop, {
    manual: true,
    onSuccess: (data) => {
      const { res, isError, err } = handleResponse(data);
      if (res) {
        showToastSuccess(checkType(res, 'string') ? res : 'Thành công');
      }
    },
  });

  const onPressFollow = () => {
    run({ shop_id: data?.id });
  };

  return (
    <Block style={styles.wrapper}>
      <ImageBackground
        source={data?.cover ? { uri: data.cover } : images.banner_shop_default}
        style={styles.banner}
        resizeMode={'cover'}>
        <Block pdHorizontal={14} pdVertical={12} direction="row">
          <ImageRemote source={data?.avatar} style={styles.avatar} resizeMode="cover" />

          <Block flex pdHorizontal={16}>
            <Block direction="row" align="flex-start" justify="space-between">
              <Block>
                <Block direction="row" align="center">
                  <Text size="size15" fFamily="BRANDING" color="COLOR_4" mgRight={5} numberOfLines={1}>
                    {data?.name}
                  </Text>
                  {data?.certificate && <IconSuccess color={Colors.COLOR_1} />}
                </Block>
                <Block direction="row" align="center">
                  <IconLocation2 />
                  <Text mgLeft={6} size={'size10'} color={'COLOR_4'}>
                    {data?.province?.name}
                  </Text>
                </Block>
              </Block>

              <Touchable onPress={onPressFollow}>
                <LinearGradient colors={COLORS} style={styles.linear} start={{ x: 0, y: 0 }}>
                  <Text fWeight="700" size={'size10'} color={'COLOR_4'}>
                    + {translate('detail_product.follow_shop')}
                  </Text>
                </LinearGradient>
              </Touchable>
            </Block>

            <Block direction="row" pdTop={16} justify={'space-between'}>
              {LIST_TARGET.map((e, i) => {
                const isLastItem = i === LIST_TARGET.length - 1;
                return (
                  <React.Fragment key={i}>
                    <Block center direction="row">
                      <Block center>
                        <Text size={'size8'} color={'COLOR_4'} fWeight={'500'}>
                          {e?.field && data?.[e?.field] ? data?.[e?.field] : 'value'}
                        </Text>
                        <Text size={'size8'} color={'COLOR_4'} fWeight={'500'}>
                          {translate(`detail_product.${e.label}`)}
                        </Text>
                      </Block>
                    </Block>
                    {!isLastItem && <Block style={styles.divider} />}
                  </React.Fragment>
                );
              })}
            </Block>
          </Block>
        </Block>
      </ImageBackground>

      <Block style={styles.medalBox}>
        <IconMedal2 />
        <Text mgLeft={8} size={'size9'} flex style={{ letterSpacing: -0.5 }}>
          Smart Power là chuỗi thức ăn và chăm sóc Smart Power lớn và uy tín nhất việt nam
        </Text>
      </Block>
    </Block>
  );
};

export const ShopInfo = memo(ShopInfoComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    height: vScale(113),
    marginTop: vScale(15),
  },
  banner: {
    height: '100%',
    width: '100%',
  },
  avatar: {
    width: scale(52),
    height: scale(52),
    borderColor: Colors.COLOR_4,
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(52),
  },
  linear: {
    padding: scale(4),
    borderRadius: moderateScale(100),
  },
  divider: {
    width: scale(1),
    height: vScale(11),
    marginTop: vScale(5),
    backgroundColor: Colors.COLOR_9,
  },
  medalBox: {
    bottom: -12,
    position: 'absolute',
    alignItems: 'center',
    flexDirection: 'row',
    borderStyle: 'dashed',
    borderColor: '#FCC00D',
    backgroundColor: '#f6f0e1',
    paddingVertical: vScale(3),
    marginHorizontal: scale(10),
    paddingHorizontal: scale(10),
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(8),
  },
});
