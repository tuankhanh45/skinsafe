import isEqual from 'react-fast-compare';
import Swiper from 'react-native-swiper';
import { StyleSheet } from 'react-native';
import React, { memo, useState } from 'react';

import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, Text, Touchable } from '@components';
import { IconLike, IconNext, IconPrevious, IconShare } from '@assets';

interface IListImageProductProps {
  product: any;
  listImages: any[];
  onPressLike: () => void;
  onPressShare: () => void;
}

const ListImageProductComponent: React.FC<IListImageProductProps> = (props) => {
  const { listImages, onPressLike, onPressShare, product } = props;

  const [currentIndex, setCurrentIndex] = useState<number>(0);

  return (
    <Block flex>
      <Block style={styles.imgContainer}>
        <Block>
          <Swiper
            showsButtons
            height={vScale(337)}
            showsPagination={false}
            nextButton={<IconNext />}
            prevButton={<IconPrevious />}
            onIndexChanged={setCurrentIndex}>
            {listImages.map((e, i) => {
              return <ImageRemote source={e?.url} key={i} style={styles.bannerImg} resizeMode={'cover'} />;
            })}
          </Swiper>
        </Block>

        <Block style={styles.footer}>
          {+product?.percentage_sale > 0 && (
            <Block style={styles.discountBox}>
              <Text style={styles.discountText}>-{product?.percentage_sale}%</Text>
            </Block>
          )}
          <Block style={styles.indexBox}>
            <Text style={styles.indexText}>
              {currentIndex + 1}/{listImages.length}
            </Text>
          </Block>
        </Block>

        <Block style={styles.header}>
          <Touchable onPress={onPressLike}>
            <IconLike fill={product?.favorite_count ? Colors.COLOR_3 : Colors.WHITE} />
          </Touchable>

          <Touchable onPress={onPressShare} style={styles.shareBtn}>
            <IconShare />
          </Touchable>
        </Block>
      </Block>
    </Block>
  );
};

export const ListImageProduct = memo(ListImageProductComponent, isEqual);

const styles = StyleSheet.create({
  bannerImg: {
    width: '100%',
    height: '100%',
    borderRadius: moderateScale(10),
  },
  imgContainer: {
    padding: scale(4),
    marginTop: vScale(2),
    borderColor: '#D0D0D0',
    marginHorizontal: scale(15),
    borderRadius: moderateScale(10),
    borderWidth: moderateScale(0.5),
  },
  discountBox: {
    paddingVertical: vScale(4),
    paddingHorizontal: scale(12),
    borderRadius: moderateScale(10),
    backgroundColor: Colors.COLOR_2,
  },
  footer: {
    left: 0,
    right: 0,
    bottom: 0,
    flexDirection: 'row',
    position: 'absolute',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  discountText: {
    color: Colors.COLOR_4,
    fontSize: FontSize.size22,
    lineHeight: moderateScale(26.25),
  },
  indexBox: {
    width: scale(50),
    alignItems: 'center',
    paddingVertical: vScale(3),
    backgroundColor: '#EDEDED',
    borderRadius: moderateScale(20),
  },
  indexText: {
    fontWeight: '600',
    color: Colors.TEXT_2,
    fontSize: FontSize.size12,
    lineHeight: moderateScale(18),
  },
  header: {
    top: 13,
    right: 13,
    position: 'absolute',
    justifyContent: 'flex-end',
  },
  shareBtn: {
    width: scale(25),
    height: scale(25),
    marginTop: vScale(15),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FF8F76',
    borderRadius: moderateScale(100),
  },
});
