import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors, FontSize } from '@themes';
import { Block, Text, Touchable } from '@components';
import { moderateScale, scale, vScale } from '@utils';
import { IconCall2, IconCart2, IconChat2, translate } from '@assets';

interface IQuickActionFooterProps {}

const LIST_ACTIONS = [
  { icon: <IconCall2 />, label: 'Gọi mua' },
  { icon: <IconChat2 />, label: 'Trao đổi' },
  { icon: <IconCart2 />, label: 'Giỏ hàng' },
];

const QuickActionFooterComponent: React.FC<IQuickActionFooterProps> = (props) => {
  const {} = props;

  return (
    <Block style={styles.wrapper}>
      <Block direction="row">
        {LIST_ACTIONS.map((e, i) => {
          return (
            <Touchable key={i} style={[styles.quickBtnItem, i === LIST_ACTIONS.length - 1 && { marginRight: 0 }]}>
              {e.icon}
              <Text style={styles.labelTextItem}>{e.label}</Text>
            </Touchable>
          );
        })}
      </Block>

      <Block direction="row" flex>
        <Touchable style={styles.addCartBtn}>
          <Text style={styles.addCartTitle}>{translate('detail_product.add_cart')}</Text>
        </Touchable>

        <Touchable style={[styles.addCartBtn, { backgroundColor: Colors.COLOR_3 }]}>
          <Text style={styles.addCartTitle}>{translate('detail_product.buy_now')}</Text>
        </Touchable>
      </Block>
    </Block>
  );
};

export const QuickActionFooter = memo(QuickActionFooterComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    paddingTop: vScale(8),
    paddingBottom: vScale(10),
    paddingHorizontal: scale(15),
  },
  quickBtnItem: {
    width: scale(32),
    height: vScale(39),
    alignItems: 'center',
    marginRight: scale(10),
    justifyContent: 'center',
  },
  labelTextItem: {
    fontWeight: '700',
    textAlign: 'center',
    fontSize: FontSize.size8,
  },
  addCartBtn: {
    flex: 1,
    alignItems: 'center',
    marginLeft: scale(10),
    justifyContent: 'center',
    backgroundColor: Colors.COLOR_6,
    borderRadius: moderateScale(100),
  },
  addCartTitle: {
    fontWeight: '700',
    textAlign: 'center',
    color: Colors.COLOR_4,
    fontSize: FontSize.size12,
  },
});
