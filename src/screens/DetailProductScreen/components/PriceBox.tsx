import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import React, { memo, useRef } from 'react';
import LinearGradient from 'react-native-linear-gradient';

import { Colors, FontSize } from '@themes';
import { currencyFormat, device, moderateScale, scale, vScale } from '@utils';
import { IconMedal, IconPlayVideo, IconQuestion, ImagesTypes, translate } from '@assets';
import { Block, CustomModal, Image, ImageRemote, StarRate, Text, Touchable } from '@components';

interface IPriceBoxProps {
  product: any;
}
interface ICommitmentType {
  img: ImagesTypes;
  label: string;
}

const COLORS = ['rgba(253, 232, 232, 0.6)', 'rgba(240, 240, 240, 0)'];
const COLORS_2 = ['#FEF6F2', '#f4fafc'];
const LIST_COMMITMENT: ICommitmentType[] = [
  { img: 'checked', label: 'Chính hãng' },
  { img: 'rotate', label: 'Đổi trả dễ dàng' },
  { img: 'liked', label: '100% Hài lòng' },
];
const LIST_REVIEW_AND_COMMENT_COUNT = [
  { icon: <IconPlayVideo />, field: 'comment_count' },
  { icon: <IconQuestion />, field: 'qa_count' },
];

const PriceBoxComponent: React.FC<IPriceBoxProps> = (props) => {
  const { product } = props;
  const refModal = useRef<any>(null);

  const onPressViewCertificate = () => refModal.current?.open();
  const onBackdropPress = () => refModal.current?.close();

  const renderContent = () => {
    return (
      <Block style={styles.modalContainer}>
        <Text size={'size15'} fFamily="BRANDING">
          {translate('detail_product.certificate')}
        </Text>
        <Block direction="row" pdTop={10}>
          {[1, 2].map((e, i) => {
            return (
              <ImageRemote
                source="https://picsum.photos/801"
                style={[styles.imgCer, i === 1 && { marginRight: 0 }]}
                key={i}
                resizeMode={'cover'}
              />
            );
          })}
        </Block>
      </Block>
    );
  };

  return (
    <Block pdHorizontal={15}>
      <LinearGradient colors={COLORS} style={styles.linear}>
        <Text style={styles.price}>{currencyFormat(product?.price_sale || product?.price_regular)}</Text>
        {product?.price_sale && <Text style={styles.originalPrice}>{currencyFormat(product?.price_regular)}</Text>}

        <Text fWeight={'500'} size={'size16'} mgTop={15}>
          {product?.product_detail?.title}
        </Text>

        <Block direction="row" mgTop={15} align={'center'} justify="space-between">
          <Block direction="row" align={'center'}>
            <StarRate rate={product?.rating_avg} />
            <Text size={'size10'} fWeight={'700'} color={'COLOR_2'}>
              ({0})
            </Text>
            <Image source="imgVerticalDivider" style={styles.verticalDivider} />
            <Text size={'size12'} style={{ color: '#323232' }}>
              Đã bán {product?.qty_sell}
            </Text>
          </Block>
          <Block direction="row" align={'center'}>
            {LIST_REVIEW_AND_COMMENT_COUNT.map((e, i) => {
              return (
                <Block key={i} style={[styles.reviewItem, { marginRight: scale(i === 0 ? 18 : 2) }]}>
                  {e.icon}
                  <Text size="size10" mgLeft={8}>
                    {product?.[e.field]}
                  </Text>
                </Block>
              );
            })}
          </Block>
        </Block>

        <LinearGradient colors={COLORS_2} style={styles.linear2} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}>
          {LIST_COMMITMENT.map((e, i) => {
            return (
              <Block key={i} direction={'row'} align="center">
                <Image source={e.img} style={styles.imgCommitment} />
                <Text style={styles.commitText}>{e.label}</Text>
              </Block>
            );
          })}
        </LinearGradient>
      </LinearGradient>

      <Touchable style={styles.medalBox} onPress={onPressViewCertificate}>
        <IconMedal />
        <Text mgLeft={8} fWeight={'700'} size={'size12'} flex>
          Name shop là chuỗi thức ăn và chăm sóc Smart Power lớn và uy tín nhất việt nam
        </Text>
      </Touchable>

      <CustomModal ref={refModal} renderContent={renderContent} onBackdropPress={onBackdropPress} />
    </Block>
  );
};

export const PriceBox = memo(PriceBoxComponent, isEqual);

const styles = StyleSheet.create({
  linear: {
    paddingVertical: vScale(12),
  },
  price: {
    fontWeight: '700',
    fontSize: FontSize.size22,
  },
  originalPrice: {
    marginTop: vScale(8),
    fontSize: FontSize.size14,
    textDecorationLine: 'line-through',
  },
  linear2: {
    flexDirection: 'row',
    marginTop: vScale(20),
    paddingVertical: vScale(5),
    paddingHorizontal: scale(10),
    justifyContent: 'space-between',
    borderRadius: moderateScale(40),
    borderWidth: moderateScale(0.5),
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  imgCommitment: {
    width: scale(17),
    height: scale(17),
  },
  commitText: {
    fontWeight: '700',
    marginLeft: scale(2),
    fontSize: FontSize.size10,
  },
  medalBox: {
    flexDirection: 'row',
    borderStyle: 'dashed',
    borderColor: '#FCC00D',
    paddingTop: vScale(13),
    paddingBottom: vScale(17),
    paddingHorizontal: scale(20),
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(8),
    backgroundColor: 'rgba(239, 206, 120, 0.2)',
  },
  modalContainer: {
    width: '100%',
    padding: scale(12),
    backgroundColor: Colors.WHITE,
  },
  imgCer: {
    height: vScale(97),
    marginRight: scale(16),
    borderRadius: moderateScale(3),
    width: (device.width - scale(16 * 2) - scale(12 * 2) - scale(16)) / 2,
  },
  verticalDivider: {
    height: vScale(16),
    marginHorizontal: scale(10),
  },
  reviewItem: {
    paddingVertical: vScale(1),
    paddingHorizontal: scale(8),
    borderRadius: moderateScale(40),
    backgroundColor: 'rgba(237, 27, 36, 0.12)',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.15,
    shadowRadius: 5.84,
    elevation: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
