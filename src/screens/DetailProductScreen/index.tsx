import { useRequest } from 'ahooks';
import { Alert } from 'react-native';
import isEqual from 'react-fast-compare';
import React, { memo, useEffect, useState } from 'react';

import {
  PriceBox,
  ShopInfo,
  DescProduct,
  ReviewProduct,
  SelectAddress,
  RelatedProducts,
  ListImageProduct,
  OutstandingReview,
  QuickActionFooter,
} from './components';
import {
  Block,
  Loading,
  HeaderBar,
  LabelCate,
  ItemProduct,
  SafeWrapper,
  CustomFlatlist,
  NotificationBadgeBtn,
  ListEmpty,
} from '@components';
import {
  handleResponse,
  RenderItemProps,
  requestGetDetailProduct,
  requestAddFavoriteProduct,
  requestRemoveFavoriteProduct,
} from '@configs';
import styles from './styles';
import { translate } from '@assets';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { checkType, fakeItemInOddArray, showToastError, showToastSuccess } from '@utils';

interface IDetailProductProps {
  route: any;
  navigation: any;
}

const DetailProductComponent: React.FC<IDetailProductProps> = (props) => {
  const { navigation, route } = props;
  const productParams = route?.params?.data;

  const user = useAppStore((x) => x.account.user);
  const token = useAppStore((x) => x.account.token);

  const [data, setData] = useState<any>(null);
  const [listImages, setListImages] = useState<any[]>([{ url: productParams?.avatar }]);

  useEffect(() => {
    runGetProduct({ id: productParams?.id, user_id: user?.id });
  }, [token]);

  const { run: runGetProduct, loading: loadingGetProduct } = useRequest(requestGetDetailProduct, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (res) {
        setData(res);
        if (Array.isArray(res?.image_related)) {
          const listImg: any[] = [{ url: res?.avatar }];
          for (const img of res?.image_related) {
            listImg.push({ url: img.url_image });
          }
          setListImages(listImg);
        }
      } else {
        Alert.alert('Lỗi', 'Không tìm thấy sản phẩn', [{ text: 'Xác nhận', onPress: () => navigation.goBack() }]);
      }
    },
  });
  const { run: runRemoveFavorite, loading: loadingRemoveFavorite } = useRequest(requestRemoveFavoriteProduct, {
    manual: true,
    onSuccess: (data) => {
      const { res, isError, err } = handleResponse(data);
      if (isError) {
        const mgs: string =
          err?.message && checkType(err.message, 'string') ? err.message : 'Bỏ yêu thích thất bại. Xin thử lại';
        showToastError(mgs);
      } else {
        setData((prev: any) => ({ ...prev, favorite_count: 0 }));
        const mgs: string = res?.data && checkType(res.data, 'string') ? res.data : 'Bỏ yêu thích thành công';
        showToastSuccess(mgs);
      }
    },
  });
  const { run: runAddFavorite, loading: loadingAddFavorite } = useRequest(requestAddFavoriteProduct, {
    manual: true,
    onSuccess: (data) => {
      const { res, isError, err } = handleResponse(data);
      if (isError) {
        const mgs: string =
          err?.message && checkType(err.message, 'string') ? err.message : 'Thêm yêu thích thất bại. Xin thử lại';
        showToastError(mgs);
      } else {
        setData((prev: any) => ({ ...prev, favorite_count: 1 }));
        const mgs: string = res?.data && checkType(res.data, 'string') ? res.data : 'Thêm yêu thích thành công';
        showToastSuccess(mgs);
      }
    },
  });

  const renderRight = () => {
    return <NotificationBadgeBtn />;
  };

  const renderItem = ({ item, index }: RenderItemProps) => {
    if (item?.stt === 'empty') return <Block w={106} />;
    return <ItemProduct data={item} />;
  };

  const onNavigate = (screen: string) => {
    navigation.navigate(screen);
  };

  const onPressLike = () => {
    if (token) {
      if (data?.favorite_count) {
        runRemoveFavorite({ product_id: productParams?.id });
      } else {
        runAddFavorite({ product_id: productParams?.id });
      }
    } else {
      navigation.navigate(SCREENS.LOGIN, {
        cbFunc: () => navigation.canGoBack() && navigation.goBack(),
      });
    }
  };
  const onPressShare = () => {
    showToastSuccess('đang cập nhật');
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'} style={styles.wrapper}>
      <HeaderBar title={translate('detail_product.title')} renderRight={renderRight} highlightCenter />
      {loadingGetProduct && <Loading />}

      <CustomFlatlist
        numColumns={3}
        renderItem={renderItem}
        data={[]}
        columnWrapperStyle={styles.columnWrapper}
        ListEmptyComponent={<ListEmpty mgBottom={40} />}
        ListHeaderComponent={
          <>
            <ListImageProduct
              product={data}
              listImages={listImages}
              onPressLike={onPressLike}
              onPressShare={onPressShare}
            />

            <PriceBox product={data} />

            <LabelCate
              pdVertical={12}
              pdHorizontal={15}
              label={translate('detail_product.address')}
              labelMore={translate('detail_product.buy_in_shop')}
              onPressMore={() => onNavigate(SCREENS.FIND_NEAR_SHOP)}
            />
            <SelectAddress navigation={navigation} />

            <LabelCate
              pdVertical={12}
              pdHorizontal={15}
              onPressMore={() => {}}
              label={translate('detail_product.outstanding_review')}
            />
            <OutstandingReview data={{}} />

            <ShopInfo data={data?.shop} />

            <LabelCate pdBottom={12} pdTop={24} pdHorizontal={15} label={translate('detail_product.description')} />
            <DescProduct data={data} />

            <RelatedProducts
              data={[]}
              labelMore={'add_now'}
              label={'usually_bought_together'}
              onPressMore={() => onNavigate(SCREENS.BUY_COMBO)}
            />

            <RelatedProducts label={'better_price'} data={[]} labelMore={'hunt_now'} />

            <ReviewProduct />

            <LabelCate pdVertical={12} pdHorizontal={15} label={translate('detail_product.related_product')} />
          </>
        }
      />

      <QuickActionFooter />
    </SafeWrapper>
  );
};

export const DetailProductScreen = memo(DetailProductComponent, isEqual);
