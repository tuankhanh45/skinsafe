import dayjs from 'dayjs';
import isEqual from 'react-fast-compare';
import React, { memo, useRef, useState } from 'react';

import {
  Block,
  Text,
  Touchable,
  ListEmpty,
  HeaderBar,
  SafeWrapper,
  CustomFlatlist,
  CustomModalize,
  HeaderInformationPet,
  NotificationBadgeBtn,
} from '@components';
import styles from './styles';
import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { RenderItemProps } from '@configs';
import { ContentModalize } from './components';
import { IconHospital2, IconLocation2, translate } from '@assets';

interface IHealthRecordsProps {
  route: any;
  navigation: any;
}

const HealthRecordsComponent: React.FC<IHealthRecordsProps> = (props) => {
  const { route, navigation } = props;
  const selectedPet = route?.params?.selectedPet;

  const refModal = useRef<any>(null);

  const [selectedHistoryItem, setSelectedHistoryItem] = useState<any>(null);

  const renderRight = () => <NotificationBadgeBtn />;

  const onPressCloseModal = () => refModal.current.close();
  const onPressDetail = (item: any) => {
    refModal.current.open();
  };

  const renderItem = ({ item }: RenderItemProps) => {
    return <Item data={item} onPressDetail={() => onPressDetail(item)} />;
  };

  const ListHeaderComponent = () => (
    <Text size="size18" fFamily="BRANDING" mgBottom={8}>
      {translate('health_record.list')}
    </Text>
  );

  const onPressInfo = () => {
    navigation.pop();
    navigation.navigate(SCREENS.UTILITIES_MEDICAL);
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('health_record.title')} renderRight={renderRight} />
      <Block bgColor={Colors.COLOR_1} flex>
        <HeaderInformationPet pet={selectedPet} onPressInfo={onPressInfo} />

        <CustomFlatlist
          data={[1, 1, 2, 2]}
          style={styles.list}
          renderItem={renderItem}
          ListEmptyComponent={ListEmpty}
          ListHeaderComponent={ListHeaderComponent}
          contentContainerStyle={styles.contentContainer}
        />
      </Block>

      <CustomModalize ref={refModal}>
        <ContentModalize data={selectedHistoryItem} onPressCloseModal={onPressCloseModal} />
      </CustomModalize>
    </SafeWrapper>
  );
};

export const HealthRecordsScreen = memo(HealthRecordsComponent, isEqual);

const Item = memo((props: any) => {
  const { data, onPressDetail } = props;

  return (
    <Touchable style={styles.item} onPress={onPressDetail}>
      <Block style={styles.calendarBox}>
        <Text size="size40" fWeight="700">
          {dayjs().format('DD')}
        </Text>
        <Text size="size18" fWeight="500" mgTop={10}>
          {dayjs().format('MM/YYYY')}
        </Text>
      </Block>

      <Block justify="center" flex pdLeft={15}>
        <Block direction="row" align="center">
          <IconHospital2 />
          <Block pdLeft={9} flex>
            <Text fWeight="500">Khám Tiêu Chảy</Text>
            <Text mgTop={3}>Bác sỹ: Nguyễn Thị A</Text>
          </Block>
        </Block>
        <Block direction="row" align="center" mgTop={5}>
          <IconLocation2 color={Colors.COLOR_9} />
          <Block pdLeft={9} flex>
            <Text numberOfLines={2} fWeight="500">
              Nơi khám: Số 1 A nguyễn văn trỗi, Hà Đông Hà Nội
            </Text>
          </Block>
        </Block>
      </Block>
    </Touchable>
  );
});
