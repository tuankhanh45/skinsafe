import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: vScale(12),
    paddingHorizontal: scale(15),
  },
  list: {
    flex: 1,
    backgroundColor: Colors.WHITE,
    borderTopLeftRadius: moderateScale(10),
    borderTopRightRadius: moderateScale(10),
  },
  item: {
    flexDirection: 'row',
    marginBottom: vScale(10),
    paddingVertical: vScale(5),
    paddingHorizontal: scale(4),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(8),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  calendarBox: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scale(5),
    paddingVertical: vScale(23),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(8),

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default styles;
