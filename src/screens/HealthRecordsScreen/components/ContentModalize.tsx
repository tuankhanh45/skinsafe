import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { IconCancel } from '@assets';
import { Divider } from 'native-base';
import { Block, Text, Touchable } from '@components';

interface IContentModalizeProps {
  data: any;
  onPressCloseModal: () => void;
}

const ContentModalizeComponent: React.FC<IContentModalizeProps> = (props) => {
  const { data, onPressCloseModal } = props;
  return (
    <Block pdHorizontal={15} pdTop={6}>
      <Touchable pd={4} alignSelf="flex-end" onPress={onPressCloseModal}>
        <IconCancel />
      </Touchable>
      <Text fWeight="700" mgBottom={6}>
        Ngày khám: 20/12/2022
      </Text>
      <Text fWeight="700" mgBottom={8}>
        Bác sỹ: Nguyễn Văn A
      </Text>
      <Divider />

      <Text fWeight="700" mgBottom={8} mgTop={4}>
        Chuẩn đoán
      </Text>
      <Text size="size12">Viêm phổi do bị lạnh</Text>
      <Text fWeight="700" mgBottom={8} mgTop={17}>
        Phương án điều trị
      </Text>
      <Text size="size12">Cho uống thuốc ABCXYZ, giữ ấm cho Smart Power, tránh nơi có gió</Text>
    </Block>
  );
};

export const ContentModalize = memo(ContentModalizeComponent, isEqual);

const styles = StyleSheet.create({});
