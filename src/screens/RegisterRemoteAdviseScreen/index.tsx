import isEqual from 'react-fast-compare';
import { useForm } from 'react-hook-form';
import { Modalize } from 'react-native-modalize';
import React, { memo, useRef, useState } from 'react';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import * as Com from '@components';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { IconCancel, translate } from '@assets';
import { CustomModalize, Touchable } from '@components';
import { showToastError, useValidateForm } from '@utils';

const { Block, Button, Form, HeaderBar, ItemDropdown, NotificationBadgeBtn, Text, SafeWrapper, TextField } = Com;

interface IRegisterRemoteAdviseProps {
  route: any;
  navigation: any;
}

const LIST_SERVICES = [
  { label: 'Tư vấn bệnh cúm' },
  { label: 'Tư vấn bệnh dại' },
  { label: 'Tư vấn đỡ đẻ' },
  { label: 'Tư vấn bệnh hô hấp' },
  { label: 'Tư vấn bệnh ghẻ' },
  { label: 'Tư vấn bệnh nấm' },
];
const LIST_METHODS = [
  { label: 'Gọi video' },
  { label: 'Gọi điện thoại' },
  { label: 'Gọi zalo âm thanh' },
  { label: 'Gọi messenger' },
];

const RegisterRemoteAdviseComponent: React.FC<IRegisterRemoteAdviseProps> = (props) => {
  const { navigation } = props;
  const user = useAppStore((state) => state?.account?.user);
  const refModalMethod = useRef<Modalize>(null);
  const refModalService = useRef<Modalize>(null);

  const [selected, setSelected] = useState<any>({
    service: null,
    method: null,
  });

  const formMethods = useForm({
    defaultValues: {
      full_name: user?.fullname,
      phone: user?.phone,
    },
  });
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;

  const onErrors = (errors: any) => {};
  const onSubmit = (form: any) => {
    if (!selected.service) return showToastError(translate('register_remote_advise.required_service'));
    if (!selected.method) return showToastError(translate('register_remote_advise.required_method'));
    navigation.navigate(SCREENS.REMOTE_ADVISE);
  };

  const renderRight = () => <NotificationBadgeBtn />;

  const onSelect = (item: any, type: 'service' | 'method') => {
    if (type === 'service') {
      refModalService.current?.close();
      setSelected((prev: any) => ({ ...prev, service: item }));
    } else {
      refModalMethod.current?.close();
      setSelected((prev: any) => ({ ...prev, method: item }));
    }
  };

  const keyExtractor = (item: any, index: number) => item + index + '';
  const renderItem = ({ item, type }: any) => {
    return (
      <Touchable mgHorizontal={34} pdVertical={10} onPress={() => onSelect(item, type)}>
        <Text>{item.label}</Text>
      </Touchable>
    );
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('register_remote_advise.title')} renderRight={renderRight} />
      <KeyboardAwareScrollView>
        <Block mgTop={54} pdHorizontal={20}>
          <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
            <Text fWeight="700" mgTop={10}>
              {translate('register_remote_advise.full_name')}
              <Text color="COLOR_1">*</Text>
            </Text>
            <TextField
              name={'full_name'}
              returnKeyType={'done'}
              styleWrapInput={styles.input}
              placeholder={translate('placeholder.full_name')}
            />

            <Text fWeight="700" mgTop={10}>
              {translate('register_remote_advise.phone')}
              <Text color="COLOR_1">*</Text>
            </Text>
            <TextField
              name={'phone'}
              returnKeyType={'done'}
              placeholder={translate('placeholder.phone')}
              styleWrapInput={styles.input}
            />

            <Text fWeight="700" mgTop={10}>
              {translate('register_remote_advise.service')}
              <Text color="COLOR_1">*</Text>
            </Text>
            <ItemDropdown
              mgTop={0}
              style={styles.dropdown}
              value={selected?.service?.label}
              onPress={() => refModalService.current?.open()}
              placeholder={translate('register_remote_advise.dropdown_placeholder')}
            />

            <Text fWeight="700" mgTop={10}>
              {translate('register_remote_advise.method')}
              <Text color="COLOR_1">*</Text>
            </Text>
            <ItemDropdown
              mgTop={0}
              style={styles.dropdown}
              value={selected?.method?.label}
              onPress={() => refModalMethod.current?.open()}
              placeholder={translate('register_remote_advise.dropdown_placeholder')}
            />
          </Form>
        </Block>
      </KeyboardAwareScrollView>

      <Block pdHorizontal={10} pdBottom={8}>
        <Button onPress={formMethods.handleSubmit(onSubmit, onSubmit)}>
          {translate('register_remote_advise.submit')}
        </Button>
      </Block>

      <CustomModalize
        ref={refModalMethod}
        HeaderComponent={<HeaderComponent label={'method'} onPress={() => refModalMethod.current?.close()} />}
        flatListProps={{
          data: LIST_METHODS,
          keyExtractor: keyExtractor,
          renderItem: ({ item }) => renderItem({ item, type: 'method' }),
        }}
      />
      <CustomModalize
        ref={refModalService}
        HeaderComponent={<HeaderComponent label={'service'} onPress={() => refModalService.current?.close()} />}
        flatListProps={{
          data: LIST_SERVICES,
          keyExtractor: keyExtractor,
          renderItem: ({ item }) => renderItem({ item, type: 'service' }),
        }}
      />
    </SafeWrapper>
  );
};

export const RegisterRemoteAdviseScreen = memo(RegisterRemoteAdviseComponent, isEqual);

const HeaderComponent = memo(({ label, onPress }: any) => {
  return (
    <Block direction="row" pd={16} justify="space-between">
      <Text fWeight="700">{translate(`register_remote_advise.${label}`)}</Text>
      <Touchable onPress={onPress}>
        <IconCancel />
      </Touchable>
    </Block>
  );
});
