import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  homeBtn: {
    alignSelf: 'center',
    marginTop: vScale(35),
    paddingVertical: vScale(10),
    paddingHorizontal: scale(37),
    borderRadius: moderateScale(4),
    backgroundColor: Colors.COLOR_1,
  },
  contentContainer: {
    paddingVertical: vScale(16),
  },
  headerBox: {
    paddingVertical: vScale(10),
    borderColor: Colors.COLOR_9,
    paddingHorizontal: scale(15),
    borderTopWidth: moderateScale(1),
    borderBottomWidth: moderateScale(1),
  },
});

export default styles;
