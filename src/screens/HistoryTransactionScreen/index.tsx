import isEqual from 'react-fast-compare';
import React, { memo, useState } from 'react';

import styles from './styles';
import { SCREENS } from '@navigation';
import { RenderItemProps } from '@configs';
import { Block, CustomFlatlist, HeaderBar, HistoryItem, ListEmpty, SafeWrapper, Text, Touchable } from '@components';

interface IHistoryTransactionProps {
  route: any;
  navigation: any;
}

const HistoryTransactionComponent: React.FC<IHistoryTransactionProps> = (props) => {
  const { navigation } = props;

  const [listHistory, setListHistory] = useState<any[]>([1, 2]);

  const renderItem = ({ item, index }: RenderItemProps) => {
    return <HistoryItem data={item} />;
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title="Lịch sử giao dịch" />

      <CustomFlatlist
        data={listHistory}
        renderItem={renderItem}
        ListHeaderComponent={ListHeaderComponent}
        contentContainerStyle={styles.contentContainer}
        ListFooterComponent={<ListFooterComponent navigation={navigation} isShow={!listHistory.length} />}
        ListEmptyComponent={<ListEmpty title={'Bạn chưa có giao dịch nào'} />}
      />
    </SafeWrapper>
  );
};

export const HistoryTransactionScreen = memo(HistoryTransactionComponent, isEqual);

const ListFooterComponent = memo(({ navigation, isShow }: any) => {
  if (!isShow) return;

  const onPress = () => navigation.navigate(SCREENS.HOME_STACK);

  return (
    <Touchable style={styles.homeBtn} onPress={onPress}>
      <Text fWeight="500" color="WHITE">
        Đến danh mục mua sắm
      </Text>
    </Touchable>
  );
});

const ListHeaderComponent = () => {
  return (
    <Block style={styles.headerBox}>
      <Text>Lịch sử giao dịch</Text>
    </Block>
  );
};
