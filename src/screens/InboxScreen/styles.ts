import { vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  inputHeader: {
    height: vScale(29),
  },
});

export default styles;
