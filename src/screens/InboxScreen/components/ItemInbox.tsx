import dayjs from 'dayjs';
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, Text, Touchable } from '@components';

interface IItemInboxProps {
  data: any;
}
const url = 'https://picsum.photos/80';

const ItemInboxComponent: React.FC<IItemInboxProps> = (props) => {
  const { data } = props;

  return (
    <Touchable style={styles.wrapper}>
      <ImageRemote source={url + 1} style={styles.img} />
      <Block mgLeft={16} flex>
        <Block direction="row" flex>
          <Text style={styles.title} numberOfLines={1}>
            Smart Power Shop
          </Text>
          <Text style={styles.date}>{dayjs().format('DD-MM-YYYY')}</Text>
        </Block>

        <Block direction="row" flex align="center">
          <Text style={styles.content}>
            Bạn có tin nhắn liên quan đến vấn đề hủy hàng hoàn tiền phản hồi với shop ngay
          </Text>
          <Block style={styles.countNotRead}>
            <Text size={'size12'} color={'COLOR_4'}>
              2
            </Text>
          </Block>
        </Block>
      </Block>
    </Touchable>
  );
};

export const ItemInbox = memo(ItemInboxComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    padding: scale(12),
    flexDirection: 'row',
    borderColor: '#F1F1F1',
    borderWidth: moderateScale(1),
  },
  img: {
    width: scale(45),
    height: scale(45),
    borderColor: Colors.COLOR_2,
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(45),
  },
  title: {
    flex: 1,
    fontWeight: '700',
    lineHeight: moderateScale(17.09),
  },
  date: {
    color: Colors.TEXT_2,
    fontSize: FontSize.size8,
    lineHeight: moderateScale(10),
  },
  content: {
    flex: 1,
    marginTop: vScale(8),
    color: Colors.COLOR_15,
    fontSize: FontSize.size12,
  },
  countNotRead: {
    paddingVertical: vScale(2),
    paddingHorizontal: scale(6),
    backgroundColor: Colors.COLOR_1,
    borderRadius: moderateScale(100),
  },
});
