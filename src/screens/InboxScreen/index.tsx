import isEqual from 'react-fast-compare';
import React, { memo, useState } from 'react';

import styles from './styles';
import { ItemInbox } from './components';
import { RenderItemProps } from '@configs';
import { IconSearch, translate } from '@assets';
import { Block, CustomFlatlist, HeaderBar, Input, ListEmpty, SafeWrapper } from '@components';

interface IInboxProps {
  route: any;
  navigation: any;
}

const InboxComponent: React.FC<IInboxProps> = (props) => {
  const {} = props;

  const [data, setData] = useState<any[]>([1, 2, 3, 4, 5]);
  const [searchText, setSearchText] = useState<string>('');

  const renderItem = ({ item, index }: RenderItemProps) => {
    return <ItemInbox data={item} />;
  };

  const onChangeText = (e: string) => {
    setSearchText(e);
  };

  const ItemSeparatorComponent = () => <Block h={2} />;

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title="Inbox" iconLeft={null} />
      <CustomFlatlist
        data={data}
        renderItem={renderItem}
        ItemSeparatorComponent={ItemSeparatorComponent}
        ListEmptyComponent={<ListEmpty title={translate('inbox.no_data')} />}
        ListHeaderComponent={
          <ListHeaderComponent searchText={searchText} onChangeText={onChangeText} dataLength={data.length} />
        }
      />
    </SafeWrapper>
  );
};

export const InboxScreen = memo(InboxComponent, isEqual);

const ListHeaderComponent = memo((props: any) => {
  const { searchText, dataLength, onChangeText } = props;

  if (dataLength === 0) return null;
  return (
    <Block pdHorizontal={10} pdBottom={16}>
      <Input
        iconRight={IconSearch}
        onChangeText={onChangeText}
        styleWrapInput={styles.inputHeader}
        placeholder={translate('inbox.placeholder_input')}
      />
    </Block>
  );
});
