import { vScale, device } from '@utils';
import { StyleSheet } from 'react-native';

import { FontSize, Colors } from '@themes';
import { moderateScale, scale } from '@src/utils';
const styles = StyleSheet.create({
  wrapBox: {
    height: scale(30),
    borderRadius: scale(6),
  },
  dropdown: {
    borderRadius: moderateScale(5),
    width: scale(165),
    height: scale(30),
    backgroundColor: Colors.COLOR_12,
    borderWidth: 0,
  },
  imgAvatar: {
    width: scale(165),
    height: vScale(165),
  },
  imgRemote: {
    width: device.width - scale(30),
    height: vScale(327),
    borderRadius: moderateScale(8),
    overflow: 'hidden',
  },
  imgUp: {
    width: scale(18),
    height: vScale(18),
  },
});

export default styles;
