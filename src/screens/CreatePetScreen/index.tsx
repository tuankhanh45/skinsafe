import dayjs from 'dayjs';
import size from 'lodash/size';
import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import { useForm } from 'react-hook-form';
import React, { memo, useRef, useState } from 'react';
import { ImageLibraryOptions, launchImageLibrary } from 'react-native-image-picker';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { Colors } from '@themes';
import * as Com from '@components';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { IconCalender, IconCancel, translate } from '@assets';
import { scale, showToastError, showToastSuccess, useValidateForm } from '@utils';
import { handleResponse, ICreatePetState, requestGetSpecies, requestCreatePet, requestUpdatePet } from '@configs';

const {
  Form,
  Text,
  Image,
  Block,
  Button,
  Loading,
  Touchable,
  HeaderBar,
  TextField,
  SafeWrapper,
  ImageRemote,
  ItemDropdown,
  CustomModalize,
  ModalSelectDate,
} = Com;

const LIST_GENDER = [
  { id: 1, name: 'Đực' },
  { id: 2, name: 'Cái' },
  { id: 3, name: 'Khác' },
];

const optionsImage: ImageLibraryOptions = {
  maxWidth: 512,
  maxHeight: 512,
  selectionLimit: 1,
  includeExtra: true,
  mediaType: 'photo',
  includeBase64: true,
  presentationStyle: 'pageSheet',
};

interface ICreatePetProps {
  navigation: any;
  route: any;
}

// name  species_id age sex weight description avatar
const CreatePetComponent: React.FC<ICreatePetProps> = (props) => {
  const { navigation, route } = props;
  const update = route?.params?.type === SCREENS.EDIT_PET;

  const listPet = useAppStore((state: ICreatePetState) => state.pet.listPet);
  const species = useAppStore((state: ICreatePetState) => state.pet.species);
  const detailsPet = useAppStore((state: ICreatePetState) => state.pet.detailsPet);
  const actUpdatePet = useAppStore((state: ICreatePetState) => state.actUpdatePet);
  const actSaveListPet = useAppStore((state: ICreatePetState) => state.actSaveListPet);
  const actSaveSpeciesPet = useAppStore((state: ICreatePetState) => state.actSaveSpeciesPet);

  const refModalDate = useRef<any>();
  const refModalGender = useRef<any>();
  const refModalSpecies = useRef<any>();

  const [date, setDate] = useState<any>(null);
  const [selectedImg, setSelectedImg] = useState<any>(null);
  const [selected, setSelected] = useState<any>({
    gender: null,
    species: null,
  });

  // form
  const formMethods = useForm();
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;

  const choseAvatar = () => {
    try {
      launchImageLibrary(optionsImage, (res: any) => {
        if (res.didCancel) {
        } else if (res.error) {
        } else if (res.customButton) {
        } else {
          try {
            setSelectedImg(res?.assets[0]);
          } catch (error) {}
        }
      });
    } catch (err) {}
  };

  const onErrors = (errors: any) => {};
  const onSubmit = (form: any) => {
    const formData = new FormData();
    formData.append('name', form?.pet_name);
    formData.append('weight', form?.pet_weight ?? '');
    formData.append('description', form?.description ?? '');
    formData.append('species_id', selected?.species?.id ?? '');
    formData.append('birthday', date ? dayjs(date).format('YYYY-MM-DD') : null);
    formData.append('sex', selected?.gender?.id ?? '');
    if (selectedImg) {
      formData.append('avatar', selectedImg);
    }
    if (update && !selectedImg) {
      formData.append('avatar_current', detailsPet?.avatar);
    }
    if (update) return runUpdate({ id: detailsPet.id, formData });
    return runCreate({ formData });
  };

  // get species pet
  const { run: runGetSpecies, loading: loadingSpecies } = useRequest(requestGetSpecies, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (Array.isArray(res)) {
        actSaveSpeciesPet(res);
      }
    },
  });
  // update pet
  const { run: runUpdate, loading: loadingUpdate } = useRequest(requestUpdatePet, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (!isError) {
        // update list
        actUpdatePet(res);
        showToastSuccess('Cập nhật thông tin thành công');
      } else {
        showToastError('Cập nhật thông tin thất bại');
      }
    },
  });
  // create pet
  const { run: runCreate, loading: loadingCreate } = useRequest(requestCreatePet, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (!isError) {
        // push new data to list
        let arr = listPet;
        arr = [...arr, res];
        actSaveListPet(arr);
        navigation.goBack();
      }
    },
  });

  //
  React.useEffect(() => {
    runGetSpecies();
    if (update) {
      formMethods.setValue('pet_name', detailsPet?.name);
      formMethods.setValue('pet_weight', detailsPet?.weight);
      formMethods.setValue('description', detailsPet?.description);
      setSelected({
        gender: { id: detailsPet?.sex, name: detailsPet?.sex === 1 ? 'Đực' : detailsPet?.sex === 2 ? 'Cái' : 'Khác' },
        species: detailsPet?.species,
      });
      setDate(detailsPet?.birthday ? dayjs(detailsPet?.birthday) : null);
    }
  }, []);

  const onSelect = (item: any, type: 'gender' | 'species') => {
    if (type === 'gender') {
      refModalGender.current?.close();
      setSelected((prev: any) => ({ ...prev, gender: item }));
    } else {
      refModalSpecies.current?.close();
      setSelected((prev: any) => ({ ...prev, species: item }));
    }
  };

  const keyExtractor = (item: any, index: number) => item + index + '';
  const renderItem = ({ item, type }: any) => {
    return (
      <Touchable mgHorizontal={34} pdVertical={10} onPress={() => onSelect(item, type)}>
        <Text>{item?.name}</Text>
      </Touchable>
    );
  };

  // avatar
  const renderAvatar = () => {
    const imgSource = selectedImg ? selectedImg.uri : update ? detailsPet?.avatar : null;
    return (
      <Block flex>
        {imgSource ? (
          <Block>
            <Touchable mgBottom={6} mgTop={10} direction="row" align="center" onPress={choseAvatar}>
              <Text
                text={translate('pet.add_img')}
                fFamily={'MEDIUM'}
                size="size18"
                fWeight="500"
                color="TEXT"
                mgRight={3.5}
              />
              <Image source="imgAddImage" style={styles.imgUp} />
            </Touchable>
            <ImageRemote source={imgSource} style={styles.imgRemote} resizeMode="cover" />
          </Block>
        ) : (
          <Block
            h={327}
            w={'100%'}
            bgColor={Colors.COLOR_12}
            borderRadius={8}
            mgTop={16}
            justify="center"
            align="center">
            <Touchable onPress={choseAvatar}>
              <Image source="imgUpload" style={styles.imgAvatar} />
              <Text
                text={translate('pet.up')}
                fFamily={'MEDIUM'}
                size="size18"
                fWeight="500"
                color="TEXT_2"
                textAlign="center"
                mgTop={16}
              />
            </Touchable>
          </Block>
        )}
      </Block>
    );
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      {(loadingSpecies || loadingCreate || loadingUpdate) && <Loading />}
      <HeaderBar title={translate('pet.title')} />
      <KeyboardAwareScrollView>
        <Block pdHorizontal={14} mgTop={16}>
          <Text text={translate(update ? 'pet.update' : 'pet.create')} fFamily={'BRANDING'} size="size15" />
          <Block mgTop={26}>
            <Text text={translate('pet.info')} fFamily={'MEDIUM'} size="size14" fWeight="500" />
            {/* name */}
            <Text text={translate('pet.name')} fFamily={'MEDIUM'} size="size14" mgTop={10} mgBottom={-8} />
            <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
              <TextField
                name={'pet_name'}
                returnKeyType={'done'}
                placeholder={translate('placeholder.fill')}
                styleWrapInput={styles.wrapBox}
              />
            </Form>
            <Block direction="row" justify="space-between">
              {/* date */}
              <Block>
                <Text text={translate('pet.date')} fFamily={'MEDIUM'} size="size14" mgTop={10} mgBottom={-8} />
                <ItemDropdown
                  mgTop={0}
                  onPress={() => refModalDate.current?.open()}
                  style={styles.dropdown}
                  icon={<IconCalender color={Colors.COLOR_9} />}
                  value={date ? dayjs(date).format('DD/MM/YYYY') : undefined}
                />
              </Block>
              {/* gender */}
              <Block>
                <Text text={translate('pet.gender')} fFamily={'MEDIUM'} size="size14" mgTop={10} mgBottom={-8} />
                <ItemDropdown
                  mgTop={0}
                  onPress={() => refModalGender.current?.open()}
                  style={styles.dropdown}
                  value={selected?.gender?.name}
                />
              </Block>
            </Block>
            <Block direction="row" justify="space-between">
              {/* spec */}
              <Block>
                <Text text={translate('pet.spe')} fFamily={'MEDIUM'} size="size14" mgTop={10} mgBottom={-8} />
                <ItemDropdown
                  mgTop={0}
                  onPress={() => refModalSpecies.current?.open()}
                  style={styles.dropdown}
                  value={selected?.species?.name}
                />
              </Block>
              {/* weight */}
              <Block>
                <Text text={translate('pet.weight')} fFamily={'MEDIUM'} size="size14" mgTop={10} mgBottom={-10} />
                <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
                  <TextField
                    name={'pet_weight'}
                    returnKeyType={'done'}
                    placeholder={translate('placeholder.fill')}
                    styleWrapInput={[styles.wrapBox, { width: scale(165) }]}
                    keyboardType="decimal-pad"
                  />
                </Form>
              </Block>
            </Block>
            <Text text={translate('pet.img')} fFamily={'BRANDING'} size="size15" mgTop={16} />
            {renderAvatar()}
            <Text text={translate('pet.note')} fFamily={'BRANDING'} size="size15" mgTop={16} color={'COLOR_1'} />
            <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
              <TextField
                name={'description'}
                returnKeyType={'done'}
                placeholder={translate('pet.note_des')}
                styleWrapInput={[styles.wrapBox, { height: scale(123) }]}
                keyboardType="decimal-pad"
                multiline
              />
            </Form>
            <Button
              isLoading={loadingCreate || loadingUpdate}
              marginTop={scale(46)}
              disabled={!!size(errors) || loadingCreate || loadingUpdate}
              onPress={formMethods.handleSubmit(onSubmit, onErrors)}>
              {translate(update ? 'pet.save' : 'pet.add')}
            </Button>
          </Block>
        </Block>
      </KeyboardAwareScrollView>
      <ModalSelectDate ref={refModalDate} setSelectedDate={setDate} maxDate={new Date()} />
      <CustomModalize
        ref={refModalGender}
        HeaderComponent={<HeaderComponent label={'pick_gender'} onPress={() => refModalGender.current?.close()} />}
        flatListProps={{
          data: LIST_GENDER,
          keyExtractor: keyExtractor,
          renderItem: ({ item }) => renderItem({ item, type: 'gender' }),
        }}
      />
      <CustomModalize
        ref={refModalSpecies}
        HeaderComponent={<HeaderComponent label={'pick_spe'} onPress={() => refModalSpecies.current?.close()} />}
        flatListProps={{
          data: species,
          keyExtractor: keyExtractor,
          renderItem: ({ item }) => renderItem({ item, type: 'species' }),
        }}
      />
    </SafeWrapper>
  );
};

export const CreatePetScreen = memo(CreatePetComponent, isEqual);
const HeaderComponent = memo(({ label, onPress }: any) => {
  return (
    <Block direction="row" pd={16} justify="space-between">
      <Text fWeight="700">{translate(`pet.${label}`)}</Text>
      <Touchable onPress={onPress}>
        <IconCancel />
      </Touchable>
    </Block>
  );
});
