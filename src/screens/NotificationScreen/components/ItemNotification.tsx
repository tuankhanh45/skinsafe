import dayjs from 'dayjs';
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { SCREENS } from '@navigation';
import { Colors, FontSize } from '@themes';
import { Block, Text, Touchable } from '@components';
import { moderateScale, scale, vScale } from '@utils';

interface IItemNotificationProps {
  data: any;
  isFirstItem: boolean;
  onPressDetail: () => void;
}

const ItemNotificationComponent: React.FC<IItemNotificationProps> = (props) => {
  const { data, isFirstItem, onPressDetail } = props;

  return (
    <Touchable style={[styles.wrapper, isFirstItem && { borderTopWidth: moderateScale(1) }]} onPress={onPressDetail}>
      {!data?.is_read ? <Block style={styles.dotNotRead} /> : <Block w={23} />}
      <Block pdRight={13}>
        <Text style={styles.title} numberOfLines={1}>
          {data?.title}
        </Text>
        <Text style={styles.date}>{dayjs(data?.created_at).format('DD-MM-YYYY HH:mm')}</Text>
        <Text numberOfLines={3} style={styles.content}>
          {data?.content}
        </Text>
      </Block>
    </Touchable>
  );
};

export const ItemNotification = memo(ItemNotificationComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    padding: scale(20),
    flexDirection: 'row',
    borderColor: '#F1F1F1',
    paddingTop: vScale(16),
    borderBottomWidth: moderateScale(1),
  },
  dotNotRead: {
    width: scale(9),
    height: scale(9),
    marginTop: vScale(3),
    marginRight: scale(14),
    backgroundColor: Colors.COLOR_3,
    borderRadius: moderateScale(100),
  },
  title: {
    fontWeight: '700',
    color: Colors.COLOR_3,
    fontSize: FontSize.size12,
    lineHeight: moderateScale(14.65),
  },
  date: {
    fontWeight: '500',
    color: Colors.TEXT,
    marginVertical: vScale(6),
    fontSize: FontSize.size12,
    lineHeight: moderateScale(15),
  },
  content: {
    color: Colors.TEXT,
    fontSize: FontSize.size12,
    lineHeight: moderateScale(13.3),
  },
});
