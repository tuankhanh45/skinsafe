import { StyleSheet } from 'react-native';

import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerWrapper: {
    padding: scale(16),
    flexDirection: 'row',
    alignItems: 'center',
  },
  filterTitle: {
    fontWeight: '700',
    lineHeight: moderateScale(17.09),
  },
  filterBtn: {
    flexDirection: 'row',
    marginLeft: scale(22),
    alignItems: 'center',
    paddingVertical: vScale(8),
    paddingHorizontal: scale(40),
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(4),
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  filterValue: {
    fontWeight: '500',
    color: Colors.TEXT,
    marginRight: scale(25),
    fontSize: FontSize.size12,
    lineHeight: moderateScale(15),
  },
  contentContainer: {
    paddingBottom: vScale(20),
  },
});

export default styles;
