import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import React, { memo, useEffect, useState } from 'react';

import styles from './styles';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { ItemNotification } from './components';
import { IconArrowDown, translate } from '@assets';
import { handleResponse, RenderItemProps, requestGetListNotification } from '@configs';
import { Block, CustomFlatlist, HeaderBar, ListEmpty, SafeWrapper, Text, Touchable } from '@components';

interface INotificationProps {
  route: any;
  navigation: any;
}

const NotificationComponent: React.FC<INotificationProps> = (props) => {
  const { navigation } = props;

  const user = useAppStore((state) => state?.account?.user);

  const [page, setPage] = useState<number>(1);
  const [listNoti, setListNoti] = useState<any[]>([]);
  const [loadMore, setLoadMore] = useState<boolean>(true);
  const [refreshing, setRefreshing] = useState<boolean>(false);

  useEffect(() => {
    runGetListNoti({ user_id: user?.id, page });
  }, [page]);

  const onEndReached = () => {
    if (loadMore && !loadingGetListNoti) {
      setPage((prev) => prev + 1);
    }
  };
  const onRefresh = () => {
    setLoadMore(true);
    setRefreshing(true);
    if (page === 1) {
      runGetListNoti({ user_id: user?.id, page: 1 });
    } else {
      setPage(1);
    }
  };

  const { run: runGetListNoti, loading: loadingGetListNoti } = useRequest(requestGetListNotification, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (Array.isArray(res?.data)) {
        if (res?.next_page_url) {
          setLoadMore(false);
        }
        if (res?.current_page === 1) {
          setListNoti(res?.data);
        } else {
          setListNoti((prev) => [...prev, ...res?.data]);
        }
      }
    },
    onFinally: () => refreshing && setRefreshing(false),
  });

  const onPressDetail = (data: any) => {
    const index: number = listNoti.findIndex((x) => x.id === data?.id);
    const isRead = listNoti[index]?.is_read;
    if (!isRead) {
      let tempData = [...listNoti];
      tempData[index].is_read = 1;
      setListNoti(tempData);
    }
    navigation.navigate(SCREENS.DETAIL_NOTIFICATION, { data });
  };

  const renderItem = ({ item, index }: RenderItemProps) => {
    const isFirstItem = index === 0;
    return <ItemNotification data={item} isFirstItem={isFirstItem} onPressDetail={() => onPressDetail(item)} />;
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('notification.title')} />
      <CustomFlatlist
        data={listNoti}
        onRefresh={onRefresh}
        renderItem={renderItem}
        refreshing={refreshing}
        onEndReached={onEndReached}
        ListEmptyComponent={ListEmpty}
        ListHeaderComponent={ListHeaderComponent}
        contentContainerStyle={styles.contentContainer}
      />
    </SafeWrapper>
  );
};

export const NotificationScreen = memo(NotificationComponent, isEqual);

const ListHeaderComponent = memo(() => {
  return (
    <Block style={styles.headerWrapper}>
      <Text style={styles.filterTitle}>{translate('notification.filter')}</Text>
      <Touchable style={styles.filterBtn}>
        <Text style={styles.filterValue}>Tất cả</Text>
        <IconArrowDown />
      </Touchable>
    </Block>
  );
});
