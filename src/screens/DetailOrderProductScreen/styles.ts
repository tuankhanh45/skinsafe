import { vScale } from '@utils';
import { Colors } from '@themes';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  divider: {
    height: vScale(5),
    backgroundColor: '#F4F3F9',
  },
});

export default styles;
