import React, { memo } from 'react';
import { Divider } from 'native-base';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { translate } from '@assets';
import { Block, Text } from '@components';
import { currencyFormat, scale, vScale } from '@utils';

interface ITotalOverviewProps {
  data: any;
}

const TotalOverviewComponent: React.FC<ITotalOverviewProps> = (props) => {
  const { data } = props;

  return (
    <Block style={styles.wrapper}>
      <Block direction="row" justify="space-between">
        <Text color="TEXT_2">{translate('detail_order_product.provisional')}</Text>
        <Text fWeight="500">{currencyFormat(data?.cart?.total_price)}</Text>
      </Block>
      <Divider marginTop={vScale(11)} marginBottom={vScale(11)} />

      <Block direction="row" justify="space-between">
        <Text color="TEXT_2">{translate('detail_order_product.discount')}</Text>
        <Text fWeight="500">{currencyFormat(data?.cart?.total_price_discount)}</Text>
      </Block>
      <Divider marginTop={vScale(11)} marginBottom={vScale(11)} />

      <Block direction="row" justify="space-between">
        <Text fWeight="500">{translate('detail_order_product.total')}</Text>
        <Block align="flex-end">
          <Text fWeight="700" size="size18">
            {currencyFormat(data?.cart?.price_final)}
          </Text>
          <Text fWeight="500" mgTop={13}>
            {translate('detail_order_product.include_vat')}
          </Text>
        </Block>
      </Block>
    </Block>
  );
};

export const TotalOverview = memo(TotalOverviewComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    paddingTop: vScale(20),
    paddingBottom: vScale(18),
    paddingHorizontal: scale(15),
  },
});
