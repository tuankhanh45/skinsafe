import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Block, Text } from '@components';
import { IconShip, translate } from '@assets';

interface IShipProps {
  shipInfo: any;
}

const ShipComponent: React.FC<IShipProps> = (props) => {
  const { shipInfo } = props;

  return (
    <Block pdHorizontal={15} pdVertical={20} direction="row">
      <IconShip />
      <Block flex pdLeft={7}>
        <Text fWeight="700">{translate('detail_order_product.ship_method')}</Text>
        <Text color="TEXT_2" mgTop={8}>
          {shipInfo?.name}
        </Text>
      </Block>
    </Block>
  );
};

export const Ship = memo(ShipComponent, isEqual);

const styles = StyleSheet.create({});
