import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { translate } from '@assets';
import { scale, vScale } from '@utils';
import { Block, Image, Text } from '@components';

interface IAddressProps {
  data: any;
}

const AddressComponent: React.FC<IAddressProps> = (props) => {
  const { data } = props;

  return (
    <Block style={styles.wrapper}>
      <Image source={'ic_location_pet'} style={styles.icon} />
      <Block mgLeft={8} pdRight={24}>
        <Text fWeight="500" size="size16">
          {translate('detail_order_product.address')}
        </Text>
        <Text fWeight="500" mgTop={18}>
          {data?.name}
        </Text>
        <Text color="TEXT_2" mgTop={13}>
          {data?.phone}
        </Text>
        <Text color="TEXT_2" mgTop={13}>
          {data?.address_full}
        </Text>
      </Block>
    </Block>
  );
};

export const Address = memo(AddressComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    padding: scale(15),
    flexDirection: 'row',
  },
  icon: {
    width: scale(17),
    height: vScale(24),
  },
});
