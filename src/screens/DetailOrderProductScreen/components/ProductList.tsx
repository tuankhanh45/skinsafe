import React, { memo } from 'react';
import { Divider } from 'native-base';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { IconHome2, translate } from '@assets';
import { Block, ImageRemote, Text } from '@components';
import { currencyFormat, moderateScale, scale, vScale } from '@utils';

interface IProductListProps {
  data: any;
}

const ProductListComponent: React.FC<IProductListProps> = (props) => {
  const { data } = props;

  return (
    <Block>
      <Block pdVertical={13} pdHorizontal={16} direction="row" align="center">
        <IconHome2 />
        <Text fWeight="500" size="size16" mgLeft={7}>
          {translate('detail_order_product.title')}
        </Text>
      </Block>
      <Divider />

      {data?.cart?.cart_items?.map((e: any, i: number) => {
        return (
          <Block key={i} style={[styles.item, i === data?.cart?.cart_items.length - 1 && { borderBottomWidth: 0 }]}>
            <ImageRemote source={e?.product?.avatar} style={styles.avatar} resizeMode="cover" />
            <Block flex pdHorizontal={20}>
              <Text fWeight="500">{e?.product?.product_detail?.title}</Text>
              <Text color="COLOR_1">{currencyFormat(e?.price)}</Text>
              <Block direction="row" justify="space-between" mgTop={5}>
                <Text fWeight="500">x{e?.quantity}</Text>
                <Text fWeight="500" color="COLOR_1">
                  {currencyFormat(e?.price_final)}
                </Text>
              </Block>
            </Block>
          </Block>
        );
      })}
    </Block>
  );
};

export const ProductList = memo(ProductListComponent, isEqual);

const styles = StyleSheet.create({
  avatar: {
    width: scale(43),
    height: scale(43),
    borderRadius: moderateScale(4),
  },
  item: {
    flexDirection: 'row',
    paddingVertical: vScale(13),
    borderColor: Colors.COLOR_9,
    paddingHorizontal: scale(10),
    borderBottomWidth: moderateScale(0.5),
  },
});
