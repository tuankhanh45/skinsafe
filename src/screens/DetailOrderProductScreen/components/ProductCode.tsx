import dayjs from 'dayjs';
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import Clipboard from '@react-native-clipboard/clipboard';

import { Block, Text, Touchable } from '@components';
import { IconCopy, IconNote, translate } from '@assets';
import { scale, showToastError, showToastSuccess } from '@utils';

interface IProductCodeProps {
  data: any;
}

const ProductCodeComponent: React.FC<IProductCodeProps> = (props) => {
  const { data } = props;

  const onPressCopy = () => {
    if (data?.code_order) {
      Clipboard.setString(data?.code_order);
      showToastSuccess(translate('cm.copy_success'));
    } else {
      showToastError(translate('cm.copy_fail'));
    }
  };

  return (
    <Block style={styles.wrapper}>
      <IconNote />
      <Block mgLeft={8}>
        <Touchable direction="row" onPress={onPressCopy}>
          <Text fWeight="500" size="size16">
            {translate('detail_order_product.code_order')}: {data?.code_order}
          </Text>
          <IconCopy />
        </Touchable>
        <Text color="TEXT_2" mgTop={20}>
          {translate('detail_order_product.date_order')}: {dayjs().format('HH:mm, DD/MM/YYYY')}
        </Text>

        <Text mgTop={16} fWeight="500">
          {data?.status_order?.name}
        </Text>
      </Block>
    </Block>
  );
};

export const ProductCode = memo(ProductCodeComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    padding: scale(15),
    flexDirection: 'row',
  },
});
