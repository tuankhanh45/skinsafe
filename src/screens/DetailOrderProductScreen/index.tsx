import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import { ScrollView } from 'react-native';
import React, { memo, useEffect, useState } from 'react';

import styles from './styles';
import { translate } from '@assets';
import { handleResponse, requestGetDetailOrder } from '@configs';
import { Block, CartBadgeBtn, HeaderBar, Loading, SafeWrapper } from '@components';
import { Address, ProductCode, ProductList, Ship, TotalOverview } from './components';

interface IDetailOrderProductProps {
  route: any;
  navigation: any;
}

const DetailOrderProductComponent: React.FC<IDetailOrderProductProps> = (props) => {
  const { route } = props;
  const data = route?.params?.data;

  const [detailOrder, setDetailOrder] = useState<any>(null);

  useEffect(() => {
    runGetDetail({ id: data?.id });
  }, []);

  const { run: runGetDetail, loading: loadingGetDetail } = useRequest(requestGetDetailOrder, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      setDetailOrder(res);
    },
  });

  const renderRight = () => <CartBadgeBtn />;

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('detail_order_product.title')} renderRight={renderRight} />
      {loadingGetDetail && <Loading />}
      <ScrollView>
        <ProductCode data={detailOrder} />
        <Block style={styles.divider} />

        <Address data={detailOrder} />
        <Block style={styles.divider} />

        <ProductList data={detailOrder} />
        <Block style={styles.divider} />

        <Ship shipInfo={detailOrder?.cart?.ship_method} />
        <Block style={styles.divider} />

        <TotalOverview data={detailOrder} />
        <Block style={styles.divider} />
      </ScrollView>
    </SafeWrapper>
  );
};

export const DetailOrderProductScreen = memo(DetailOrderProductComponent, isEqual);
