import isEqual from 'react-fast-compare';
import React, { memo, useState } from 'react';
import ProgressCircle from 'react-native-progress-circle';
import { TabView, TabBar } from 'react-native-tab-view';

import { Block, Text, Touchable, SafeWrapper, HeaderBar, Image } from '@components';
import { Dimensions, ScrollView, View } from 'react-native';
import { Colors } from '@themes';

const { width } = Dimensions.get('window');
const imageHeight = (4 / 6) * width;

interface IProps {
  route: any;
  navigation: any;
}
const longText = `The skin is the largest organ of the human body, serving as a protective barrier between the internal organs and the external environment. It plays a crucial role in regulating body temperature, preventing dehydration, and defending against harmful pathogens. The skin consists of multiple layers, each with its own specialized functions.
The outermost layer, called the epidermis, provides waterproofing and serves as a barrier to infection. Beneath the epidermis lies the dermis, which contains hair follicles, sweat glands, and blood vessels. The dermis is responsible for providing strength and elasticity to the skin.
The skin also contains melanocytes, which produce melanin, the pigment that gives skin its color and helps protect against UV radiation from the sun. Additionally, the skin serves as a sensory organ, with millions of nerve endings that detect touch, pressure, temperature, and pain.
Maintaining healthy skin is essential for overall well-being. Proper skincare practices, including regular cleansing, moisturizing, and sun protection, can help keep the skin hydrated, balanced, and youthful. Additionally, a nutritious diet rich in vitamins and antioxidants can support skin health from the inside out.`;
const DetailsCareComponent: React.FC<IProps> = (props) => {
  const { navigation, route } = props;
  const item = route?.params?.item;
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'meaning', title: 'Meaning' },
    { key: 'causes', title: 'Causes' },
    { key: 'symptoms', title: 'Symptoms' },
    { key: 'treatment', title: 'Treatment' },
  ]);
  const initialLayout = { width: Dimensions.get('window').width };

  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: Colors.COLOR_1 }}
      style={{ backgroundColor: Colors.COLOR_12 }}
      renderLabel={({ route, focused, color }) => (
        <Text style={{ color: focused ? Colors.COLOR_1 : Colors.COLOR_15, fontSize: 14 }}>{route.title}</Text>
      )}
    />
  );

  const renderScene = ({ route }: any) => {
    switch (route.key) {
      case 'meaning':
        return (
          <Block pd={16}>
            <Text style={{ fontSize: 15 }}>{longText}</Text>
          </Block>
        );
      case 'causes':
        return (
          <Block pd={16}>
            <Text style={{ fontSize: 15 }}>{longText}</Text>
          </Block>
        );
      case 'symptoms':
        return (
          <Block pd={16}>
            <Text style={{ fontSize: 15 }}>{longText}</Text>
          </Block>
        );
      case 'treatment':
        return (
          <Block pd={16}>
            <Text style={{ fontSize: 15 }}>{longText}</Text>
          </Block>
        );
      default:
        return null;
    }
  };
  const Content = (content?: any) => (
    <Block flex bgColor="red">
      <Text>{content}</Text>
    </Block>
  );
  // const ImageSwiper = () => {
  //   return (
  //     <Swiper autoplay paginationStyle={{ bottom: 20 }} dotColor={Colors.TEXT_2} activeDotColor={Colors.COLOR_2}>
  //       {listBanner.map((e: any, i: number) => {
  //         return (
  //           <Block key={i}>
  //             <ImageRemote source={'https://picsum.photos/500'} style={styles.bannerImg} resizeMode={'cover'} />
  //           </Block>
  //         );
  //       })}
  //     </Swiper>
  //   );
  // };
  return (
    <SafeWrapper bgColor="COLOR_2">
      <HeaderBar bgColor="COLOR_2" title={'Result'} styleTitle={{ fontWeight: '700' }} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Block flex mgTop={16}>
          {/* progress */}
          <Block pd={12} pdVertical={8} direction="row" align="center">
            <ProgressCircle
              percent={item?.best ? 100 : 0}
              radius={25}
              borderWidth={5}
              color={item?.best ? '#4dff4d' : '#3399FF'}
              shadowColor="#f0f4f0"
              bgColor="#fff">
              <Text style={{ fontSize: 14, color: Colors.COLOR_17, fontWeight: '700' }}>{item?.best ? 'A' : 'N'}</Text>
            </ProgressCircle>
            <Block mgLeft={16} flex>
              <Text style={{ fontSize: 16, color: item?.best ? Colors.COLOR_17 : Colors.BLACK, fontWeight: '700' }}>{item?.title}</Text>
              <Text style={{ fontSize: 14, marginTop: 8, color: Colors.TEXT_2 }}>{item?.des}</Text>
            </Block>
          </Block>
          <Block flex h={0.5} bgColor={Colors.COLOR_9} />
          {/* read more */}
          {/* <Block mgHorizontal={16} mgTop={12}>
            <ReadMoreText text={longText} maxChars={150} />
          </Block> */}
          <Block align="center" mgTop={16} mgBottom={24}>
            <Touchable
              align="center"
              justify="center"
              pdVertical={8}
              direction="row"
              pdHorizontal={12}
              style={{ backgroundColor: Colors.COLOR_6, borderRadius: 20 }}>
              <Text style={{ fontWeight: '700', fontSize: 15, color: '#fff', textAlign: 'center' }}>{`Connect to closed \n SkinSafe Providers`}</Text>
              <Image source="icNextDoor" style={{ width: 20, height: 20, tintColor: '#fff', marginLeft: 16 }} />
            </Touchable>
          </Block>
        </Block>
        <Block flex h={800}>
          <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            renderTabBar={renderTabBar}
          />
        </Block>
      </ScrollView>
    </SafeWrapper>
  );
};

export const DetailsCareScreen = memo(DetailsCareComponent, isEqual);

const ReadMoreText = ({ text, maxChars = 100 }) => {
  const [showAll, setShowAll] = useState(false);
  const displayText = showAll ? text : text.slice(0, maxChars) + '...';

  return (
    <View>
      <Text>{displayText}</Text>
      {text.length > maxChars && (
        <Touchable onPress={() => setShowAll(!showAll)}>
          <Text style={{ color: 'blue' }}>{showAll ? 'Show less' : 'Read more'}</Text>
        </Touchable>
      )}
    </View>
  );
};
