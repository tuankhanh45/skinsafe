import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, Text, Touchable } from '@components';
import { useNavigation } from '@react-navigation/native';

interface IExampleProps {}

const ExampleComponent: React.FC<IExampleProps> = (props) => {
  const navigation = useNavigation<any>();
  return <Block style={styles.wrapper}></Block>;
};

export const Example = memo(ExampleComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  txt: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size16, fontWeight: '400' },
});
