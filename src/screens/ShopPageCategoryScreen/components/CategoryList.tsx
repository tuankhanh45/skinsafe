import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import chunk from 'lodash/chunk';
import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, Touchable, Text } from '@components';
import { useNavigation } from '@react-navigation/native';
import Swiper from 'react-native-swiper';

interface ICategoryListProps {}
const url = 'https://picsum.photos/80';
const IMG_SIZE = scale(75);
const list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
const CategoryListComponent: React.FC<ICategoryListProps> = (props) => {
  const navigation = useNavigation<any>();
  const onPressItem = (e: any) => {
    navigation.navigate(SCREENS.SHOPE_PAGE_CATEGORY_GENERAL);
  };
  return (
    <Block style={styles.wrapper}>
      <Text mgTop={16} size={'size15'} fFamily={'BRANDING'} mgLeft={15}>
        {translate(`shop_page.category`)}
      </Text>
      <Block h={236} mgTop={16}>
        <Swiper height={vScale(236)} dotColor={'#B2BAC7'} paginationStyle={styles.dot} activeDotColor={'#001934'}>
          {chunk(list, 8).map((parent, index) => (
            <Block direction="row" key={index + 'parent'} style={{ flexWrap: 'wrap' }}>
              {parent.map((e, i) => {
                return (
                  <Touchable key={i + 'chid'} mgBottom={10} onPress={() => onPressItem(e)} mgLeft={15}>
                    <ImageRemote source={url + i} style={styles.img} resizeMode={'cover'} />
                    <Text mgTop={4} size={'size12'} fWeight="600" textAlign="center">
                      Chó
                    </Text>
                  </Touchable>
                );
              })}
            </Block>
          ))}
        </Swiper>
      </Block>
    </Block>
  );
};

export const CategoryList = memo(CategoryListComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  txt: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size16, fontWeight: '400' },
  dot: { bottom: 0 },
  img: { height: IMG_SIZE, width: IMG_SIZE, borderRadius: scale(5) },
});
