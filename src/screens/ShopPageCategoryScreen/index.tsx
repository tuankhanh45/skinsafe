import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import chunk from 'lodash/chunk';
import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, CustomFlatlist, Text, ListEmpty, ItemProduct, SafeWrapper } from '@components';
import { useNavigation } from '@react-navigation/native';
import { RenderItemProps } from '@configs';
import { Header } from '../ShopPageScreen/components';
import styles from './styles';
import { Banner, CategoryList } from './components';
interface IShopPageCategoryProps {
  route: any;
  navigation: any;
}
const list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

const ShopPageCategoryComponent: React.FC<IShopPageCategoryProps> = (props) => {
  const { navigation } = props;
  const ListHeaderComponent = memo((props: any) => {
    const {} = props;
    return (
      <Block>
        <CategoryList />
        <Banner />
        <Text size={'size15'} fFamily={'BRANDING'} mgLeft={15} mgTop={12}>
          {translate(`shop_page.list_product`)}
        </Text>
        <Block h={0.6} bgColor={Colors.COLOR_9} mgHorizontal={15} mgTop={16} />
      </Block>
    );
  });
  const renderItem = ({ item }: RenderItemProps) => {
    return (
      <Block mgTop={16}>
        <ItemProduct data={{}} />
      </Block>
    );
  };
  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <Header navigation={navigation} />
      <CustomFlatlist
        style={{ flex: 1 }}
        columnWrapperStyle={{ justifyContent: 'space-between', paddingHorizontal: scale(15) }}
        contentContainerStyle={styles.contentContainer}
        numColumns={3}
        data={list}
        renderItem={renderItem}
        ListEmptyComponent={<ListEmpty />}
        ListHeaderComponent={<ListHeaderComponent />}
      />
    </SafeWrapper>
  );
};

export const ShopPageCategoryScreen = memo(ShopPageCategoryComponent, isEqual);
