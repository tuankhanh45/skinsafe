import React, { memo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import size from 'lodash/size';
import styles from './styles';
import { IconEyeClose, IconEyeOpen, IconKey } from '@assets';
import { Button, Text, WrapperImageBg, TextField, Form, Block, Touchable } from '@components';
import { useForm } from 'react-hook-form';
import { scale, useValidateForm } from '@utils';
import { SCREENS } from '@navigation';
import { translate, images } from '@assets';

import { handleResponse, requestCreatePassword } from '@configs';
import { useRequest } from 'ahooks';
import { Alert } from 'react-native';
import { Loading } from '@components';
interface ICreatePasswordProps {
  route: any;
  navigation: any;
}

const CreatePasswordComponent: React.FC<ICreatePasswordProps> = (props) => {
  const { navigation, route } = props;
  const [isHidePass, setIsHidePass] = useState<boolean>(true);
  const [isHideRePass, setIsHideRePass] = useState<boolean>(true);
  // form
  const formMethods = useForm();
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;
  const onErrors = (errors: any) => {};
  const onSubmit = (form: any) => {
    console.log('submit', form);
    const params = {
      phone: route?.params?.phone ?? '',
      otp: route?.params?.otp ?? '',
      password: form?.password ?? '',
      re_password: form?.re_password ?? '',
    };
    run(params);
  };
  // active phone
  const { run: run, loading } = useRequest(requestCreatePassword, {
    manual: true,
    onSuccess: (data) => {
      // test flow
            navigation.navigate(SCREENS.SUCCESS_ACTION_AUTH, { type: route?.params?.type });
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
      }
      navigation.navigate(SCREENS.SUCCESS_ACTION_AUTH, { type: route?.params?.type });
    },
    onError: (err) => {
      return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
    },
  });
  return (
    <WrapperImageBg style={styles.wrapper} source={images.background_triangular}>
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <Text style={styles.title}>{translate('create_password.title')}</Text>
        <Text
          style={styles.des}
          text={
            route?.params?.type === SCREENS.FORGOT_PASSWORD
              ? translate('create_password.description_forgot')
              : translate('create_password.description_register')
          }
        />
        <Block mgTop={49}>
          <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
            <TextField
              name={'password'}
              returnKeyType={'done'}
              placeholder={translate('placeholder.password')}
              iconLeft={() => <IconKey />}
              secureTextEntry={isHidePass}
              iconRight={() => (isHidePass ? <IconEyeClose /> : <IconEyeOpen />)}
              onPressIcon={() => setIsHidePass(!isHidePass)}
            />
            <TextField
              name={'re_password'}
              returnKeyType={'done'}
              placeholder={translate('placeholder.confirm_password')}
              iconLeft={() => <IconKey />}
              secureTextEntry={isHideRePass}
              iconRight={() => (isHideRePass ? <IconEyeClose /> : <IconEyeOpen />)}
              onPressIcon={() => setIsHideRePass(!isHideRePass)}
            />
          </Form>
        </Block>

        <Button disabled={!!size(errors)} onPress={formMethods.handleSubmit(onSubmit, onErrors)} marginTop={scale(46)}>
          {translate('cm.next')}
        </Button>
      </KeyboardAwareScrollView>
    </WrapperImageBg>
  );
};

export const CreatePasswordScreen = memo(CreatePasswordComponent, isEqual);
