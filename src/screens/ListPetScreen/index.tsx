import React, { memo, useEffect, useState, useCallback } from 'react';
import isEqual from 'react-fast-compare';
import styles from './styles';
import { Block, HeaderBar, SafeWrapper, Text, Touchable } from '@components';
import { FlatList, RefreshControl } from 'react-native';
import { translate } from '@assets';
import { PetItem } from '../MainPetScreen/components';
import { useRequest } from 'ahooks';
import { useAppStore } from '@store';
import { handleResponse, ICreatePetState, requestGetListPet } from '@configs';
import { useNavigation } from '@react-navigation/native';
import { SCREENS } from '@navigation';
import { Colors, Fonts, FontSize } from '@themes';

interface IListPetProps {}

const ListPetComponent: React.FC<IListPetProps> = () => {
  const navigation = useNavigation<any>();

  const listPet = useAppStore((state: ICreatePetState) => state.pet.listPet);
  const actSaveListPet = useAppStore((state: ICreatePetState) => state.actSaveListPet);

  const [page, setPage] = useState<number>(1);
  const [refreshing, setRefreshing] = useState(false);
  const [loadMore, setLoadMore] = useState<boolean>(true);

  useEffect(() => {
    loadData(page);
  }, [page]);

  const loadData = (pageNum: number) => {
    run({ page: pageNum, size: 10 });
    setRefreshing(false);
  };

  // get list pet
  const { run, loading, cancel } = useRequest(requestGetListPet, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (Array.isArray(res?.data)) {
        if (page === 1) {
          actSaveListPet(res?.data);
        } else {
          let arr = listPet;
          arr = [...arr, ...res?.data];
          actSaveListPet(arr);
        }
      }
    },
  });

  // Refreshing call api again
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    setLoadMore(true);
    if (page === 1) {
      return loadData(1);
    } else {
      setPage(1);
    }
  }, [page]);

  const onEndReached = () => {
    if (!loading && loadMore) {
      setPage((prev) => prev + 1);
    }
  };

  const renderItem = useCallback((item: any) => {
    return (
      <Block mgTop={15} mg={1}>
        <PetItem data={item} size={true} />
      </Block>
    );
  }, []);

  const renderHeader = useCallback(
    () => (
      <Block mgTop={15} mg={1}>
        <Block style={styles.wrapper} mgHorizontal={1} pdHorizontal={4} mgTop={16} pdVertical={12}>
          <Block direction="row" justify="space-between" align="center">
            <Text text={translate('pet.my_pet')} fFamily={'BRANDING'} size="size18" />
            <Touchable onPress={() => navigation.navigate(SCREENS.CREATE_PET)}>
              <Text
                text={translate('pet.add_pet')}
                fFamily={'MEDIUM'}
                size="size12"
                color="COLOR_1"
                style={styles.under}
              />
            </Touchable>
          </Block>
        </Block>
      </Block>
    ),
    [],
  );
  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('pet.title')} />

      <Block flex pdHorizontal={15}>
        <FlatList
          columnWrapperStyle={{ justifyContent: 'space-between' }}
          style={{ flex: 1 }}
          data={listPet}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.id + 'pet'}
          renderItem={({ item }) => renderItem(item)}
          refreshControl={
            <RefreshControl
              onRefresh={onRefresh}
              refreshing={refreshing}
              colors={[Colors.PRIMARY]}
              tintColor={Colors.PRIMARY}
              progressBackgroundColor={'transparent'}
            />
          }
          onEndReached={onEndReached}
          onEndReachedThreshold={0.2}
          ListFooterComponent={() => <Block h={100} />}
          ListHeaderComponent={() => renderHeader()}
        />
      </Block>
    </SafeWrapper>
  );
};

export const ListPetScreen = memo(ListPetComponent, isEqual);
