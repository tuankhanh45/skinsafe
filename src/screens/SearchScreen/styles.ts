import { Colors } from '@themes';
import { StyleSheet } from 'react-native';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  noSearchImg: {
    width: scale(219),
    height: vScale(263),
  },
  contentContainer: {
    paddingHorizontal: scale(15),
    paddingVertical: vScale(12),
  },
  columnWrapper: {
    marginBottom: vScale(28),
    justifyContent: 'space-between',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: vScale(20),
    backgroundColor: '#F1C48D',
    paddingVertical: vScale(6),
    paddingHorizontal: scale(11),
    borderRadius: moderateScale(4),
    justifyContent: 'space-between',
  },
  sortBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: vScale(8),
    paddingHorizontal: scale(15),
    borderRadius: moderateScale(4),
    backgroundColor: Colors.COLOR_4,
  },
  itemHistory: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#D8D8D8',
    marginBottom: vScale(4),
    paddingVertical: vScale(8),
    paddingHorizontal: scale(15),
    borderBottomWidth: moderateScale(1),
  },
});

export default styles;
