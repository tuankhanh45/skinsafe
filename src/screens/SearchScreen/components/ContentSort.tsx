import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { IconSuccess } from '@assets';
import { Colors, FontSize } from '@themes';
import { Block, Text, Touchable } from '@components';
import { moderateScale, scale, vScale } from '@utils';

interface IContentSortProps {
  listSort: any[];
  selected: any;
  onSelectSort: (e: any) => void;
}

const ContentSortComponent: React.FC<IContentSortProps> = (props) => {
  const { listSort, selected, onSelectSort } = props;
  const insets = useSafeAreaInsets();

  return (
    <Block style={[styles.wrapper, { paddingBottom: insets.bottom + vScale(10) }]}>
      {Array.isArray(listSort) &&
        listSort.map((e, i) => {
          const isSelected = selected?.key === e?.key;
          return (
            <Touchable
              key={i}
              onPress={() => onSelectSort(e)}
              style={[styles.itemSort, i === listSort.length - 1 && { borderBottomWidth: 0 }]}>
              <Text style={styles.textSort}>{e?.label}</Text>
              {isSelected && <IconSuccess />}
            </Touchable>
          );
        })}
    </Block>
  );
};

export const ContentSort = memo(ContentSortComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: scale(15),
    backgroundColor: Colors.COLOR_4,
    borderRadius: moderateScale(20),
  },
  textSort: {
    fontWeight: '500',
    fontSize: FontSize.size12,
    textTransform: 'capitalize',
  },
  itemSort: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: vScale(10),
    justifyContent: 'space-between',
    borderBottomWidth: moderateScale(1),
    borderBottomColor: 'rgba(171, 158, 150, 0.2)',
  },
});
