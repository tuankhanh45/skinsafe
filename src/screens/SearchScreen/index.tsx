import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import { Portal } from 'react-native-portalize';
import { Modalize } from 'react-native-modalize';
import { RefreshControl, FlatList } from 'react-native';
import React, { memo, useCallback, useEffect, useRef, useState } from 'react';

import {
  Text,
  Block,
  Image,
  Loading,
  HeaderBar,
  Touchable,
  SafeWrapper,
  ItemProduct,
  NotificationBadgeBtn,
} from '@components';
import styles from './styles';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { ContentSort } from './components';
import { fakeItemInOddArray } from '@utils';
import { IconArrowDown, IconCancel, translate } from '@assets';
import { handleResponse, RenderItemProps, requestGetListProduct } from '@configs';

interface ISearchProps {
  route: any;
  navigation: any;
}
const LIST_SORT = [
  { key: '', label: 'mặc định' },
  { label: 'giá', key: 'qty_sell' },
  { label: 'lượt xem', key: 'view' },
  { label: 'giảm nhiều', key: 'percentage_sale' },
];

const SearchComponent: React.FC<ISearchProps> = (props) => {
  const { navigation, route } = props;
  const keySearch = route?.params?.keySearch;

  const historySearch = useAppStore((x) => x.account.historySearch);
  const actSaveHistorySearch = useAppStore((x) => x.actSaveHistorySearch);
  const actRemoveHistorySearch = useAppStore((x) => x.actRemoveHistorySearch);

  const refSort = useRef<Modalize>(null);

  const [page, setPage] = useState<number>(1);
  const [data, setData] = useState<any[]>([]);
  const [sort, setSort] = useState<any>(LIST_SORT[0]);
  const [loadMore, setLoadMore] = useState<boolean>(true);
  const [refreshing, setRefreshing] = useState<boolean>(false);
  const [searchText, setSearchText] = useState<string>(keySearch || '');

  useEffect(() => {
    if (keySearch) {
      runGetListProduct({ page, keyword: keySearch, sort: sort?.key });
    }
  }, [page]);

  const { run: runGetListProduct, loading: loadingGetListProduct } = useRequest(requestGetListProduct, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (!res?.next_page_url) {
        setLoadMore(false);
      }
      if (Array.isArray(res?.data)) {
        if (res?.current_page === 1) {
          setData(res?.data);
        } else {
          setData((prev) => [...prev, ...res.data]);
        }
      }
    },
    onFinally: () => refreshing && setRefreshing(false),
  });

  const renderRight = useCallback(() => {
    return <NotificationBadgeBtn />;
  }, []);

  const keyExtractor = (item: any, index: number) => index + 'search';
  const renderItem = ({ item, index }: RenderItemProps) => {
    if (item?.stt === 'empty') return <Block w={106} />;
    return <ItemProduct data={item} />;
  };

  const onSubmitEditing = () => {
    const isExistedSearch: boolean = Boolean(historySearch.find((x) => x.text === searchText));
    !isExistedSearch && actSaveHistorySearch({ text: searchText, id: Number(new Date()) });
    navigation.push(SCREENS.SEARCH, { keySearch: searchText });
  };

  const onPressSort = () => refSort.current?.open();
  const onSelectSort = (item: any) => {
    setSort(item);
    refSort.current?.close();
    setLoadMore(true);
    if (page === 1) {
      keySearch && runGetListProduct({ page, keyword: keySearch, sort: item?.key });
    } else {
      setPage(1);
    }
  };

  const onEndReached = () => {
    if (loadMore && !loadingGetListProduct) {
      setPage((prev) => prev + 1);
    }
  };
  const onRefresh = () => {
    setLoadMore(true);
    setRefreshing(true);
    if (page === 1) {
      keySearch && runGetListProduct({ page, keyword: keySearch, sort: sort?.key });
    } else {
      setPage(1);
    }
  };

  const ListFooterComponent = () => {
    return (
      <Text center>
        {!loadMore ? 'Hết sản phẩm phù hợp' : loadingGetListProduct ? 'Đang tìm sản phẩm phù hợp' : ''}
      </Text>
    );
  };

  const onRemoveSearch = (e: any) => {
    actRemoveHistorySearch(e);
  };

  const onPressSearch = (e: any) => {
    navigation.push(SCREENS.SEARCH, { keySearch: e?.text });
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar
        autoFocus={!keySearch}
        searchValue={searchText}
        defaultValue={keySearch}
        renderRight={renderRight}
        onChangeText={setSearchText}
        onSubmitEditing={onSubmitEditing}
      />
      {loadingGetListProduct && !data.length && <Loading />}
      <FlatList
        numColumns={3}
        windowSize={15}
        refreshControl={
          <RefreshControl
            onRefresh={onRefresh}
            refreshing={refreshing}
            colors={[Colors.PRIMARY]}
            tintColor={Colors.PRIMARY}
            progressBackgroundColor={'transparent'}
          />
        }
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        onEndReached={onEndReached}
        data={fakeItemInOddArray(data)}
        ListEmptyComponent={ListEmptyComponent}
        ListFooterComponent={ListFooterComponent}
        columnWrapperStyle={styles.columnWrapper}
        contentContainerStyle={styles.contentContainer}
        ListHeaderComponent={
          <ListHeaderComponent
            data={data}
            sort={sort}
            onPressSort={onPressSort}
            historySearch={historySearch}
            onRemoveSearch={onRemoveSearch}
            onPressSearch={onPressSearch}
          />
        }
      />

      <Portal>
        <Modalize adjustToContentHeight ref={refSort} withHandle={false}>
          <ContentSort listSort={LIST_SORT} selected={sort} onSelectSort={onSelectSort} />
        </Modalize>
      </Portal>
    </SafeWrapper>
  );
};

export const SearchScreen = memo(SearchComponent, isEqual);

const ListEmptyComponent = () => {
  return (
    <Block flex center pdTop={100}>
      <Image source={'no_search'} style={styles.noSearchImg} />
      <Text>{translate('search.no_data')}</Text>
    </Block>
  );
};

const ListHeaderComponent = memo((props: any) => {
  const { data, sort, onPressSort, historySearch, onRemoveSearch, onPressSearch } = props;

  const isShowHistory: boolean = Array.isArray(historySearch) && historySearch.length > 0 && data.length === 0;

  return (
    <>
      {isShowHistory ? <Text>Lịch sử tìm kiếm</Text> : null}
      {isShowHistory &&
        historySearch.slice(0, 5).map((e: any, i: number) => {
          return (
            <Touchable
              key={i + 'history'}
              onPress={() => onPressSearch(e)}
              style={[styles.itemHistory, i === historySearch.slice(0, 5).length - 1 && { borderBottomWidth: 0 }]}>
              <Text flex numberOfLines={1} size={'size12'}>
                {e?.text}
              </Text>

              <Touchable pdHorizontal={4} onPress={() => onRemoveSearch(e)}>
                <IconCancel />
              </Touchable>
            </Touchable>
          );
        })}

      {data.length === 0 ? null : (
        <Block style={styles.headerContainer}>
          <Text fWeight="500">{translate('search.have_number_data_length', { length: data.length })}</Text>
          <Touchable style={styles.sortBtn} onPress={onPressSort}>
            <Text size="size12" fWeight="500" mgRight={5}>
              {translate('search.sort_by')} {sort?.label}
            </Text>
            <IconArrowDown />
          </Touchable>
        </Block>
      )}
    </>
  );
});
