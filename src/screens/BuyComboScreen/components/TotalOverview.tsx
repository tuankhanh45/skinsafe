import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { vScale } from '@utils';
import { Colors } from '@themes';
import { translate } from '@assets';
import { Block, Text, Touchable } from '@components';

interface ITotalOverviewProps {}

const TotalOverviewComponent: React.FC<ITotalOverviewProps> = (props) => {
  const {} = props;

  return (
    <Block style={styles.wrapper}>
      <Block h="100%" justify="center" pdHorizontal={12} flex>
        <Text>{translate('buy_combo.selected')} 0/5</Text>
      </Block>

      <Touchable center w={182} bgColor={Colors.COLOR_1}>
        <Text size="size12" color="WHITE">
          {translate('buy_combo.press_buy_combo')}
        </Text>
      </Touchable>
    </Block>
  );
};

export const TotalOverview = memo(TotalOverviewComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    height: vScale(40),
    flexDirection: 'row',
    backgroundColor: Colors.WHITE,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
