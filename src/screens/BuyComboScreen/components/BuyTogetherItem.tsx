import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { IconArrowDown, IconRadioChecked, translate } from '@assets';
import { currencyFormat, moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, StarRate, Text, Touchable } from '@components';

interface IBuyTogetherItemProps {
  data: any;
  onPressItem: () => void;
}

const BuyTogetherItemComponent: React.FC<IBuyTogetherItemProps> = (props) => {
  const { data, onPressItem } = props;

  return (
    <Touchable style={styles.wrapper} onPress={onPressItem}>
      <IconRadioChecked />
      <Block direction="row" pdLeft={4} flex>
        <ImageRemote source={'https://picsum.photos/801'} style={styles.img} />
        <Block pdLeft={10} flex>
          <Text fWeight="500" size={'size10'} numberOfLines={1}>
            Sản phẩm dành cho Smart Power CAT RANG Sản phẩm dành cho Smart Power CAT RANG
          </Text>
          <Block direction="row" pdTop={12} align="center">
            <StarRate rate={5} />
            <Text fWeight="300" size={'size6'} mgLeft={5}>
              Đã bán 100K+
            </Text>
          </Block>
          <Text mgVertical={6} size="size10">
            {currencyFormat(500000)}
          </Text>

          <Touchable direction="row" align="center">
            <Text size="size9" color={'COLOR_1'} mgRight={4}>
              {translate('buy_combo.choose_att')}
            </Text>
            <IconArrowDown color={Colors.COLOR_1} />
          </Touchable>
        </Block>
      </Block>
    </Touchable>
  );
};

export const BuyTogetherItem = memo(BuyTogetherItemComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: scale(16),
    marginHorizontal: scale(10),
    paddingVertical: vScale(11),
    paddingHorizontal: scale(5),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(6),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  img: {
    width: scale(103),
    height: vScale(100),
    borderRadius: moderateScale(5),
  },
});
