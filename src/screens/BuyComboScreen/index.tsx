import dayjs from 'dayjs';
import React, { memo, useRef } from 'react';
import isEqual from 'react-fast-compare';

import {
  Block,
  Text,
  Touchable,
  HeaderBar,
  ImageRemote,
  SafeWrapper,
  CartBadgeBtn,
  CustomFlatlist,
  PopupSelectAttProduct,
} from '@components';
import styles from './styles';
import { Colors } from '@themes';
import { currencyFormat } from '@utils';
import { RenderItemProps } from '@configs';
import { BuyTogetherItem, TotalOverview } from './components';
import { IconArrowDown, IconRadioChecked, translate } from '@assets';

interface IBuyComboProps {
  route: any;
  navigation: any;
}

const BuyComboComponent: React.FC<IBuyComboProps> = (props) => {
  const {} = props;
  const refModalAtt = useRef<any>(null);

  const renderRight = () => <CartBadgeBtn />;

  const renderItem = ({ item, index }: RenderItemProps) => {
    return <BuyTogetherItem data={item} onPressItem={() => onPressItem(item)} />;
  };

  const onPressItem = (item: any) => {
    refModalAtt.current?.open(item);
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('buy_combo.title')} renderRight={renderRight} />
      <CustomFlatlist
        data={[1, 2, 3]}
        renderItem={renderItem}
        ListHeaderComponent={<ListHeaderComponent onPressItem={() => onPressItem({})} />}
      />

      <TotalOverview />

      <PopupSelectAttProduct ref={refModalAtt} />
    </SafeWrapper>
  );
};

export const BuyComboScreen = memo(BuyComboComponent, isEqual);

const ListHeaderComponent = memo((props: any) => {
  const { onPressItem } = props;

  return (
    <Block pdVertical={16} pdHorizontal={10}>
      <Touchable style={styles.mainItem} onPress={onPressItem}>
        <IconRadioChecked />
        <Block direction="row" pdLeft={4} flex>
          <ImageRemote source={'https://picsum.photos/801'} style={styles.img} />
          <Block pdLeft={10} flex>
            <Text fWeight="500" size={'size16'} numberOfLines={2} color={'TEXT'}>
              Sản phẩm dành cho Smart Power CAT RANG
            </Text>
            <Text mgVertical={3} color={'COLOR_1'}>
              {currencyFormat(500000)}
            </Text>
            <Text fWeight="500" size={'size12'} color={'TEXT_2'}>
              {dayjs().format('DD-MM-YYYY HH:mm')}
            </Text>

            <Touchable direction="row" alignSelf="flex-end" align="center">
              <Text fWeight="500" color={'COLOR_1'} mgRight={8}>
                {translate('buy_combo.choose_att')}
              </Text>
              <IconArrowDown color={Colors.COLOR_1} />
            </Touchable>
          </Block>
        </Block>
      </Touchable>

      <Text size={'size15'} fFamily="BRANDING" mgVertical={16}>
        {translate('buy_combo.usually_bought_together')}
      </Text>
    </Block>
  );
});
