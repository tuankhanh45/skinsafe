import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: vScale(11),
    paddingHorizontal: scale(5),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(6),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  img: {
    width: scale(103),
    height: vScale(100),
    borderRadius: moderateScale(5),
  },
});

export default styles;
