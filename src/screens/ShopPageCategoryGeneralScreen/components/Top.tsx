import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, Image, Text, Touchable } from '@components';
import { useNavigation } from '@react-navigation/native';

interface ITopProps {
  pressFilter: () => void;
}

const TopComponent: React.FC<ITopProps> = (props) => {
  return (
    <Block pdHorizontal={15} mgTop={16}>
      <Text size={'size15'} fFamily={'BRANDING'}>
        Tất cả sản phẩm dành cho chó{' '}
      </Text>
      <Text size={'size14'} mgTop={5}>
        696 969 sản phẩm{' '}
      </Text>
      <Touchable style={styles.coverBox} direction="row" align="center" onPress={props.pressFilter}>
        <Image source="imgChoseCategory" style={styles.img} />
        <Text size={'size14'} fFamily={'MEDIUM'} mgLeft={20}>
          Chọn danh mục{' '}
        </Text>
      </Touchable>
    </Block>
  );
};

export const Top = memo(TopComponent, isEqual);

const styles = StyleSheet.create({
  coverBox: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    marginTop: scale(14),
    paddingHorizontal: scale(20),
    paddingVertical: vScale(13),
  },
  img: { width: scale(20), height: vScale(20) },
  txt: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size16, fontWeight: '400' },
});
