import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import chunk from 'lodash/chunk';
import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, Touchable, Text } from '@components';
import { useNavigation } from '@react-navigation/native';
interface ICategoryListProps {}
const url = 'https://picsum.photos/80';
const DOT_COLOR = 'rgba(255, 255, 255, 0.3)';
const IMG_SIZE = scale(111);
const list = [1, 2, 3, 4, 5, 6];
const CategoryListComponent: React.FC<ICategoryListProps> = (props) => {
  const navigation = useNavigation<any>();
  const onPressItem = (e: any) => {};
  return (
    <Block pdHorizontal={15}>
      <Text mgTop={4} size={'size15'} fFamily={'BRANDING'}>
        Danh mục ưa thích
      </Text>
      <Block direction="row" style={{ flexWrap: 'wrap' }} justify="space-between" mgTop={16}>
        {list.map((e, i) => {
          return (
            <Touchable key={i + 'fav'} mgBottom={10} onPress={() => onPressItem(e)}>
              <ImageRemote source={url + i} style={styles.img} resizeMode={'cover'} />
              <Text mgTop={4} size={'size12'} fWeight="600" textAlign="center">
                Hạt
              </Text>
            </Touchable>
          );
        })}
      </Block>
    </Block>
  );
};

export const FavoriteCategory = memo(CategoryListComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  txt: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size16, fontWeight: '400' },
  dot: { bottom: 0 },
  img: { height: IMG_SIZE, width: IMG_SIZE, borderRadius: scale(5) },
});
