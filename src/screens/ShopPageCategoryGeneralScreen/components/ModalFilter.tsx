import { Alert, StyleSheet } from 'react-native';
import React, { useCallback, useImperativeHandle, useRef, useState } from 'react';
import indexOf from 'lodash/indexOf';

import { currencyFormat, moderateScale, scale, vScale } from '@utils';
import { IconRadioUncheck, translate } from '@assets';
import { Colors, FontSize } from '@themes';
import { Block, RangeSlider, Text, Touchable, Button, CustomModalize, Image } from '@components';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import { Switch } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import { SCREENS } from '@navigation';

interface Props {
  category: any;
}
export const ModalFilterGeneral = React.forwardRef((props: Props, ref) => {
  const { category } = props;
  const refModal = useRef<any>(null);
  const navigation = useNavigation<any>();
  // action modal
  useImperativeHandle(ref, () => ({
    open: () => refModal.current?.open(),
    close: () => refModal.current?.close(),
  }));
  const pressItem = (item: any) => {
    refModal.current?.close();
    navigation.navigate(SCREENS.SHOPE_PAGE_CATEGORY_DETAIL, { item });
  };
  // header
  const Header = useCallback(
    () => (
      <Block direction="row" align="center" justify="space-between" mgTop={24} mgHorizontal={16}>
        <Block w={45} />
        <Text text={translate('shop_page.category')} fWeight="600" size="size18" fFamily="MEDIUM" color="TEXT" />
        <Text
          onPress={() => refModal.current?.close()}
          text={translate('shop_page.cancel')}
          fWeight="400"
          size="size14"
          fFamily="MEDIUM"
          color="COLOR_3"
        />
      </Block>
    ),
    [],
  );
  // category
  const Category = useCallback(
    () => (
      <Block pdHorizontal={24} mgTop={17}>
        {category?.map((e: any, i: number) => (
          <Touchable key={i + 'cat'} onPress={() => pressItem(e)} mgBottom={16}>
            <Text text={e?.name} size="size14" fFamily="MEDIUM" color="TEXT" />
          </Touchable>
        ))}
      </Block>
    ),
    [category],
  );
  const renderContent = () => {
    return (
      <Block style={styles.modalContainer}>
        <Header />
        <KeyboardAwareScrollView>
          <Category />
          <Block h={100} />
        </KeyboardAwareScrollView>
      </Block>
    );
  };
  return (
    <CustomModalize ref={refModal} modalHeight={scale(351)}>
      {renderContent()}
    </CustomModalize>
  );
});

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: Colors.WHITE,
    width: scale(375),
    borderTopLeftRadius: scale(20),
    borderTopRightRadius: scale(20),
  },
  confirmButton: {
    borderRadius: scale(20),
    height: scale(40),
    width: scale(120),
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: scale(1),
    borderColor: Colors.COLOR_1,
    backgroundColor: Colors.COLOR_1,
  },
  confirmButtonText: {
    color: Colors.WHITE,
    fontSize: scale(16),
    lineHeight: scale(24),
    fontWeight: '600',
    letterSpacing: scale(-0.32),
  },
  iconCat: { width: scale(20), height: vScale(20) },
});
