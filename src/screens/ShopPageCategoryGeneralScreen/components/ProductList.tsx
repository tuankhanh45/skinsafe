import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, CustomFlatlist, Text, ListEmpty, ItemProduct } from '@components';
import { useNavigation } from '@react-navigation/native';
import { ScrollView } from 'react-native';

interface IProductListProps {
  data: any;
}
const list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
const ProductListComponent: React.FC<IProductListProps> = (props) => {
  const navigation = useNavigation<any>();
  return (
    <Block pdHorizontal={16} mgTop={16}>
      <Block>
        <Text size={'size15'} fFamily={'BRANDING'}>
          Sản phẩm mua nhiều{' '}
        </Text>
      </Block>
      <ScrollView horizontal>
        {list.map((e, i) => (
          <Block key={i + 'item'} mgRight={14} mgTop={16}>
            <ItemProduct data={{}} />
          </Block>
        ))}
      </ScrollView>
    </Block>
  );
};

export const ProductList = memo(ProductListComponent, isEqual);
const styles = StyleSheet.create({
  wrapper: {},
  txt: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size16, fontWeight: '400' },
  contentContainer: {},
});
