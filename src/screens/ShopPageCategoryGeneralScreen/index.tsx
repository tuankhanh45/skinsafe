import React, { memo, useRef } from 'react';
import isEqual from 'react-fast-compare';
import { Colors } from '@themes';
import { Block, SafeWrapper } from '@components';
import { Header } from '../ShopPageScreen/components';
import { BannerGeneral, FavoriteCategory, ModalFilterGeneral, ProductList, Top } from './components';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
interface IShopPageCategoryGeneralProps {
  route: any;
  navigation: any;
}
const category = [
  { id: 1, name: 'Danh mục cho mèo' },
  { id: 2, name: 'Danh mục cho mèo' },
  { id: 3, name: 'Danh mục cho mèo' },
  { id: 4, name: 'Danh mục cho mèo' },
];
const ShopPageCategoryGeneralComponent: React.FC<IShopPageCategoryGeneralProps> = (props) => {
  const { navigation } = props;
  const refModalFilter = useRef<any>();

  const pressFilter = () => {
    refModalFilter.current.open();
  };
  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <Header navigation={navigation} />
      <KeyboardAwareScrollView>
        <Top pressFilter={pressFilter} />
        <Block h={0.6} bgColor={Colors.COLOR_9} mgTop={16} mgHorizontal={16} />
        <FavoriteCategory />
        <Block h={0.6} bgColor={Colors.COLOR_9} mgTop={6} mgHorizontal={16} />
        <BannerGeneral />
        <Block h={0.6} bgColor={Colors.COLOR_9} mgTop={10} mgHorizontal={16} />
        <ProductList data={[]} />
      </KeyboardAwareScrollView>
      <ModalFilterGeneral ref={refModalFilter} category={category} />
    </SafeWrapper>
  );
};

export const ShopPageCategoryGeneralScreen = memo(ShopPageCategoryGeneralComponent, isEqual);
