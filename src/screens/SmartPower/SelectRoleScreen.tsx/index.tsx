import { Block, Image, Text } from '@components';
import React, { memo, useEffect, useRef, useState } from 'react';
import { Platform, TouchableOpacity, View } from 'react-native';
import isEqual from 'react-fast-compare';
import MapView, { Circle, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { PERMISSIONS, request } from 'react-native-permissions';
import RNLocation from 'react-native-location';

import styles from './styles';
import { scale } from '@utils';
import { images } from '@assets';

const SelectRoleComponent: React.FC<IMapProps> = (props) => {
  return (
    <View style={styles.wrapper}>
      <Text>SelectRoleComponent</Text>
    </View>
  );
};

export const SelectRoleScreen = memo(SelectRoleComponent, isEqual);
