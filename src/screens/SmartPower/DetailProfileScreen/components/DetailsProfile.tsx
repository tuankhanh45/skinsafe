import React, { memo, useCallback, useRef } from 'react';
import isEqual from 'react-fast-compare';
import { Alert, StyleSheet } from 'react-native';
import { Block, Text, Touchable, Button } from '@components';
import LinearGradient from 'react-native-linear-gradient';
import { moderateScale, scale } from '@utils';
import { IconEditCalender, translate } from '@assets';
import { Fonts, FontSize } from '@themes';
import { useNavigation } from '@react-navigation/native';
import { SCREENS } from '@navigation';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { ICreateAccountState } from '@configs';
import { useRequest } from 'ahooks';
import { handleResponse, requestLogout, requestRemoveUser } from '@configs';
import dayjs from 'dayjs';
import { ScrollView } from 'react-native';
const coverLinear = [
  'rgba(255, 222, 136, 0.33)',
  'rgba(255, 146, 139, 0.23)',
  'rgba(255, 194, 190, 0.11)',
  'rgba(98, 255, 246, 0)',
];
const splashLinear = [
  'rgba(255, 222, 136, 1)',
  'rgba(255, 146, 139, 0.69)',
  'rgba(255, 194, 190, 0.33)',
  'rgba(98, 255, 246, 0)',
];
const dot = [
  'rgba(255, 222, 136, 1)',
  'rgba(255, 146, 139, 0.69)',
  'rgba(255, 194, 190, 0.33)',
  'rgba(98, 255, 246, 0)',
];
interface IDetailsProfileProps {}
const DetailsProfileComponent: React.FC<IDetailsProfileProps> = (props) => {
  const actRemoveLoggerUser = useAppStore((state: ICreateAccountState) => state.actRemoveLoggerUser);
  const user = useAppStore((state: ICreateAccountState) => state?.account?.user);

  const navigation = useNavigation<any>();
  const gender = user?.gender === 1 ? 'Nam' : user?.gender === 0 ? 'Nữ' : 'Khác';
  const pressEditProfile = () => {
    navigation.navigate(SCREENS.EDIT_PROFILE);
  };
  const pressChangePassword = () => {
    navigation.navigate(SCREENS.CHANGE_PASSWORD, { phone: user?.phone });
  };
  // logout
  const onPressLogout = useCallback(() => {
    Alert.alert('Xác nhận', 'Bạn chắc chắn muốn đăng xuất', [
      { text: 'Đăng xuất', onPress: () => run(), style: 'destructive' },
      { text: 'Cancel', onPress: () => {} },
    ]);
  }, []);
  const { run } = useRequest(requestLogout, {
    manual: true,
    onFinally: () => {
      actRemoveLoggerUser();
      navigation.goBack();
    },
  });
  // remove account
  const onPressRemoveAccount = useCallback(() => {
    Alert.alert('Xác nhận', 'Bạn chắc chắn muốn xoá tài khoản?', [
      { text: 'Xoá ngay', onPress: () => runRemoveAccount(), style: 'destructive' },
      { text: 'Cancel', onPress: () => {} },
    ]);
  }, []);
  const { run: runRemoveAccount } = useRequest(requestRemoveUser, {
    manual: true,
    onFinally: () => {
      actRemoveLoggerUser();
      navigation.goBack();
    },
  });

  return (
    <ScrollView>
      <Block bgColor={Colors.WHITE} borderRadius={10} mgTop={28}>
        <LinearGradient colors={coverLinear} style={styles.containerLinear}></LinearGradient>
        <Block mgTop={-255} pdHorizontal={20} pdTop={20} borderRadius={10}>
          <Block direction="row" align="center" justify="space-between">
            <Block w={18} />
            <Text text={translate('profile.info')} style={styles.title} />
            <Touchable onPress={pressEditProfile}>
              <IconEditCalender />
            </Touchable>
          </Block>
          {/* slash */}
          <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={splashLinear} style={styles.splash} />
          {/* name */}
          <Block mgTop={7}>
            <Text text={translate('profile.full_name')} style={styles.key} />
            <Text text={user?.fullname} style={styles.value} />
            <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
          </Block>
          {/* nick */}
          {/* <Block mgTop={7}>
              <Text text={translate('profile.nickname')} style={styles.key} />
              <Text text={user?.nickname} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block> */}
          {/* birth  */}
          <Block mgTop={7}>
            <Text text={translate('profile.birthday')} style={styles.key} />
            <Text text={dayjs(user?.birthday).format('DD/MM/YYYY')} style={styles.value} />
            <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
          </Block>
          {/* passport  */}
          <Block mgTop={7}>
            <Text text={translate('profile.passport')} style={styles.key} />
            <Text text={user?.passport} style={styles.value} />
            <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
          </Block>
          {/* gender */}
          <Block mgTop={7}>
            <Text text={translate('profile.gender')} style={styles.key} />
            <Text text={gender} style={styles.value} />
            <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
          </Block>
          {/* country  */}
          {/* <Block mgTop={7}>
              <Text text={translate('profile.country')} style={styles.key} />
              <Text text={user?.country} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block> */}
          {/* phone */}
          <Block mgTop={7}>
            <Text text={translate('profile.phone')} style={styles.key} />
            <Text text={user?.phone} style={styles.value} />
            <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
          </Block>
          {/* email */}
          <Block mgTop={7}>
            <Text text={translate('profile.email')} style={styles.key} />
            <Text text={user?.email} style={styles.value} />
            <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
          </Block>
          {/* btn change password */}
          <Touchable w={120} onPress={pressChangePassword} mgTop={18} mgBottom={20}>
            <Text text={translate('profile.change_password')} style={styles.change_password} />
          </Touchable>
        </Block>
      </Block>
      <Block
        direction="row"
        justify="space-between"
        pdHorizontal={30}
        borderRadius={10}
        pdTop={14}
        pdBottom={100}
        bgColor={Colors.WHITE}
        mgTop={10}>
        <Touchable w={105} onPress={onPressLogout} style={styles.coverBtn}>
          <Text style={styles.txtBtn}>{translate('profile.logout')}</Text>
        </Touchable>
        <Touchable w={126} onPress={onPressRemoveAccount} style={styles.coverBtn}>
          <Text style={styles.txtBtn}>{translate('profile.remove_account')}</Text>
        </Touchable>
      </Block>
    </ScrollView>
  );
};

export const DetailsProfile = memo(DetailsProfileComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  containerLinear: { height: scale(255), borderRadius: 10 },
  title: { fontSize: FontSize.size16, fontFamily: Fonts.BRANDING },
  splash: {
    marginTop: scale(12),
    height: moderateScale(1),
  },
  key: { fontWeight: '500', fontSize: FontSize.size16, fontFamily: Fonts.MEDIUM },
  value: { fontWeight: '500', fontSize: FontSize.size14, color: Colors.COLOR_1, marginTop: scale(3) },
  change_password: { fontWeight: '600', fontSize: FontSize.size16, color: Colors.COLOR_7 },
  coverBtn: {
    borderRadius: scale(100),
    backgroundColor: Colors.WHITE,
    borderWidth: moderateScale(1),
    borderColor: Colors.COLOR_1,
    height: scale(33),
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtBtn: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size14, color: Colors.COLOR_1 },
});
