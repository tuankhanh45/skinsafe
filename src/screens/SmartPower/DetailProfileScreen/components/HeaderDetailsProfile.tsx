import React, { memo, useRef, useState } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Colors, Fonts, FontSize } from '@themes';
import { moderateScale, scale } from '@utils';
import { Block, Text, Touchable, ImageRemote } from '@components';
import {
  IconArrowLeft,
  IconCamera,
  IconCoinsAccumulated,
  IconOfferPackage,
  IconRectangle,
  IconStandard,
  translate,
} from '@assets';
import { ICreateAccountState } from '@configs';
import { useNavigation } from '@react-navigation/native';
import { ModalOptionAvatar } from './components';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';

const box = [
  {
    icon: <IconStandard />,
    title: 'profile.standard',
    content: 'profile.no_sale_off',
  },
  {
    icon: <IconCoinsAccumulated />,
    title: 'profile.coin_accumulated',
    content: 'profile.no_cost',
  },
  {
    icon: <IconOfferPackage />,
    title: 'profile.offer_package',
    content: 'profile.no_cost',
  },
];
interface IHeaderDetailsProfileProps {}
const HeaderDetailsProfileComponent: React.FC<IHeaderDetailsProfileProps> = (props) => {
  const navigation = useNavigation<any>();
  const user = useAppStore((state: ICreateAccountState) => state?.account?.user);
  const refModal = useRef<any>(null);
  const choseAvatar = () => {
    return refModal?.current?.open();
  };
  const pressRegister = () => {
    navigation.navigate(SCREENS.PARTNER_REGISTER);
  };
  const pressView = () => {
    navigation.navigate(SCREENS.INCOME);
  };
  return (
    <Block style={styles.wrapper}>
      <Block direction="row" align="center">
        <Touchable onPress={() => navigation.goBack()} pd={10}>
          <IconArrowLeft />
        </Touchable>
        <Touchable onPress={choseAvatar} style={styles.coverAvatar}>
          <ImageRemote resizeMode={'cover'} source={user?.avatar} style={styles.avatar} />
          <Block style={styles.coverCamera}>
            <IconCamera />
          </Block>
        </Touchable>
        {/* <Block pdHorizontal={10} flex>
          <Text text={user?.fullname} style={styles.name} />
          <Block direction="row">
            <Touchable style={styles.coverBtn} onPress={pressRegister}>
              <Text text={'Đăng kí đối tác > '} style={styles.txtBtn} />
            </Touchable>
          </Block>
          <Block direction="row">
            <Touchable style={styles.coverBtn} onPress={pressView}>
              <Text text={'Xem thông tin đối tác > '} style={styles.txtBtn} />
            </Touchable>
          </Block>
        </Block> */}
        {/* <ImageRemote resizeMode={'cover'} source={user?.qr} style={styles.qr} /> */}
      </Block>
      {/* <IconRectangle style={styles.rec} /> */}
      {/* <Block style={styles.coverBox}>
        {box.map((e: any, idx: number) => (
          <Block direction="row" align="center" key={'item' + idx}>
            {idx === 1 ? <Block h={23} w={1} mgRight={15} bgColor={Colors.COLOR_6} /> : <Block />}
            {e?.icon}
            <Block mgLeft={2.5}>
              <Text text={translate(e?.title)} style={styles.title} />
              <Text text={translate(e?.content)} style={styles.content} />
            </Block>
            {idx === 1 ? <Block h={23} w={1} mgLeft={15} bgColor={Colors.COLOR_6} /> : <Block />}
          </Block>
        ))}
      </Block> */}
      <ModalOptionAvatar ref={refModal} />
    </Block>
  );
};

export const HeaderDetailsProfile = memo(HeaderDetailsProfileComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: { marginHorizontal: scale(16) },
  avatar: { width: scale(60), height: scale(60), borderRadius: scale(60) },
  name: { fontSize: FontSize.size14, color: Colors.BLACK, fontFamily: Fonts.BRANDING },
  coverBtn: {
    marginTop: scale(6),
    backgroundColor: '#FF73C2',
    borderRadius: 500,
    paddingVertical: scale(3),
    paddingHorizontal: scale(10),
  },
  txtBtn: { fontSize: FontSize.size12, fontWeight: '400', color: Colors.WHITE, fontFamily: Fonts.MEDIUM },
  qr: { width: scale(60), height: scale(60), borderRadius: scale(5) },
  rec: { marginTop: scale(5), marginBottom: -2, marginLeft: scale(19) },
  coverBox: {
    backgroundColor: '#FEE4A1',
    borderRadius: scale(10),
    paddingHorizontal: scale(14.5),
    paddingVertical: scale(11.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  coverAvatar: {
    borderWidth: moderateScale(1),
    borderColor: Colors.WHITE,
    width: scale(62),
    height: scale(62),
    borderRadius: scale(62),
  },
  coverCamera: {
    position: 'absolute',
    right: -4,
    bottom: 0,
    padding: scale(4),
    backgroundColor: '#EBEBEB',
    borderRadius: scale(18),
    borderWidth: moderateScale(1),
    borderColor: Colors.WHITE,
  },
  title: { fontSize: FontSize.size10, fontFamily: Fonts.BRANDING },
  content: { fontSize: FontSize.size8, fontFamily: Fonts.MEDIUM },
});
