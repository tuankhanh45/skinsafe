import React, { useImperativeHandle, useRef, useState } from 'react';
import isEqual from 'react-fast-compare';
import { Alert, Modal, StyleSheet } from 'react-native';
import { Block, Text, Touchable, Loading, CustomModal } from '@components';
import { scale } from '@utils';
import { Colors, Fonts, FontSize } from '@themes';
import { IconCameraModal, IconPhotoModal, translate } from '@assets';
import { useRequest } from 'ahooks';
import { useAppStore } from '@store';
import { ICreateAccountState, requestUpdateAvatar, handleResponse, requestGetUserInfo } from '@configs';
import { ImageLibraryOptions, launchImageLibrary, launchCamera } from 'react-native-image-picker';

type Props = {};
const optionsImage: ImageLibraryOptions = {
  maxWidth: 512,
  maxHeight: 512,
  selectionLimit: 1,
  includeExtra: true,
  mediaType: 'photo',
  includeBase64: true,
  presentationStyle: 'pageSheet',
};
export const ModalOptionAvatar = React.forwardRef((props: Props, ref) => {
  const refModal = useRef<any>(null);
  useImperativeHandle(ref, () => ({
    open: () => refModal.current?.open(),
    close: () => refModal.current?.close(),
  }));
  const actSaveLoggerUser = useAppStore((state: ICreateAccountState) => state.actSaveLoggerUser);
  const account = useAppStore((state: ICreateAccountState) => state?.account);
  const onBackdropPress = () => refModal.current?.close();

  // picker
  const choseAvatar = () => {
    try {
      launchImageLibrary(optionsImage, (res: any) => {
        if (res.didCancel) {
        } else if (res.error) {
        } else if (res.customButton) {
        } else {
          try {
            const data = res?.assets[0];
            const formData = new FormData();
            formData.append('avatar', {
              uri: data?.[0]?.uri,
              name: data?.[0]?.fileName,
              type: data?.[0]?.type,
            });
            runUpdateAvatar(formData);
          } catch (error) {
            refModal.current?.close();
          }
        }
      });
    } catch (err) {}
  };
  // shot
  const openCamera = () => {
    refModal.current?.close();
    try {
      launchCamera(optionsImage, (res: any) => {
        if (res.didCancel) {
        } else if (res.error) {
        } else if (res.customButton) {
        } else {
          try {
            const data = res?.assets[0];
            const formData = new FormData();
            formData.append('avatar', {
              uri: data?.[0]?.uri,
              name: data?.[0]?.fileName,
              type: data?.[0]?.type,
            });
            runUpdateAvatar(formData);
          } catch (error) {
            refModal.current?.close();
          }
        }
      });
    } catch (err) {}
  };
  // update avatar
  const { run: runUpdateAvatar, loading: loadingUpdateAvatar } = useRequest(requestUpdateAvatar, {
    manual: true,
    onSuccess: (data: any) => {
      const { res, err, isError } = handleResponse(data);
      if (!isError) {
        runGetUser();
        return;
      }
      return Alert.alert('Lỗi', 'Cập nhật ảnh thất bại. Xin thử lại');
    },
    onError: () => {
      refModal.current?.close();
      return Alert.alert('Lỗi', 'Cập nhật ảnh thất bại. Xin thử lại');
    },
  });
  // get user info
  const { run: runGetUser, loading: loadingUser } = useRequest(requestGetUserInfo, {
    manual: true,
    onSuccess: (data: any) => {
      const { res, err, isError } = handleResponse(data);

      if (res?.id) {
        const newAcc = account;
        newAcc.user = res;
        actSaveLoggerUser(newAcc);
        refModal.current?.close();
        return Alert.alert('Cập nhật ảnh thành công');
      }
      return Alert.alert('Lỗi', 'Cập nhật ảnh thất bại. Xin thử lại');
    },
    onError: (err) => {
      refModal.current?.close();
      return Alert.alert('Lỗi', 'Cập nhật ảnh thất bại. Xin thử lại');
    },
  });
  const renderContent = () => (
    <Block style={styles.modalContainer}>
      <Text style={styles.title} text={translate('profile.change_avatar')} />
      {/* camera */}
      <Touchable
        w={327}
        mgTop={26}
        direction="row"
        justify="space-between"
        mgHorizontal={8}
        align="center"
        onPress={openCamera}>
        <Text text={translate('profile.shot')} style={styles.content} />
        <IconCameraModal />
      </Touchable>
      <Block w={343} h={0.5} bgColor={'#E1E1E1'} mgTop={8} />
      {/* lib */}
      <Touchable
        w={327}
        mgTop={26}
        direction="row"
        justify="space-between"
        mgHorizontal={8}
        align="center"
        onPress={choseAvatar}>
        <Text text={translate('profile.chose')} style={styles.content} />
        <IconPhotoModal />
      </Touchable>
      <Block w={343} h={0.5} bgColor={'#E1E1E1'} mgTop={8} />
      {(loadingUpdateAvatar || loadingUser) && <Loading />}
    </Block>
  );

  return <CustomModal ref={refModal} renderContent={renderContent} onBackdropPress={onBackdropPress} />;
});

const styles = StyleSheet.create({
  buttonTitle: {
    fontWeight: '600',
    fontSize: FontSize.size14,
    lineHeight: scale(22),
    color: Colors.COLOR_1,
  },
  modalContainer: {
    backgroundColor: Colors.WHITE,
    height: scale(148),
    width: scale(375),
    borderRadius: scale(20),
    paddingHorizontal: scale(16),
    position: 'absolute',
    bottom: 30,
    alignItems: 'center',
  },
  backDrop: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  title: {
    color: Colors.TEXT,
    fontSize: FontSize.size16,
    fontFamily: Fonts.BRANDING,
    // textAlign: 'center',
    marginTop: scale(16),
  },
  content: {
    color: Colors.TEXT,
    fontSize: FontSize.size14,
    fontWeight: '400',
    fontFamily: 'Helvetica Neue',
  },
});
