import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { WrapperImageBg } from '@components';
import { HeaderDetailsProfile, DetailsProfile } from './components';
interface IDetailsProfileProps {}
const DetailsProfileComponent: React.FC<IDetailsProfileProps> = () => {
  return (
    <WrapperImageBg enabledGoBack={false}>
      <HeaderDetailsProfile />
      <DetailsProfile />
    </WrapperImageBg>
  );
};
export const DetailsProfileScreen = memo(DetailsProfileComponent, isEqual);
