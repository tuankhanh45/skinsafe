import React, { memo, useEffect, useRef, useState } from 'react';
import isEqual from 'react-fast-compare';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import size from 'lodash/size';
import styles from './styles';
import { Button, Text, WrapperImageBg, TextField, Form, Block, Touchable, ModalSelectDate, Loading } from '@components';
import { useForm } from 'react-hook-form';
import { scale, useValidateForm } from '@utils';
import { SCREENS } from '@navigation';
import { IconCalender, IconEditCalender, images, translate } from '@assets';
import LinearGradient from 'react-native-linear-gradient';
import { useAppStore } from '@store';
import { ICreateAccountState } from '@configs';
import { Colors } from '@themes';
import dayjs from 'dayjs';
import { ModalSelectGender } from './components';
import { handleResponse, requestUpdateProfile, requestGetUserInfo } from '@configs';
import { useRequest } from 'ahooks';
import { Alert } from 'react-native';
interface IEditProfileProps {
  route: any;
  navigation: any;
}
const coverLinear = [
  'rgba(255, 222, 136, 0.33)',
  'rgba(255, 146, 139, 0.23)',
  'rgba(255, 194, 190, 0.11)',
  'rgba(98, 255, 246, 0)',
];

const EditProfileComponent: React.FC<IEditProfileProps> = (props) => {
  const { navigation, route } = props;
  const refModalDate = useRef<any>(null);
  const refModalGender = useRef<any>(null);

  const account = useAppStore((state: ICreateAccountState) => state?.account);
  const user = useAppStore((state: ICreateAccountState) => state?.account?.user);
  const actSaveLoggerUser = useAppStore((state: ICreateAccountState) => state.actSaveLoggerUser);
  const [gender, setGender] = useState<any>(user?.gender ? user?.gender : 2);
  const [selectedDate, setSelectedDate] = useState<any>(user?.birthday ? dayjs(user?.birthday) : undefined);

  // form
  const formMethods = useForm();
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;

  const onErrors = (errors: any) => {};
  const onSubmit = (form: any) => {
    const params = {
      phone: form?.phone ?? '',
      username: form?.username ?? '',
      fullname: form?.full_name ?? '',
      birthday: selectedDate ? dayjs(selectedDate).format('YYYY-MM-DD') : '',
      passport: form?.passport ?? '',
      email: form?.email ?? '',
      gender: form?.gender ?? '',
    };
    return run(params);
  };

  const { run, loading, cancel } = useRequest(requestUpdateProfile, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      console.log('res', res);
      if (!isError) {
        runGetUser();
        return;
      }
      return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại sau!');
    },
    onError: (err) => {
      return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại sau!');
    },
  });
  // get user info
  const { run: runGetUser, loading: loadingUser } = useRequest(requestGetUserInfo, {
    manual: true,
    onSuccess: (data: any) => {
      const { res, err, isError } = handleResponse(data);
      if (res?.id) {
        const newAcc = account;
        newAcc.user = res;
        actSaveLoggerUser(newAcc);
        return Alert.alert('Thành công', 'Đã cập nhật thông tin cá nhân');
      }
      return Alert.alert('Lỗi', 'Cập nhật thất bại. Xin thử lại');
    },
    onError: (err) => {
      return Alert.alert('Lỗi', 'Cập thất bại. Xin thử lại');
    },
  });
  const pressChangePassword = () => {
    navigation.navigate(SCREENS.OTP_CONFIRM, { type: SCREENS.CHANGE_PASSWORD, phone: user?.phone });
  };
  useEffect(() => {
    formMethods.setValue('full_name', user?.fullname);
    // formMethods.setValue('nickname', user?.nickname);
    formMethods.setValue('birthday', user?.birthday);
    formMethods.setValue('passport', user?.passport);
    formMethods.setValue('gender', user?.gender);
    formMethods.setValue('phone', user?.phone);
    formMethods.setValue('email', user?.email);
  }, []);

  return (
    <WrapperImageBg style={styles.wrapper} source={images.background_triangular}>
      {(loading || loadingUser) && <Loading />}
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <LinearGradient colors={coverLinear} style={styles.containerLinear} />
        <Block mgTop={-255} pdTop={27}>
          <Text text={translate('profile.change_info')} style={styles.title} />
          <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
            {/* name */}
            <Block mgHorizontal={16}>
              <Text text={translate('profile.full_name')} style={styles.key} />
              <TextField name={'full_name'} returnKeyType={'done'} />
            </Block>
            <Block h={5} bgColor={'#F5F4F9'} mgTop={6} />
            {/* nick */}
            {/* <Block mgHorizontal={16}>
              <Text text={translate('profile.nickname')} style={styles.key} />
              <TextField name={'nickname'} returnKeyType={'done'} />
            </Block>
            <Block h={5} bgColor={'#F5F4F9'} mgTop={6} /> */}
            {/* passport  */}
            <Block mgHorizontal={16}>
              <Text text={translate('profile.passport')} style={styles.key} />
              <TextField name={'passport'} returnKeyType={'done'} disableEdit={true} />
            </Block>
            <Block h={5} bgColor={'#F5F4F9'} mgTop={6} />
            {/* birth  */}
            <Block pdHorizontal={16}>
              <Text text={translate('profile.birthday')} style={styles.key} />
              <Touchable onPress={() => refModalDate.current.open()} style={styles.coverDate}>
                <Block flex>
                  <Text
                    style={styles.date}
                    text={selectedDate ? dayjs(selectedDate).format('DD/MM/YYYY') : user?.birthday}
                  />
                </Block>
                <IconCalender color={Colors.BLACK} />
              </Touchable>
            </Block>
            <Block h={5} bgColor={'#F5F4F9'} mgTop={6} />
            {/* gender */}
            <Block pdHorizontal={16}>
              <Text text={translate('profile.gender')} style={styles.key} />
              <Touchable onPress={() => refModalGender.current.open()} style={styles.coverDate}>
                <Block flex>
                  <Text style={styles.date} text={gender === 0 ? 'Nam' : gender === 1 ? 'Nữ' : 'Khác'} />
                </Block>
                <IconEditCalender color={Colors.BLACK} />
              </Touchable>
            </Block>
            <Block h={5} bgColor={'#F5F4F9'} mgTop={6} />
            {/* country  */}
            {/* <Block mgHorizontal={16}>
              <Text text={translate('profile.country')} style={styles.key} />
              <TextField name={'country'} returnKeyType={'done'} />
            </Block>
            <Block h={5} bgColor={'#F5F4F9'} mgTop={6} /> */}
            {/* phone */}
            <Block mgHorizontal={16}>
              <Text text={translate('profile.phone')} style={styles.key} />
              <TextField name={'phone'} returnKeyType={'done'} />
            </Block>
            <Block h={5} bgColor={'#F5F4F9'} mgTop={6} />
            {/* email */}
            <Block mgHorizontal={16}>
              <Text text={translate('profile.email')} style={styles.key} />
              <TextField name={'email'} returnKeyType={'done'} />
            </Block>
            <Block h={5} bgColor={'#F5F4F9'} mgTop={6} />
          </Form>
          {/* btn change password */}
          <Touchable w={120} onPress={pressChangePassword} mgVertical={20} mgHorizontal={16}>
            <Text text={translate('profile.change_password')} style={styles.change_password} />
          </Touchable>
          <Block h={5} bgColor={'#F5F4F9'} mgTop={6} />

          <Block mgHorizontal={16}>
            <Button
              disabled={!!size(errors)}
              onPress={formMethods.handleSubmit(onSubmit, onErrors)}
              marginTop={scale(46)}>
              {translate('profile.confirm')}
            </Button>
          </Block>
        </Block>

        <ModalSelectDate
          ref={refModalDate}
          maxDate={new Date()}
          initDate={selectedDate}
          setSelectedDate={setSelectedDate}
        />
        <ModalSelectGender selected={gender} setSelected={setGender} ref={refModalGender} />
      </KeyboardAwareScrollView>
    </WrapperImageBg>
  );
};

export const EditProfileScreen = memo(EditProfileComponent, isEqual);
