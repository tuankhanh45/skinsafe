import { StyleSheet } from 'react-native';
import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.COLOR_4,
  },
  container: {
    marginTop: vScale(58),
    backgroundColor: Colors.COLOR_4,
    borderTopLeftRadius: moderateScale(25),
    borderTopRightRadius: moderateScale(25),
    paddingBottom: scale(100),
  },
  containerLinear: { height: scale(255), borderRadius: 10 },
  title: { fontWeight: '600', fontSize: FontSize.size16, textAlign: 'center', marginBottom: scale(8) },
  key: {
    fontWeight: '500',
    fontSize: FontSize.size16,
    marginBottom: scale(-10),
    marginLeft: scale(21),
    marginTop: scale(10),
  },
  change_password: {
    fontWeight: '600',
    fontSize: FontSize.size16,
    color: Colors.COLOR_7,
    textDecorationLine: 'underline',
  },
  date: { fontSize: FontSize.size14, color: Colors.TEXT, fontWeight: '400' },
  coverDate: {
    borderRadius: scale(8),
    height: scale(48),
    marginTop: scale(16),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(16),
    paddingTop: scale(5),
    backgroundColor: '#F1F1F1',
  },
});

export default styles;
