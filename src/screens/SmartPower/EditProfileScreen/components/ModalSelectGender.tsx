import React, { useImperativeHandle, useRef, useState } from 'react';
import isEqual from 'react-fast-compare';
import { Alert, Modal, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, Touchable, CustomModal } from '@components';
import { scale } from '@utils';
import { Colors, FontSize } from '@themes';
import { translate } from '@assets';
type Props = {
  setSelected: (txt: string | number) => void;
  selected: string;
};

export const ModalSelectGender = React.forwardRef((props: Props, ref) => {
  const { setSelected, selected } = props;
  const refModal = useRef<any>(null);

  useImperativeHandle(ref, () => ({
    open: () => refModal.current?.open(),
    close: () => refModal.current?.close(),
  }));
  const close = () => refModal.current?.close();
  const onBackdropPress = () => refModal.current?.close();
  const renderContent = () => (
    <Block style={styles.modalContainer}>
      <Text style={styles.title} text={translate('profile.gender')} />
      <Block mgTop={30} direction="row" justify="space-between" w={234}>
        <Touchable
          w={62}
          h={22}
          align="center"
          justify="center"
          bgColor={Colors.COLOR_12}
          borderRadius={50}
          onPress={() => {
            setSelected(0);
            close();
          }}>
          <Text text="Nam" style={styles.gender} />
        </Touchable>
        <Touchable
          w={62}
          h={22}
          align="center"
          justify="center"
          bgColor={Colors.COLOR_12}
          borderRadius={50}
          onPress={() => {
            setSelected(1);
            close();
          }}>
          <Text text="Nữ" style={styles.gender} />
        </Touchable>
        <Touchable
          w={62}
          h={22}
          align="center"
          justify="center"
          bgColor={Colors.COLOR_12}
          borderRadius={50}
          onPress={() => {
            setSelected(2);
            close();
          }}>
          <Text text="Khác" style={styles.gender} />
        </Touchable>
      </Block>
    </Block>
  );

  return <CustomModal ref={refModal} renderContent={renderContent} onBackdropPress={onBackdropPress} />;

  return (
    <Modal visible={true} transparent>
      <TouchableWithoutFeedback
        onPress={() => {
          props.setShowModal(false);
        }}>
        <Block flex bgColor={'rgba(0,0,0,0.5)'} justify="center">
          <Block style={styles.modalContainer}>
            <Text style={styles.title} text={translate('profile.gender')} />
            <Block mgTop={30} direction="row" justify="space-between" w={234}>
              <Touchable
                w={62}
                h={22}
                align="center"
                justify="center"
                bgColor={Colors.COLOR_12}
                borderRadius={50}
                onPress={() => {
                  props?.setSelected(0);
                  props?.setShowModal(false);
                }}>
                <Text text="Nam" style={styles.gender} />
              </Touchable>
              <Touchable
                w={62}
                h={22}
                align="center"
                justify="center"
                bgColor={Colors.COLOR_12}
                borderRadius={50}
                onPress={() => {
                  props?.setSelected(1);
                  props?.setShowModal(false);
                }}>
                <Text text="Nữ" style={styles.gender} />
              </Touchable>
              <Touchable
                w={62}
                h={22}
                align="center"
                justify="center"
                bgColor={Colors.COLOR_12}
                borderRadius={50}
                onPress={() => {
                  props?.setSelected(2);
                  props?.setShowModal(false);
                }}>
                <Text text="Khác" style={styles.gender} />
              </Touchable>
            </Block>
          </Block>
        </Block>
      </TouchableWithoutFeedback>
    </Modal>
  );
});

const styles = StyleSheet.create({
  buttonTitle: {
    fontWeight: '600',
    fontSize: FontSize.size14,
    lineHeight: scale(22),
    color: Colors.COLOR_1,
  },
  modalContainer: {
    backgroundColor: Colors.WHITE,
    height: scale(122),
    width: scale(266),
    alignSelf: 'center',
    borderRadius: scale(20),
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scale(16),
  },
  backDrop: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  title: {
    color: Colors.TEXT,
    fontSize: scale(16),
    fontWeight: '500',
    fontFamily: 'Helvetica Neue',
  },
  gender: {
    color: Colors.TEXT,
    fontSize: scale(14),
    fontWeight: '400',
    fontFamily: 'Helvetica Neue',
  },
});
