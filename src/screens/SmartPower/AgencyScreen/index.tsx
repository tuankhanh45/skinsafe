import React, { memo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { ImageBackground } from 'react-native';
import { RenderItemProps } from '@configs';

import styles from './styles';
import { SCREENS } from '@navigation';
import { IconSearch, images, translate } from '@assets';
import {
  Block,
  HeaderBar,
  CustomFlatlist,
  SafeWrapper,
  Text,
  Touchable,
  ImageRemote,
  ListEmpty,
  Input,
} from '@components';
import { scale } from '@utils';
import { Colors } from '@themes';
const url = 'https://picsum.photos/80';
interface ISupportProps {
  route: any;
  navigation: any;
}

const AgencyComponent: React.FC<ISupportProps> = (props) => {
  const { navigation } = props;

  const [data, setData] = useState<any[]>([1, 2, 3, 4, 5]);
  const [searchText, setSearchText] = useState<string>('');
  const onChangeText = (e: string) => {
    setSearchText(e);
  };
  const pressItem = () => {};
  const renderItem = ({ item, index }: RenderItemProps) => {
    return (
      <Block
        style={{
          borderRadius: scale(10),
          borderWidth: 0.5,
          padding: scale(16),
          marginBottom: scale(20),
          borderColor: Colors.COLOR_5,
        }}>
        <Block direction="row" align="center">
          <ImageRemote source={url} style={{ width: scale(50), height: scale(50), borderRadius: scale(4) }} />
          <Block mgLeft={10}>
            <Text fWeight="600">Đại lý Nam Khánh</Text>
            <Text mgTop={8}>ID: 100001111</Text>
            <Text mgTop={8}>Địa chỉ: Hà Nội</Text>
          </Block>
        </Block>
        <Block
          direction="row"
          align="center"
          justify="space-between"
          bgColor={'#d1d4d9'}
          pdHorizontal={16}
          pdVertical={10}
          mgTop={10}>
          <Text>Số lượng thiết bị: 5</Text>
          <Text>Offline: 0</Text>
          <Text>Online: 5</Text>
        </Block>
        <Block mgTop={10}>
          <Text fWeight="600">Quy định:</Text>
          <Text>Dùng thử: 5 phút</Text>
          <Text>Trả phí: 10,000đ / 60 phút đầu tiên ; 30,000đ cho mỗi 24h... </Text>
        </Block>
      </Block>
    );
  };
  const ItemSeparatorComponent = () => <Block h={2} />;

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={'Đại lý'} enableGoBack={false} />
      <Block bgColor="white" flex mgTop={10} pdHorizontal={16}>
        <CustomFlatlist
          showsVerticalScrollIndicator={false}
          data={data}
          renderItem={renderItem}
          ItemSeparatorComponent={ItemSeparatorComponent}
          ListEmptyComponent={<ListEmpty title={translate('inbox.no_data')} />}
          ListHeaderComponent={
            <ListHeaderComponent searchText={searchText} onChangeText={onChangeText} dataLength={data.length} />
          }
        />
      </Block>
    </SafeWrapper>
  );
};

export const AgencyScreen = memo(AgencyComponent, isEqual);
const ListHeaderComponent = memo((props: any) => {
  const { searchText, dataLength, onChangeText } = props;

  if (dataLength === 0) return null;
  return (
    <Block pdHorizontal={10} pdBottom={16}>
      <Input
        iconRight={IconSearch}
        onChangeText={onChangeText}
        styleWrapInput={styles.inputHeader}
        placeholder={'Search'}
      />
    </Block>
  );
});
