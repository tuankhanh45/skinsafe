import React, { memo } from 'react';
import { ScrollView } from 'native-base';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { scale } from '@utils';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { ICreateAccountState } from '@configs';
import { Colors, FontSize, Fonts } from '@themes';
import { IconCartProfile, translate } from '@assets';
import { ContentProfile, HeaderProfile } from './components';
import { Block, WrapperImageBg, Touchable, Text } from '@components';

interface IProfileProps {
  route: any;
  navigation: any;
}

const ProfileComponent: React.FC<IProfileProps> = (props) => {
  const { navigation } = props;
  const token = useAppStore((state: ICreateAccountState) => state?.account?.token);

  if (!token) {
    return (
      <WrapperImageBg enabledGoBack={false}>
        <Block h={200}>
          <Block direction="row" justify="space-between" mgHorizontal={16} pdTop={16}>
            <Block w={50} />
            <Touchable
              style={styles.coverBtn}
              onPress={() => navigation.navigate(SCREENS.AUTH_STACK, { screen: SCREENS.LOGIN })}>
              <Text text={translate('profile.login_register') + ' >'} style={styles.txtBtn} />
            </Touchable>
          </Block>
        </Block>
        <Block bgColor={Colors.WHITE} flex align="center" pdTop={40}>
          <IconCartProfile />
          <Block direction="row">
            <Text style={[styles.des, { color: Colors.COLOR_1 }]}>Đăng nhập / Tạo tài khoản</Text>
            <Text style={styles.des}> để nhận ưu đãi</Text>
          </Block>
        </Block>
      </WrapperImageBg>
    );
  }

  return (
    <WrapperImageBg enabledGoBack={false}>
      <HeaderProfile />
      {/* <ScrollView showsVerticalScrollIndicator={false}> */}
      <ContentProfile navigation={navigation} />
      {/* </ScrollView> */}
    </WrapperImageBg>
  );
};
export const ProfileScreen = memo(ProfileComponent, isEqual);
const styles = StyleSheet.create({
  coverBtn: {
    marginTop: scale(6),
    backgroundColor: '#FF73C2',
    borderRadius: 500,
    paddingVertical: scale(3),
    paddingHorizontal: scale(10),
  },
  txtBtn: { fontSize: FontSize.size15, fontWeight: '400', color: Colors.WHITE, fontFamily: Fonts.BRANDING },

  coverBox: {
    backgroundColor: '#FEE4A1',
    borderRadius: scale(10),
    paddingHorizontal: scale(14.5),
    paddingVertical: scale(11.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  des: { marginTop: scale(3), fontFamily: Fonts.MEDIUM },
});
