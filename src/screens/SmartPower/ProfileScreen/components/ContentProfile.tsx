import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import {
  IconDone,
  IconRate,
  IconBought,
  IconViewed,
  IconFollow,
  IconSupport,
  IconVoucher,
  IconShipping,
  IconFavorite,
  IconAddressNote,
  IconWalletProfile,
  IconPackageConfirm,
  IconPetCareProfile,
  IconCart2,
  IconWallet,
} from '@assets';
import { scale } from '@utils';
import { Block } from '@components';
import { BoxItem } from './BoxItem';
import { SCREENS } from '@navigation';
import LinearGradient from 'react-native-linear-gradient';

interface IContentProfileProps {
  navigation: any;
}
const option1 = [
  {
    title: 'Đơn hàng',
    color: '#FDE8E8',
    icon: <IconPackageConfirm />,
    screen: SCREENS.ORDER,
  },
  {
    title: 'Quét mã',
    color: '#FDE8E8',
    icon: <IconDone />,
    screen: SCREENS.QR,
  },
  {
    title: 'Hỗ trợ',
    color: '#FDE8E8',
    icon: <IconSupport />,
    screen: SCREENS.SUPPORT,
  },
];
const option2 = [
  // {
  //   title: 'Ví của tôi',
  //   color: '#FDE8E8',
  //   icon: <IconWalletProfile />,
  //   screen: SCREENS.MY_WALLET,
  // },
  {
    title: 'Ưu đãi',
    color: '#FDE8E8',
    icon: <IconVoucher />,
    screen: SCREENS.VOUCHER,
  },
  {
    title: 'Liên hệ',
    color: '#FDE8E8',
    icon: <IconFollow />,
    screen: SCREENS.CONTACT,
  },
  {
    title: '',
    color: '#FDE8E8',
    icon: <IconFollow />,
    screen: SCREENS.CONTACT,
  },
];
const coverLinear = [
  'rgba(255, 222, 136, 0.33)',
  'rgba(255, 146, 139, 0.23)',
  'rgba(255, 194, 190, 0.11)',
  'rgba(98, 255, 246, 0)',
];

const ContentProfileComponent: React.FC<IContentProfileProps> = (props) => {
  const { navigation } = props;

  return (
    <Block style={styles.wrapper} bgColor="white" flex mgTop={20} pdTop={20}>
      {/* <LinearGradient colors={coverLinear} style={styles.containerLinear}> */}
      <BoxItem data={option1} navigation={navigation} />
      <BoxItem data={option2} navigation={navigation} />

      {/* </LinearGradient> */}
    </Block>
  );
};

export const ContentProfile = memo(ContentProfileComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  containerLinear: {
    borderRadius: scale(10),
    paddingTop: scale(8),
    paddingHorizontal: scale(20),
    minHeight: scale(1000),
  },
});
