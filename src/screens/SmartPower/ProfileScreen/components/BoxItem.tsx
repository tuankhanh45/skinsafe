import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { moderateScale, scale } from '@utils';
import { Colors, Fonts, FontSize } from '@themes';
import { IconArrowRight, translate } from '@assets';
import { Block, Text, Touchable } from '@components';
interface IBoxItemProps {
  data: any;
  navigation: any;
}

const BoxItemComponent: React.FC<IBoxItemProps> = (props) => {
  const { data, navigation } = props;
  const onPressItem = (item: any) => {
    console.log('press', item);
    if (item?.screen) {
      navigation.navigate(item.screen, {});
    }
  };

  return (
    <Block direction="row" align="center" mgTop={20} justify="space-around">
      {data?.map((e: any, idx: number) => (
        <Block>
          {e.title ? (
            <Touchable key={idx + 'item'} style={styles.coverItem} onPress={() => onPressItem(e)} w={100}>
              <Block bgColor={e?.color} pd={10} borderRadius={15}>
                {e?.icon}
              </Block>
              <Text text={e?.title} style={styles.txtItem} />
            </Touchable>
          ) : (
            <Block w={100} />
          )}
        </Block>
      ))}
    </Block>
  );
};

export const BoxItem = memo(BoxItemComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    marginTop: scale(28),
  },
  title: {
    fontSize: FontSize.size15,
    fontFamily: Fonts.BRANDING,
  },
  history: {
    fontSize: FontSize.size14,
    fontWeight: '400',
    fontFamily: Fonts.MEDIUM,
    marginRight: scale(6),
  },
  splash: {
    marginTop: scale(8),
    height: moderateScale(1),
  },
  coverBox: {
    flexWrap: 'wrap',
    marginHorizontal: scale(20),
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomLeftRadius: scale(10),
    borderBottomRightRadius: scale(10),
  },
  coverItem: {
    marginBottom: scale(20),
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
  },
  txtItem: {
    marginTop: scale(8),
    fontSize: FontSize.size16,
    textAlign: 'center',
  },
});
