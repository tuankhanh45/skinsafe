import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { moderateScale, scale, vScale } from '@utils';
import { ICreateAccountState } from '@configs';
import { Colors, Fonts, FontSize } from '@themes';
import { Block, Text, Touchable, ImageRemote } from '@components';
import { IconBell, IconCoinsAccumulated, IconOfferPackage, IconRectangle, IconStandard, translate } from '@assets';

const box = [
  { icon: <IconStandard />, title: 'profile.standard', content: 'profile.no_sale_off' },
  { icon: <IconCoinsAccumulated />, title: 'profile.coin_accumulated', content: 'profile.no_cost' },
  { icon: <IconOfferPackage />, title: 'profile.offer_package', content: 'profile.no_cost' },
];
interface IHeaderProfileProps {}

const HeaderProfileComponent: React.FC<IHeaderProfileProps> = (props) => {
  const navigation = useNavigation<any>();

  const user = useAppStore((state: ICreateAccountState) => state?.account?.user);

  const onPress = () => {
    return navigation.navigate(SCREENS.DETAILS_PROFILE);
  };

  const pressBtnHeader = () => {
    navigation.navigate(SCREENS.DETAILS_PROFILE);
  };

  return (
    <Block style={styles.wrapper}>
      <Block direction="row" align="center">
        <Touchable onPress={onPress} style={styles.coverAvatar}>
          <ImageRemote resizeMode={'cover'} source={user?.avatar} style={styles.avatar} />
          {/* <Block style={styles.coverCamera}>
            <IconCamera />
          </Block> */}
        </Touchable>
        <Block pdHorizontal={10} flex>
          <Text text={user?.fullname + 111} style={styles.name} />
          <Block direction="row">
            <Touchable style={styles.coverBtn} onPress={pressBtnHeader}>
              <Text text={translate('profile.info') + ' >'} style={styles.txtBtn} />
            </Touchable>
          </Block>
        </Block>
        <Touchable
          bgColor={Colors.WHITE}
          pd={5}
          borderRadius={30}
          onPress={() => {
            navigation.navigate(SCREENS.NOTIFICATION);
          }}>
          <IconBell color={Colors.COLOR_6} />
        </Touchable>
      </Block>

      {/* <IconRectangle style={styles.rec} /> */}
      {/* <Block style={styles.coverBox}>
        {box.map((e: any, idx: number) => (
          <Block direction="row" align="center" key={'item' + idx}>
            {idx === 1 ? <Block h={23} w={1} mgRight={15} bgColor={Colors.COLOR_6} /> : <Block />}
            {e?.icon}
            <Block mgLeft={2.5}>
              <Text text={translate(e?.title)} style={styles.title} />
              <Text text={translate(e?.content)} style={styles.content} />
            </Block>
            {idx === 1 ? <Block h={23} w={1} mgLeft={15} bgColor={Colors.COLOR_6} /> : <Block />}
          </Block>
        ))}
      </Block> */}
    </Block>
  );
};

export const HeaderProfile = memo(HeaderProfileComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    paddingBottom: vScale(10),
    marginHorizontal: scale(16),
  },
  avatar: {
    width: scale(60),
    height: scale(60),
    borderRadius: scale(60),
  },
  name: {
    fontSize: FontSize.size14,
    color: Colors.BLACK,
    fontFamily: Fonts.BRANDING,
  },
  coverBtn: {
    marginTop: scale(6),
    backgroundColor: '#FF73C2',
    borderRadius: 500,
    paddingVertical: scale(3),
    paddingHorizontal: scale(10),
  },
  txtBtn: {
    fontSize: FontSize.size12,
    fontWeight: '400',
    color: Colors.WHITE,
    fontFamily: Fonts.MEDIUM,
  },
  qr: {
    width: scale(60),
    height: scale(60),
    borderRadius: scale(5),
  },
  rec: {
    marginBottom: -2,
    marginTop: scale(5),
    marginLeft: scale(19),
  },
  coverBox: {
    flexDirection: 'row',
    borderRadius: scale(10),
    backgroundColor: '#FEE4A1',
    paddingVertical: scale(11.5),
    paddingHorizontal: scale(14.5),
    justifyContent: 'space-between',
  },
  coverAvatar: {
    width: scale(62),
    height: scale(62),
    borderRadius: scale(62),
    borderColor: Colors.WHITE,
    borderWidth: moderateScale(1),
  },
  coverCamera: {
    right: -4,
    bottom: 0,
    padding: scale(4),
    position: 'absolute',
    borderRadius: scale(18),
    backgroundColor: '#EBEBEB',
    borderColor: Colors.WHITE,
    borderWidth: moderateScale(1),
  },
  title: {
    fontSize: FontSize.size10,
    fontFamily: Fonts.BRANDING,
  },
  content: {
    fontSize: FontSize.size8,
    fontFamily: Fonts.MEDIUM,
  },
});
