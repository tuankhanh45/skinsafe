import { Block, Button, Text, WrapperImageBg } from '@components';
import React, { memo, useMemo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { TouchableOpacity } from 'react-native';
import styles from './styles';

type ScreenType = 'income' | 'refund';
type periodType = 'today' | 'week' | 'month';

const IncomeScreen = () => {
  const [screenType, setScreenType] = useState<ScreenType>('income');
  const [type, setType] = useState('today');

  const renderTodayStat = useMemo(
    () => (
      <Block mgTop={20}>
        <Text style={styles.rowText}>
          Số đơn: <Text>10</Text>
        </Text>
        <Text style={styles.rowText}>
          Số tiền thu: <Text>1,525,000 VND</Text>
        </Text>
      </Block>
    ),
    [],
  );

  const renderWeekStat = useMemo(
    () => (
      <Block mgTop={20}>
        <Text style={styles.rowText}>
          Số đơn: <Text>65</Text>
        </Text>
        <Text style={styles.rowText}>
          Số tiền thu: <Text>7,250,000 VND</Text>
        </Text>
      </Block>
    ),
    [],
  );

  const renderMonthStat = useMemo(
    () => (
      <Block mgTop={20}>
        <Text style={styles.rowText}>
          Số đơn: <Text>310</Text>
        </Text>
        <Text style={styles.rowText}>
          Số tiền thu: <Text>30,250,000 VND</Text>
        </Text>
      </Block>
    ),
    [],
  );

  return (
    <WrapperImageBg>
      <Block flex>
        <Block direction="row">
          <TouchableOpacity
            style={[styles.switchBtn, { borderBottomWidth: screenType === 'income' ? 2 : 0 }]}
            onPress={() => setScreenType('income')}>
            <Text>Doanh thu</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.switchBtn, { borderBottomWidth: screenType === 'refund' ? 2 : 0 }]}
            onPress={() => setScreenType('refund')}>
            <Text>Hoàn phí</Text>
          </TouchableOpacity>
        </Block>
        <Block flex bgColor="white" pdHorizontal={16}>
          <Block direction="row" justify="center" mgTop={20}>
            <TouchableOpacity
              style={[styles.typeBtn, { backgroundColor: type === 'today' ? 'red' : 'transparent' }]}
              onPress={() => setType('today')}>
              <Text style={{ color: type === 'today' ? 'white' : 'red' }}>Hôm nay</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.typeBtn, { backgroundColor: type === 'week' ? 'red' : 'transparent' }]}
              onPress={() => setType('week')}>
              <Text style={{ color: type === 'week' ? 'white' : 'red' }}>Hôm nay</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.typeBtn, { backgroundColor: type === 'month' ? 'red' : 'transparent' }]}
              onPress={() => setType('month')}>
              <Text style={{ color: type === 'month' ? 'white' : 'red' }}>Tháng</Text>
            </TouchableOpacity>
          </Block>
          {type === 'today' && renderTodayStat}
          {type === 'week' && renderWeekStat}
          {type === 'month' && renderMonthStat}
        </Block>
      </Block>
    </WrapperImageBg>
  );
};

export default index = memo(IncomeScreen, isEqual);
