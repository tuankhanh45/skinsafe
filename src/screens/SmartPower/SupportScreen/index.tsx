import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { ImageBackground } from 'react-native';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { SCREENS } from '@navigation';
import { IconSearch, images, translate } from '@assets';
import { Block, HeaderBar, Input, SafeWrapper, Text, Touchable } from '@components';

interface ISupportProps {
  route: any;
  navigation: any;
}

const LIST_QUICK_ACTION = [
  { img: images.profile, label: 'profile', screen: '' },
  { img: images.order_and_payment, label: 'order_and_payment', screen: '' },
  // { img: images.shipping, label: 'shipping', screen: '' },
  // { img: images.freeship, label: 'freeship', screen: '' },
  // { img: images.refund, label: 'refund', screen: '' },
  // { img: images.support, label: 'support', screen: '' },
];

const LIST_ORDER = ['Câu hỏi thường gặp', 'Hướng dẫn đặt hàng', 'Thanh toán'];
const LIST_SERVICE = ['Dịch vụ', 'Khuyến mãi'];
const LIST_SHIP = ['Các dịch vụ giao hàng', 'Câu hỏi thường gặp về giao nhận hàng', 'Thời gian vận chuyển và cước phí'];
const LIST_ACCOUNT = ['Bản tin'];

const SupportComponent: React.FC<ISupportProps> = (props) => {
  const { navigation } = props;

  const onPressDetail = () => {};
  const onPressRequestSupport = () => {
    return navigation.navigate(SCREENS.REQUEST_SUPPORT);
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('support.title')} />
      <KeyboardAwareScrollView>
        <Text fWeight="500" center color="COLOR_3" mgTop={40} mgHorizontal={24}>
          {translate('support.welcome_text')}
        </Text>

        <Block pdHorizontal={15} pdVertical={12}>
          <Input
            iconLeft={IconSearch}
            styleContainer={{ flex: 1 }}
            styleWrapInput={styles.wrapInput}
            placeholder={translate('cm.search')}
          />
        </Block>

        <Block style={styles.requestTitleBox}>
          <Text fWeight="500" color="COLOR_3" center>
            {translate('support.request_support')}
          </Text>
        </Block>

        <Block style={styles.quickBox}>
          {LIST_QUICK_ACTION.map((e, i) => {
            const inLastInRow = i % 3 === 2;
            return (
              <Touchable key={i} mgBottom={28} mgRight={inLastInRow ? 0 : 35} onPress={onPressRequestSupport}>
                <ImageBackground source={e.img} style={styles.quickImg}>
                  <Text size="size15" color="WHITE" fFamily="BRANDING" center>
                    {translate(`support.quick_${e.label}`)}
                  </Text>
                </ImageBackground>
              </Touchable>
            );
          })}
        </Block>

        <Block style={styles.divider} />

        <Label label={translate('support.quick_order_and_payment')} listContent={LIST_ORDER} onPress={onPressDetail} />
        <Label label={translate('support.quick_support')} listContent={LIST_SERVICE} onPress={onPressDetail} />
        {/* <Label label={translate('support.quick_shipping')} listContent={LIST_SHIP} onPress={onPressDetail} /> */}
        <Label label={translate('support.quick_profile')} listContent={LIST_ACCOUNT} onPress={onPressDetail} />
      </KeyboardAwareScrollView>
    </SafeWrapper>
  );
};

export const SupportScreen = memo(SupportComponent, isEqual);

const Label = memo(({ label, listContent, onPress }: any) => {
  return (
    <Block pdHorizontal={15}>
      <Text size="size18" fWeight="700" mgTop={29} color="COLOR_3" mgBottom={7}>
        {label}
      </Text>
      {Array.isArray(listContent) &&
        listContent.map((e, i) => {
          return (
            <Touchable key={i} pdTop={13}>
              <Text fWeight="500" style={{ color: '#BF7534' }}>
                {e}
              </Text>
            </Touchable>
          );
        })}
    </Block>
  );
});
