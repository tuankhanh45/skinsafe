import { Colors } from '@themes';
import { device, moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapInput: {
    marginTop: 0,
    height: vScale(32),
    paddingHorizontal: scale(12),
    backgroundColor: Colors.WHITE,
  },
  requestTitleBox: {
    width: '50%',
    alignSelf: 'center',
    marginTop: vScale(20),
    paddingBottom: vScale(4),
    borderColor: Colors.COLOR_9,
    borderBottomWidth: moderateScale(1),
  },
  quickBox: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingTop: vScale(16),
    paddingHorizontal: scale(15),
  },
  quickImg: {
    height: vScale(111),
    paddingBottom: vScale(10),
    justifyContent: 'flex-end',
    paddingHorizontal: scale(4),
    width: (device.width - scale(15 * 2 + 35 * 2)) / 3,
  },
  divider: {
    height: vScale(1),
    marginHorizontal: scale(15),
    backgroundColor: Colors.COLOR_9,
  },
});

export default styles;
