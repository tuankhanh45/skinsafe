import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { IconFollow, IconNext, IconSupport } from '@assets';
import { moderateScale, scale } from '@utils';
import { Block, Text, Touchable } from '@components';
import { SCREENS } from '@navigation';
import { FontSize, Fonts } from '@themes';

interface IContentProfileProps {
  navigation: any;
}
const option = [
  {
    title: 'Thông tin hỗ trợ',
    color: '#FDE8E8',
    icon: <IconSupport />,
    screen: SCREENS.SUPPORT,
  },
  {
    title: 'Liên hệ với chúng tội',
    color: '#FDE8E8',
    icon: <IconFollow />,
    screen: SCREENS.CONTACT,
  },
];

const ContentProfileComponent: React.FC<IContentProfileProps> = (props) => {
  const { navigation } = props;
  const onPressItem = (item: any) => {
    console.log('press', item);
    if (item?.screen) {
      navigation.navigate(item.screen, {});
    }
  };
  return (
    <Block pdHorizontal={20} bgColor="white" flex mgTop={20} pdTop={20}>
      {option?.map((e: any, idx: number) => (
        <Touchable mgTop={16} direction="row" align="center" key={idx + 'item'} onPress={() => onPressItem(e)}>
          <Block bgColor={e?.color} pd={10} borderRadius={15}>
            {e?.icon}
          </Block>
          <Text text={e?.title} style={styles.txtItem} />
          <IconNext />
        </Touchable>
      ))}
    </Block>
  );
};

export const ContentProfile = memo(ContentProfileComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  containerLinear: {
    borderRadius: scale(10),
    paddingTop: scale(8),
    paddingHorizontal: scale(20),
    minHeight: scale(1000),
  },
  title: {
    fontSize: FontSize.size15,
    fontFamily: Fonts.BRANDING,
  },
  history: {
    fontSize: FontSize.size14,
    fontWeight: '400',
    fontFamily: Fonts.MEDIUM,
    marginRight: scale(6),
  },
  splash: {
    marginTop: scale(8),
    height: moderateScale(1),
  },
  coverBox: {
    flexWrap: 'wrap',
    marginHorizontal: scale(20),
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomLeftRadius: scale(10),
    borderBottomRightRadius: scale(10),
  },

  txtItem: {
    marginTop: scale(8),
    fontSize: FontSize.size18,
    fontWeight: '600',
    marginLeft: 12,
    flex: 1,
  },
});
