import React, { memo, useCallback } from 'react';
import isEqual from 'react-fast-compare';
import { Alert, StyleSheet } from 'react-native';
import { Portal } from 'react-native-portalize';
import { Modalize } from 'react-native-modalize';
import { Block, Text, Touchable, Button } from '@components';
import LinearGradient from 'react-native-linear-gradient';
import { moderateScale, scale } from '@utils';
import { IconEditCalender, translate } from '@assets';
import { FontSize } from '@themes';
import { useNavigation } from '@react-navigation/native';
import { SCREENS } from '@navigation';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { ICreateAccountState } from '@configs';
import { useRequest } from 'ahooks';
import { handleResponse, requestLogout, requestRemoveUser } from '@configs';
import dayjs from 'dayjs';
interface IDetailsProfileProps {
  refInfo: any;
}
const DetailsProfileComponent: React.FC<IDetailsProfileProps> = (props) => {
  const actRemoveLoggerUser = useAppStore((state: ICreateAccountState) => state.actRemoveLoggerUser);
  const user = useAppStore((state: ICreateAccountState) => state?.account?.user);

  const navigation = useNavigation<any>();
  const gender = user?.gender === 1 ? 'Nam' : user?.gender === 0 ? 'Nữ' : 'Khác';
  const coverLinear = [
    'rgba(255, 222, 136, 0.33)',
    'rgba(255, 146, 139, 0.23)',
    'rgba(255, 194, 190, 0.11)',
    'rgba(98, 255, 246, 0)',
  ];
  const splashLinear = [
    'rgba(255, 222, 136, 1)',
    'rgba(255, 146, 139, 0.69)',
    'rgba(255, 194, 190, 0.33)',
    'rgba(98, 255, 246, 0)',
  ];
  const dot = [
    'rgba(255, 222, 136, 1)',
    'rgba(255, 146, 139, 0.69)',
    'rgba(255, 194, 190, 0.33)',
    'rgba(98, 255, 246, 0)',
  ];
  const pressEditProfile = () => {
    props?.refInfo?.current?.close();
    navigation.navigate(SCREENS.EDIT_PROFILE);
  };
  const pressChangePassword = () => {
    props?.refInfo?.current?.close();
    navigation.navigate(SCREENS.CHANGE_PASSWORD, { phone: user?.phone });
  };
  // logout
  const onPressLogout = useCallback(() => {
    Alert.alert('Xác nhận', 'Bạn chắc chắn muốn đăng xuất', [
      { text: 'Đăng xuất', onPress: () => run(), style: 'destructive' },
      { text: 'Cancel', onPress: () => {} },
    ]);
  }, []);
  const { run } = useRequest(requestLogout, {
    manual: true,
    onFinally: () => {
      actRemoveLoggerUser();
      props?.refInfo?.current?.close();
    },
  });
  // remove account
  const onPressRemoveAccount = useCallback(() => {
    Alert.alert('Xác nhận', 'Bạn chắc chắn muốn xoá tài khoản?', [
      { text: 'Xoá ngay', onPress: () => runRemoveAccount(), style: 'destructive' },
      { text: 'Cancel', onPress: () => {} },
    ]);
  }, []);
  const { run: runRemoveAccount } = useRequest(requestRemoveUser, {
    manual: true,
    onFinally: () => {
      props?.refInfo?.current?.close();
      actRemoveLoggerUser();
    },
  });

  return (
    <Portal>
      <Modalize adjustToContentHeight ref={props?.refInfo}>
        <Block bgColor={Colors.WHITE} borderRadius={10} mgBottom={70}>
          <LinearGradient colors={coverLinear} style={styles.containerLinear}></LinearGradient>
          <Block mgTop={-255} pdHorizontal={20} pdTop={20} borderRadius={10}>
            <Block direction="row" align="center" justify="space-between">
              <Block w={18} />
              <Text text={translate('profile.info')} style={styles.title} />
              <Touchable onPress={pressEditProfile}>
                <IconEditCalender />
              </Touchable>
            </Block>
            {/* slash */}
            <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={splashLinear} style={styles.splash} />
            {/* name */}
            <Block mgTop={7}>
              <Text text={translate('profile.full_name')} style={styles.key} />
              <Text text={user?.fullname} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block>
            {/* nick */}
            {/* <Block mgTop={7}>
              <Text text={translate('profile.nickname')} style={styles.key} />
              <Text text={user?.nickname} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block> */}
            {/* birth  */}
            <Block mgTop={7}>
              <Text text={translate('profile.birthday')} style={styles.key} />
              <Text text={dayjs(user?.birthday).format('DD-MM-YYYY')} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block>
            {/* passport  */}
            <Block mgTop={7}>
              <Text text={translate('profile.passport')} style={styles.key} />
              <Text text={user?.passport} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block>
            {/* gender */}
            <Block mgTop={7}>
              <Text text={translate('profile.gender')} style={styles.key} />
              <Text text={gender} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block>
            {/* country  */}
            {/* <Block mgTop={7}>
              <Text text={translate('profile.country')} style={styles.key} />
              <Text text={user?.country} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block> */}
            {/* phone */}
            <Block mgTop={7}>
              <Text text={translate('profile.phone')} style={styles.key} />
              <Text text={user?.phone} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block>
            {/* email */}
            <Block mgTop={7}>
              <Text text={translate('profile.email')} style={styles.key} />
              <Text text={user?.email} style={styles.value} />
              <LinearGradient start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} colors={dot} style={styles.splash} />
            </Block>
            {/* btn change password */}
            <Touchable w={120} onPress={pressChangePassword} mgTop={18}>
              <Text text={translate('profile.change_password')} style={styles.change_password} />
            </Touchable>
            <Button onPress={onPressLogout} marginTop={scale(46)}>
              {translate('profile.logout')}
            </Button>
          </Block>
        </Block>
      </Modalize>
    </Portal>
  );
};

export const DetailsProfile = memo(DetailsProfileComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  containerLinear: { height: scale(255), borderRadius: 10 },
  title: { fontWeight: '600', fontSize: FontSize.size16 },
  splash: {
    marginTop: scale(12),
    height: moderateScale(1),
  },
  key: { fontWeight: '500', fontSize: FontSize.size16 },
  value: { fontWeight: '500', fontSize: FontSize.size14, color: Colors.COLOR_1, marginTop: scale(3) },
  change_password: { fontWeight: '600', fontSize: FontSize.size16, color: Colors.COLOR_7 },
});
