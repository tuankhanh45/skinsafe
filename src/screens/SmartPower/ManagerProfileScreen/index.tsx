import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { ImageBackground } from 'react-native';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { SCREENS } from '@navigation';
import { IconCartProfile, IconSearch, images, translate } from '@assets';
import { Block, HeaderBar, Input, SafeWrapper, Text, Touchable, WrapperImageBg } from '@components';
import { ContentProfile, HeaderProfile } from './components';
import { useAppStore } from '@store';
import { ICreateAccountState } from '@configs';
import { Colors } from '@themes';

interface ISupportProps {
  route: any;
  navigation: any;
}

const ManagerProfileComponent: React.FC<ISupportProps> = (props) => {
  const { navigation } = props;
  return (
    <WrapperImageBg enabledGoBack={false}>
      <HeaderProfile />
      <ContentProfile navigation={navigation} />
    </WrapperImageBg>
  );
};

export const ManagerProfileScreen = memo(ManagerProfileComponent, isEqual);
