import React, { memo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { Linking, StyleSheet } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { moderateScale, scale } from '@utils';
import { Colors, Fonts, FontSize } from '@themes';
import { Block, Button, Text, Touchable, WrapperImageBg } from '@components';
import { Image } from 'react-native-svg';
import { IconCancel } from '@assets';
interface IQRScanProps {
  data: any;
  navigation: any;
  route: any;
}

const QRScanComponent: React.FC<IQRScanProps> = (props) => {
  const { navigation } = props;
  const [disable, setDisable] = useState(true);
  const [data, setData] = useState<any>({});

  const onPressItem = (item: any) => {
    console.log('press', item);
    if (item?.title === 'Quét mã') {
      console.log('quét');
    }
    if (item?.screen) {
      navigation.navigate(item.screen, {});
    }
  };
  const onSuccess = (e: any) => {
    // Linking.openURL(e.data).catch((err: any) => console.error('An error occured', err));
    console.log('code', e);
    setDisable(false);
    setData(e);
  };
  const goBack = () => {
    navigation.goBack();
  };
  const Top = () => (
    <Block pdHorizontal={16} mgTop={40} flex w={'100%'}>
      <Touchable onPress={goBack}>
        <IconCancel />
      </Touchable>
      <Text style={{ textAlign: 'center', fontWeight: '600' }}>Quét mã để thanh toán </Text>
      <Text numberOfLines={3} style={{ marginTop: 10, marginBottom: 16 }}>
        Code: {data?.rawData || data?.data}
      </Text>
    </Block>
  );
  const Bottom = () => (
    <Block pdHorizontal={5} pdTop={20}>
      <Touchable
        disabled={!disable}
        bgColor={disable ? Colors.COLOR_9 : Colors.COLOR_16}
        borderRadius={10}
        pdHorizontal={16}
        pdVertical={10}>
        <Text style={{ color: 'white' }}>Thanh toán</Text>
      </Touchable>
    </Block>
  );
  return (
    <QRCodeScanner
      onRead={onSuccess}
      // flashMode={RNCamera.Constants.FlashMode.torch}
      topContent={<Top />}
      bottomContent={<Bottom />}
    />
  );
};

export const QRScanScreen = memo(QRScanComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    marginTop: scale(28),
  },
  title: {
    fontSize: FontSize.size15,
    fontFamily: Fonts.BRANDING,
  },
  history: {
    fontSize: FontSize.size14,
    fontWeight: '400',
    fontFamily: Fonts.MEDIUM,
    marginRight: scale(6),
  },
  splash: {
    marginTop: scale(8),
    height: moderateScale(1),
  },
  coverBox: {
    flexWrap: 'wrap',
    marginHorizontal: scale(20),
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomLeftRadius: scale(10),
    borderBottomRightRadius: scale(10),
  },
  coverItem: {
    marginBottom: scale(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtItem: {
    marginTop: scale(8),
    fontSize: FontSize.size16,
    textAlign: 'center',
  },
});
