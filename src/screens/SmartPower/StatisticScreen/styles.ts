import { StyleSheet } from 'react-native';
import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.COLOR_4,
  },
  container: {
    flex: 1,
    marginTop: vScale(58),
    paddingTop: vScale(32),
    paddingHorizontal: scale(16),
    backgroundColor: Colors.COLOR_4,
    borderTopLeftRadius: moderateScale(25),
    borderTopRightRadius: moderateScale(25),
  },
  switchBtn: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    height: scale(50),
    borderColor: 'red',
  },
  typeBtn: {
    height: scale(50),
    paddingHorizontal: scale(20),
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: scale(10),
    borderWidth: 1,
    borderColor: 'red',
    borderRadius: scale(10),
  },
  rowText: {
    marginTop: scale(10),
    fontWeight: '700',
  },
  valueText: {},
});

export default styles;
