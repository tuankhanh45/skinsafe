import React, { memo, useMemo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { ImageBackground, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { SCREENS } from '@navigation';
import { IconSearch, images, translate } from '@assets';
import { Block, HeaderBar, Input, SafeWrapper, Text, Image } from '@components';
import { Colors } from '@themes';
interface ISupportProps {
  route: any;
  navigation: any;
}
type ScreenType = 'income' | 'refund';
const StatisticComponent: React.FC<ISupportProps> = (props) => {
  const { navigation } = props;

  const [screenType, setScreenType] = useState<ScreenType>('income');
  const [type, setType] = useState('today');

  const renderTodayStat = useMemo(
    () => (
      <Block mgTop={20}>
        <Text style={styles.rowText}>
          Số đơn: <Text>10</Text>
        </Text>
        <Text style={styles.rowText}>
          Số tiền thu: <Text>1,525,000 VND</Text>
        </Text>
      </Block>
    ),
    [],
  );

  const renderWeekStat = useMemo(
    () => (
      <Block mgTop={20}>
        <Text style={styles.rowText}>
          Số đơn: <Text>65</Text>
        </Text>
        <Text style={styles.rowText}>
          Số tiền thu: <Text>7,250,000 VND</Text>
        </Text>
      </Block>
    ),
    [],
  );

  const renderMonthStat = useMemo(
    () => (
      <Block mgTop={20}>
        <Text style={styles.rowText}>
          Số đơn: <Text>310</Text>
        </Text>
        <Text style={styles.rowText}>
          Số tiền thu: <Text>30,250,000 VND</Text>
        </Text>
      </Block>
    ),
    [],
  );

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={'Thống kê'} enableGoBack={false} />
      <Block pdHorizontal={16} pdTop={20}>
        <Block
          style={{ borderWidth: 0.5 }}
          direction="row"
          borderRadius={8}
          borderColor={Colors.COLOR_5}
          pdVertical={30}
          pdHorizontal={10}>
          <Block justify="center" align="center" flex>
            <Image source="icStack" style={{ width: 45, height: 45, tintColor: Colors.COLOR_6 }} />
            <Text fWeight="600" mgTop={16}>
              Doanh thu
            </Text>
            <Text mgTop={8}>61,230,000 VND</Text>
          </Block>
          <Block justify="center" align="center" flex>
            <Image source="icIncome" style={{ width: 45, height: 45, tintColor: Colors.COLOR_6 }} />
            <Text fWeight="600" mgTop={16}>
              Tích luỹ
            </Text>
            <Text mgTop={8}>61,230,000 VND</Text>
          </Block>
        </Block>
        {/*  */}
        <Block direction="row" justify="space-between" mgTop={35} mgHorizontal={30}>
          <Block align="center" justify="center">
            <Text>Tổng số trạm</Text>
            <Text mgTop={10} fWeight="600">
              75
            </Text>
          </Block>
          <Block h={40} w={2} bgColor={Colors.COLOR_5} />
          <Block align="center">
            <Text>Tổng số thiết bị</Text>
            <Text mgTop={10} fWeight="600">
              2100
            </Text>
          </Block>
        </Block>
        {/*  */}
        <Block mgTop={20}>
          <Block direction="row">
            <TouchableOpacity
              style={[styles.switchBtn, { borderBottomWidth: screenType === 'income' ? 3 : 0 }]}
              onPress={() => setScreenType('income')}>
              <Text>Doanh thu</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.switchBtn, { borderBottomWidth: screenType === 'refund' ? 3 : 0 }]}
              onPress={() => setScreenType('refund')}>
              <Text>Hoàn phí</Text>
            </TouchableOpacity>
          </Block>
          <Block flex bgColor="white" pdHorizontal={16}>
            <Block direction="row" justify="center" mgTop={20}>
              <TouchableOpacity
                style={[styles.typeBtn, { backgroundColor: type === 'today' ? 'red' : 'transparent' }]}
                onPress={() => setType('today')}>
                <Text style={{ color: type === 'today' ? 'white' : 'red' }}>Hôm nay</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.typeBtn, { backgroundColor: type === 'week' ? 'red' : 'transparent' }]}
                onPress={() => setType('week')}>
                <Text style={{ color: type === 'week' ? 'white' : 'red' }}>Hôm nay</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.typeBtn, { backgroundColor: type === 'month' ? 'red' : 'transparent' }]}
                onPress={() => setType('month')}>
                <Text style={{ color: type === 'month' ? 'white' : 'red' }}>Tháng</Text>
              </TouchableOpacity>
            </Block>
            {type === 'today' && renderTodayStat}
            {type === 'week' && renderWeekStat}
            {type === 'month' && renderMonthStat}
          </Block>
        </Block>
      </Block>
    </SafeWrapper>
  );
};

export const StatisticScreen = memo(StatisticComponent, isEqual);
