import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { Block, HeaderBar, Image, SafeWrapper, Text, Touchable } from '@components';

interface IContactProps {
  route: any;
  navigation: any;
}

const LIST = [
  { label: 'Email', content: 'support@tmgs.vn' },
  { label: 'Địa chỉ', content: 'Số 33 Ngõ 99 Nguyễn Khang, Cầu Giấy, Hà Nội.' },
  { label: 'Điện thoại', content: '0243 204 7808' },
];

const ContactComponent: React.FC<IContactProps> = (props) => {
  const { navigation } = props;

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={'Liên hệ'} />
      <KeyboardAwareScrollView>
        <Text fWeight="500" center color="COLOR_3" mgTop={40} mgHorizontal={24}>
          {'Kết nối ngay với chúng tôi để nhận được nhiều ưu đãi và giải đáp các thắc mắc '}
        </Text>
        <Block mgTop={10} mgHorizontal={16}>
          <Block align="center" justify="center">
            <Image source="logo" style={{ width: 100, height: 100 }} />
          </Block>
          {LIST.map((e: any) => (
            <Block direction="row" mgTop={10}>
              <Text style={{ flex: 1 }}>{e?.label}:</Text>
              <Text numberOfLines={2} style={{ flex: 3 }}>
                {e?.content}
              </Text>
            </Block>
          ))}
        </Block>
      </KeyboardAwareScrollView>
    </SafeWrapper>
  );
};

export const ContactScreen = memo(ContactComponent, isEqual);

const Label = memo(({ label, listContent, onPress }: any) => {
  return (
    <Block pdHorizontal={15}>
      <Text size="size18" fWeight="700" mgTop={29} color="COLOR_3" mgBottom={7}>
        {label}
      </Text>
      {Array.isArray(listContent) &&
        listContent.map((e, i) => {
          return (
            <Touchable key={i} pdTop={13}>
              <Text fWeight="500" style={{ color: '#BF7534' }}>
                {e}
              </Text>
            </Touchable>
          );
        })}
    </Block>
  );
});
