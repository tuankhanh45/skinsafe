import isEqual from 'react-fast-compare';
import React, { memo, useState } from 'react';

import styles from './styles';
import { RenderItemProps } from '@configs';
import { IconSearch, translate } from '@assets';
import {
  Block,
  CustomFlatlist,
  Touchable,
  Input,
  ListEmpty,
  SafeWrapper,
  HeaderBar,
  Text,
  ImageRemote,
} from '@components';
import { scale } from '@utils';
import { Colors } from '@themes';
const url = 'https://picsum.photos/81';

const VoucherScreen = () => {
  const [data, setData] = useState<any[]>([1, 2, 3, 4, 5]);
  const [searchText, setSearchText] = useState<string>('');
  const onChangeText = (e: string) => {
    setSearchText(e);
  };
  const pressItem = () => {};
  const renderItem = ({ item, index }: RenderItemProps) => {
    return (
      <Touchable
        direction="row"
        align="center"
        onPress={pressItem}
        style={{
          borderRadius: scale(10),
          borderWidth: 0.5,
          padding: scale(16),
          marginBottom: scale(20),
          borderColor: Colors.COLOR_5,
        }}>
        <ImageRemote source={url} style={{ width: scale(50), height: scale(50), borderRadius: scale(8) }} />
        <Block mgLeft={10}>
          <Text fWeight="600">Giảm giá 20%</Text>
          <Text mgTop={8}>Giá: 15000 đ</Text>
          <Text mgTop={8}>Áp dụng với mọi sản phẩm ...</Text>
        </Block>
      </Touchable>
    );
  };
  const ItemSeparatorComponent = () => <Block h={2} />;
  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={'Danh sách thiết bị'} enableGoBack={false} />
      <Block bgColor="white" flex mgTop={10} pdHorizontal={16}>
        <CustomFlatlist
          showsVerticalScrollIndicator={false}
          data={data}
          renderItem={renderItem}
          ItemSeparatorComponent={ItemSeparatorComponent}
          ListEmptyComponent={<ListEmpty title={translate('inbox.no_data')} />}
          ListHeaderComponent={
            <ListHeaderComponent searchText={searchText} onChangeText={onChangeText} dataLength={data.length} />
          }
        />
      </Block>
    </SafeWrapper>
  );
};

export default index = memo(VoucherScreen, isEqual);
const ListHeaderComponent = memo((props: any) => {
  const { searchText, dataLength, onChangeText } = props;

  if (dataLength === 0) return null;
  return (
    <Block pdHorizontal={10} pdBottom={16}>
      <Input
        iconRight={IconSearch}
        onChangeText={onChangeText}
        styleWrapInput={styles.inputHeader}
        placeholder={'Search'}
      />
    </Block>
  );
});
