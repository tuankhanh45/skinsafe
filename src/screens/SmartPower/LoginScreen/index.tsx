import size from 'lodash/size';
import { useRequest } from 'ahooks';
import { Alert } from 'react-native';
import isEqual from 'react-fast-compare';
import { useForm } from 'react-hook-form';
import React, { memo, useState } from 'react';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { translate } from '@assets';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { ICreateAccountState } from '@configs';
import { scale, showToastSuccess, useValidateForm } from '@utils';
import { handleResponse, requestLogin } from '@configs';
import { IconCall, IconEyeClose, IconEyeOpen, IconKey } from '@assets';
import { Block, Button, Text, WrapperImageBg, TextField, Form, Touchable, Loading } from '@components';
import { CommonActions } from '@react-navigation/native';

interface ILoginProps {
  route: any;
  navigation: any;
}

const LoginComponent: React.FC<ILoginProps> = (props) => {
  const { navigation, route } = props;
  const cbFunc = route?.params?.cbFunc;

  const actSaveInfoLogin = useAppStore((state: ICreateAccountState) => state.actSaveInfoLogin);
  const actSaveLoggerUser = useAppStore((state: ICreateAccountState) => state.actSaveLoggerUser);
  const actSaveRole = useAppStore((state: any) => state.actSaveRole);
  const [isHidePass, setIsHidePass] = useState<boolean>(true);

  // form
  const formMethods = useForm();
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;
  const onErrors = (errors: any) => {};

  const onSubmit = (form: any) => {
    // actSaveInfoLogin({ phone: form.phone, password: form.password });
    // run({ phone: form.phone, password: form.password });
    const customer = {
      token: 'token1111',
      user: {
        fullname: 'Nguyễn Văn Nam',
        birthday: '2000/01/01',
        gender: 9,
        phone: '0912345678',
        email: 'Nam123@gmail.com',
        avatar: 'https://vcdn-vnexpress.vnecdn.net/2021/03/02/103650164-731814290963011-1374-5806-7233-1614677857.jpg',
        role: 'customer',
      },
    };

    const station = {
      token: 'token1111',
      user: {
        fullname: 'Nguyễn Văn Nam',
        birthday: '2000/01/01',
        gender: 9,
        phone: '0912345678',
        email: 'Nam123@gmail.com',
        avatar: 'https://vcdn-vnexpress.vnecdn.net/2021/03/02/103650164-731814290963011-1374-5806-7233-1614677857.jpg',
        role: 'station',
      },
    };

    const investor = {
      token: 'token1111',
      user: {
        fullname: 'Nguyễn Văn Nam',
        birthday: '2000/01/01',
        gender: 9,
        phone: '0912345678',
        email: 'Nam123@gmail.com',
        avatar: 'https://vcdn-vnexpress.vnecdn.net/2021/03/02/103650164-731814290963011-1374-5806-7233-1614677857.jpg',
        role: 'investor',
      },
    };

    const agency = {
      token: 'token1111',
      user: {
        fullname: 'Nguyễn Văn Nam',
        birthday: '2000/01/01',
        gender: 9,
        phone: '0912345678',
        email: 'Nam123@gmail.com',
        avatar: 'https://vcdn-vnexpress.vnecdn.net/2021/03/02/103650164-731814290963011-1374-5806-7233-1614677857.jpg',
        role: 'agency',
      },
    };

    console.log('login', form);
    if (form.phone === '0912345678' && form.password === 'Abc@1234') {
      actSaveLoggerUser(customer);
      return navigation.dispatch(
        CommonActions.reset({
          routes: [{ name: SCREENS.MAIN_STACK }],
        }),
      );
    }
    if (form.phone === '0912666888' && form.password === 'Abc@1234') {
      actSaveLoggerUser(investor);
      return navigation.dispatch(
        CommonActions.reset({
          routes: [{ name: SCREENS.MAIN_STACK }],
        }),
      );
    }
    if (form.phone === '0912333444' && form.password === 'Abc@1234') {
      actSaveLoggerUser(agency);
      return navigation.dispatch(
        CommonActions.reset({
          routes: [{ name: SCREENS.MAIN_STACK }],
        }),
      );
    }
    if (form.phone === '0912686868' && form.password === 'Abc@1234') {
      actSaveLoggerUser(station);
      return navigation.dispatch(
        CommonActions.reset({
          routes: [{ name: SCREENS.MAIN_STACK }],
        }),
      );
    } else {
      return Alert.alert('Tài khoản không chính xác');
    }
  };

  const { run, loading } = useRequest(requestLogin, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        formMethods.setValue('password', '');
        return Alert.alert('Lỗi', err?.message ?? 'Đăng nhập thất bại. Xin thử lại');
      } else {
        actSaveLoggerUser(res);
        if (cbFunc) {
          showToastSuccess('Đăng nhập thành công');
          return cbFunc();
        }
        navigation.goBack();
      }
    },
    onError: (err) => {
      formMethods.setValue('password', '');
      return Alert.alert('Lỗi', err?.message ?? 'Đăng nhập thất bại. Xin thử lại');
    },
  });

  return (
    <WrapperImageBg style={styles.wrapper}>
      {loading && <Loading />}
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <Text style={styles.title}>{translate('login.title')}</Text>
        <Text style={styles.des} text={translate('login.description')} />
        <Block mgTop={49}>
          <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
            <TextField
              name={'phone'}
              returnKeyType={'done'}
              placeholder={translate('placeholder.phone')}
              keyboardType="number-pad"
              iconLeft={() => <IconCall />}
            />
            <TextField
              name={'password'}
              returnKeyType={'done'}
              placeholder={translate('placeholder.password')}
              iconLeft={() => <IconKey />}
              secureTextEntry={isHidePass}
              iconRight={() => (isHidePass ? <IconEyeClose /> : <IconEyeOpen />)}
              onPressIcon={() => setIsHidePass(!isHidePass)}
            />
          </Form>
        </Block>

        <Touchable pdTop={12} onPress={() => navigation.navigate(SCREENS.FORGOT_PASSWORD)}>
          <Text text={translate('profile.forgot')} style={styles.forgot} />
        </Touchable>
        <Button
          isLoading={loading}
          marginTop={scale(46)}
          disabled={!!size(errors) || loading}
          onPress={formMethods.handleSubmit(onSubmit, onErrors)}>
          {translate('cm.next')}
        </Button>

        <Block direction="row" mgTop={40}>
          <Text text={translate('profile.no_account')} style={styles.suggest} />
          <Touchable pdLeft={12} onPress={() => navigation.navigate(SCREENS.REGISTER)}>
            <Text text={translate('profile.register_now')} style={styles.register} />
          </Touchable>
        </Block>
        <Block h={40} />
      </KeyboardAwareScrollView>
    </WrapperImageBg>
  );
};

export const LoginScreen = memo(LoginComponent, isEqual);
