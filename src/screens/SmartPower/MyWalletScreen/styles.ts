import { Colors } from '@themes';
import { moderateScale, scale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  avatar: {
    width: scale(58),
    height: scale(58),
    borderRadius: moderateScale(58),
  },
  avatarContainer: {
    width: scale(60),
    height: scale(60),
    marginLeft: scale(10),
    borderColor: Colors.WHITE,
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(60),
  },
  textMore: {
    color: Colors.COLOR_1,
    textDecorationLine: 'underline',
  },
});

export default styles;
