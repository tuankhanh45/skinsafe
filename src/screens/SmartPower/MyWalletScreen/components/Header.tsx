import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { IconHistory } from '@assets';
import { SCREENS } from '@navigation';
import { Block, Image, Text, Touchable } from '@components';
import { convertPhone, currencyFormat, formatMoney, moderateScale, scale, vScale } from '@utils';

interface IHeaderProps {
  user: any;
  navigation: any;
}

const HeaderCom: React.FC<IHeaderProps> = (props) => {
  const { navigation, user } = props;

  const onPressCoin = () => {
    navigation.navigate(SCREENS.VOUCHER, { initTab: 1 });
  };

  return (
    <Block style={styles.wrapper}>
      <Text size="size17" fWeight="500" color="COLOR_7">
        {convertPhone(user?.phone)}
      </Text>

      <Block direction="row" pdTop={12}>
        <Block flex align="flex-start">
          <Text size="size18" fWeight="500">
            {currencyFormat(1100000, 'vnđ')}
          </Text>
          <Touchable style={styles.recharge}>
            <Text size="size12" fWeight="500" color="COLOR_4">
              Nạp tiền
            </Text>
          </Touchable>
        </Block>

        <Block flex align="flex-end">
          <Block direction="row" align="center">
            <Text size="size18" fWeight="500" mgRight={5}>
              {formatMoney('1100000')}
            </Text>
            <Image source="coin" style={styles.coin} />
          </Block>
          <Touchable style={styles.recharge} onPress={onPressCoin}>
            <Text size="size12" fWeight="500" color="COLOR_4">
              Đổi xu
            </Text>
          </Touchable>
        </Block>
      </Block>

      <Touchable style={styles.historyBtn}>
        <IconHistory />
        <Text size="size12" fWeight="500" mgLeft={5}>
          Tra cứu giao dịch
        </Text>
      </Touchable>
    </Block>
  );
};

export const Header = memo(HeaderCom, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    borderColor: '#A9A9A9',
    paddingVertical: vScale(4),
    marginHorizontal: scale(15),
    paddingHorizontal: scale(18),
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(11),
  },
  coin: {
    width: scale(16),
    height: scale(16),
  },
  recharge: {
    marginTop: vScale(21),
    paddingVertical: vScale(3),
    paddingHorizontal: scale(20),
    borderRadius: moderateScale(28),
    backgroundColor: Colors.COLOR_1,
  },
  historyBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: vScale(14),
    alignSelf: 'flex-start',
    paddingVertical: vScale(3),
    borderColor: Colors.COLOR_9,
    paddingHorizontal: scale(16),
    borderWidth: moderateScale(0.3),
    borderRadius: moderateScale(28),
  },
});
