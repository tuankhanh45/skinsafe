import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

import styles from './styles';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { convertPhone } from '@utils';
import { Header } from './components';
import { IconArrowLeft } from '@assets';
import { RenderItemProps } from '@configs';
import { Block, Text, WrapperImageBg, Touchable, ImageRemote, CustomFlatlist, HistoryItem } from '@components';

interface IMyWalletProps {
  route: any;
  navigation: any;
}

const MyWalletComponent: React.FC<IMyWalletProps> = (props) => {
  const { navigation } = props;
  const user = useAppStore((state) => state?.account?.user);

  const onPressGoBack = () => {
    navigation.canGoBack() && navigation.goBack();
  };

  const renderItem = ({ item, index }: RenderItemProps) => {
    return <HistoryItem index={index} data={item} />;
  };

  return (
    <WrapperImageBg style={styles.wrapper} enabledGoBack={false}>
      <Block pdHorizontal={15} pdTop={15} pdBottom={11} direction={'row'} align="center">
        <Touchable pd={4} onPress={onPressGoBack}>
          <IconArrowLeft />
        </Touchable>
        <ImageRemote
          resizeMode="cover"
          style={styles.avatar}
          source={user?.avatar}
          containerStyle={styles.avatarContainer}
        />
        <Block flex pdLeft={15}>
          <Text size="size18" fFamily="BRANDING" color="BLACK" mgBottom={4}>
            {user?.fullname}
          </Text>

          <Block pdVertical={3} pdHorizontal={10} bgColor={'#FF73C2'} borderRadius={500} alignSelf="flex-start">
            <Text color="WHITE" size="size12">
              {user?.phone ? convertPhone(user?.phone) : user?.email} {'>'}
            </Text>
          </Block>
        </Block>
      </Block>

      <Header navigation={navigation} user={user} />

      <CustomFlatlist
        data={[1, 2]}
        renderItem={renderItem}
        ListHeaderComponent={<ListHeaderComponent navigation={navigation} />}
      />
    </WrapperImageBg>
  );
};

export const MyWalletScreen = memo(MyWalletComponent, isEqual);

const ListHeaderComponent = memo(({ navigation }: any) => {
  const onPressMore = () => navigation.navigate(SCREENS.HISTORY_TRANSACTION);

  return (
    <Block direction="row" align="center" justify="space-between" pdHorizontal={15} pdTop={14} mgBottom={12}>
      <Text fWeight="500">Lịch sử giao dịch</Text>
      <Touchable onPress={onPressMore}>
        <Text style={styles.textMore}>Xem tất cả</Text>
      </Touchable>
    </Block>
  );
});
