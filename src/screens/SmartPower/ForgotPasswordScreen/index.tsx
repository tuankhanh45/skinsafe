import React, { memo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import size from 'lodash/size';
import styles from './styles';
import { translate } from '@assets';
import { Button, Text, WrapperImageBg, TextField, Form, Block } from '@components';
import { useForm } from 'react-hook-form';
import { scale, useValidateForm } from '@utils';
import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { images } from '@assets';
import { handleResponse, requestEnterPhone, requestCheckExistPhone } from '@configs';
import { useRequest } from 'ahooks';
import { Alert } from 'react-native';
import { Loading } from '@components';
interface IForgotPasswordProps {
  route: any;
  navigation: any;
}

const ForgotPasswordComponent: React.FC<IForgotPasswordProps> = (props) => {
  const { navigation } = props;
  // form
  const formMethods = useForm();
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;
  const onErrors = (errors: any) => {};
  const onSubmit = (form: any) => {
    return navigation.navigate(SCREENS.OTP_CONFIRM, {
      phone: formMethods.getValues('phone'),
      type: SCREENS.FORGOT_PASSWORD,
    });
    console.log('submit', form);
    const params = { phone: form?.phone ?? '' };
    run(params);
  };
  // active phone
  const { run: run, loading } = useRequest(requestCheckExistPhone, {
    manual: true,
    onSuccess: (data) => {
      // test flow
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
      }
      return runGetOTP({ phone: formMethods.getValues('phone') });
    },
    onError: (err) => {
      return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
    },
  });
  // resendOTP
  const { run: runGetOTP, loading: loadingGetOTP } = useRequest(requestEnterPhone, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
      }
      return navigation.navigate(SCREENS.OTP_CONFIRM, {
        phone: formMethods.getValues('phone'),
        type: SCREENS.FORGOT_PASSWORD,
      });
    },
    onError: (err) => {
      return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
    },
  });
  return (
    <WrapperImageBg style={styles.wrapper} source={images.background_triangular}>
      {(loading || loadingGetOTP) && <Loading />}
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <Text style={styles.title}>{translate('forgot_password.title')}</Text>
        <Text style={styles.des} text={translate('forgot_password.description')} />
        <Block mgTop={49}>
          <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
            <TextField
              name={'phone'}
              returnKeyType={'done'}
              keyboardType={'phone-pad'}
              placeholder={translate('placeholder.forgot_phone')}
              // iconLeft={() => <IconCall />}
            />
          </Form>
        </Block>
        <Button disabled={!!size(errors)} onPress={formMethods.handleSubmit(onSubmit, onErrors)} marginTop={scale(46)}>
          {translate('cm.next')}
        </Button>
      </KeyboardAwareScrollView>
    </WrapperImageBg>
  );
};

export const ForgotPasswordScreen = memo(ForgotPasswordComponent, isEqual);
