import { Block, Image, Text } from '@components';
import React, { memo, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { FlatList, ScrollView, Platform, TouchableOpacity, View } from 'react-native';
import isEqual from 'react-fast-compare';
import MapView, { Circle, Geojson, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { PERMISSIONS, request } from 'react-native-permissions';
import RNLocation from 'react-native-location';
import BottomSheet, { BottomSheetFlatList } from '@gorhom/bottom-sheet';

import styles from './styles';
import { scale } from '@utils';
import { images } from '@assets';
import { Colors } from '@themes';

interface IMapProps {
  route: any;
  navigation: any;
}

type LocationProps = {
  accuracy: number | undefined;
  altitude: number | undefined;
  altitudeAccuracy: number | undefined;
  course: number | undefined;
  floor?: number | undefined;
  latitude: number | undefined;
  longitude: number | undefined;
  speed: number | undefined;
  timestamp: number;
};

const listStation = [
  {
    name: 'Trạm sạc Duy Tân',
    address: '80 Duy Tân, Cầu Giấy, Hà Nội',
    latitude: 21.0314467,
    latitudeDelta: 0.002,
    longitude: 105.7806106,
    longitudeDelta: 0.002,
    available: true,
    numberOfBattery: 11,
    distance: '0,5 km',
  },
  {
    name: 'Nhà văn hoá phường Mai Dịch',
    address: '1 Trần Bình, Mai Dịch, Cầu Giấy, Hà Nội',
    latitude: 21.0306425,
    latitudeDelta: 0.002,
    longitude: 105.7792176,
    longitudeDelta: 0.002,
    available: false,
    numberOfBattery: 2,
    distance: '1,2 km',
  },
  {
    name: 'Trạm sạc ĐH Thương Mại',
    address: '79 Hồ Tùng Mậu, Mai Dịch, Cầu Giấy, Hà Nội',
    latitude: 21.03684,
    latitudeDelta: 0.002,
    longitude: 105.77503,
    longitudeDelta: 0.002,
    available: true,
    numberOfBattery: 0,
    distance: '2,3 km',
  },
  {
    name: 'Bến xe Mỹ Đình',
    address: 'Mỹ Đình, Nam Từ Liêm, Hà Nội',
    latitude: 21.0279,
    latitudeDelta: 0.01,
    longitude: 105.77879,
    longitudeDelta: 0.01,
    available: true,
    numberOfBattery: 3,
    distance: '1,0 km',
  },
];

const MapComponent: React.FC<IMapProps> = (props) => {
  return <View style={styles.wrapper}></View>;
};

export const MapScreen = memo(MapComponent, isEqual);
