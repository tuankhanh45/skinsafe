import { StyleSheet } from 'react-native';
import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.COLOR_4,
  },
  container: {
    flex: 1,
    marginTop: vScale(58),
    paddingTop: vScale(32),
    paddingHorizontal: scale(16),
    backgroundColor: Colors.COLOR_4,
    borderTopLeftRadius: moderateScale(25),
    borderTopRightRadius: moderateScale(25),
  },
  vMap: {
    flex: 1,
    // height: '110%',
  },
  btnMap: {
    backgroundColor: 'white',
    padding: scale(8),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: scale(100),
  },
  contentContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // borderTopLeftRadius: scale(24),
    // borderTopRightRadius: scale(24),
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  bodyBottomSheet: {
    flex: 1,
    marginHorizontal: scale(23),
    marginTop: scale(12),
  },
  body: { flexDirection: 'row', alignItems: 'center' },
  txtTime: {
    color: '#E39908',
    fontWeight: '700',
  },
  btnList: {
    borderWidth: scale(1),
    borderColor: '#E0E3E8',
    borderRadius: scale(12),
    padding: scale(12),
    marginBottom: scale(12),
  },
  txtTitle: {
    flex: 1,
    lineHeight: scale(24),
    fontSize: scale(16),
    fontWeight: '700',
  },
  status: {
    lineHeight: scale(20),
    fontSize: scale(14),
  },
  imageStation: {
    width: scale(100),
    height: scale(100),
  },
  address: {
    flex: 1,
    fontSize: scale(14),
    lineHeight: scale(20),
  },
  stationInfo: {
    marginLeft: scale(12),
    flex: 1,
  },
  btn: {
    height: scale(30),
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scale(12),
    borderWidth: scale(1),
    borderRadius: scale(8),
    borderColor: '#E0E3E8',
  },
  btnText: {},
  bottomSheetTitle: {
    fontWeight: '700',
    fontSize: scale(16),
    textAlign: 'center',
    marginTop: scale(12),
  },
});

export default styles;
