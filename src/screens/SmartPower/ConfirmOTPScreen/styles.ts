import { StyleSheet } from 'react-native';
import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.COLOR_4,
  },
  container: {
    flex: 1,
    marginTop: vScale(58),
    paddingTop: vScale(32),
    paddingHorizontal: scale(16),
    backgroundColor: Colors.COLOR_4,
    borderTopLeftRadius: moderateScale(25),
    borderTopRightRadius: moderateScale(25),
  },
  title: {
    fontWeight: '700',
    textAlign: 'center',
    color: Colors.COLOR_1,
    fontSize: FontSize.size18,
  },
  phone: {
    fontWeight: '700',
    textAlign: 'center',
    color: Colors.COLOR_1,
    fontSize: FontSize.size18,
    marginTop: scale(16),
  },
  containerOTP: { flex: 1 },
  inputStyle: { borderBottomWidth: moderateScale(1) },
  time: { fontWeight: '400', color: Colors.COLOR_15, fontSize: FontSize.size14, textDecorationLine: 'underline' },
  timeUp: { fontWeight: '500', color: Colors.COLOR_1, fontSize: FontSize.size14, textDecorationLine: 'underline' },
});

export default styles;
