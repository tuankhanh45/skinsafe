import React, { memo, useState, useRef, useEffect } from 'react';
import isEqual from 'react-fast-compare';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import styles from './styles';
import { Button, Text, WrapperImageBg, TextField, Form, Block, Touchable } from '@components';
import { scale } from '@utils';
import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import OTPTextInput from 'react-native-otp-textinput';
import { translate } from '@assets';
import { images } from '@assets';
import { handleResponse, requestEnterPhone, requestActivePhone } from '@configs';
import { useRequest } from 'ahooks';
import { Alert } from 'react-native';
import { Loading } from '@components';
interface IConfirmOTPProps {
  route: any;
  navigation: any;
}

const ConfirmOTPComponent: React.FC<IConfirmOTPProps> = (props) => {
  const { navigation, route } = props;
  const [OTP, setOTP] = useState<string>('');
  const [timeLeft, setTimeLeft] = useState<number>(60);
  useEffect(() => {
    if (!timeLeft) return;
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);
    return () => {
      clearInterval(intervalId);
    };
  }, [timeLeft]);
  const otpInput = useRef(null);
  const onSubmit = () => {
    console.log('otpInput', OTP);
    const params = { phone: route?.params?.phone, otp: OTP };
    run(params);
  };
  const onReSend = async () => {
    // call api get OTP
    runGetOTP({ phone: route?.params?.phone ?? '' });
    setTimeLeft(60);
  };
  // const clearText = () => {
  //   otpInput?.current?.clear();
  // };
  // const setText = (txt: string) => {
  //   otpInput?.current?.setValue(txt);
  // };

  // active phone
  const { run: run, loading } = useRequest(requestActivePhone, {
    manual: true,
    onSuccess: (data) => {
      // test flow
      navigation.navigate(SCREENS.CREATE_PASSWORD, {
        type: route?.params?.type,
        phone: route?.params?.phone,
        otp: OTP,
      });
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
      }
      return navigation.navigate(SCREENS.CREATE_PASSWORD, {
        type: route?.params?.type,
        phone: route?.params?.phone,
        otp: OTP,
      });
    },
    onError: (err) => {
      return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
    },
  });
  // resendOTP
  const { run: runGetOTP, loading: loadingGetOTP } = useRequest(requestEnterPhone, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
      }
      return;
    },
    onError: (err) => {
      return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
    },
  });
  return (
    <WrapperImageBg style={styles.wrapper} source={images.background_triangular}>
      {loading && loadingGetOTP && <Loading />}

      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <Text style={styles.title} text={translate('OTP.title')} />
        <Text style={styles.phone} text={route?.params?.phone} />
        <Block mgHorizontal={12} mgTop={30} align="center" bgColor="red">
          <OTPTextInput
            ref={otpInput}
            tintColor={Colors.COLOR_9}
            containerStyle={styles.containerOTP}
            textInputStyle={styles.inputStyle}
            handleTextChange={(txt: string) => setOTP(txt)}
            inputCount={6}
          />
        </Block>
        <Block direction="row" justify="center" mgTop={90}>
          <Touchable pd={10} onPress={onReSend} disabled={timeLeft > 0}>
            <Text
              text={`${translate('OTP.resend')} ${timeLeft ? '(' + timeLeft + 's)' : ''}`}
              style={timeLeft ? styles.time : styles.timeUp}
            />
          </Touchable>
        </Block>
        <Button disabled={OTP?.length < 6} onPress={() => onSubmit()} marginTop={scale(46)}>
          {translate('cm.next')}
        </Button>
      </KeyboardAwareScrollView>
    </WrapperImageBg>
  );
};

export const ConfirmOTPScreen = memo(ConfirmOTPComponent, isEqual);
