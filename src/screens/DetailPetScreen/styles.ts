import { StyleSheet } from 'react-native';
import { Colors, Fonts, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  card: { width: scale(375), height: scale(220), borderRadius: scale(5) },
  wrapper: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: Colors.COLOR_4,
  },
  coverBtn: {
    borderRadius: scale(8),
    backgroundColor: Colors.WHITE,
    borderWidth: moderateScale(1),
    borderColor: Colors.COLOR_1,
    height: scale(33),
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtBtn: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size14, color: Colors.COLOR_1 },
});

export default styles;
