import { useRequest } from 'ahooks';
import { Alert } from 'react-native';
import isEqual from 'react-fast-compare';
import React, { memo, useCallback, useEffect } from 'react';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { scale } from '@utils';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { handleResponse, ICreatePetState, requestDeletePet, requestGetDetailsPet } from '@configs';
import { Block, HeaderBar, ImageRemote, SafeWrapper, Loading, Touchable, Text } from '@components';
import { IconPetEdit, IconPetFoot, IconPetGender, IconPetShare, IconPetWeight, IconRedPet, translate } from '@assets';

interface IDetailPetProps {
  navigation: any;
  route: any;
}

const DetailPetComponent: React.FC<IDetailPetProps> = (props) => {
  const { navigation, route } = props;
  const { pet } = route?.params;

  const actDeletePet = useAppStore((state: ICreatePetState) => state.actDeletePet);
  const detailsPet = useAppStore((state: ICreatePetState) => state.pet.detailsPet);
  const actSaveDetailsPet = useAppStore((state: ICreatePetState) => state.actSaveDetailsPet);

  useEffect(() => {
    run({ id: pet?.id });
  }, []);

  // get pet details
  const { run, loading, cancel } = useRequest(requestGetDetailsPet, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (res?.id) {
        actSaveDetailsPet(res);
      }
    },
  });

  // delete app
  const onDeletePet = useCallback(() => {
    Alert.alert('Xác nhận', 'Bạn chắc chắn muốn xoá Smart Power', [
      { text: 'Xoá ngay', onPress: () => runDelete({ id: pet?.id }), style: 'destructive' },
      { text: 'Huỷ', onPress: () => {} },
    ]);
  }, [pet]);

  const { run: runDelete, loading: loadingDelete } = useRequest(requestDeletePet, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (!isError) {
        actDeletePet(detailsPet);
        navigation.goBack();
      }
    },
  });
  //
  const renderRight = useCallback(() => {
    return (
      <Touchable onPress={sharePet}>
        <IconPetShare />
      </Touchable>
    );
  }, []);

  const sharePet = () => {};

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      {loading && <Loading />}
      <HeaderBar title={translate('pet.title')} renderRight={renderRight} />
      <KeyboardAwareScrollView>
        <ImageRemote style={styles.card} source={'https://picsum.photos/801'} resizeMode={'cover'} />
        <Block
          style={[styles.wrapper, { borderRadius: scale(15) }]}
          mgHorizontal={15}
          pdHorizontal={4}
          mgTop={-20}
          pdVertical={12}>
          {/* header */}
          <Block direction="row" pdLeft={69} mgRight={4} mgTop={16}>
            <IconRedPet />
            <Block mgLeft={15} flex mgTop={-8}>
              <Text text={detailsPet?.name} fFamily={'BRANDING'} size="size26" />
              <Text
                text={detailsPet?.age ? detailsPet?.age + ' tháng' : ''}
                fFamily={'MEDIUM'}
                size="size12"
                color="TEXT_2"
              />
            </Block>
            <Touchable onPress={() => navigation.navigate(SCREENS.CREATE_PET, { type: SCREENS.EDIT_PET })}>
              <IconPetEdit />
            </Touchable>
          </Block>
          {/* info */}
          <Block direction="row" align="center" mgHorizontal={36} mgVertical={26} justify="space-between">
            <IconPetFoot />
            <Text
              text={detailsPet?.species?.name ?? ''}
              fFamily={'MEDIUM'}
              size="size18"
              color="TEXT_2"
              fWeight="500"
              mgLeft={7}
            />
            <Block w={1} h={22} bgColor={Colors.TEXT_2} mgHorizontal={10} />
            <IconPetGender />
            <Text
              text={detailsPet?.sex ? 'Đực' : 'Cái'}
              fFamily={'MEDIUM'}
              size="size18"
              color="TEXT_2"
              fWeight="500"
              mgLeft={7}
            />
            <Block w={1} h={22} bgColor={Colors.TEXT_2} mgHorizontal={10} />
            <IconPetWeight />
            <Text
              text={detailsPet?.weight ? detailsPet?.weight + ' kg' : ''}
              fFamily={'MEDIUM'}
              size="size18"
              color="TEXT_2"
              fWeight="500"
              mgLeft={7}
            />
          </Block>
        </Block>
        <Block style={styles.wrapper} mgHorizontal={15} pdHorizontal={4} mgTop={16} pdVertical={12}>
          <Text text={translate('pet.des')} fFamily={'BRANDING'} size="size15" />
          <Text text={detailsPet?.description} fFamily={'MEDIUM'} size="size10" mgHorizontal={16} mgTop={8} />
        </Block>
        <Touchable mgTop={20} mgHorizontal={16} onPress={onDeletePet} style={styles.coverBtn}>
          <Text style={styles.txtBtn}>{translate('pet.delete')}</Text>
        </Touchable>
      </KeyboardAwareScrollView>
    </SafeWrapper>
  );
};

export const DetailPetScreen = memo(DetailPetComponent, isEqual);
