import React, { memo } from 'react';
import { FlatList } from 'react-native';
import isEqual from 'react-fast-compare';

import styles from './styles';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { RenderItemProps } from '@configs';
import { TotalOverview } from './components';
import { Block, HeaderBar, ListEmpty, NotificationBadgeBtn, SafeWrapper, Text, Touchable } from '@components';

interface ICartProps {
  route: any;
  navigation: any;
}

const CartComponent: React.FC<ICartProps> = (props) => {
  const { navigation } = props;
  const token = useAppStore((state) => state?.account?.token);

  const renderRight = () => <NotificationBadgeBtn />;

  const keyExtractor = (item: any, index: number) => index + 'item_cart';
  const renderItem = ({ item }: RenderItemProps) => {
    return <Block />;
  };

  const onPressFavorite = () => {
    if (token) {
      navigation.navigate(SCREENS.TAKE_CARE, { status: 2 });
    } else {
      navigation.navigate(SCREENS.LOGIN, {
        cbFunc: () => navigation.replace(SCREENS.TAKE_CARE, { status: 2 }),
      });
    }
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title="Giỏ hàng" iconLeft={null} renderRight={renderRight} highlightCenter />
      <FlatList
        data={[]}
        renderItem={renderItem}
        ListHeaderComponent={<ListHeaderComponent onPress={onPressFavorite} />}
        keyExtractor={keyExtractor}
        ListEmptyComponent={<ListEmpty title={`Bạn chưa có sản phẩm nào\nHãy tiếp tục mua sắm rồi qua lại đây`} />}
      />

      <TotalOverview data={{}} />
    </SafeWrapper>
  );
};

export const CartScreen = memo(CartComponent, isEqual);

const ListHeaderComponent = ({ onPress }: any) => {
  return (
    <Block pdHorizontal={15} pdTop={20} pdBottom={14}>
      <Touchable onPress={onPress}>
        <Text color="COLOR_1">ĐẾN MỤC YÊU THÍCH</Text>
      </Touchable>
    </Block>
  );
};
