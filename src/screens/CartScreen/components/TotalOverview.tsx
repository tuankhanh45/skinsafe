import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { Block, Text, Touchable } from '@components';
import { moderateScale, scale, vScale } from '@utils';

interface ITotalOverviewProps {
  data: any;
}

const TotalOverviewComponent: React.FC<ITotalOverviewProps> = (props) => {
  const { data } = props;

  return (
    <Block style={styles.wrapper}>
      <Block flex align="flex-end" pdRight={13}>
        <Text>
          Tổng thanh toán: <Text fWeight="700">28.954.000đ</Text>
        </Text>
        <Text mgTop={3}>
          Tiền tích luỹ:{' '}
          <Text fWeight="700" color="COLOR_1">
            289.540đ
          </Text>
        </Text>
      </Block>

      <Touchable style={styles.btn}>
        <Text size="size12" fWeight="700" color="WHITE">
          Mua hàng
        </Text>
      </Touchable>
    </Block>
  );
};

export const TotalOverview = memo(TotalOverviewComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    paddingTop: vScale(8),
    paddingBottom: vScale(28),
    paddingHorizontal: scale(15),
    borderColor: Colors.COLOR_1,
    borderTopWidth: moderateScale(0.3),
  },
  btn: {
    justifyContent: 'center',
    paddingVertical: scale(8),
    paddingHorizontal: scale(22),
    backgroundColor: Colors.COLOR_1,
    borderRadius: moderateScale(100),
  },
});
