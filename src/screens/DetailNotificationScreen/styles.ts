import { StyleSheet } from 'react-native';
import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  title: {
    fontWeight: '700',
    color: Colors.COLOR_3,
    fontSize: FontSize.size12,
    lineHeight: moderateScale(14.65),
  },
  contentContainer: {
    flex: 1,
    padding: scale(16),
  },
  date: {
    fontWeight: '500',
    color: Colors.TEXT,
    marginVertical: vScale(6),
    fontSize: FontSize.size12,
    lineHeight: moderateScale(15),
  },
  content: {
    color: Colors.TEXT,
    fontSize: FontSize.size12,
    lineHeight: moderateScale(17.3),
  },
});

export default styles;
