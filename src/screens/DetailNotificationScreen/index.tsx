import dayjs from 'dayjs';
import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import { ScrollView } from 'react-native';
import React, { memo, useEffect, useState } from 'react';

import styles from './styles';
import { translate } from '@assets';
import { HeaderBar, Loading, SafeWrapper, Text } from '@components';
import { handleResponse, requestGetDetailNotification } from '@configs';

interface IDetailNotificationProps {
  route: any;
  navigation: any;
}

const DetailNotificationComponent: React.FC<IDetailNotificationProps> = (props) => {
  const { route } = props;
  const dataParams = route?.params?.data;

  const [data, setData] = useState<any>(null);

  useEffect(() => {
    run({ id: dataParams?.id });
  }, []);

  const { run, loading } = useRequest(requestGetDetailNotification, {
    manual: true,
    onSuccess: (data) => {
      const { res, isError, err } = handleResponse(data);
      res && setData(res);
    },
  });

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('detail_notification.title')} highlightCenter />
      {loading && <Loading />}
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <Text style={styles.title}>{data?.title}</Text>
        <Text style={styles.date}>{dayjs(data?.created_at).format('DD-MM-YYYY HH:mm')}</Text>
        <Text style={styles.content}>{data?.content}</Text>
      </ScrollView>
    </SafeWrapper>
  );
};

export const DetailNotificationScreen = memo(DetailNotificationComponent, isEqual);
