import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import chunk from 'lodash/chunk';
import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, CustomFlatlist, Text, ListEmpty, ItemProduct, SafeWrapper, Touchable, Image } from '@components';
import { useNavigation } from '@react-navigation/native';
import { RenderItemProps } from '@configs';
import { Header } from '../ShopPageScreen/components';
import { styles } from './styles';
import { DetailBanner } from './components';
interface IShopPageCategoryDetailProps {
  route: any;
  navigation: any;
}
const list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

const ShopPageCategoryDetailComponent: React.FC<IShopPageCategoryDetailProps> = (props) => {
  const { navigation } = props;
  const ListHeaderComponent = memo((props: any) => {
    const {} = props;
    return (
      <Block pdHorizontal={16}>
        <Text size={'size15'} fFamily={'BRANDING'} mgTop={12}>
          Tất cả sản phẩm dành cho chó
        </Text>
        <Block h={0.6} bgColor={Colors.COLOR_9} mgTop={16} />
        <Text size={'size15'} fFamily={'BRANDING'} mgTop={12}>
          Hạt
        </Text>
        <Text size={'size14'} fFamily={'MEDIUM'} mgTop={5}>
          696 969 sản phẩm
        </Text>
        <DetailBanner />
        <Block
          mgTop={27}
          bgColor={'#FDE8E8'}
          borderRadius={5}
          pdHorizontal={4}
          pdVertical={8}
          direction="row"
          align="center"
          justify="space-between">
          <Touchable onPress={() => {}}>
            <Image source="menuDot" style={styles.dot} />
          </Touchable>
          <Touchable onPress={() => {}} direction="row" align="center">
            <Text size={'size12'} fFamily={'MEDIUM'} fWeight="500" mgRight={7}>
              Mặt hàng phổ biến
            </Text>
            <Image source="down" style={styles.down} />
          </Touchable>
          <Touchable onPress={() => {}}>
            <Image source="menuLine" style={styles.line} />
          </Touchable>
        </Block>
      </Block>
    );
  });
  const renderItem = ({ item }: RenderItemProps) => {
    return (
      <Block mgTop={16}>
        <ItemProduct data={{}} />
      </Block>
    );
  };
  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <Header navigation={navigation} />
      <CustomFlatlist
        style={{ flex: 1 }}
        columnWrapperStyle={{ justifyContent: 'space-between', paddingHorizontal: scale(15) }}
        contentContainerStyle={styles.contentContainer}
        numColumns={3}
        data={list}
        renderItem={renderItem}
        ListEmptyComponent={<ListEmpty />}
        ListHeaderComponent={<ListHeaderComponent />}
      />
    </SafeWrapper>
  );
};

export const ShopPageCategoryDetailScreen = memo(ShopPageCategoryDetailComponent, isEqual);
