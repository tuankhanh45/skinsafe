import { scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  contentContainer: {},
  dot: { width: scale(24), height: vScale(24) },
  line: { width: scale(12), height: vScale(15) },
  down: { width: scale(10), height: vScale(10) },
});
