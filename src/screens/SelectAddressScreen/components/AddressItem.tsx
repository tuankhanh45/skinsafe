import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import React, { memo, useRef } from 'react';

import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { convertPhone, scale, vScale } from '@utils';
import { AlertModal, Block, Text, Touchable } from '@components';
import { IconArrowRight, IconSuccess, translate } from '@assets';

interface IAddressItemProps {
  data: any;
  runDeleteAddress: (e: any) => void;
  onNavigate: (screen: string, data?: any) => void;
}

// home_company: 1 - home, 2 - company
const AddressItemComponent: React.FC<IAddressItemProps> = (props) => {
  const { data, onNavigate, runDeleteAddress } = props;
  const refModalDeleteAddress = useRef<any>(null);

  const onCloseModal = () => refModalDeleteAddress.current.close();
  const onOpenModal = () => refModalDeleteAddress.current.open();

  const onPressDeleteAddress = () => {
    runDeleteAddress({ id: data?.id });
    refModalDeleteAddress.current.open();
  };

  return (
    <>
      <Touchable style={styles.wrapper}>
        <Text size="size18" fWeight="700" color="TEXT" mgLeft={20}>
          {data?.name} |{' '}
          <Text size="size18" fWeight="700" color="COLOR_7">
            {convertPhone(data?.phone)}
          </Text>
        </Text>
        <Text mgLeft={20} mgTop={5} size="size12" color="COLOR_1">
          {translate(`select_address.${data?.home_company === 1 ? 'home' : 'company'}`)}
        </Text>
        <Block direction="row" pdTop={27}>
          {data?.default ? <IconSuccess color={Colors.COLOR_16} /> : <Block w={19} />}
          <Text mgLeft={6} flex>
            {data?.address_full}
          </Text>
          <Block mgLeft={16}>
            <Touchable
              direction={'row'}
              align={'center'}
              onPress={() => onNavigate(SCREENS.CREATE_NEW_ADDRESS, { data })}>
              <Text size="size10" mgRight={10} color="COLOR_16">
                {translate('select_address.change')}
              </Text>
              <IconArrowRight color={Colors.COLOR_16} />
            </Touchable>
            <Touchable mgTop={10} onPress={onOpenModal}>
              <Text size="size10" mgRight={10} color="COLOR_16">
                {translate('select_address.delete')}
              </Text>
            </Touchable>
          </Block>
        </Block>
      </Touchable>

      <AlertModal
        onClose={onCloseModal}
        ref={refModalDeleteAddress}
        onSubmit={onPressDeleteAddress}
        title={translate('select_address.delete')}
        content={translate('select_address.confirm_delete')}
      />
    </>
  );
};

export const AddressItem = memo(AddressItemComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    paddingTop: vScale(6),
    marginBottom: vScale(3),
    paddingBottom: vScale(26),
    paddingHorizontal: scale(16),
    backgroundColor: Colors.WHITE,
  },
  container: {
    paddingLeft: scale(10),
  },
});
