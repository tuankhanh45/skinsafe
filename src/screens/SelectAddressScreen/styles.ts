import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
  },
  addAddressBtn: {
    width: '100%',
    alignItems: 'center',
    borderStyle: 'dashed',
    justifyContent: 'center',
    paddingVertical: vScale(8),
    borderColor: Colors.COLOR_6,
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(6),
  },
  imgBtn: {
    width: scale(26),
    height: vScale(26),
  },
});

export default styles;
