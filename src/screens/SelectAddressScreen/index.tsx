import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import { useFocusEffect } from '@react-navigation/native';
import React, { memo, useCallback, useState } from 'react';

import styles from './styles';
import { Colors } from '@themes';
import * as Com from '@components';
import { translate } from '@assets';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { AddressItem } from './components';
import { checkType, showToastError, showToastSuccess } from '@utils';
import { handleResponse, RenderItemProps, requestDeleteAddress, requestGetListAddress } from '@configs';

const { Text, Block, Image, Loading, Touchable, HeaderBar, SafeWrapper, CustomFlatlist, NotificationBadgeBtn } = Com;

interface ISelectAddressProps {
  route: any;
  navigation: any;
}

const SelectAddressComponent: React.FC<ISelectAddressProps> = (props) => {
  const { navigation } = props;

  const listAddress = useAppStore((state) => state?.address.listAddress);
  const actSaveListAddress = useAppStore((state) => state.actSaveListAddress);

  const [refreshing, setRefreshing] = useState(false);

  useFocusEffect(
    useCallback(() => {
      runGetListAddress();
    }, []),
  );

  const { run: runGetListAddress, loading: loadingGetListAddress } = useRequest(requestGetListAddress, {
    manual: true,
    onSuccess: (data) => {
      const { res, err } = handleResponse(data);
      if (err?.message === 'Bạn chưa có dữ liệu nào') {
        return actSaveListAddress([]);
      }
      if (Array.isArray(res)) {
        return actSaveListAddress(res);
      }
    },
    onFinally: () => refreshing && setRefreshing(false),
  });
  const { run: runDeleteAddress, loading: loadingDeleteAddress } = useRequest(requestDeleteAddress, {
    manual: true,
    onSuccess: (data) => {
      const { res, isError, err } = handleResponse(data);
      if (isError) {
        showToastError(
          err?.message && checkType(err?.message, 'string') ? err.message : translate('select_address.delete_failed'),
        );
      } else {
        runGetListAddress();
        showToastSuccess(translate('select_address.delete_success'));
      }
    },
  });

  const onRefresh = () => {
    setRefreshing(true);
    runGetListAddress();
  };

  const renderRight = () => <NotificationBadgeBtn />;

  const renderItem = ({ item, index }: RenderItemProps) => {
    return <AddressItem data={item} onNavigate={onNavigate} runDeleteAddress={runDeleteAddress} />;
  };

  const onNavigate = (screen: string, data?: any) => {
    navigation.navigate(screen, data && data);
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('select_address.title')} renderRight={renderRight} highlightCenter />
      {((loadingGetListAddress && !listAddress.length) || loadingDeleteAddress) && <Loading />}
      <CustomFlatlist
        data={listAddress}
        onRefresh={onRefresh}
        refreshing={refreshing}
        renderItem={renderItem}
        style={styles.container}
        ListHeaderComponent={<ListHeaderComponent dataLength={listAddress.length} />}
        ListFooterComponent={<ListFooterComponent onNavigate={onNavigate} dataLength={listAddress.length} />}
      />
    </SafeWrapper>
  );
};

export const SelectAddressScreen = memo(SelectAddressComponent, isEqual);

const ListFooterComponent = memo(({ onNavigate, dataLength }: any) => {
  if (dataLength === 2) return null;
  return (
    <Block pdVertical={24} pdHorizontal={10} bgColor={Colors.WHITE} borderRadius={5}>
      <Touchable style={styles.addAddressBtn} onPress={() => onNavigate(SCREENS.CREATE_NEW_ADDRESS)}>
        <Image source={'ic_location_pet'} style={styles.imgBtn} />
        <Text size={'size12'}>{translate('select_address.add_new_address')}</Text>
      </Touchable>
    </Block>
  );
});

const ListHeaderComponent = memo(({ dataLength }: any) => {
  if (!dataLength) return null;
  return (
    <Text mgLeft={16} mgTop={6} mgBottom={10} fFamily="BRANDING">
      {translate('select_address.list')}
    </Text>
  );
});
