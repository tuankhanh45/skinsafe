import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: vScale(40),
    paddingHorizontal: scale(15),
  },
  footer: {
    padding: scale(16),
    borderColor: '#F4F3F9',
    borderTopWidth: moderateScale(1),
    borderBottomWidth: moderateScale(1),
  },
});

export default styles;
