import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { translate } from '@assets';
import { SCREENS } from '@navigation';
import { showToastSuccess } from '@utils';
import { Block, Button, HeaderBar, SafeWrapper, Text } from '@components';

interface IRequestSupportProps {
  route: any;
  navigation: any;
}

const RequestSupportComponent: React.FC<IRequestSupportProps> = (props) => {
  const { navigation } = props;

  const onPressSubmit = () => {
    showToastSuccess(translate('request_support.submit_success'));
    navigation.reset({ index: 0, routes: [{ name: SCREENS.MAIN_STACK }] });
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('request_support.title')} />
      <KeyboardAwareScrollView contentContainerStyle={styles.contentContainer}>
        <Text w="60%" fWeight="700" size="size24" color={'COLOR_1'}>
          {translate('request_support.welcome_text_1')}
        </Text>
        <Text w="80%" mgTop={16} textAlign="justify">
          {translate('request_support.welcome_text_2')}
        </Text>
      </KeyboardAwareScrollView>

      <Block style={styles.footer}>
        <Button onPress={onPressSubmit}>{translate('request_support.submit')}</Button>
      </Block>
    </SafeWrapper>
  );
};

export const RequestSupportScreen = memo(RequestSupportComponent, isEqual);
