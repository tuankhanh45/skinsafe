import React, { memo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import size from 'lodash/size';
import styles from './styles';
import { IconCall, IconSuccess, IconUncheck, translate } from '@assets';
import { Button, Text, WrapperImageBg, TextField, Form, Block, Touchable } from '@components';
import { useForm } from 'react-hook-form';
import { scale, useValidateForm } from '@utils';
import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { images } from '@assets';

import { handleResponse, requestEnterPhone } from '@configs';
import { useRequest } from 'ahooks';
import { Alert } from 'react-native';
import { Loading } from '@components';

interface IRegisterProps {
  route: any;
  navigation: any;
}

const RegisterComponent: React.FC<IRegisterProps> = (props) => {
  const { navigation } = props;
  const [agree, setAgree] = useState<boolean>(false);
  // form
  const formMethods = useForm();
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;
  const onErrors = (errors: any) => {};
  const onSubmit = (form: any) => {
    console.log('submit', form);
    const params = { phone: form?.phone ?? '' };
    run(params);
  };
  const { run, loading } = useRequest(requestEnterPhone, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
      }
      return navigation.navigate(SCREENS.OTP_CONFIRM, {
        phone: formMethods.getValues('phone'),
        type: SCREENS.REGISTER,
      });
    },
    onError: (err) => {
      return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại');
    },
  });
  return (
    <WrapperImageBg style={styles.wrapper} source={images.background_triangular}>
      {loading && <Loading />}
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <Text style={styles.title}>{translate('register.title')}</Text>
        <Text style={styles.des} text={translate('register.description')} />
        <Block mgTop={49}>
          <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
            <TextField
              name={'phone'}
              returnKeyType={'done'}
              keyboardType={'phone-pad'}
              placeholder={translate('placeholder.phone')}
              iconLeft={() => <IconCall />}
            />
          </Form>
        </Block>
        <Block direction="row" align="center" mgTop={50}>
          <Touchable onPress={() => setAgree(!agree)} mgRight={5}>
            {!agree ? <IconUncheck color={Colors.COLOR_1} /> : <IconSuccess />}
          </Touchable>
          <Text style={styles.agree} text={translate('register.agree')} />
          <Touchable>
            <Text style={[styles.agree, { color: Colors.COLOR_6 }]} text={translate('register.policy')} />
          </Touchable>
        </Block>
        <Button
          disabled={!!size(errors) || !agree}
          onPress={formMethods.handleSubmit(onSubmit, onErrors)}
          marginTop={scale(46)}>
          {translate('cm.next')}
        </Button>
      </KeyboardAwareScrollView>
    </WrapperImageBg>
  );
};

export const RegisterScreen = memo(RegisterComponent, isEqual);
