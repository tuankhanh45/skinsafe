import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import { ScrollView } from 'native-base';
import React, { memo, useEffect, useState } from 'react';

import styles from './styles';
import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { showToastError } from '@utils';
import { handleResponse, requestGetStatistical } from '@configs';
import { IconArrowRightFat, IconOrderProduct, IconOrderService, translate } from '@assets';
import { Block, HeaderBar, Loading, NotificationBadgeBtn, SafeWrapper, Text, Touchable } from '@components';

interface IOrderOverviewProps {
  route: any;
  navigation: any;
}

const LIST_STATUS_PRODUCT: any[] = [
  { label: 'wait_confirm', key: 'wait_confirm' },
  { label: 'wait_pickup', key: 'wait_pickup' },
  { label: 'delivering', key: 'delivering' },
  { label: 'delivered', key: 'delivered' },
  { label: 'canceled', key: 'canceled' },
  { label: 'return', key: 'return' },
];

const LIST_STATUS_SERVICE: any[] = [
  { label: 'wait_confirm', key: 'wait_confirm' },
  { label: 'wait_pickup', key: 'wait_pickup' },
  { label: 'done', key: 'done' },
  { label: 'canceled', key: 'canceled' },
];

const OrderOverviewComponent: React.FC<IOrderOverviewProps> = (props) => {
  const { route, navigation } = props;
  const type = route?.params?.type;

  const [statistical, setStatistical] = useState<any>(null);

  useEffect(() => {
    run();
  }, []);

  //TODO: update status order
  const { run, loading } = useRequest(requestGetStatistical, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (res) {
        setStatistical(res);
      } else {
        showToastError('Lấy thông tin đơn hàng thất bại');
      }
    },
  });

  const renderRight = () => <NotificationBadgeBtn />;

  const onPressDetail = () => {
    if (type === 'order_product') return navigation.navigate(SCREENS.ORDER_PRODUCT);
    return navigation.navigate(SCREENS.ORDER_SERVICE);
  };

  const list = type === 'order_product' ? LIST_STATUS_PRODUCT : LIST_STATUS_SERVICE;

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('order_overview.title')} renderRight={renderRight} highlightCenter />
      {loading && <Loading />}
      <ScrollView style={styles.container}>
        <Touchable pdHorizontal={15} pdTop={26} pdBottom={14} bgColor={Colors.WHITE} onPress={onPressDetail}>
          <Block direction="row" align="flex-end">
            {type === 'order_product' ? <IconOrderProduct /> : <IconOrderService />}
            <Text flex size="size18" fWeight="700" mgLeft={12}>
              {translate(`order_overview.label_${type}`)} ({statistical?.[type]?.total || 0})
            </Text>
            <IconArrowRightFat />
          </Block>

          <Block style={styles.listStatus}>
            {list.map((e, i) => {
              const isShowDivider = i !== list.length - 1;
              return (
                <Block key={i} mgBottom={12} direction={'row'}>
                  <Block>
                    <Text>{translate(`order_overview.${e.key}`)} (xx)</Text>
                  </Block>
                  {isShowDivider && <Block style={styles.divider} />}
                </Block>
              );
            })}
          </Block>
        </Touchable>
      </ScrollView>
    </SafeWrapper>
  );
};

export const OrderOverviewScreen = memo(OrderOverviewComponent, isEqual);
