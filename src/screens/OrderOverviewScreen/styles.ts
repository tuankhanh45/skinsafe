import { Colors } from '@themes';
import { scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  listStatus: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingTop: vScale(16),
  },
  divider: {
    height: '100%',
    width: scale(1),
    marginHorizontal: scale(10),
    backgroundColor: Colors.COLOR_9,
  },
});

export default styles;
