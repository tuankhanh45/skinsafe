import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ItemService, Text, Touchable } from '@components';
import { useNavigation } from '@react-navigation/native';

interface IUsedServiceProps {}

const UsedServiceComponent: React.FC<IUsedServiceProps> = (props) => {
  const navigation = useNavigation<any>();
  const pressLocationSearch = () => {};
  const listUsedService = [1, 2, 3, 4, 5];
  return (
    <Block style={styles.wrapper} pdHorizontal={15} mgTop={22}>
      <Block direction="row" justify="space-between" align="center">
        <Text text={translate('pet.used_service')} fFamily={'BRANDING'} size="size18" />
        <Touchable onPress={pressLocationSearch}>
          <Text
            text={translate('pet.location')}
            fFamily={'MEDIUM'}
            size="size12"
            color="COLOR_1"
            style={styles.under}
          />
        </Touchable>
      </Block>
      {listUsedService.map((e: any, i: number) => (
        <Block key={i + 'sv'} mgTop={16}>
          <ItemService data={{}} />
        </Block>
      ))}
    </Block>
  );
};

export const UsedService = memo(UsedServiceComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  txt: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size16, fontWeight: '400' },
  under: { textDecorationLine: 'underline' },
});
