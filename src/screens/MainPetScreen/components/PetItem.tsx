import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, Button, ImageRemote, Text, Touchable } from '@components';
import { useNavigation } from '@react-navigation/native';

interface IPetItemProps {
  data: any;
  size?: boolean;
}

const PetItemComponent: React.FC<IPetItemProps> = (props) => {
  const { data, size } = props;
  const navigation = useNavigation<any>();
  return (
    <Block style={size ? styles.wrapper_size : styles.wrapper}>
      <ImageRemote style={styles.avatar} source={data?.avatar} resizeMode="cover" />
      <Text text={data?.name} fFamily={'BRANDING'} size="size18" color="TEXT" numberOfLines={1} mgTop={10} />
      <Text
        text={data?.age ? data?.age + ' tháng' : 'Chưa cập nhật'}
        fFamily={'MEDIUM'}
        size="size12"
        color="TEXT_2"
        numberOfLines={1}
        mgTop={4}
      />
      <Block w={size ? 122 : 103} h={32} mgTop={12}>
        <Button
          style={{ borderRadius: scale(40) }}
          onPress={() => navigation.navigate(SCREENS.DETAILS_PET, { pet: data })}>
          {translate('pet.view_details')}
        </Button>
      </Block>
    </Block>
  );
};

export const PetItem = memo(PetItemComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    height: scale(235),
    width: scale(139),
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    alignItems: 'center',
  },
  wrapper_size: {
    height: scale(238),
    width: scale(165),
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    alignItems: 'center',
  },
  txt: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size16, fontWeight: '400' },
  avatar: {
    width: scale(103),
    height: scale(103),
    borderRadius: scale(103),
    marginTop: scale(15),
  },
});
