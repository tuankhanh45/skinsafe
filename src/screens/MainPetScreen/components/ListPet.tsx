import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import { ScrollView } from 'react-native';
import { StyleSheet } from 'react-native';
import React, { memo, useEffect } from 'react';

import { scale } from '@utils';
import { PetItem } from './PetItem';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { Colors, Fonts, FontSize } from '@themes';
import { IconEmptyList, translate } from '@assets';
import { Block, Text, Touchable } from '@components';
import { handleResponse, ICreatePetState, requestGetListPet } from '@configs';
interface IListPetProps {
  navigation: any;
}

const ListPetComponent: React.FC<IListPetProps> = (props) => {
  const { navigation } = props;

  const token = useAppStore((x) => x.account.token);
  const listPet = useAppStore((state: ICreatePetState) => state.pet.listPet);
  const actSaveListPet = useAppStore((state: ICreatePetState) => state.actSaveListPet);

  useEffect(() => {
    if (token) {
      run({ page: 1, size: 10 });
    }
  }, []);

  // get list pet
  const { run, loading } = useRequest(requestGetListPet, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (Array.isArray(res?.data)) {
        actSaveListPet(res?.data);
      }
    },
  });

  return (
    <Block>
      <Block style={styles.wrapper} mgHorizontal={16} pdHorizontal={4} mgTop={16}>
        <Block direction="row" justify="space-between" align="center" mgTop={5}>
          <Text text={translate('pet.my_pet')} fFamily={'BRANDING'} size="size18" />
          <Touchable onPress={() => navigation.navigate(SCREENS.LIST_PET)}>
            <Text
              text={translate('pet.view_all')}
              fFamily={'MEDIUM'}
              size="size12"
              color="COLOR_1"
              style={styles.under}
            />
          </Touchable>
        </Block>

        {listPet?.length ? (
          <ScrollView showsVerticalScrollIndicator={false} horizontal={true}>
            <Block pdLeft={4} direction="row" mgBottom={10}>
              {listPet.slice(0, 3).map((pet: any, idx: number) => (
                <Block key={idx + 'pet'} mgTop={17} mgRight={15}>
                  <PetItem data={pet} />
                </Block>
              ))}
            </Block>
          </ScrollView>
        ) : (
          <Block bgColor="#D9D9D9" mg={16} align="center" h={215} justify="center">
            <IconEmptyList />
            <Text text={translate('pet.not')} fFamily={'MEDIUM'} size="size14" color="TEXT" fWeight="600" />
          </Block>
        )}
      </Block>
      <Touchable
        style={styles.btn}
        onPress={() => navigation.navigate(SCREENS.CREATE_PET, { type: SCREENS.CREATE_PET })}>
        <Text
          fFamily={'MEDIUM'}
          size="size14"
          text={translate('pet.add_pet')}
          color="COLOR_1"
          fWeight="600"
          textAlign="center"
        />
      </Touchable>
      <Block
        bgColor={Colors.COLOR_9}
        h={1}
        mgHorizontal={17}
        w={'90%'}
        mgTop={20}
        justify="center"
        alignSelf="center"
      />
    </Block>
  );
};

export const ListPet = memo(ListPetComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: Colors.COLOR_4,
  },
  txt: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size16, fontWeight: '400' },
  under: { textDecorationLine: 'underline' },
  btn: {
    marginHorizontal: scale(17),
    height: scale(38),
    borderRadius: scale(5),
    borderWidth: scale(1),
    borderColor: Colors.COLOR_1,
    justifyContent: 'center',
    marginTop: scale(20),
  },
});
