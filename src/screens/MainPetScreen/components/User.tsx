import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors, Fonts, FontSize } from '@themes';
import { SCREENS } from '@navigation';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, Text, Touchable } from '@components';
import { useNavigation } from '@react-navigation/native';
import { useAppStore } from '@store';
import { ICreateAccountState } from '@configs';

interface IUserProps {}

const UserComponent: React.FC<IUserProps> = (props) => {
  const navigation = useNavigation<any>();
  const user = useAppStore((state: ICreateAccountState) => state?.account?.user);

  return (
    <Block
      style={styles.wrapper}
      direction="row"
      align="center"
      justify="space-between"
      mgHorizontal={16}
      mgTop={16}
      pd={4}>
      <Block>
        <Text text={user?.fullname} fFamily={'BRANDING'} size="size18" color="COLOR_1" />
        <Text text={translate('pet.welcome')} fFamily={'MEDIUM'} size="size12" color="BLACK" mgTop={8} />
      </Block>
      <Touchable onPress={() => navigation.navigate(SCREENS.PROFILE_STACK)}>
        <ImageRemote style={styles.avatar} source={user?.avatar} resizeMode="cover" />
      </Touchable>
    </Block>
  );
};

export const User = memo(UserComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: Colors.COLOR_4,
  },
  txt: { fontFamily: Fonts.MEDIUM, fontSize: FontSize.size16, fontWeight: '400' },
  avatar: {
    width: scale(60),
    height: scale(60),
    borderRadius: scale(60),
    borderWidth: moderateScale(1.5),
    borderColor: Colors.COLOR_1,
  },
});
