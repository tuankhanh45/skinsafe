import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

import styles from './styles';
import { Colors } from '@themes';
import { ListPet, UsedService, User } from './components';
import { IconCartBottom, IconSearch, translate } from '@assets';
import { Block, HeaderBar, SafeWrapper, Touchable } from '@components';
import { SCREENS } from '@navigation';
import { ScrollView } from 'react-native';

interface IMainPetProps {
  route: any;
  navigation: any;
}

const MainPetComponent: React.FC<IMainPetProps> = (props) => {
  const { navigation } = props;

  const onPressRight = () => navigation.navigate(SCREENS.SEARCH);

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar
        highlightCenter
        iconLeft={null}
        onPressRight={onPressRight}
        title={translate('pet.title')}
        iconRight={<IconSearch color={Colors.WHITE} />}
      />

      <ScrollView>
        <User />
        <ListPet navigation={navigation} />
        <UsedService />
      </ScrollView>
    </SafeWrapper>
  );
};

export const MainPetScreen = memo(MainPetComponent, isEqual);
