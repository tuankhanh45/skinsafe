import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { ScrollView } from 'react-native';

import styles from './styles';
import { translate } from '@assets';
import { SCREENS } from '@navigation';
import { Block, Button, HeaderBar, NotificationBadgeBtn, SafeWrapper, Text } from '@components';

interface IDetailNearShopProps {
  route: any;
  navigation: any;
}

const DetailNearShopComponent: React.FC<IDetailNearShopProps> = (props) => {
  const { route, navigation } = props;
  const type = route?.params?.type;
  const data = route?.params?.data;

  const title = translate(`detail_near_shop.title_${type}`);

  const renderRight = () => <NotificationBadgeBtn />;

  const onPress = () => {
    return navigation.navigate(SCREENS.SUCCESS, {
      title: translate(`detail_near_shop.title_success_${type}`),
      onSubmit: () => {},
    });
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={title} renderRight={renderRight} />
      <ScrollView style={styles.container}>
        <Text size="size18" fFamily="BRANDING">
          {translate('detail_near_shop.introduce')}
        </Text>
      </ScrollView>

      <Block pdHorizontal={4} pdVertical={8}>
        <Button onPress={onPress}>{translate(`detail_near_shop.submit_${type}`)}</Button>
      </Block>
    </SafeWrapper>
  );
};

export const DetailNearShopScreen = memo(DetailNearShopComponent, isEqual);
