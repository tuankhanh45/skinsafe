import { scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: vScale(16),
    paddingHorizontal: scale(15),
  },
});

export default styles;
