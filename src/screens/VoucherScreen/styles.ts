import { StyleSheet } from 'react-native';
import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
  avatar: {
    width: scale(58),
    height: scale(58),
    borderRadius: moderateScale(58),
  },
  avatarContainer: {
    width: scale(60),
    height: scale(60),
    marginLeft: scale(10),
    borderColor: Colors.WHITE,
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(60),
  },
  textMore: {
    color: Colors.COLOR_1,
    textDecorationLine: 'underline',
  },
  tabBarItem: {
    height: vScale(45),
    marginTop: vScale(18),
    borderColor: Colors.COLOR_9,
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(6),
    borderWidth: moderateScale(0.5),
  },
  labelItem: {
    fontWeight: '700',
    color: Colors.TEXT,
    textTransform: 'none',
    fontSize: FontSize.size12,
  },
});

export default styles;
