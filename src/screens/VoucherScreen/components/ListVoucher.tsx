import { useRequest } from 'ahooks';
import debounce from 'lodash/debounce';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import React, { memo, useEffect, useState } from 'react';

import { Colors } from '@themes';
import { IconSearch } from '@assets';
import { scale, vScale } from '@utils';
import { ItemVoucher } from './ItemVoucher';
import { Block, CustomFlatlist, Input, Loading } from '@components';
import { handleResponse, RenderItemProps, requestGetListVoucher } from '@configs';

interface IListVoucherProps {
  navigation: any;
  onPressTab?: () => void;
}

const ListVoucherCom: React.FC<IListVoucherProps> = (props) => {
  const { navigation, onPressTab } = props;

  const [page, setPage] = useState<number>(1);
  const [listData, setListData] = useState<any[]>([]);
  const [searchText, setSearchText] = useState<string>('');
  const [canLoadMore, setCanLoadMore] = useState<boolean>(true);
  const [isRefreshing, setIsRefreshing] = useState<boolean>(false);

  const debounceSearch = debounce((val) => setSearchText(val), 350);

  useEffect(() => {
    run({ page, per_page: 10 });
  }, [page]);

  const { run, loading } = useRequest(requestGetListVoucher, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (res?.last_page === res?.current_page) {
        setCanLoadMore(false);
      }
      if (Array.isArray(res?.data)) {
        if (res?.current_page === 1) {
          setListData(res?.data);
        } else {
          setListData((prev) => [...prev, ...res?.data]);
        }
      }
    },
    onFinally: () => isRefreshing && setIsRefreshing(false),
  });

  const renderItem = ({ item }: RenderItemProps) => {
    return <ItemVoucher data={item} onPressTab={onPressTab} />;
  };

  const onRefresh = () => {
    setIsRefreshing(true);
    setCanLoadMore(true);
    if (page === 1) {
      run({ page: 1, per_page: 10 });
    } else {
      setPage(1);
    }
  };

  const onEndReached = () => {
    if (canLoadMore && !loading) {
      setPage((prev) => prev + 1);
    }
  };

  return (
    <Block flex>
      {loading && <Loading />}

      <CustomFlatlist
        numColumns={2}
        data={listData}
        onRefresh={onRefresh}
        renderItem={renderItem}
        refreshing={isRefreshing}
        ListHeaderComponent={
          <Block pdHorizontal={15} pdBottom={20}>
            <Input
              iconRight={IconSearch}
              onChangeText={debounceSearch}
              styleWrapInput={styles.input}
              placeholder={'Tìm kiếm voucher'}
            />
          </Block>
        }
        columnWrapperStyle={styles.columnWrapper}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.3}
      />
    </Block>
  );
};

export const ListVoucher = memo(ListVoucherCom, isEqual);

const styles = StyleSheet.create({
  input: {
    height: vScale(32),
    backgroundColor: Colors.COLOR_4,
  },
  columnWrapper: {
    marginBottom: vScale(20),
    paddingHorizontal: scale(7.5),
  },
});
