import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import React, { memo, useRef } from 'react';

import { Colors } from '@themes';
import { device, moderateScale, scale, showToastSuccess, vScale } from '@utils';
import { AlertModal, Block, CustomModal, Image, Text, Touchable } from '@components';

interface IItemVoucherProps {
  data: any;
  onPressTab?: () => void;
}

const ItemVoucherCom: React.FC<IItemVoucherProps> = ({ data, onPressTab }) => {
  const refModalAlert = useRef<any>(null);
  const refModalBuyMore = useRef<any>(null);

  const onCloseAlert = () => refModalAlert.current.close();
  const onPressUse = () => refModalAlert.current.open();

  const onPressBuyMore = () => refModalBuyMore.current.open();
  const onCloseBuyMore = () => refModalBuyMore.current.close();

  const onConfirmBuyMore = () => {
    onCloseBuyMore();
    onPressTab && onPressTab();
    showToastSuccess('đây là thông báo mua thành công voucher');
  };

  const renderContent = () => {
    return (
      <Block style={styles.modalContainer}>
        <Block direction="row" center pdHorizontal={40}>
          <Text>
            Bạn muốn đổi <Text fWeight="700">{data?.xu}</Text>{' '}
          </Text>
          <Image source="coin" style={styles.coinImg} />
          <Text> lấy</Text>
        </Block>

        <Text mgTop={8} fWeight="700" size="size18" color="COLOR_1" center>
          {data?.name}
        </Text>

        <Block direction="row">
          <Touchable style={styles.buyBtn} onPress={onConfirmBuyMore}>
            <Text size="size16" fWeight="500" color="WHITE">
              Đổi
            </Text>
          </Touchable>
          <Touchable style={[styles.buyBtn, { backgroundColor: Colors.COLOR_6 }]} onPress={onCloseBuyMore}>
            <Text size="size16" fWeight="500" color="WHITE">
              Thoát
            </Text>
          </Touchable>
        </Block>
      </Block>
    );
  };

  return (
    <Block style={styles.wrapper}>
      <Block direction="row">
        <Block flex pdRight={4}>
          <Text size="size12" fWeight="700" numberOfLines={4} color="WARNING">
            {data?.name}
          </Text>
        </Block>
        <Image source={'giftBox'} style={styles.giftImg} />
      </Block>

      {onPressTab ? (
        <Touchable style={styles.buyVoucherBtn} onPress={onPressBuyMore}>
          <Text size="size12" mgRight={4} color="WHITE" fWeight="500">
            {data?.xu || 0}
          </Text>
          <Image source="coin" style={styles.coinImg} />
        </Touchable>
      ) : (
        <Block style={styles.footer}>
          <Touchable onPress={onPressUse}>
            <Text pdHorizontal={4} color="WHITE" fWeight="500" size="size12">
              Dùng
            </Text>
          </Touchable>
          <Text color="WHITE" fWeight="500" size="size12">
            -
          </Text>
          <Touchable direction="row" flex center onPress={onPressBuyMore}>
            <Text color="WHITE" fWeight="500" size="size12" mgRight={4}>
              Mua thêm
            </Text>
            <Image source="coin" style={styles.coinImg} />
          </Touchable>
        </Block>
      )}
      {!onPressTab && (
        <AlertModal
          ref={refModalAlert}
          title={'Thông báo'}
          onClose={onCloseAlert}
          content={'Hãy đi đến phần thanh toán cho sản phẩm để sử dụng Voucher'}
        />
      )}

      <CustomModal ref={refModalBuyMore} renderContent={renderContent} />
    </Block>
  );
};

export const ItemVoucher = memo(ItemVoucherCom, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    paddingVertical: vScale(12),
    paddingHorizontal: scale(10),
    marginHorizontal: scale(7.5),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(12),
    width: (device.width - scale(15 * 3)) / 2,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  giftImg: {
    width: scale(70),
    height: scale(70),
  },
  footer: {
    flexDirection: 'row',
    marginTop: vScale(16),
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: vScale(6),
    paddingHorizontal: scale(4),
    borderRadius: moderateScale(8),
    backgroundColor: Colors.COLOR_1,
  },
  coinImg: {
    width: scale(16),
    height: scale(16),
  },
  modalContainer: {
    width: '100%',
    overflow: 'hidden',
    paddingTop: vScale(24),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(4),
  },
  buyBtn: {
    flex: 1,
    alignItems: 'center',
    marginTop: vScale(20),
    justifyContent: 'center',
    paddingVertical: vScale(20),
    backgroundColor: Colors.COLOR_1,
  },
  buyVoucherBtn: {
    marginTop: vScale(20),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: vScale(6),
    borderRadius: moderateScale(8),
    backgroundColor: Colors.COLOR_1,
  },
});
