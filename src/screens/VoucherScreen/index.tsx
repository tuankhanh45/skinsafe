import isEqual from 'react-fast-compare';
import React, { memo, useCallback, useState } from 'react';
import { SceneMap, TabBar, TabBarItem, TabView } from 'react-native-tab-view';

import styles from './styles';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { IconArrowLeft } from '@assets';
import { Header, ListVoucher } from './components';
import { convertPhone, device, moderateScale } from '@utils';
import { Block, Text, WrapperImageBg, Touchable, ImageRemote } from '@components';

interface IVoucherScreenProps {
  route: any;
  navigation: any;
}
const ROUTES = [
  { key: 'first', title: 'Voucher của bạn' },
  { key: 'second', title: 'Đổi xu lấy Voucher' },
];

const VoucherScreenComponent: React.FC<IVoucherScreenProps> = (props) => {
  const { navigation, route } = props;
  const user = useAppStore((state) => state?.account?.user);
  const initTab = route?.params?.initTab;

  const onPressGoBack = () => {
    navigation.canGoBack() && navigation.goBack();
  };

  const [indexTab, setIndexTab] = useState<number>(initTab ?? 0);

  const renderScene = SceneMap({
    first: useCallback(() => <ListVoucher navigation={navigation} />, []),
    second: useCallback(() => <ListVoucher navigation={navigation} onPressTab={() => onPressTab(0)} />, []),
  });

  const renderTabBarItem = (tabProps: any) => {
    const isFirstTab = tabProps.key === 'first';
    return (
      <TabBarItem
        {...tabProps}
        style={
          isFirstTab
            ? {
                borderColor: Colors.COLOR_9,
                borderRightWidth: moderateScale(0.5),
              }
            : undefined
        }
      />
    );
  };

  const onPressTab = (index: number) => {
    setIndexTab(index);
  };

  const renderTabBar = (props: any) => {
    return (
      <TabBar
        {...props}
        style={styles.tabBarItem}
        activeColor={Colors.COLOR_1}
        labelStyle={styles.labelItem}
        indicatorStyle={{ height: 0 }}
        renderTabBarItem={renderTabBarItem}
        getLabelText={(e: any) => e.route?.title}
      />
    );
  };

  return (
    <WrapperImageBg style={styles.wrapper} enabledGoBack={false}>
      <Block pdHorizontal={15} pdTop={15} pdBottom={11} direction={'row'} align="center">
        <Touchable pd={4} onPress={onPressGoBack}>
          <IconArrowLeft />
        </Touchable>
        <ImageRemote
          resizeMode="cover"
          style={styles.avatar}
          source={user?.avatar}
          containerStyle={styles.avatarContainer}
        />
        <Block flex pdLeft={15}>
          <Text size="size18" fFamily="BRANDING" color="BLACK" mgBottom={4}>
            {user?.fullname}
          </Text>

          <Block pdVertical={3} pdHorizontal={10} bgColor={'#FF73C2'} borderRadius={500} alignSelf="flex-start">
            <Text color="WHITE" size="size12">
              {user?.phone ? convertPhone(user?.phone) : user?.email} {'>'}
            </Text>
          </Block>
        </Block>
      </Block>

      <Header navigation={navigation} user={user} />

      <TabView
        lazy
        renderScene={renderScene}
        onIndexChange={setIndexTab}
        renderTabBar={renderTabBar}
        keyboardDismissMode={'on-drag'}
        initialLayout={{ width: device.width }}
        navigationState={{ index: indexTab, routes: ROUTES }}
      />
    </WrapperImageBg>
  );
};

export const VoucherScreenScreen = memo(VoucherScreenComponent, isEqual);
