import isEqual from 'react-fast-compare';
import { BackHandler } from 'react-native';
import React, { useEffect, memo } from 'react';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

import styles from './styles';
import { scale } from '@utils';
import { translate } from '@assets';
import { SCREENS } from '@navigation';
import { IconSuccessAuth, images } from '@assets';
import { Button, WrapperImageBg, Block, Text } from '@components';

interface ISuccessActionAuthProps {
  route: any;
  navigation: any;
}

const SuccessActionAuthComponent: React.FC<ISuccessActionAuthProps> = (props) => {
  const { navigation, route } = props;

  const register = route?.params?.type === SCREENS.REGISTER;
  const changePassword = route?.params?.type === SCREENS.CHANGE_PASSWORD;

  const title = register ? translate('success.title_register') : translate('success.title_change_password');
  const label = register ? translate('success.description_register') : null;

  const onSubmit = () => {
    navigation.popToTop();
    if (changePassword) {
      return navigation.navigate(SCREENS.AUTH_STACK, { screen: SCREENS.LOGIN });
    }
  };

  // android go back
  useEffect(() => {
    const backAction = () => {
      return null;
    };
    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);
    return () => backHandler.remove();
  }, []);

  return (
    <WrapperImageBg style={styles.wrapper} enabledGoBack={false} source={images.background_triangular}>
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <Block mgTop={50} direction="row" align="center" justify="center">
          <IconSuccessAuth />
        </Block>
        <Text style={[styles.title, { paddingHorizontal: !register ? scale(100) : 0 }]}>{title}</Text>
        {label ? <Text style={styles.des} text={label} /> : <Block />}
        <Button onPress={onSubmit} marginTop={scale(46)}>
          {translate('login.title')}
        </Button>
      </KeyboardAwareScrollView>
    </WrapperImageBg>
  );
};

export const SuccessActionAuthScreen = memo(SuccessActionAuthComponent, isEqual);
