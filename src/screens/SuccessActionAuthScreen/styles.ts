import { StyleSheet } from 'react-native';
import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.COLOR_4,
  },
  container: {
    flex: 1,
    marginTop: vScale(58),
    paddingTop: vScale(32),
    paddingHorizontal: scale(16),
    backgroundColor: Colors.COLOR_4,
    borderTopLeftRadius: moderateScale(25),
    borderTopRightRadius: moderateScale(25),
  },
  title: {
    fontWeight: '700',
    textAlign: 'center',
    color: Colors.TEXT,
    fontSize: FontSize.size18,
    marginTop: scale(30),
  },
  des: {
    fontWeight: '500',
    textAlign: 'center',
    color: Colors.TEXT,
    fontSize: FontSize.size14,
    marginTop: scale(20),
    paddingHorizontal: scale(83),
  },
  agree: {
    fontWeight: '400',
    color: Colors.TEXT,
    fontSize: FontSize.size14,
    fontFamily: 'Helvetica Neue',
  },
});

export default styles;
