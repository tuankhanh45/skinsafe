import { Colors, FontSize } from '@themes';
import { scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
  tabBarItem: {
    backgroundColor: Colors.WHITE,
  },
  labelItem: {
    fontWeight: '500',
    color: Colors.BLACK,
    fontSize: FontSize.size12,
    textTransform: 'capitalize',
  },
});

export default styles;
