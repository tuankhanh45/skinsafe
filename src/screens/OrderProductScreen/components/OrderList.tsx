import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import React, { memo, useEffect, useRef, useState } from 'react';

import { translate } from '@assets';
import { OrderItem } from './OrderItem';
import { showToastError, showToastSuccess } from '@utils';
import { AlertModal, CustomFlatlist, ListEmpty, Loading } from '@components';
import { handleResponse, RenderItemProps, requestGetListOrder } from '@configs';

interface IOrderListProps {
  status: string;
  navigation: any;
}

const OrderListComponent: React.FC<IOrderListProps> = (props) => {
  const { status, navigation } = props;
  const refModalCancelOrder = useRef<any>(null);
  const refOrderItemToCancel = useRef<any>(null);

  const [page, setPage] = useState<number>(1);
  const [listOrder, setListOrder] = useState<any[]>([]);
  const [canLoadMore, setCanLoadMore] = useState<boolean>(true);
  const [isRefreshing, setIsRefreshing] = useState<boolean>(false);

  useEffect(() => {
    runGetList({ page, size: 10, status });
  }, [page]);

  const { run: runGetList, loading: loadingGetList } = useRequest(requestGetListOrder, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (Array.isArray(res?.data)) {
        if (res?.last_page === res?.current_page) {
          setCanLoadMore(false);
        }
        if (res?.current_page === 1) {
          setListOrder(res?.data);
        } else {
          setListOrder((prev) => [...prev, ...res?.data]);
        }
      }
    },
    onFinally: () => isRefreshing && setIsRefreshing(false),
  });

  // Refreshing call api again
  const onRefresh = () => {
    setIsRefreshing(true);
    setCanLoadMore(true);
    if (page === 1) {
      runGetList({ page: 1, size: 10, status });
    } else {
      setPage(1);
    }
  };

  // load more
  const handleLoadMore = () => {
    if (canLoadMore && !loadingGetList) {
      setPage(page + 1);
    }
  };

  const renderItem = ({ item, index }: RenderItemProps) => {
    return <OrderItem data={item} navigation={navigation} index={index} onPressCancel={() => onPressCancel(item)} />;
  };

  const onPressCancel = (item: any) => {
    refOrderItemToCancel.current = item;
    refModalCancelOrder.current.open();
  };
  const onCloseModalCancel = () => {
    refModalCancelOrder.current.close();
    refOrderItemToCancel.current = null;
  };
  const onSubmitModalCancel = () => {
    refModalCancelOrder.current.close();
    refOrderItemToCancel.current = null;
    if (+new Date() % 2) {
      showToastSuccess(translate('order_product.cancel_order_success'));
    } else {
      showToastError(translate('order_product.cancel_order_fail'));
    }
  };

  return (
    <React.Fragment>
      {loadingGetList && !listOrder.length && <Loading />}
      <CustomFlatlist
        data={listOrder}
        onRefresh={onRefresh}
        renderItem={renderItem}
        refreshing={isRefreshing}
        onEndReachedThreshold={0.3}
        onEndReached={handleLoadMore}
        ListEmptyComponent={<ListEmpty title={translate('order_product.no_order')} />}
      />

      <AlertModal
        ref={refModalCancelOrder}
        onClose={onCloseModalCancel}
        onSubmit={onSubmitModalCancel}
        titleSubmit={translate('cm.ok')}
        titleClose={translate('cm.cancel')}
        content={translate('order_product.confirm_cancel_order')}
      />
    </React.Fragment>
  );
};

export const OrderList = memo(OrderListComponent, isEqual);

const styles = StyleSheet.create({});
