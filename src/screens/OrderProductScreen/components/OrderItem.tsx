import dayjs from 'dayjs';
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { IconArrowRight, translate } from '@assets';
import { Block, ImageRemote, Text, Touchable } from '@components';
import { currencyFormat, moderateScale, scale, vScale } from '@utils';

interface IOrderItemProps {
  data: any;
  index: number;
  navigation: any;
  onPressCancel: () => void;
}

const OrderItemComponent: React.FC<IOrderItemProps> = (props) => {
  const { data, navigation, index, onPressCancel } = props;

  const onPressDetail = () => {
    navigation.navigate(SCREENS.DETAIL_ORDER_PRODUCT, { data });
  };

  return (
    <Touchable onPress={onPressDetail} style={[styles.wrapper, index === 0 && { borderTopWidth: moderateScale(5) }]}>
      <Block style={styles.top}>
        <Text size="size12" color="COLOR_1">
          {data?.status_order?.name}
        </Text>
      </Block>

      <Block style={styles.product}>
        <ImageRemote source={data?.cart?.cart_items?.[0]?.product?.avatar} style={styles.avatar} />
        <Block flex pdLeft={20}>
          <Block direction="row" align="flex-start">
            <Text flex fWeight="500">
              {data?.cart?.cart_items?.[0]?.product?.product_detail?.title}
            </Text>
            <Touchable direction="row" align="center" mgLeft={8}>
              <Text style={styles.detailText}>{translate('order_product.detail')} </Text>
              <IconArrowRight color={Colors.COLOR_1} />
            </Touchable>
          </Block>
          <Text color="COLOR_1">{currencyFormat(120000)}</Text>
          <Text size="size12" fWeight="500" color="COLOR_9" mgVertical={5}>
            {dayjs().format('DD-MM-YYYY HH:mm')}
          </Text>
          <Block direction="row" justify="space-between">
            <Text fWeight="500">x{data?.cart?.cart_items?.[0]?.quantity}</Text>
            <Text color="COLOR_1" fWeight="500">
              {currencyFormat(data?.cart?.cart_items?.[0]?.total_price)}
            </Text>
          </Block>
        </Block>
      </Block>

      {data?.cart?.cart_items?.length > 1 && (
        <Touchable style={styles.boxAnotherProduct}>
          <Text size="size12" mgRight={4}>
            {translate('order_product.see_more')}
          </Text>
          <IconArrowRight />
        </Touchable>
      )}

      <Block direction="row" pdHorizontal={15} pdBottom={11}>
        <Touchable style={[styles.button, { marginRight: scale(15) }]} onPress={onPressCancel}>
          <Text fWeight="500" size="size16" color="WHITE">
            {translate('cm.cancel')}
          </Text>
        </Touchable>
        <Touchable style={[styles.button, { backgroundColor: Colors.COLOR_6 }]}>
          <Text fWeight="500" size="size16" color="WHITE">
            {translate('order_product.repurchase')}
          </Text>
        </Touchable>
      </Block>
    </Touchable>
  );
};

export const OrderItem = memo(OrderItemComponent, isEqual);

const styles = StyleSheet.create({
  top: {
    alignItems: 'flex-end',
    paddingTop: vScale(24),
    paddingRight: scale(9),
    paddingBottom: vScale(11),
  },
  product: {
    flexDirection: 'row',
    paddingTop: vScale(5),
    paddingBottom: vScale(3),
    paddingHorizontal: scale(10),
  },
  avatar: {
    width: scale(43),
    height: scale(43),
    borderRadius: moderateScale(4),
  },
  detailText: {
    color: Colors.COLOR_1,
    textDecorationLine: 'underline',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    marginTop: vScale(25),
    justifyContent: 'center',
    paddingVertical: vScale(8),
    borderRadius: moderateScale(4),
    backgroundColor: Colors.COLOR_1,
  },
  boxAnotherProduct: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: vScale(4),
    borderColor: Colors.COLOR_9,
    borderTopWidth: moderateScale(0.5),
    borderBottomWidth: moderateScale(0.5),
  },
  wrapper: {
    borderColor: '#F4F3F9',
    borderBottomWidth: moderateScale(5),
  },
});
