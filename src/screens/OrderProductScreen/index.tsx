import isEqual from 'react-fast-compare';
import React, { memo, useState, useCallback } from 'react';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import styles from './styles';
import { device } from '@utils';
import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { OrderList } from './components';
import { IconSearch, translate } from '@assets';
import { HeaderBar, SafeWrapper, Touchable } from '@components';

interface IOrderProductProps {
  route: any;
  navigation: any;
}

const ROUTES = [
  { key: 'first', id: 1, title: 'Chờ xác nhận' },
  { key: 'second', id: 2, title: 'Đang vận chuyển' },
  { key: 'third', id: 3, title: 'Đã nhận' },
  { key: 'fourth', id: 4, title: 'Đã hủy' },
  { key: 'fifth', id: 5, title: 'Khác' },
];

const OrderProductComponent: React.FC<IOrderProductProps> = (props) => {
  const { route, navigation } = props;
  const status = route?.params?.status;

  //TODO: update list status
  const index = Number(status) ? status - 1 : 0;

  const [indexTab, setIndexTab] = useState<number>(index);

  const renderScene = SceneMap({
    first: useCallback(() => <OrderList navigation={navigation} status={'1'} />, []),
    second: useCallback(() => <OrderList navigation={navigation} status={'2'} />, []),
    third: useCallback(() => <OrderList navigation={navigation} status={'3'} />, []),
    fourth: useCallback(() => <OrderList navigation={navigation} status={'4'} />, []),
    fifth: useCallback(() => <OrderList navigation={navigation} status={'5'} />, []),
  });

  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      scrollEnabled
      style={styles.tabBarItem}
      activeColor={Colors.COLOR_1}
      labelStyle={styles.labelItem}
      getLabelText={(e: any) => e.route?.title}
      indicatorStyle={{ backgroundColor: Colors.COLOR_1 }}
    />
  );

  const renderRight = () => {
    return (
      <Touchable pd={8} onPress={() => navigation.navigate(SCREENS.SEARCH)}>
        <IconSearch color={Colors.WHITE} width={24} height={24} />
      </Touchable>
    );
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('order_product.title')} renderRight={renderRight} highlightCenter />

      <TabView
        lazy
        renderScene={renderScene}
        onIndexChange={setIndexTab}
        renderTabBar={renderTabBar}
        keyboardDismissMode={'on-drag'}
        initialLayout={{ width: device.width }}
        navigationState={{ index: indexTab, routes: ROUTES }}
      />
    </SafeWrapper>
  );
};

export const OrderProductScreen = memo(OrderProductComponent, isEqual);
