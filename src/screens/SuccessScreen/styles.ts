import { StyleSheet } from 'react-native';
import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.COLOR_1,
  },
  content: {
    marginTop: vScale(47),
    backgroundColor: Colors.COLOR_4,
    borderTopLeftRadius: moderateScale(25),
    borderTopRightRadius: moderateScale(25),
  },
  title: {
    fontWeight: '700',
    textAlign: 'center',
    color: Colors.TEXT,
    marginTop: scale(30),
    fontSize: FontSize.size18,
  },
  des: {
    fontWeight: '500',
    textAlign: 'center',
    color: Colors.TEXT,
    marginTop: scale(20),
    fontSize: FontSize.size14,
    paddingHorizontal: scale(83),
  },
});

export default styles;
