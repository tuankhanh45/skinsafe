import { Animated } from 'react-native';
import { ScrollView } from 'native-base';
import isEqual from 'react-fast-compare';
import React, { useEffect, memo, useRef } from 'react';
import SafeAreaView from 'react-native-safe-area-view';
import { BackHandler, SafeAreaView as RNSafeAreaView } from 'react-native';

import styles from './styles';
import { device } from '@utils';
import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { Block, Text, Touchable } from '@components';
import { IconCancel, translate, IconSuccessAuth } from '@assets';

interface ISuccessScreenProps {
  route: any;
  navigation: any;
}
const Safe = device.isIos ? RNSafeAreaView : SafeAreaView;

const SuccessScreenComponent: React.FC<ISuccessScreenProps> = (props) => {
  const { navigation, route } = props;

  const refAnimation = useRef(new Animated.Value(0)).current;

  const title = route?.params?.title;
  const content = route?.params?.content;
  const onCbSubmit = route?.params?.onCbSubmit;

  const onSubmit = () => {
    onCbSubmit && onCbSubmit();
  };

  useEffect(() => {
    Animated.timing(refAnimation, {
      toValue: 1,
      duration: 1400,
      useNativeDriver: true,
    }).start();
    const backAction = () => {
      return null;
    };
    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);
    return () => backHandler.remove();
  }, []);

  const onPressHome = () => {
    navigation.reset({ index: 0, routes: [{ name: SCREENS.MAIN_STACK }] });
  };

  const scaleAnim = refAnimation.interpolate({
    inputRange: [0, 0.1, 0.8, 1],
    outputRange: [1, 0.9, 1.2, 1],
  });

  return (
    <>
      <Safe style={styles.wrapper}>
        <Touchable alignSelf="flex-end" pd={8} onPress={onPressHome}>
          <IconCancel color={Colors.WHITE} />
        </Touchable>

        <ScrollView style={styles.content}>
          <Block center mgTop={100}>
            <Animated.View style={{ transform: [{ scale: scaleAnim }] }}>
              <IconSuccessAuth />
            </Animated.View>
          </Block>

          <Text style={styles.title}>{title}</Text>
          {content ? <Text style={styles.des} text={content} /> : <Block />}

          {onCbSubmit && (
            <Touchable onPress={onSubmit} center pd={8} mgTop={30}>
              <Text>{translate('detail_near_shop.check_info')}</Text>
            </Touchable>
          )}
        </ScrollView>
      </Safe>
      <Safe style={{ flex: 0, backgroundColor: Colors.WHITE }} />
    </>
  );
};

export const SuccessScreen = memo(SuccessScreenComponent, isEqual);
