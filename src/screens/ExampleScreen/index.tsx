import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

import styles from './styles';
import { Block, HeaderBar, SafeWrapper, Text } from '@components';

interface IExampleProps {
  route: any;
  navigation: any;
}

const ExampleComponent: React.FC<IExampleProps> = (props) => {
  const {} = props;

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title="Example" />
    </SafeWrapper>
  );
};

export const ExampleScreen = memo(ExampleComponent, isEqual);
