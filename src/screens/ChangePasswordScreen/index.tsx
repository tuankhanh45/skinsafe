import React, { memo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import size from 'lodash/size';
import styles from './styles';
import { IconEyeClose, IconEyeOpen, IconKey } from '@assets';
import { Button, Text, WrapperImageBg, TextField, Form, Block, Touchable } from '@components';
import { useForm } from 'react-hook-form';
import { scale, useValidateForm } from '@utils';
import { SCREENS } from '@navigation';
import { translate, images } from '@assets';
import { useAppStore } from '@store';
import { ICreateAccountState, requestChangePassword } from '@configs';
import { handleResponse, requestLogin } from '@configs';
import { useRequest } from 'ahooks';
import { Alert } from 'react-native';
interface IChangePasswordProps {
  route: any;
  navigation: any;
}

const ChangePasswordComponent: React.FC<IChangePasswordProps> = (props) => {
  const { navigation, route } = props;
  const actRemoveLoggerUser = useAppStore((state: ICreateAccountState) => state.actRemoveLoggerUser);
  const [isHideOldPass, setIsHideOldPass] = useState<boolean>(true);
  const [isHidePass, setIsHidePass] = useState<boolean>(true);
  const [isHideRePass, setIsHideRePass] = useState<boolean>(true);
  console.log('ChangePasswordComponent', route?.params);
  // form
  const formMethods = useForm();
  const { rules } = useValidateForm(formMethods.getValues);
  const { errors } = formMethods.formState;
  const onErrors = (errors: any) => {};
  const onSubmit = (form: any) => {
    console.log('submit', form);
    const params = {
      phone: route?.params?.phone ?? '',
      old_password: form?.old_password ?? '',
      new_password: form?.password ?? '',
      confirm_password: form?.re_password ?? '',
    };
    run(params);
  };
  const { run, loading, cancel } = useRequest(requestChangePassword, {
    manual: true,
    onSuccess: (data) => {
      const { res, err, isError } = handleResponse(data);
      if (isError) {
        return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại sau!');
      } else {
        actRemoveLoggerUser();
        navigation.navigate(SCREENS.SUCCESS_ACTION_AUTH, { type: SCREENS.CHANGE_PASSWORD });
      }
    },
    onError: (err) => {
      return Alert.alert('Lỗi', err?.message ?? 'Xin thử lại sau');
    },
  });

  return (
    <WrapperImageBg style={styles.wrapper} source={images.background_triangular}>
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <Text style={styles.title}>{translate('change_password.title')}</Text>
        <Text style={styles.des} text={translate('change_password.description')} />
        <Block mgTop={49}>
          <Form {...{ ...formMethods, onSubmit, onErrors, rules }}>
            <TextField
              name={'old_password'}
              returnKeyType={'done'}
              placeholder={translate('placeholder.old_password')}
              iconLeft={() => <IconKey />}
              secureTextEntry={isHideOldPass}
              iconRight={() => (isHideOldPass ? <IconEyeClose /> : <IconEyeOpen />)}
              onPressIcon={() => setIsHideOldPass(!isHideOldPass)}
            />
            <TextField
              name={'password'}
              returnKeyType={'done'}
              placeholder={translate('placeholder.new_password')}
              iconLeft={() => <IconKey />}
              secureTextEntry={isHidePass}
              iconRight={() => (isHidePass ? <IconEyeClose /> : <IconEyeOpen />)}
              onPressIcon={() => setIsHidePass(!isHidePass)}
            />
            <TextField
              name={'re_password'}
              returnKeyType={'done'}
              placeholder={translate('placeholder.confirm_new_password')}
              iconLeft={() => <IconKey />}
              secureTextEntry={isHideRePass}
              iconRight={() => (isHideRePass ? <IconEyeClose /> : <IconEyeOpen />)}
              onPressIcon={() => setIsHideRePass(!isHideRePass)}
            />
          </Form>
        </Block>

        <Button disabled={!!size(errors)} onPress={formMethods.handleSubmit(onSubmit, onErrors)} marginTop={scale(46)}>
          {translate('cm.next')}
        </Button>
      </KeyboardAwareScrollView>
    </WrapperImageBg>
  );
};

export const ChangePasswordScreen = memo(ChangePasswordComponent, isEqual);
