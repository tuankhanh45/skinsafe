import isEqual from 'react-fast-compare';
import { ScrollView } from 'react-native';
import React, { memo, useState } from 'react';

import styles from './styles';
import { scale } from '@utils';
import { translate } from '@assets';
import { useAppStore } from '@store';
import { SCREENS } from '@navigation';
import { Attribute } from './components';
import { Block, Button, HeaderBar, HotelItem, ImageRemote, SafeWrapper, SpaItem, Text, Touchable } from '@components';

interface IDetailHotelAndSpaProps {
  route: any;
  navigation: any;
}

const DetailHotelAndSpaComponent: React.FC<IDetailHotelAndSpaProps> = (props) => {
  const { route, navigation } = props;
  const type = route?.params?.type;
  const data = route?.params?.data;

  const listPet = useAppStore((x) => x.pet.listPet);

  const [selectedPet, setSelectedPet] = useState<any[]>([]);

  const onSelectItem = (item: any) => {
    const index = selectedPet.findIndex((x) => x?.id === item?.id);
    if (index > -1) {
      setSelectedPet((prev) => prev.filter((x) => x?.id !== item?.id));
    } else {
      setSelectedPet((prev) => [...prev, item]);
    }
  };

  const onPressSubmit = () => {
    return navigation.navigate(SCREENS.SUCCESS, { title: translate(`detail_hotel_spa.title_success_${type}`) });
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate(`detail_hotel_spa.title_${type}`)} />

      <ScrollView style={styles.container}>
        <Block pdHorizontal={15}>
          <ImageRemote source="https://picsum.photos/801" style={styles.bannerImg} resizeMode="cover" />
          <Block pdTop={16}>
            {type === 'hotel' ? (
              <HotelItem data={data} type={type} navigation={navigation} />
            ) : (
              <SpaItem data={data} type={type} navigation={navigation} />
            )}
          </Block>

          <Attribute />

          <Text size="size15" fFamily="BRANDING" mgTop={28}>
            {translate('detail_hotel_spa.select_pet')}
          </Text>
        </Block>
        <ScrollView horizontal contentContainerStyle={{ paddingHorizontal: scale(15) }}>
          {listPet.map((e, i) => {
            const index = selectedPet.findIndex((x) => x?.id === e.id);
            const isSelected = index > -1;
            return (
              <Block key={i} center w={48}>
                <Touchable style={[styles.petItem, isSelected && { opacity: 1 }]} onPress={() => onSelectItem(e)}>
                  <ImageRemote source={e?.avatar} style={styles.avatar} resizeMode={'cover'} />
                </Touchable>
                <Text size="size15" mgTop={6} numberOfLines={2}>
                  {e?.name}
                </Text>
              </Block>
            );
          })}
        </ScrollView>
      </ScrollView>

      <Block pdHorizontal={4} pdVertical={8}>
        <Button disabled={selectedPet.length === 0} onPress={onPressSubmit}>
          {translate('detail_hotel_spa.submit')}
        </Button>
      </Block>
    </SafeWrapper>
  );
};

export const DetailHotelAndSpaScreen = memo(DetailHotelAndSpaComponent, isEqual);
