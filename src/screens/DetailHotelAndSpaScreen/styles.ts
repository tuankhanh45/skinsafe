import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingVertical: vScale(8),
  },
  bannerImg: {
    width: '100%',
    height: vScale(120),
    borderRadius: moderateScale(2),
  },
  avatar: {
    width: scale(44),
    height: scale(44),
    borderRadius: moderateScale(44),
  },
  petItem: {
    opacity: 0.5,
    width: scale(48),
    height: scale(48),
    alignItems: 'center',
    marginTop: vScale(16),
    justifyContent: 'center',
    borderRadius: moderateScale(48),
    backgroundColor: Colors.COLOR_11,
  },
});

export default styles;
