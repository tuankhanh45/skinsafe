import isEqual from 'react-fast-compare';
import React, { memo, useRef, useState } from 'react';
import { Animated, StyleSheet, LayoutAnimation } from 'react-native';

import { vScale } from '@utils';
import { Colors } from '@themes';
import { IconArrowDown, translate } from '@assets';
import { Block, Text, Touchable } from '@components';

interface IAttributeProps {}

const AttributeComponent: React.FC<IAttributeProps> = (props) => {
  const {} = props;

  const refAnim = useRef(new Animated.Value(0)).current;

  const [isShow, setIsShow] = useState<boolean>(true);

  const onPressShow = () => {
    Animated.timing(refAnim, {
      toValue: isShow ? 0 : 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
    setIsShow((prev) => !prev);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const rotate = refAnim.interpolate({
    inputRange: [0, 1],
    outputRange: ['180deg', '360deg'],
  });

  return (
    <Block>
      <Block direction="row" align="center" justify="space-between">
        <Text size="size15" fFamily="BRANDING">
          {translate('detail_hotel_spa.att')}
        </Text>
        <Touchable direction="row" align="center" onPress={onPressShow}>
          <Text mgRight={4}>{translate(`detail_hotel_spa.${isShow ? 'hide' : 'show'}`)}</Text>
          <Animated.View style={{ transform: [{ rotate }] }}>
            <IconArrowDown />
          </Animated.View>
        </Touchable>
      </Block>

      {isShow && (
        <Block>
          {['Chuồng riêng', 'Đệm', 'Ăn(3 bữa/ngày)', 'Khu vui chơi'].map((e, i) => {
            return (
              <Block key={i} mgHorizontal={23}>
                <Block style={styles.attItem}>
                  <Text>{e}</Text>
                  <Text>Có</Text>
                </Block>
                {i !== 3 && <Block style={styles.dashed} />}
              </Block>
            );
          })}
        </Block>
      )}
    </Block>
  );
};

export const Attribute = memo(AttributeComponent, isEqual);

const styles = StyleSheet.create({
  attItem: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: vScale(5),
    justifyContent: 'space-between',
  },
  dashed: {
    height: 1,
    width: '100%',
    borderRadius: 1,
    borderWidth: 1,
    borderStyle: 'dotted',
    borderColor: Colors.COLOR_9,
  },
});
