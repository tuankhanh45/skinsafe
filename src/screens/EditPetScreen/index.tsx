import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

import styles from './styles';
import { Block, HeaderBar, SafeWrapper, Text } from '@components';

interface IEditPetProps {
  route: any;
  navigation: any;
}

const EditPetComponent: React.FC<IEditPetProps> = (props) => {
  const {} = props;

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title="EditPet" />
    </SafeWrapper>
  );
};

export const EditPetScreen = memo(EditPetComponent, isEqual);
