import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { translate } from '@assets';
import { RenderItemProps } from '@configs';
import { Block, CustomFlatlist, ListEmpty } from '@components';

interface IOrderListProps {
  navigation: any;
  status: string;
}

const OrderListComponent: React.FC<IOrderListProps> = (props) => {
  const {} = props;

  const renderItem = ({ item }: RenderItemProps) => {
    return <Block></Block>;
  };

  return (
    <CustomFlatlist
      data={[]}
      renderItem={renderItem}
      ListEmptyComponent={<ListEmpty title={translate('order_product.no_order')} />}
    />
  );
};

export const OrderList = memo(OrderListComponent, isEqual);

const styles = StyleSheet.create({});
