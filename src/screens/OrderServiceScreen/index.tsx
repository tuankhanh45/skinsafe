import isEqual from 'react-fast-compare';
import React, { memo, useState, useCallback } from 'react';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import styles from './styles';
import { device } from '@utils';
import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { OrderList } from './components';
import { IconSearch, translate } from '@assets';
import { STATUS_ORDER_PRODUCT } from '@src/contants';
import { Block, HeaderBar, SafeWrapper, Touchable } from '@components';

interface IOrderServiceProps {
  route: any;
  navigation: any;
}

const ROUTES = [
  { key: 'first', title: 'confirm' },
  { key: 'second', title: 'shipping' },
  { key: 'third', title: 'shipped' },
  { key: 'fourth', title: 'rate' },
];

const OrderServiceComponent: React.FC<IOrderServiceProps> = (props) => {
  const { route, navigation } = props;
  const status = route?.params?.status;

  const index =
    status === STATUS_ORDER_PRODUCT.CONFIRM
      ? 0
      : status === STATUS_ORDER_PRODUCT.SHIPPING
      ? 1
      : status === STATUS_ORDER_PRODUCT.SHIPPED
      ? 2
      : status === STATUS_ORDER_PRODUCT.RATE
      ? 3
      : 0;

  const [indexTab, setIndexTab] = useState<number>(index);

  const renderScene = SceneMap({
    first: useCallback(() => <OrderList navigation={navigation} status={STATUS_ORDER_PRODUCT.CONFIRM} />, []),
    second: useCallback(() => <OrderList navigation={navigation} status={STATUS_ORDER_PRODUCT.SHIPPING} />, []),
    third: useCallback(() => <OrderList navigation={navigation} status={STATUS_ORDER_PRODUCT.SHIPPED} />, []),
    fourth: useCallback(() => <OrderList navigation={navigation} status={STATUS_ORDER_PRODUCT.RATE} />, []),
  });

  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      scrollEnabled
      style={styles.tabBarItem}
      activeColor={Colors.COLOR_1}
      labelStyle={styles.labelItem}
      indicatorStyle={{ backgroundColor: Colors.COLOR_1 }}
      getLabelText={(e) => translate(`order_product.${e.route?.title}`)}
    />
  );

  const renderRight = () => {
    return (
      <Touchable pd={8} onPress={() => navigation.navigate(SCREENS.SEARCH)}>
        <IconSearch color={Colors.WHITE} />
      </Touchable>
    );
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('order_service.title')} renderRight={renderRight} highlightCenter />

      <TabView
        lazy
        renderScene={renderScene}
        onIndexChange={setIndexTab}
        renderTabBar={renderTabBar}
        keyboardDismissMode={'on-drag'}
        initialLayout={{ width: device.width }}
        navigationState={{ index: indexTab, routes: ROUTES }}
      />
    </SafeWrapper>
  );
};

export const OrderServiceScreen = memo(OrderServiceComponent, isEqual);
