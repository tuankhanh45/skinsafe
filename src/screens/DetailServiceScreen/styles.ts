import { Colors } from '@themes';
import { scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  columnWrapper: {
    marginBottom: vScale(20),
    paddingHorizontal: scale(15),
    justifyContent: 'space-between',
  },
});

export default styles;
