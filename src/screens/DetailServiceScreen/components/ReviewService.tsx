import dayjs from 'dayjs';
import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { ImageBackground, StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { images, translate } from '@assets';
import { currencyFormat, device, moderateScale, scale, vScale } from '@utils';
import { Block, Button, ImageRemote, StarRate, Text, Touchable } from '@components';
import { useNavigation } from '@react-navigation/native';
import { SCREENS } from '@navigation';

interface IReviewServiceProps {}

const data = {
  price: 200000,
  review: {
    avatar: 'https://picsum.photos/801',
    name: 'thedozsd9909',
    rate: 5,
    title: 'Sản phẩm rất tốt',
    des: 'Rất hài lòng. Lần đầu mua của shop. Khá lo lắng nhưng khi nhận được giày thì rất chi là vừa ý. Hợp giá tiền. Đáng mua nha mọi người. Hộp hơi hơi móp nên hơi tiếc xíu.',
    rate_type: [
      { name: 'Chất lượng phục vụ của shop', rate: 5 },
      { name: 'Đóng gói', rate: 5 },
      { name: 'Rất đáng tiền', rate: 5 },
    ],
  },
  total_review: 5,
};
const ReviewServiceComponent: React.FC<IReviewServiceProps> = (props) => {
  const {} = props;
  const navigation = useNavigation<any>();

  const review = () => {
    navigation.navigate(SCREENS.REVIEW, { type: 'SERVICE' });
  };
  const moreReview = () => {
    navigation.navigate(SCREENS.DETAIL_REVIEW_SERVICE);
  };
  const orderService = () => {};
  return (
    <Block pdHorizontal={15}>
      <Touchable style={styles.coverBtn} onPress={review}>
        <Text text={translate('service.rate_write')} color="COLOR_1" fWeight="600" size={'size14'} />
      </Touchable>
      {/*  */}
      <Block style={styles.coverReview}>
        <Text text={translate('service.rate')} color="TEXT" fWeight="500" size={'size14'} mgTop={5} />
        <ItemReview data={data?.review} />
        <Text
          onPress={moreReview}
          text={`Xem tất cả (${data?.total_review - 1})`}
          color="COLOR_1"
          fWeight="600"
          size={'size14'}
          textAlign="center"
          mgTop={16}
          style={{ textDecorationLine: 'underline' }}
        />
      </Block>
      {/*  order */}
      <Button onPress={orderService}>{translate('service.order') + `(${currencyFormat(data?.price, 'vnd')})`}</Button>
    </Block>
  );
};

export const ReviewService = memo(ReviewServiceComponent, isEqual);
const DATA = [1, 2, 3];
const ItemReview = memo((props: any) => {
  const { data } = props;
  return (
    <Block style={styles.itemWrapper}>
      <Block direction="row">
        {/* info */}
        <ImageRemote source={data?.avatar} style={styles.avatar} />
        <Block flex align="flex-start" pdLeft={10}>
          <Block direction="row" mgBottom={4}>
            <Text fWeight="600" size="size12" color="PRIMARY" mgRight={10} flex>
              {data?.name}
            </Text>
          </Block>
          <StarRate rate={data?.rate} />
          <Text text={data?.title} mgTop={10} size={'size12'} color="TEXT" fWeight="700" />
          <Text text={data?.des} mgTop={10} size={'size10'} color="TEXT" />
          <Block w="95%" mgTop={8} mgBottom={18}>
            {data?.rate_type?.map((e: any, i: number) => (
              <Block key={i + 'tp'} direction="row" align="center" justify="space-between" mgTop={3}>
                <Text text={e?.name} size={'size10'} color="TEXT" />
                <StarRate rate={e?.rate} size={10} />
              </Block>
            ))}
          </Block>

          {/* img list */}
          <Block direction="row" mgVertical={8}>
            {DATA.map((e, i) => {
              const isLastItem = i === DATA.length - 1;
              return (
                <Touchable style={styles.imgBox} key={i}>
                  <ImageRemote style={styles.img} source={'https://picsum.photos/801'} key={i} resizeMode={'cover'} />
                  {isLastItem && (
                    <Block style={styles.moreBox}>
                      <Text fWeight="600" size={'size18'} color={'COLOR_4'}>
                        +2
                      </Text>
                    </Block>
                  )}
                </Touchable>
              );
            })}
          </Block>
          {/* date */}
          <Text text={dayjs().format('DD-MM-YYYY hh:mm')} mgTop={12} size={'size10'} color="TEXT_2" />
        </Block>
      </Block>
    </Block>
  );
});

const styles = StyleSheet.create({
  wrapper: {},
  coverBtn: {
    height: vScale(38),
    width: device.width - scale(30),
    alignItems: 'center',
    borderColor: Colors.COLOR_1,
    borderWidth: moderateScale(2),
    borderRadius: scale(8),
    justifyContent: 'center',
    marginTop: vScale(32),
  },
  coverReview: {
    marginTop: vScale(39),
    backgroundColor: '#F4F4F4',
    marginBottom: vScale(28),
    paddingBottom: scale(16),
  },
  itemWrapper: {
    borderBottomColor: Colors.TEXT_2,
    borderBottomWidth: moderateScale(1),
    borderTopColor: Colors.TEXT_2,
    borderTopWidth: moderateScale(1),
    paddingVertical: vScale(13),
    marginTop: vScale(13),
  },
  avatar: {
    height: scale(31),
    width: scale(31),
    borderRadius: moderateScale(31),
  },
  imgBox: {
    width: scale(90),
    height: vScale(94),
    borderRadius: moderateScale(6),
    marginRight: (device.width - scale(8 * 2) - scale(15 * 2) - scale(4 * 75)) / 3,
  },
  img: {
    height: '100%',
    width: '100%',
    borderRadius: moderateScale(6),
  },
  moreBox: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(6),
    backgroundColor: 'rgba(49, 49, 49, .7)',
  },
});
