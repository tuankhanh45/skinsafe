import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Block, ImageRemote, StarRate, Text } from '@components';
import { moderateScale, scale, vScale } from '@utils';
import { Colors } from '@themes';
import { IconStar, translate } from '@assets';
import times from 'lodash/times';
interface IDescServiceProps {
  data: any;
}

const DescServiceComponent: React.FC<IDescServiceProps> = (props) => {
  // const { data } = props;
  const data = {
    img: 'https://bizweb.dktcdn.net/100/091/443/products/hinh-cho-grooming.jpg?v=1653390558433',
    rate: 5,
    duration: '15 - 20p',
    des: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et, fames mauris interdum vitae blandit id et vestibulum. Tellus in non pharetra egestas iaculis ullamcorper. Purus imperdiet eget amet porttitor fringilla ornare felis arcu. Diam et risus sagittis nam sed habitant sit enim nibh. Dictum adipiscing cras dui aliquam ut velit scelerisque adipiscing. Adipiscing commodo, ultricies ultrices mauris. In in lorem sed vel ornare non id molestie id. Congue vulputate sed sed nisl sit. Etiam arcu nunc, dignissim pharetra ante nam sagittis. Sit purus amet vitae sit sapien vulputate. Vehicula mauris turpis lectus dolor ultricies semper euismod. Nisl et at turpis ante a.',
  };
  return (
    <Block>
      <ImageRemote source={data?.img} resizeMode="cover" style={styles.avatar} />
      <Block mgHorizontal={15}>
        <Block style={[styles.coverStar]} direction="row" align="center" justify="space-between" mgTop={16}>
          <Text text={translate('service.detail')} fWeight="700" size="size14" fFamily="MEDIUM" color="TEXT" />
          {/* star */}
          <StarRate rate={data?.rate} size={14} />
          <Text text={data?.duration} fWeight="500" size="size12" fFamily="MEDIUM" color="COLOR_3" />
        </Block>
        {/* des */}
        <Block style={styles.coverBox} mgTop={8}>
          <Text size="size12" textAlign="justify" text={data?.des}></Text>
        </Block>
      </Block>
    </Block>
  );
};

export const DescService = memo(DescServiceComponent, isEqual);

const styles = StyleSheet.create({
  avatar: { height: vScale(144), width: '100%' },
  coverStar: {
    borderBottomColor: Colors.COLOR_9,
    borderTopColor: Colors.COLOR_9,
    borderTopWidth: moderateScale(0.3),
    borderBottomWidth: moderateScale(0.3),
    paddingVertical: vScale(8),
  },
  coverBox: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    padding: scale(10),
  },
});
