import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { Block, HeaderBar, SafeWrapper, CartBadgeBtn, Touchable } from '@components';
import styles from './styles';
import { IconSearch, translate } from '@assets';
import { SCREENS } from '@navigation';
import { RenderItemProps } from '@configs';
import { useNavigation } from '@react-navigation/native';
import { Colors } from '@themes';
import { DescService, ReviewService } from './components';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

interface IDetailServiceProps {}

const DetailServiceComponent: React.FC<IDetailServiceProps> = (props) => {
  const navigation = useNavigation<any>();
  const renderRight = () => {
    return (
      <Block direction="row" align="center">
        <Touchable>
          <IconSearch color={Colors.WHITE} width={24} height={22} />
        </Touchable>
        <CartBadgeBtn />
      </Block>
    );
  };
  const onNavigate = (screen: string) => {
    navigation.navigate(screen);
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={'Tỉa lông Smart Power'} renderRight={renderRight} />
      <KeyboardAwareScrollView>
        <DescService data={{}} />
        <ReviewService />
      </KeyboardAwareScrollView>
    </SafeWrapper>
  );
};

export const DetailServiceScreen = memo(DetailServiceComponent, isEqual);
