import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  dateBtn: {
    paddingVertical: vScale(7),
    paddingHorizontal: scale(5),
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(4),
    borderColor: 'rgba(0, 0, 0, .1)',
  },
  contentContainer: {
    paddingHorizontal: scale(15),
  },
});

export default styles;
