import dayjs from 'dayjs';
import { Divider } from 'native-base';
import isEqual from 'react-fast-compare';
import React, { memo, useRef, useState } from 'react';

import {
  Block,
  Text,
  Touchable,
  HeaderBar,
  ListEmpty,
  SafeWrapper,
  CustomFlatlist,
  ModalSelectDate,
  NotificationBadgeBtn,
} from '@components';
import styles from './styles';
import { translate } from '@assets';
import { RenderItemProps } from '@configs';
import { ItemHistory } from './components';

interface IHistoryAdviseProps {
  route: any;
  navigation: any;
}

const HistoryAdviseComponent: React.FC<IHistoryAdviseProps> = (props) => {
  const {} = props;

  const refModalDateFrom = useRef<any>(null);
  const refModalDateTo = useRef<any>(null);

  const [filterDate, setFilterDate] = useState<any>({
    from: dayjs().subtract(1, 'month'),
    to: dayjs().toDate(),
  });

  const onPressFrom = () => refModalDateFrom.current.open();
  const onPressTo = () => refModalDateTo.current.open();

  const onSelectDateFrom = (date: Date) => {
    setFilterDate((prev: any) => ({ ...prev, from: date }));
  };
  const onSelectDateTo = (date: Date) => {
    setFilterDate((prev: any) => ({ ...prev, to: date }));
  };

  const renderRight = () => <NotificationBadgeBtn />;

  const renderItem = ({ item }: RenderItemProps) => {
    return <ItemHistory data={item} />;
  };

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderBar title={translate('history_advise.title')} renderRight={renderRight} />
      <CustomFlatlist
        data={[1, 2, 3]}
        renderItem={renderItem}
        contentContainerStyle={styles.contentContainer}
        ListEmptyComponent={ListEmpty}
        ListHeaderComponent={
          <ListHeaderComponent filterDate={filterDate} onPressFrom={onPressFrom} onPressTo={onPressTo} />
        }
      />

      <ModalSelectDate
        maxDate={new Date()}
        ref={refModalDateFrom}
        initDate={filterDate?.from}
        setSelectedDate={onSelectDateFrom}
      />
      <ModalSelectDate
        maxDate={new Date()}
        ref={refModalDateTo}
        initDate={filterDate?.to}
        minDate={filterDate?.from}
        setSelectedDate={onSelectDateTo}
      />
    </SafeWrapper>
  );
};

export const HistoryAdviseScreen = memo(HistoryAdviseComponent, isEqual);

const ListHeaderComponent = memo((props: any) => {
  const { filterDate, onPressTo, onPressFrom } = props;

  return (
    <Block pdVertical={16}>
      <Block direction="row" justify="space-between" pdBottom={16}>
        <Touchable style={styles.dateBtn} onPress={onPressFrom}>
          <Text size="size12" fWeight="500">
            Từ: {dayjs(filterDate?.from).format('DD/MM/YYYY')}
          </Text>
        </Touchable>
        <Touchable style={styles.dateBtn} onPress={onPressTo}>
          <Text size="size12" fWeight="500">
            Đến: {dayjs(filterDate?.to).format('DD/MM/YYYY')}
          </Text>
        </Touchable>
      </Block>

      <Divider />

      <Text fWeight="500" mgTop={16}>
        {translate('history_advise.title')}
      </Text>
    </Block>
  );
});
