import dayjs from 'dayjs';
import React, { memo } from 'react';
import { Divider } from 'native-base';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { IconCall, IconStar, translate } from '@assets';
import { Block, ImageRemote, Text, Touchable } from '@components';

interface IItemHistoryProps {
  data: any;
}
const LIST_CRITERIA = [{ label: 'booking' }, { label: 'advise' }, { label: 'rate' }];

const ItemHistoryComponent: React.FC<IItemHistoryProps> = (props) => {
  const { data } = props;

  return (
    <Block style={styles.wrapper}>
      <Block pd={6} direction="row">
        <ImageRemote source={'https://picsum.photos/801'} style={styles.avatar} />
        <Block flex pdLeft={11}>
          <Text numberOfLines={1} size="size16" fWeight="500">
            GS: Đặng Trị Bệnh
          </Text>
          <Text numberOfLines={1} size="size12" fWeight="500" color="COLOR_15" mgTop={4}>
            Chuyên khoa: Hô hấp
          </Text>
        </Block>
      </Block>

      <Block style={styles.criteria}>
        {LIST_CRITERIA.map((e, i) => {
          return (
            <Block style={[styles.item, i === 2 && { borderRightWidth: 0 }]} key={i}>
              <Text size="size12" fWeight="700">
                {translate(`remote_advise.${e.label}`)}
              </Text>
              <Block direction="row" center mgTop={5}>
                {i === 2 && <IconStar />}
                <Text size="size12" color="COLOR_3" mgLeft={i === 2 ? 3 : 0}>
                  5000
                </Text>
              </Block>
            </Block>
          );
        })}
      </Block>

      <Divider />

      <Block direction="row">
        <Block style={styles.leftInfo}>
          <Text size="size16" fWeight="500">
            Tư vấn hô hấp
          </Text>
          <Text mgTop={6} color="COLOR_9" size="size12">
            {dayjs().format('DD/MM/YYYY')}
          </Text>
        </Block>
        <Block flex center>
          <Text size="size12" fWeight="500">
            Gọi điện
          </Text>
          <Text size="size12" color="COLOR_1" mgTop={6}>
            090 666 8888
          </Text>
        </Block>
      </Block>

      <Divider />

      <Touchable center direction="row" pdTop={8} pdBottom={11}>
        <IconCall color={Colors.COLOR_7} />
        <Text color={'COLOR_7'} fWeight="500" size="size16" mgLeft={7}>
          {translate(`remote_advise.book_advise`)}
        </Text>
      </Touchable>
    </Block>
  );
};

export const ItemHistory = memo(ItemHistoryComponent, isEqual);

const styles = StyleSheet.create({
  avatar: {
    width: scale(40),
    height: scale(40),
    borderRadius: moderateScale(40),
  },
  criteria: {
    flexDirection: 'row',
    marginTop: vScale(10),
    marginBottom: vScale(5),
  },
  wrapper: {
    marginBottom: vScale(10),
    borderColor: Colors.COLOR_9,
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(8),
    borderWidth: moderateScale(0.3),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  item: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Colors.COLOR_9,
    borderRightWidth: moderateScale(1),
  },
  leftInfo: {
    flex: 1,
    marginVertical: vScale(6),
    borderColor: Colors.COLOR_9,
    paddingHorizontal: scale(12),
    borderRightWidth: moderateScale(1),
  },
});
