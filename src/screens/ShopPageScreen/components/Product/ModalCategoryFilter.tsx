import { Alert, StyleSheet } from 'react-native';
import React, { useCallback, useImperativeHandle, useRef, useState } from 'react';
import indexOf from 'lodash/indexOf';

import { currencyFormat, moderateScale, scale, vScale } from '@utils';
import { IconRadioUncheck, translate } from '@assets';
import { Colors, FontSize } from '@themes';
import { Block, RangeSlider, Text, Touchable, Button, CustomModalize, Image } from '@components';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import { Switch } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import { SCREENS } from '@navigation';

interface Props {
  onChangeValue: (key: string, value: any) => void;
  filterValue: any;
  submit: () => void;
  category: any;
}
export const ModalCategoryFilter = React.forwardRef((props: Props, ref) => {
  const { onChangeValue, filterValue, submit, category } = props;
  const refModal = useRef<any>(null);
  const navigation = useNavigation<any>();
  // action modal
  useImperativeHandle(ref, () => ({
    open: () => refModal.current?.open(),
    close: () => refModal.current?.close(),
  }));
  const onSubmit = () => {
    refModal.current?.close();
    navigation.navigate(SCREENS.SHOPE_PAGE_CATEGORY);
    submit();
  };
  // header
  const Header = useCallback(
    () => (
      <Block direction="row" align="center" justify="space-between" mgTop={24} mgHorizontal={16}>
        <Block w={45} />
        <Text text={translate('shop_page.category')} fWeight="700" size="size20" fFamily="MEDIUM" color="TEXT" />
        <Text
          onPress={() => refModal.current?.close()}
          text={translate('shop_page.cancel')}
          fWeight="400"
          size="size14"
          fFamily="MEDIUM"
          color="COLOR_3"
        />
      </Block>
    ),
    [],
  );
  // category
  const Category = useCallback(
    () => (
      <Block style={styles.coverBox}>
        {category?.map((e: any, i: number) => (
          <Touchable key={i + 'cat'} onPress={() => onChangeValue('category', e)}>
            <Block direction="row" align="center" justify="space-between">
              <Text text={e?.name} fWeight="500" size="size14" fFamily="MEDIUM" color="TEXT" />
              <Image source={filterValue?.category === e ? 'imgTick' : 'imgUntick'} style={styles.iconCat} />
            </Block>
            {i < category.length - 1 && <Block h={1} w="100%" bgColor="rgba(226, 226, 226, 0.38)" mgVertical={8} />}
          </Touchable>
        ))}
      </Block>
    ),
    [category, filterValue.category],
  );

  // submit btn
  const AcceptButton = useCallback(
    () => (
      <Block mgTop={30} mgHorizontal={15}>
        <Button onPress={onSubmit}>{translate('shop_page.accept')}</Button>
      </Block>
    ),
    [],
  );

  const renderContent = () => {
    return (
      <Block style={styles.modalContainer}>
        <Header />
        <KeyboardAwareScrollView>
          <Category />
          <AcceptButton />
          <Block h={100} />
        </KeyboardAwareScrollView>
      </Block>
    );
  };
  return (
    <CustomModalize ref={refModal} modalHeight={scale(600)}>
      {renderContent()}
    </CustomModalize>
  );
});

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: Colors.WHITE,
    width: scale(375),
    borderTopLeftRadius: scale(20),
    borderTopRightRadius: scale(20),
  },
  confirmButton: {
    borderRadius: scale(20),
    height: scale(40),
    width: scale(120),
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: scale(1),
    borderColor: Colors.COLOR_1,
    backgroundColor: Colors.COLOR_1,
  },
  confirmButtonText: {
    color: Colors.WHITE,
    fontSize: scale(16),
    lineHeight: scale(24),
    fontWeight: '600',
    letterSpacing: scale(-0.32),
  },
  //
  coverBox: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    marginHorizontal: scale(6),
    marginTop: scale(16),
    paddingHorizontal: scale(10),
    paddingVertical: vScale(15),
  },
  iconCat: { width: scale(20), height: vScale(20) },
});
