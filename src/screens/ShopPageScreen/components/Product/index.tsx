import React, { memo, useRef, useState } from 'react';
import isEqual from 'react-fast-compare';

import styles from './styles';
import { Block, CustomFlatlist, ItemProduct, SafeWrapper, ListEmpty, Text, RangeSlider } from '@components';
import { RenderItemProps } from '@configs';
import { translate } from '@assets';
import { Header } from './Header';
import { ModalQuickFilter } from './ModalQuickFilter';
import { ModalCategoryFilter } from './ModalCategoryFilter';
import { ModalPriceFilter } from './ModalPriceFilter';

interface IProductProps {}
const category = [
  { id: 1, name: 'Tất cả danh mục' },
  { id: 2, name: 'Thức ăn cho chó' },
  { id: 3, name: 'Thức ăn cho mèo' },
  { id: 4, name: 'Thức ăn cho mèo 2' },
];
const priceOption = [
  { id: 'vote', name: 'Sản phẩm đánh giá cao', des: 'Các mục được xếp hạng từ 4 đến 5 sao', active: true },
  { id: 'sale_off', name: 'Các mặt hàng giảm giá', des: '', active: false },
  { id: 'voucher', name: 'Sản phẩm có voucher', des: '', active: false },
  { id: 'real', name: 'Sản phẩm chính hãng', des: 'Sản phẩm uy tín có chứng nhận đảm bảo', active: false },
];
const nations = [
  { name: 'Việt Nam', id: 1 },
  { name: 'Hàn Quốc', id: 2 },
  { name: 'Trung Quốc', id: 3 },
];
const data = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const ProductComponent: React.FC<IProductProps> = (props) => {
  const {} = props;
  const [filterValue, setFilterValue] = useState<any>({
    price_from: 0,
    price_to: 50000,
    nation_id: [],
    price_sale: 0,
    is_auth: 0,
    category: {},
  });
  const refModalQuickFilter = useRef<any>();
  const refModalPriceFilter = useRef<any>();
  const refModalCategoryFilter = useRef<any>();

  const pressPrice = () => refModalPriceFilter.current.open();
  const pressCategory = () => refModalCategoryFilter.current.open();
  const pressFilter = () => refModalQuickFilter.current.open();
  // submit
  const submit = () => {};
  // change value
  const onChangeValue = (field: string, value: string | number | null) => {
    if (field && value !== undefined) {
      setFilterValue((prev: any) => ({ ...prev, [field]: value }));
    }
  };
  const renderItem = ({ item }: RenderItemProps) => {
    return (
      <Block mgTop={16}>
        <ItemProduct data={{}} />
      </Block>
    );
  };
  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <Header pressPrice={pressPrice} pressCategory={pressCategory} pressFilter={pressFilter} />
      <CustomFlatlist
        style={{ flex: 1 }}
        columnWrapperStyle={{ justifyContent: 'space-between' }}
        contentContainerStyle={styles.contentContainer}
        numColumns={3}
        data={data}
        renderItem={renderItem}
        ListEmptyComponent={<ListEmpty />}
        ListHeaderComponent={<ListHeaderComponent />}
      />
      <ModalQuickFilter
        ref={refModalQuickFilter}
        filterValue={filterValue}
        submit={submit}
        onChangeValue={onChangeValue}
        nations={nations}
        category={category}
        priceOption={priceOption}
      />
      <ModalCategoryFilter
        ref={refModalCategoryFilter}
        filterValue={filterValue}
        submit={submit}
        onChangeValue={onChangeValue}
        category={category}
      />
      <ModalPriceFilter
        ref={refModalPriceFilter}
        filterValue={filterValue}
        submit={submit}
        onChangeValue={onChangeValue}
        priceOption={priceOption}
      />
    </SafeWrapper>
  );
};

export const Product = memo(ProductComponent, isEqual);
const ListHeaderComponent = memo((props: any) => {
  const {} = props;
  return (
    <Block mgTop={8}>
      <Text size={'size15'} fFamily={'BRANDING'}>
        {translate('shop_page.list_product')}
      </Text>
    </Block>
  );
});
