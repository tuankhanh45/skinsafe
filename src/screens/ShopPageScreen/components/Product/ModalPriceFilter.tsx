import { Alert, StyleSheet } from 'react-native';
import React, { useCallback, useImperativeHandle, useRef, useState } from 'react';
import indexOf from 'lodash/indexOf';

import { currencyFormat, moderateScale, scale, vScale } from '@utils';
import { IconRadioUncheck, translate } from '@assets';
import { Colors, FontSize } from '@themes';
import { Block, RangeSlider, Text, Touchable, Button, CustomModalize, Image } from '@components';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import { Switch } from 'native-base';

interface Props {
  onChangeValue: (key: string, value: any) => void;
  filterValue: any;
  submit: () => void;
  priceOption: any;
}
export const ModalPriceFilter = React.forwardRef((props: Props, ref) => {
  const { onChangeValue, filterValue, submit, priceOption } = props;
  const refModal = useRef<any>(null);
  // change price
  const changeValue = (low: any, high: any) => {
    console.log('changeValue', low, high);
    onChangeValue('price_from', low);
    onChangeValue('price_to', high);
  };
  // action modal
  useImperativeHandle(ref, () => ({
    open: () => refModal.current?.open(),
    close: () => refModal.current?.close(),
  }));
  const onSubmit = () => {
    refModal.current?.close();
    submit();
  };
  // header
  const Header = useCallback(
    () => (
      <Block direction="row" align="center" justify="space-between" mgTop={24} mgHorizontal={16}>
        <Block w={45} />
        <Text text={translate('shop_page.chose_price')} fWeight="700" size="size20" fFamily="MEDIUM" color="TEXT" />
        <Text
          onPress={() => refModal.current?.close()}
          text={translate('shop_page.cancel')}
          fWeight="400"
          size="size14"
          fFamily="MEDIUM"
          color="COLOR_3"
        />
      </Block>
    ),
    [],
  );

  // price
  const Price = useCallback(
    () => (
      <Block>
        <Text text={translate('shop_page.chose_price')} fWeight="500" size="size14" fFamily="MEDIUM" color="TEXT" />
        <Block direction="row" justify="space-between" mgTop={23}>
          <Block style={styles.coverPrice}>
            <Text style={styles.txtFrom}>Từ</Text>
            <Text style={styles.txtLeft}>đ</Text>
            <Text style={styles.txtPrice}>{currencyFormat(filterValue?.price_from, '', true)}</Text>
            <Text style={styles.txtRight}>K</Text>
          </Block>
          <Block style={styles.coverPrice}>
            <Text style={styles.txtFrom}>Đến</Text>
            <Text style={styles.txtLeft}>đ</Text>
            <Text style={styles.txtPrice}>{currencyFormat(filterValue?.price_to, '', true) ?? '99999999+ đ'}</Text>
            <Text style={styles.txtRight}>K</Text>
          </Block>
        </Block>
      </Block>
    ),
    [filterValue.price_from, filterValue.price_to],
  );
  // ranger slide
  const Slider = useCallback(
    () => (
      <Block align="center" mgTop={25}>
        <RangeSlider
          handleValueChange={changeValue}
          min={0}
          max={50000}
          step={10}
          initLow={filterValue.price_from}
          initHigh={filterValue.price_to}
        />
      </Block>
    ),
    [],
  );
  // switch
  const SwitchOption = useCallback(
    () => (
      <Block>
        {priceOption?.map((e: any, i: number) => (
          <Block key={i + 'opt'}>
            <Block h={1} w="100%" bgColor="rgba(226, 226, 226, 0.38)" mgVertical={8} />
            <Block direction="row" align="center" justify="space-between">
              <Block>
                <Text text={e?.name} fWeight="500" size="size14" fFamily="MEDIUM" color="TEXT" />
                {!!e?.des && <Text text={e?.des} fWeight="400" size="size12" fFamily="MEDIUM" color="TEXT" mgTop={2} />}
              </Block>
              <Switch
                size="sm"
                onTrackColor={Colors.COLOR_2}
                // onValueChange={(value) => onSwitch(data, value)}
                // defaultIsChecked={filterValue[data?.id] ? true : false}
              />
            </Block>
          </Block>
        ))}
      </Block>
    ),
    [],
  );
  // madeIn
  const AcceptButton = useCallback(
    () => (
      <Block mgTop={30} mgHorizontal={15}>
        <Button onPress={onSubmit}>{translate('shop_page.accept')}</Button>
      </Block>
    ),
    [],
  );

  const renderContent = () => {
    return (
      <Block style={styles.modalContainer}>
        <Header />
        <KeyboardAwareScrollView>
          <Block style={styles.coverBox}>
            <Price />
            <Slider />
            <SwitchOption />
          </Block>
          <AcceptButton />
          <Block h={100} />
        </KeyboardAwareScrollView>
      </Block>
    );
  };
  return (
    <CustomModalize ref={refModal} modalHeight={scale(600)}>
      {renderContent()}
    </CustomModalize>
  );
});

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: Colors.WHITE,
    width: scale(375),
    borderTopLeftRadius: scale(20),
    borderTopRightRadius: scale(20),
  },
  confirmButton: {
    borderRadius: scale(20),
    height: scale(40),
    width: scale(120),
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: scale(1),
    borderColor: Colors.COLOR_1,
    backgroundColor: Colors.COLOR_1,
  },
  confirmButtonText: {
    color: Colors.WHITE,
    fontSize: scale(16),
    lineHeight: scale(24),
    fontWeight: '600',
    letterSpacing: scale(-0.32),
  },
  //
  coverBox: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    marginHorizontal: scale(6),
    marginTop: scale(16),
    paddingHorizontal: scale(10),
    paddingVertical: vScale(15),
  },
  // price
  containerPrice: { borderBottomColor: '#E2E2E2', borderBottomWidth: moderateScale(1) },
  coverPrice: {
    borderRadius: vScale(5),
    width: vScale(160),
    padding: vScale(10),
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: moderateScale(1),
    borderColor: Colors.COLOR_9,
  },

  txtFrom: {
    fontWeight: '400',
    fontSize: FontSize.size10,
    lineHeight: moderateScale(14),
    color: '#A3A3A3',
  },
  txtLeft: {
    fontWeight: '400',
    fontSize: FontSize.size8,
    lineHeight: moderateScale(14),
    color: '#000',
    marginLeft: scale(5),
    marginTop: -8,
  },
  txtPrice: {
    fontWeight: '600',
    fontSize: FontSize.size16,
    lineHeight: moderateScale(18),
    color: Colors.TEXT,
  },
  txtRight: {
    fontWeight: '400',
    fontSize: FontSize.size10,
    lineHeight: moderateScale(14),
    color: Colors.TEXT,
    marginTop: scale(4),
    marginLeft: scale(2),
  },
});
