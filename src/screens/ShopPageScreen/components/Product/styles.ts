import { moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  contentContainer: {
    paddingHorizontal: scale(15),
  },
});

export default styles;
