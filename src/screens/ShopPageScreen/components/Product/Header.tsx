import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import Swiper from 'react-native-swiper';
import { StyleSheet } from 'react-native';
import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { Block, Text, Touchable } from '@components';
import { IconArrowDown, translate } from '@assets';

interface IHeaderProps {
  pressPrice: () => void;
  pressCategory: () => void;
  pressFilter: () => void;
}
const HeaderComponent: React.FC<IHeaderProps> = (props) => {
  const { pressPrice, pressCategory, pressFilter } = props;
  return (
    <Block mgHorizontal={16}>
      <Block w={'100%'} h={5} bgColor={'#F4F3F9'} />
      <Block direction="row" justify="space-between" align="center" mgTop={15}>
        <Touchable onPress={pressPrice}>
          <Text size={'size12'} fFamily={'MEDIUM'} fWeight="500">
            {translate('shop_page.chose_price')}
          </Text>
        </Touchable>
        <Block w={0.5} h={15} bgColor={Colors.COLOR_9} />
        <Touchable onPress={pressCategory}>
          <Text size={'size12'} fFamily={'MEDIUM'} fWeight="500">
            {translate('shop_page.category')}
          </Text>
        </Touchable>
        <Block w={0.5} h={15} bgColor={Colors.COLOR_9} />
        <Touchable onPress={pressFilter} direction="row" align="center">
          <Text size={'size12'} fFamily={'MEDIUM'} fWeight="500" mgRight={5}>
            {translate('shop_page.quick_filter')}
          </Text>
          <IconArrowDown />
        </Touchable>
      </Block>
      <Block w={'100%'} h={0.6} bgColor={Colors.COLOR_9} mgTop={15} />
    </Block>
  );
};

export const Header = memo(HeaderComponent, isEqual);

const styles = StyleSheet.create({});
