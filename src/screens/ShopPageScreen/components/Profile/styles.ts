import { StyleSheet } from 'react-native';
import { moderateScale, scale, vScale } from '@utils';
import { Colors } from '@themes';

const styles = StyleSheet.create({
  coverBox: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    marginHorizontal: scale(16),
    marginVertical: scale(15),
    paddingHorizontal: scale(3),
    paddingVertical: vScale(3),
  },
  coverInfo: {
    backgroundColor: '#F2F6FC',
    borderRadius: moderateScale(8),
    paddingHorizontal: scale(13),
    paddingVertical: scale(4),
  },
});

export default styles;
