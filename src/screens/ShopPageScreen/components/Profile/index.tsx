import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import styles from './styles';
import { Block, HeaderBar, SafeWrapper, Text } from '@components';
import { IconBell, translate } from '@assets';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
interface IProfileProps {}

const ProfileComponent: React.FC<IProfileProps> = (props) => {
  const {} = props;
  const data = { cup: 3, create: '3 năm', total_order: '9000', done: '4000', rate: '4.8', num: '69' };
  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <KeyboardAwareScrollView>
        {/* general */}
        <Block style={styles.coverBox}>
          <Text text={translate('shop_page.general')} size="size18" fFamily="MEDIUM" color="TEXT" fWeight="700" />
          <Block direction="row" align="center" justify="space-between" mgTop={13}>
            <Text text={translate('shop_page.best_man')} size="size12" fFamily="MEDIUM" color="TEXT" fWeight="500" />
            <Block style={styles.coverInfo}>
              <Text text={'3'} size="size10" fFamily="MEDIUM" color="TEXT" fWeight="400" />
            </Block>
          </Block>
          <Block w="100%" h={1} bgColor={'rgba(226, 226, 226, 0.38)'} mgVertical={13} />
          <Block direction="row" align="center" justify="space-between">
            <Text text={translate('shop_page.create')} size="size12" fFamily="MEDIUM" color="TEXT" fWeight="500" />
            <Block style={styles.coverInfo}>
              <Text text={data?.create} size="size10" fFamily="MEDIUM" color="TEXT" fWeight="400" />
            </Block>
          </Block>
        </Block>
        <Block w="100%" h={5} bgColor={'rgba(162, 201, 242, 0.2)'} />

        {/* order */}
        <Block style={styles.coverBox}>
          <Text text={translate('shop_page.product_order')} size="size18" fFamily="MEDIUM" color="TEXT" fWeight="700" />
          <Block direction="row" align="center" justify="space-between" mgTop={13}>
            <Text text={translate('shop_page.total_order')} size="size12" fFamily="MEDIUM" color="TEXT" fWeight="500" />
            <Block style={styles.coverInfo}>
              <Text text={data?.total_order} size="size10" fFamily="MEDIUM" color="TEXT" fWeight="400" />
            </Block>
          </Block>
          <Block w="100%" h={1} bgColor={'rgba(226, 226, 226, 0.38)'} mgVertical={13} />
          <Block direction="row" align="center" justify="space-between">
            <Text text={translate('shop_page.total_done')} size="size12" fFamily="MEDIUM" color="TEXT" fWeight="500" />
            <Block style={styles.coverInfo}>
              <Text text={data?.done} size="size10" fFamily="MEDIUM" color="TEXT" fWeight="400" />
            </Block>
          </Block>
        </Block>
        <Block w="100%" h={5} bgColor={'rgba(162, 201, 242, 0.2)'} />

        {/* rate */}
        <Block style={styles.coverBox}>
          <Text text={translate('shop_page.rate')} size="size18" fFamily="MEDIUM" color="TEXT" fWeight="700" />
          <Block direction="row" align="center" justify="space-between" mgTop={13}>
            <Text text={translate('shop_page.avg')} size="size12" fFamily="MEDIUM" color="TEXT" fWeight="500" />
            <Block style={styles.coverInfo}>
              <Text text={`${data?.rate} / 5`} size="size10" fFamily="MEDIUM" color="TEXT" fWeight="400" />
            </Block>
          </Block>
          <Block w="100%" h={1} bgColor={'rgba(226, 226, 226, 0.38)'} mgVertical={13} />
          <Block direction="row" align="center" justify="space-between">
            <Text text={translate('shop_page.num')} size="size12" fFamily="MEDIUM" color="TEXT" fWeight="500" />
            <Block style={styles.coverInfo}>
              <Text text={data?.num} size="size10" fFamily="MEDIUM" color="TEXT" fWeight="400" />
            </Block>
          </Block>
        </Block>
      </KeyboardAwareScrollView>
    </SafeWrapper>
  );
};

export const Profile = memo(ProfileComponent, isEqual);
