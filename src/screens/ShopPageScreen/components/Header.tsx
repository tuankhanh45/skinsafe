import React, { memo } from 'react';
import { SCREENS } from '@navigation';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { IconArrowLeft, IconSearch, translate } from '@assets';
import { Block, NotificationBadgeBtn, Text, Touchable } from '@components';

interface IHeaderProps {
  navigation: any;
}

const HeaderComponent: React.FC<IHeaderProps> = (props) => {
  const { navigation } = props;

  const onPressSearch = () => navigation.navigate(SCREENS.SEARCH);

  return (
    <Block style={styles.wrapper}>
      <Touchable w={33} center onPress={() => navigation.goBack()}>
        <IconArrowLeft />
      </Touchable>

      <Touchable style={styles.input} onPress={onPressSearch}>
        <IconSearch />
        <Text size={'size12'} color={'COLOR_9'} mgLeft={10}>
          {translate('cm.search')}
        </Text>
      </Touchable>

      <NotificationBadgeBtn />
    </Block>
  );
};

export const Header = memo(HeaderComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    height: vScale(54),
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: vScale(10),
    paddingBottom: vScale(14),
    paddingHorizontal: scale(12),
    backgroundColor: Colors.COLOR_1,
  },
  input: {
    flex: 1,
    height: scale(32),
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: scale(6),
    paddingHorizontal: scale(12),
    backgroundColor: Colors.COLOR_4,
    borderRadius: moderateScale(100),
  },
});
