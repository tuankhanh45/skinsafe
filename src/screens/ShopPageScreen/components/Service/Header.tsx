import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { Block, Text, Touchable } from '@components';
import { IconArrowDown, translate } from '@assets';

interface IHeaderProps {
  pressFilter: () => void;
}
const HeaderComponent: React.FC<IHeaderProps> = (props) => {
  const des = 'Số 56 Lạc Long Quân, Tây Hồ, Hà Nội \nCửa hàng cung cấp sản phẩm và dịch vụ ...';
  return (
    <Block>
      <Block style={styles.coverBox}>
        <Text text={translate('shop_page.des')} fWeight="700" size="size14" fFamily="MEDIUM" color="TEXT" />
        <Block w={'100%'} h={1} bgColor={Colors.TEXT_2} mgTop={5} />
        <Text text={des} fWeight="400" size="size12" fFamily="MEDIUM" color="TEXT" mgVertical={15} />
      </Block>
      <Block w={'100%'} h={5} bgColor={'#F4F3F9'} />
      <Block mgTop={13} direction="row" align="center" justify="space-between" mgHorizontal={15}>
        <Text size={'size15'} fFamily={'BRANDING'}>
          {translate('shop_page.service')}
        </Text>
        <Touchable direction="row" align="center" style={styles.coverBtn} onPress={props.pressFilter}>
          <Text size={'size14'} fFamily={'MEDIUM'} fWeight="500" mgHorizontal={14}>
            {translate('shop_page.all')}
          </Text>
          <IconArrowDown />
        </Touchable>
      </Block>
    </Block>
  );
};

export const Header = memo(HeaderComponent, isEqual);

const styles = StyleSheet.create({
  coverBox: {
    borderRadius: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.COLOR_4,
    marginHorizontal: scale(16),
    marginVertical: scale(15),
    paddingHorizontal: scale(3),
    paddingVertical: vScale(3),
  },
  coverBtn: {
    borderColor: Colors.TEXT_2,
    borderRadius: moderateScale(8),
    borderWidth: moderateScale(1),
    paddingVertical: scale(8),
    paddingHorizontal: scale(16),
  },
});
