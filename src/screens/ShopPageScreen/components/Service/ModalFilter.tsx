import { Alert, StyleSheet } from 'react-native';
import React, { useCallback, useImperativeHandle, useRef, useState } from 'react';
import indexOf from 'lodash/indexOf';
import { currencyFormat, moderateScale, scale, vScale } from '@utils';
import { IconRadioUncheck, translate } from '@assets';
import { Colors, FontSize } from '@themes';
import { Block, RangeSlider, Text, Touchable, Button, CustomModalize, Image } from '@components';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
type Props = {};
export const ModalFilter = React.forwardRef((props: Props, ref) => {
  const {} = props;
  const refModal = useRef<any>(null);
  // submit
  const submit = () => {};
  // action modal
  useImperativeHandle(ref, () => ({
    open: () => refModal.current?.open(),
    close: () => refModal.current?.close(),
  }));

  const renderContent = () => {
    return (
      <Block style={styles.modalContainer}>
        <KeyboardAwareScrollView>
          <Text size={'size12'} fFamily={'MEDIUM'} fWeight="500" textAlign="center">
            {translate('shop_page.quick_filter')}
          </Text>
          <Block h={100} />
        </KeyboardAwareScrollView>
      </Block>
    );
  };
  return (
    <CustomModalize ref={refModal} modalHeight={scale(700)}>
      {renderContent()}
    </CustomModalize>
  );
});

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: Colors.WHITE,
    borderTopLeftRadius: scale(20),
    borderTopRightRadius: scale(20),
  },

  txtFrom: {
    fontWeight: '400',
    fontSize: FontSize.size10,
    lineHeight: moderateScale(14),
    color: '#A3A3A3',
  },
});
