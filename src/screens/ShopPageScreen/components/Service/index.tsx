import React, { memo, useRef } from 'react';
import isEqual from 'react-fast-compare';
import styles from './styles';
import { RenderItemProps } from '@configs';
import { Block, CustomFlatlist, ItemProduct, SafeWrapper, ListEmpty, Text, ItemService, Touchable } from '@components';
import { Header } from './Header';
import { ModalFilter } from './ModalFilter';
import { IconArrowDown, translate } from '@assets';
import { Colors } from '@themes';
import { moderateScale } from '@utils';

interface IServiceProps {}
const ServiceComponent: React.FC<IServiceProps> = (props) => {
  const {} = props;
  const refModalFilter = useRef<any>();
  const data = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  const pressFilter = () => refModalFilter.current.open();
  const renderItem = ({ item }: RenderItemProps) => {
    return (
      <Block mgTop={16} mgHorizontal={15}>
        <ItemService data={{}} />
      </Block>
    );
  };
  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <CustomFlatlist
        style={{ flex: 1 }}
        data={data}
        renderItem={renderItem}
        ListEmptyComponent={<ListEmpty />}
        ListHeaderComponent={<Header pressFilter={pressFilter} />}
      />
      <ModalFilter ref={refModalFilter} />
    </SafeWrapper>
  );
};

export const Service = memo(ServiceComponent, isEqual);
