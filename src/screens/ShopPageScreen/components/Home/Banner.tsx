import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import Swiper from 'react-native-swiper';
import { StyleSheet } from 'react-native';
import { Colors } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote } from '@components';

interface IBannerProps {}
const url = 'https://picsum.photos/80';
const DOT_COLOR = 'rgba(255, 255, 255, 0.3)';
const BannerComponent: React.FC<IBannerProps> = (props) => {
  const {} = props;
  return (
    <Block h={155} mgTop={22}>
      <Swiper
        autoplay
        height={vScale(143)}
        dotColor={DOT_COLOR}
        paginationStyle={styles.dot}
        activeDotColor={Colors.COLOR_4}>
        {[1, 2, 3, 4, 5].map((e, i) => {
          return <ImageRemote source={url + i} key={i} style={styles.bannerImg} resizeMode={'cover'} />;
        })}
      </Swiper>
    </Block>
  );
};

export const Banner = memo(BannerComponent, isEqual);

const styles = StyleSheet.create({
  bannerImg: {
    height: vScale(143),
    marginHorizontal: scale(15),
    borderRadius: moderateScale(8),
  },
  dot: {
    bottom: vScale(2),
  },
});
