import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { SafeWrapper } from '@components';
import { ListProduct } from './ListProduct';
import { ListPreferentialService } from './ListPreferentialService';
import { Banner } from './Banner';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
interface IHomeProps {}

const HomeComponent: React.FC<IHomeProps> = (props) => {
  const {} = props;

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <KeyboardAwareScrollView>
        <Banner />
        <ListProduct label="new_product" data={[1, 2, 3]} />
        <ListProduct label="fast_product" data={[1, 2, 3, 4, 5, 6]} />
        <ListPreferentialService label="preferential_service" data={[1, 2, 3]} />
        <ListProduct label="related_product" data={[1, 2, 3, 4, 5, 6]} />
      </KeyboardAwareScrollView>
    </SafeWrapper>
  );
};

export const Home = memo(HomeComponent, isEqual);
