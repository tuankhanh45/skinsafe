import React, { memo, useRef } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Colors, Fonts, FontSize } from '@themes';
import { moderateScale, scale } from '@utils';
import { Block, Text, ItemProduct } from '@components';
import { useAppStore } from '@store';
import { useRequest } from 'ahooks';
import { useNavigation } from '@react-navigation/native';
import { SCREENS } from '@navigation';
import { translate } from '@assets';

interface IListProductProps {
  data: any[];
  label?: string;
}
const ListProductComponent: React.FC<IListProductProps> = (props) => {
  const navigation = useNavigation<any>();
  const { data, label } = props;
  return (
    <Block style={styles.wrapper}>
      <Text mgTop={16} size={'size15'} fFamily={'BRANDING'}>
        {translate(`shop_page.${label}`)}
      </Text>
      <Block direction="row" justify="space-between" style={styles.flexWrap}>
        {data?.map((e, i) => (
          <Block mgTop={15} key={i + 'pr'}>
            <ItemProduct  data={{}} />
          </Block>
        ))}
      </Block>
    </Block>
  );
};

export const ListProduct = memo(ListProductComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: { marginHorizontal: scale(16) },
  flexWrap: { flexWrap: 'wrap' },
});
