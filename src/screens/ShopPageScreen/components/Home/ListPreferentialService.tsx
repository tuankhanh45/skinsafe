import React, { memo, useRef } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Colors, Fonts, FontSize } from '@themes';
import { moderateScale, scale } from '@utils';
import { Block, ItemService, Text } from '@components';
import { useAppStore } from '@store';
import { useRequest } from 'ahooks';
import { useNavigation } from '@react-navigation/native';
import { SCREENS } from '@navigation';
import { translate } from '@assets';

interface IListPreferentialServiceProps {
  data: any[];
  label?: string;
}
const ListPreferentialServiceComponent: React.FC<IListPreferentialServiceProps> = (props) => {
  const navigation = useNavigation<any>();
  const { data, label } = props;
  return (
    <Block style={styles.wrapper}>
      <Text mgTop={16} size={'size15'} fFamily={'BRANDING'}>
        {translate(`shop_page.${label}`)}
      </Text>
      <Block>
        {data?.map((e, i) => (
          <Block mgTop={15} key={i + 'sv'}>
            <ItemService data={{}} />
          </Block>
        ))}
      </Block>
    </Block>
  );
};

export const ListPreferentialService = memo(ListPreferentialServiceComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: { marginHorizontal: scale(16) },
});
