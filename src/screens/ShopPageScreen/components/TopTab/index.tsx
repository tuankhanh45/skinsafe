import React, { memo, useState } from 'react';
import isEqual from 'react-fast-compare';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';
import { Block } from '@components';
import { Home, Service, Product, Profile } from '../../components';
import { device, moderateScale, scale, vScale } from '@utils';
import { Colors, Fonts, FontSize } from '@themes';
import { StyleSheet } from 'react-native';

interface ITopTabProps {}
const ROUTES = [
  { key: 'first', title: 'Trang chủ' },
  { key: 'second', title: 'Sản phẩm' },
  { key: 'third', title: 'Dịch vụ' },
  { key: 'fourth', title: 'Hồ sơ' },
];
const TobTabComponent: React.FC<ITopTabProps> = (props) => {
  const {} = props;
  const [indexTab, setIndexTab] = useState<number>(0);

  const renderScene = SceneMap({
    first: () => <Home />,
    second: () => <Product />,
    third: () => <Service />,
    fourth: () => <Profile />,
  });
  const renderTabBar = (prs: any) => {
    return (
      <TabBar
        {...prs}
        activeColor={Colors.COLOR_1}
        style={styles.tabBar}
        tabStyle={styles.tabStyle}
        labelStyle={styles.tabBarLabel}
        indicatorStyle={{ backgroundColor: Colors.COLOR_1 }}
      />
    );
  };
  return (
    <Block flex mgTop={24} style={styles.wrapper}>
      <TabView
        swipeEnabled={false}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={setIndexTab}
        keyboardDismissMode={'on-drag'}
        initialLayout={{ width: device.width }}
        navigationState={{ index: indexTab, routes: ROUTES }}
      />
    </Block>
  );
};

export const TopTab = memo(TobTabComponent, isEqual);
const styles = StyleSheet.create({
  wrapper: {
    borderTopColor: '#F4F3F9',
    borderTopWidth: moderateScale(0.5),
  },
  tabBar: {
    backgroundColor: Colors.WHITE,
    height: scale(45),
  },
  tabBarLabel: {
    fontWeight: '600',
    color: Colors.TEXT,
    textTransform: 'none',
    fontSize: FontSize.size12,
    lineHeight: moderateScale(17),
    fontFamily: Fonts.MEDIUM,
  },
  tabStyle: {
    paddingVertical: vScale(13),
  },
});
