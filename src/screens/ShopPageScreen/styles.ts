import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { moderateScale, scale, vScale } from '@utils';

import { Colors } from '@themes';

const styles = StyleSheet.create({});

export default styles;
