import React, { memo } from 'react';
import isEqual from 'react-fast-compare';

import { Header, TopTab } from './components';
import { Block, SafeWrapper } from '@components';
import { ShopInfo } from '../DetailProductScreen/components';

interface IShopPageProps {
  route: any;
  navigation: any;
}

const ShopPageComponent: React.FC<IShopPageProps> = (props) => {
  const { route, navigation } = props;
  const shopId = route?.params?.id;
  console.log('🚀 ~ file: index.tsx:16 ~ shopId:', shopId);

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <Header navigation={navigation} />
      <Block mgTop={-15}>
        <ShopInfo data={{}} />
      </Block>
      <TopTab />
    </SafeWrapper>
  );
};

export const ShopPageScreen = memo(ShopPageComponent, isEqual);
