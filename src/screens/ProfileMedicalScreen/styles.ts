import { Colors, Fonts, FontSize } from '@themes';
import { device, moderateScale, scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: Colors.COLOR_4,
  },
  servicesContainer: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingTop: vScale(13),
    paddingHorizontal: scale(15),
    justifyContent: 'space-between',
  },
  imgService: {
    height: vScale(111),
    marginBottom: vScale(28),
    paddingBottom: vScale(15),
    justifyContent: 'flex-end',
    borderRadius: moderateScale(10),
    width: (device.width - scale(15 * 2 + 35 * 2)) / 3,
  },
  labelService: {
    color: Colors.WHITE,
    textAlign: 'center',
    fontSize: FontSize.size15,
    marginHorizontal: scale(8),
    fontFamily: Fonts.BRANDING,
  },
  coverAvatar: {
    width: scale(62),
    height: scale(62),
    borderRadius: scale(62),
    borderColor: Colors.WHITE,
    borderWidth: moderateScale(1),
  },
  avatar: {
    width: scale(60),
    height: scale(60),
    borderRadius: scale(60),
  },
  name: {
    color: Colors.BLACK,
    fontSize: FontSize.size14,
    fontFamily: Fonts.BRANDING,
  },
  qr: {
    width: scale(60),
    height: scale(60),
    borderRadius: scale(5),
  },
  linear: {
    height: vScale(1),
    alignSelf: 'center',
    marginTop: vScale(19),
    marginBottom: vScale(16),
    width: device.width - scale(15 * 2),
  },
  cmnd: {
    width: '100%',
    height: vScale(219),
    borderRadius: moderateScale(8),
  },
  dot: {
    bottom: vScale(-20),
  },
  infoBox: {
    marginTop: vScale(4),
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-start',
    paddingVertical: vScale(3),
    paddingHorizontal: scale(10),
    backgroundColor: Colors.COLOR_3,
    borderRadius: moderateScale(10),
  },
});

export default styles;
