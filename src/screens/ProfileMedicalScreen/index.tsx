import isEqual from 'react-fast-compare';
import Swiper from 'react-native-swiper';
import React, { memo, useState } from 'react';
import { ImageRequireSource } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { ImageBackground, ScrollView } from 'react-native';

import styles from './styles';
import { vScale } from '@utils';
import { Colors } from '@themes';
import { useAppStore } from '@store';
import { SCREENS, ScreensType } from '@navigation';
import { IconArrowRight, images, translate } from '@assets';
import { Block, ImageRemote, Text, Touchable, WrapperImageBg } from '@components';

interface IProfileMedicalProps {
  route: any;
  navigation: any;
}
interface SERVICE_ITEM {
  label: string;
  screen?: ScreensType;
  image: ImageRequireSource;
}
const LIST_SERVICES: SERVICE_ITEM[] = [
  { image: images.history_medical, label: 'history_medical', screen: 'INJECTION_HISTORY' },
  { image: images.certifications_vaccination, label: 'certifications_vaccination', screen: 'INJECTION_CERTIFICATE' },
  { image: images.file_health, label: 'file_health', screen: 'HEALTH_RECORDS' },
  { image: images.booking_examination, label: 'booking_examination', screen: 'BOOKING_EXAMINATION' },
  { image: images.book_injection, label: 'book_injection', screen: 'BOOKING_INJECTION' },
  { image: images.remote_advise, label: 'remote_advise', screen: 'REGISTER_REMOTE_ADVISE' },
];
const COLORS = ['#FFDE88', '#FF928BAF', '#FFC2BE55', '#62FFF6'];

const ProfileMedicalComponent: React.FC<IProfileMedicalProps> = (props) => {
  const { navigation, route } = props;
  const indexParams = route?.params?.index || 0;

  const user = useAppStore((state) => state?.account?.user);
  const listPet = useAppStore((state) => state?.pet.listPet);

  const [index, setIndex] = useState<number>(indexParams);

  const onNavigate = (screen?: ScreensType, data?: any) => {
    if (screen) {
      navigation.navigate(SCREENS[screen], data && data);
    }
  };

  const onPressInfo = () => navigation.goBack();

  return (
    <WrapperImageBg style={styles.wrapper}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Block direction="row" align="center" pdHorizontal={15} pdTop={16}>
          <Block style={styles.coverAvatar}>
            <ImageRemote resizeMode={'cover'} source={user?.avatar} style={styles.avatar} />
          </Block>
          <Block pdHorizontal={10} flex>
            <Text text={user?.fullname} style={styles.name} />
            <Touchable style={styles.infoBox} onPress={onPressInfo}>
              <Text size="size10" color="WHITE" mgRight={4}>
                {translate('profile_medical.info')}
              </Text>
              <IconArrowRight color={Colors.WHITE} />
            </Touchable>
          </Block>
        </Block>

        <LinearGradient colors={COLORS} style={styles.linear} start={{ x: -1, y: 0 }} end={{ x: 1, y: 0 }} />
        <Swiper
          height={vScale(219)}
          onIndexChanged={setIndex}
          dotColor={Colors.COLOR_9}
          paginationStyle={styles.dot}
          activeDotColor={Colors.COLOR_1}>
          {listPet.map((e, i) => {
            return (
              <Block pdHorizontal={15} key={i}>
                <ImageRemote resizeMode={'cover'} source={e?.avatar} style={styles.cmnd} />
              </Block>
            );
          })}
        </Swiper>

        <LinearGradient
          colors={COLORS}
          end={{ x: 1, y: 0 }}
          start={{ x: -1, y: 0 }}
          style={[styles.linear, { marginTop: vScale(38) }]}
        />

        <Block style={styles.servicesContainer}>
          {LIST_SERVICES.map((e, i) => {
            return (
              <Touchable key={i} onPress={() => onNavigate(e?.screen, { selectedPet: listPet?.[index] })}>
                <ImageBackground source={e.image} style={styles.imgService}>
                  <Text style={styles.labelService}>{translate(`profile_medical.${e.label}`)}</Text>
                </ImageBackground>
              </Touchable>
            );
          })}
        </Block>
      </ScrollView>
    </WrapperImageBg>
  );
};

export const ProfileMedicalScreen = memo(ProfileMedicalComponent, isEqual);
