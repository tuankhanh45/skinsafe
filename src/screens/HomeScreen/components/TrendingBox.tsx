import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { Portal } from 'react-native-portalize';
import { Modalize } from 'react-native-modalize';
import React, { memo, useRef, useState } from 'react';
import LinearGradient from 'react-native-linear-gradient';

import { Colors } from '@themes';
import { IconFilter, translate } from '@assets';
import { ContentFilter } from './ContentFilter';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ItemProduct, Text, Touchable } from '@components';

interface ITrendingBoxProps {
  data: any;
}

const GRADIENT_COLOR = ['#FCADEF', '#fff'];
const LIST_FILTER = ['Tất cả', 'Thức ăn', 'Đồ chơi', 'Gối ngủ', 'Chuồng', 'Quần áo', 'Cỏ', 'Khác'];

const TrendingBoxComponent: React.FC<ITrendingBoxProps> = (props) => {
  const { data } = props;
  const refFilter = useRef<Modalize>(null);

  const [selectedFilter, setSelectedFilter] = useState<any>(LIST_FILTER[0]);

  const onPressFilter = () => refFilter.current?.open();

  const onPressItemFilter = (item: any) => {
    setSelectedFilter(item);
    refFilter.current?.close();
  };

  return (
    <Block style={styles.wrapper}>
      <LinearGradient colors={GRADIENT_COLOR} style={styles.linear}>
        <Text size={'size15'} color={'BLACK'} fFamily={'BRANDING'}>
          {translate('home.trending')}
        </Text>
        <Touchable direction="row" align="center" onPress={onPressFilter}>
          <Text fWeight="600" size="size16" mgRight={8}>
            {translate('home.filter')}
          </Text>
          <IconFilter />
        </Touchable>
      </LinearGradient>

      <Block style={styles.productContainer}>
        {data.map((e: any, i: number) => {
          return <ItemProduct data={e} key={i} style={styles.item} />;
        })}
      </Block>

      <Portal>
        <Modalize ref={refFilter} adjustToContentHeight withHandle={false}>
          <ContentFilter
            listFilter={LIST_FILTER}
            selectedFilter={selectedFilter}
            onPressItemFilter={onPressItemFilter}
          />
        </Modalize>
      </Portal>
    </Block>
  );
};

export const TrendingBox = memo(TrendingBoxComponent, isEqual);

const styles = StyleSheet.create({
  linear: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: vScale(8),
    paddingHorizontal: scale(16),
    justifyContent: 'space-between',
    borderTopLeftRadius: moderateScale(8),
    borderTopRightRadius: moderateScale(8),
  },
  wrapper: {
    backgroundColor: Colors.COLOR_4,
    borderTopLeftRadius: moderateScale(8),
    borderTopRightRadius: moderateScale(8),
  },
  productContainer: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingTop: vScale(18),
    paddingHorizontal: scale(15),
    justifyContent: 'space-between',
  },
  item: {
    marginBottom: vScale(28),
  },
});
