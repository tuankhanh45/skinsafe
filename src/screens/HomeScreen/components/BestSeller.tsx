import { useRequest } from 'ahooks';
import isEqual from 'react-fast-compare';
import { ScrollView, StyleSheet } from 'react-native';
import React, { memo, useEffect, useState } from 'react';
import LinearGradient from 'react-native-linear-gradient';

import { Colors, FontSize } from '@themes';
import { moderateScale, scale, vScale } from '@utils';
import { handleResponse, requestGetListProduct } from '@configs';
import { Block, ItemProduct, Text, Touchable } from '@components';

interface IBestSellerProps {
  data: any[];
}

const LIST_CATE = ['Tất cả', 'Thức ăn', 'Đồ chơi', 'Quần áo', 'Gối ngủ', 'Chuồng', 'Cỏ', 'Khác'];

const BestSellerComponent: React.FC<IBestSellerProps> = (props) => {
  //   const { data } = props;

  const [listProduct, setSelectedProduct] = useState<any[]>([]);
  const [selectedCate, setSelectedCate] = useState<any>(LIST_CATE[0]);

  useEffect(() => {
    runGetListProduct({
      page: 1,
      per_page: 10,
      sort: 'qty_sell',
    });
  }, [selectedCate]);

  const { run: runGetListProduct, loading: loadingGetListProduct } = useRequest(requestGetListProduct, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (Array.isArray(res?.data)) {
        setSelectedProduct(res?.data);
      }
    },
  });

  const onPressCate = (item: any) => {
    setSelectedCate(item);
  };

  return (
    <Block style={styles.wrapper}>
      <LinearGradient colors={['#E30613', '#FE0E5600']} style={styles.linear}>
        {LIST_CATE.map((item, index) => {
          const isSelected = selectedCate === item;
          return (
            <Touchable
              key={index}
              style={[styles.cateItem, isSelected && { borderColor: 'transparent' }]}
              onPress={() => onPressCate(item)}>
              <Text style={[styles.cateLabel, isSelected && { color: Colors.COLOR_1 }]}>{item}</Text>
              {isSelected && (
                <Block center style={{ position: 'absolute', right: -7, top: 0, bottom: 0 }}>
                  <Block style={styles.triangle} />
                </Block>
              )}
            </Touchable>
          );
        })}
      </LinearGradient>

      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {listProduct.map((e, i) => {
          return <ItemProduct data={e} key={i} style={styles.item} />;
        })}
      </ScrollView>
    </Block>
  );
};

export const BestSeller = memo(BestSellerComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    marginBottom: vScale(18),
    paddingHorizontal: scale(15),
  },
  linear: {
    alignSelf: 'flex-start',
    paddingTop: vScale(11),
    paddingBottom: vScale(18),
    paddingHorizontal: scale(6),
    borderRadius: moderateScale(10),
  },
  cateItem: {
    width: scale(57),
    borderColor: '#C4C4C4',
    marginBottom: vScale(5),
    paddingVertical: vScale(2),
    backgroundColor: Colors.WHITE,
    borderRadius: moderateScale(5),
    borderWidth: moderateScale(0.5),
  },
  cateLabel: {
    color: '#595959',
    fontWeight: '700',
    textAlign: 'center',
    fontSize: FontSize.size10,
  },
  triangle: {
    width: 0,
    height: 0,
    borderLeftWidth: 6,
    borderRightWidth: 6,
    borderBottomWidth: 6,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: Colors.WHITE,
    transform: [{ rotate: '90deg' }],
  },
  item: {
    marginLeft: scale(10),
    alignSelf: 'flex-start',
  },
});
