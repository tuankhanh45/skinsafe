import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { SCREENS } from '@navigation';
import { scale, vScale } from '@utils';
import { ImagesTypes, translate } from '@assets';
import { Block, Image, LabelCate, Text, Touchable } from '@components';

interface ICategoryProps {
  navigation: any;
  isLogged: boolean;
}
interface CateItem {
  label: string;
  screen?: string;
  img: ImagesTypes;
  checkLogin?: boolean;
}
const LIST_SERVICES: CateItem[] = [
  { label: 'spa', img: 'service_spa', screen: SCREENS.FIND_NEAR_SHOP },
  { label: 'hotel', img: 'service_hotel', screen: SCREENS.FIND_NEAR_SHOP },
  { label: 'medical', img: 'service_medical', screen: SCREENS.UTILITIES_MEDICAL },
  { label: 'money', img: 'service_money', checkLogin: true, screen: SCREENS.MY_WALLET },
  { label: 'vip', img: 'service_vip' },
];

const LIST_CATE: CateItem[] = [
  { label: 'cate_food', img: 'cate_food' },
  { label: 'cate_toy', img: 'cate_toy' },
  { label: 'cate_fashion', img: 'cate_fashion' },
  { label: 'cate_shower_gel', img: 'cate_shower_gel' },
  { label: 'cate_cage', img: 'cate_cage' },
];

const CategoryComponent: React.FC<ICategoryProps> = (props) => {
  const { navigation, isLogged } = props;

  const onPressHunt = () => {};

  const onNavigate = (screen?: string, data?: any) => {
    if (screen) {
      navigation.navigate(screen, data && data);
    }
  };

  const onPressCate = (e: any) => {
    if (e.checkLogin && !isLogged) {
      return navigation.navigate(SCREENS.LOGIN, {
        cbFunc: () => navigation.replace(e?.screen, ['spa', 'hotel'].includes(e.label) && { type: e.label }),
      });
    }
    onNavigate(e?.screen, ['spa', 'hotel'].includes(e.label) && { type: e.label });
  };

  return (
    <Block style={styles.wrapper}>
      <Image source="shadow_line" style={styles.line} resizeMode="cover" />
      <Block pdHorizontal={15}>
        <LabelCate label={translate('home.cate')} labelMore={translate('home.cate_more')} onPressMore={onPressHunt} />
        <Block style={styles.cateContainer}>
          {LIST_CATE.map((e, i) => {
            const isLastItem = i === LIST_CATE.length - 1;
            return (
              <Touchable
                key={i}
                style={[styles.cateItem, isLastItem && { marginRight: 0 }]}
                onPress={() => onPressCate(e)}>
                <Image source={e.img} style={styles.img} />
                <Text center fWeight="700" size="size10" mgTop={5}>
                  {translate(`home.${e.label}`)}
                </Text>
              </Touchable>
            );
          })}
        </Block>
      </Block>

      <Image source="shadow_line" style={styles.line} resizeMode="cover" />
      <Block pdHorizontal={15}>
        <LabelCate label={translate('home.service')} labelMore={translate('home.hunt_now')} onPressMore={onPressHunt} />
        <Block style={styles.cateContainer}>
          {LIST_SERVICES.map((e, i) => {
            const isLastItem = i === LIST_SERVICES.length - 1;
            return (
              <Touchable
                key={i}
                style={[styles.cateItem, isLastItem && { marginRight: 0 }]}
                onPress={() => onPressCate(e)}>
                <Image source={e.img} style={styles.img} />
                <Text center fWeight="700" size="size10" mgTop={5}>
                  {translate(`home.${e.label}`)}
                </Text>
              </Touchable>
            );
          })}
        </Block>
      </Block>
    </Block>
  );
};

export const Category = memo(CategoryComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {},
  cateContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: vScale(16),
    marginBottom: vScale(14),
    justifyContent: 'space-between',
  },
  cateItem: {
    flex: 1,
    marginRight: scale(16),
  },
  line: {
    width: '100%',
    height: vScale(16),
  },
  img: {
    height: scale(56),
    width: scale(56),
  },
});
