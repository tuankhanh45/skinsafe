import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import Swiper from 'react-native-swiper';
import LinearGradient from 'react-native-linear-gradient';
import { ImageBackground, StyleSheet } from 'react-native';

import { SCREENS } from '@navigation';
import { Colors, FontSize } from '@themes';
import { images, translate } from '@assets';
import { currencyFormat, moderateScale, scale, vScale } from '@utils';
import { Block, Image, ImageRemote, Text, Touchable } from '@components';

interface IBannerBoxProps {
  slideHome: any;
  navigation: any;
}

const url = 'https://picsum.photos/80';
const DOT_COLOR = 'rgba(255, 255, 255, 0.3)';
const GRADIENT_COLOR = ['#e9b9fc', '#d4bffc'];
const SMALL_ITEM_HEIGHT = vScale(111.21);
const LARGE_IMG_ITEM_HEIGHT = vScale(87.46);
const SMALL_IMG_ITEM_HEIGHT = vScale(75.99);
const LARGE_ITEM_HEIGHT = vScale(128);
const SMALL_BG_ITEM_HEIGHT = vScale(65.16);
const LARGE_BG_ITEM_HEIGHT = vScale(75);

const BannerBoxComponent: React.FC<IBannerBoxProps> = (props) => {
  const { slideHome, navigation } = props;

  if (!slideHome?.['1'] || !Array.isArray(slideHome?.['1'])) return null;

  const onPressBanner = (item: any) => {
    navigation.navigate(SCREENS.SHOPE_PAGE, { id: item?.shop_id });
  };

  const listBanner = slideHome?.['1'];
  return (
    <Block mgBottom={8}>
      <Block pdTop={3}>
        <Swiper
          autoplay
          height={vScale(143)}
          dotColor={DOT_COLOR}
          paginationStyle={styles.dot}
          activeDotColor={Colors.COLOR_4}>
          {listBanner.map((e: any, i: number) => {
            return (
              <Touchable key={i} onPress={() => onPressBanner(e)}>
                <ImageRemote source={e?.avatar} style={styles.bannerImg} resizeMode={'cover'} />
              </Touchable>
            );
          })}
        </Swiper>
      </Block>

      <Block style={styles.linearGradientBox}>
        <LinearGradient colors={GRADIENT_COLOR} style={styles.linearGradient}>
          {[1, 2, 3].map((e, i) => {
            const sourceSaleBg = i === 0 ? 'bg_sale_product_0' : i === 1 ? 'bg_sale_product_1' : 'bg_sale_product_2';
            return (
              <Touchable
                key={i}
                style={[
                  styles.saleItem,
                  i !== 1 && { flex: 0.8, height: SMALL_ITEM_HEIGHT, marginBottom: vScale(3.79) },
                ]}>
                <ImageRemote
                  resizeMode={'cover'}
                  source={url + (i + 4)}
                  style={[styles.productImg, i !== 1 && { height: SMALL_IMG_ITEM_HEIGHT }]}
                />
                <Block style={[styles.bottomSaleItem, i !== 1 && { height: SMALL_BG_ITEM_HEIGHT }]}>
                  <ImageBackground
                    resizeMode={'cover'}
                    source={images[sourceSaleBg]}
                    style={[styles.saleBgImg, i !== 1 && { height: SMALL_BG_ITEM_HEIGHT }]}>
                    <Text style={styles.saleText}>{translate('home.sale')}</Text>
                    <Text style={styles.priceText}>{currencyFormat(20000)}</Text>
                  </ImageBackground>
                </Block>
              </Touchable>
            );
          })}
        </LinearGradient>
        <Image source={'buy_now'} style={styles.buyNowImg} resizeMode={'cover'} containerStyle={styles.buyNowImgBox} />
      </Block>
    </Block>
  );
};

export const BannerBox = memo(BannerBoxComponent, isEqual);

const styles = StyleSheet.create({
  bannerImg: {
    height: vScale(143),
    marginHorizontal: scale(15),
    borderRadius: moderateScale(8),
  },
  dot: {
    bottom: vScale(2),
  },
  linearGradient: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingBottom: vScale(10),
    paddingHorizontal: scale(5.5),
    borderRadius: moderateScale(8),
  },
  buyNowImg: {
    width: scale(157),
    height: vScale(27.48),
  },
  linearGradientBox: {
    height: vScale(156),
    marginTop: vScale(18),
    paddingHorizontal: scale(15),
    borderRadius: moderateScale(8),
  },
  buyNowImgBox: {
    top: -vScale(16),
    alignSelf: 'center',
    position: 'absolute',
  },
  saleItem: {
    flex: 1,
    overflow: 'hidden',
    height: LARGE_ITEM_HEIGHT,
    marginHorizontal: scale(5.5),
    borderRadius: moderateScale(8),
  },
  productImg: {
    width: '100%',
    height: LARGE_IMG_ITEM_HEIGHT,
    borderTopLeftRadius: moderateScale(8),
    borderTopRightRadius: moderateScale(8),
  },
  saleBgImg: {
    width: '100%',
    alignItems: 'center',
    paddingTop: vScale(24),
    height: LARGE_BG_ITEM_HEIGHT,
  },
  bottomSaleItem: {
    bottom: 0,
    width: '100%',
    height: '100%',
    position: 'absolute',
    justifyContent: 'flex-end',
  },
  saleText: {
    fontWeight: '500',
    color: Colors.COLOR_4,
    fontSize: FontSize.size12,
    lineHeight: moderateScale(14.65),
  },
  priceText: {
    fontWeight: '700',
    color: Colors.COLOR_4,
    fontSize: FontSize.size16,
    lineHeight: moderateScale(19.5),
  },
});
