import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { openSettings } from 'react-native-permissions';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { Colors } from '@themes';
import { translate } from '@assets';
import { moderateScale, scale, vScale } from '@utils';
import { Block, Button, Image, Text, Touchable } from '@components';

interface IContentPermissionNotificationProps {
  onClose: () => void;
}

const ContentPermissionNotificationComponent: React.FC<IContentPermissionNotificationProps> = (props) => {
  const { onClose } = props;
  const insets = useSafeAreaInsets();

  const onPressPermission = () => {
    openSettings();
    onClose();
  };

  return (
    <Block style={[styles.wrapper, { paddingBottom: vScale(10) + insets.bottom }]}>
      <Text size="size18" center>
        {translate('home.turn_on_notification')}
      </Text>
      <Text style={styles.content}>{translate('home.turn_on_notification_content')}</Text>
      <Image source={'turn_on_notification'} style={styles.img} />

      <Block pdHorizontal={24}>
        <Button style={styles.button} onPress={onPressPermission}>
          {translate('home.turn_on_notification')}
        </Button>
        <Touchable pdHorizontal={16} pdVertical={4} alignSelf={'center'} onPress={onClose}>
          <Text>{translate('home.later')}</Text>
        </Touchable>
      </Block>
    </Block>
  );
};

export const ContentPermissionNotification = memo(ContentPermissionNotificationComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    paddingTop: vScale(16),
    backgroundColor: Colors.COLOR_4,
    borderTopLeftRadius: moderateScale(8),
    borderTopRightRadius: moderateScale(8),
  },
  content: {
    fontWeight: '500',
    textAlign: 'center',
    marginVertical: vScale(20),
    marginHorizontal: scale(24),
  },
  img: {
    width: scale(217),
    height: vScale(126),
    alignSelf: 'center',
  },
  button: {
    marginVertical: vScale(12),
    borderRadius: moderateScale(100),
  },
});
