import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import Swiper from 'react-native-swiper';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { moderateScale, scale, vScale } from '@utils';
import { Block, ImageRemote, Touchable } from '@components';

interface IListSuggestShopProps {
  data: any[];
  navigation: any;
}
const DOT_COLOR = 'rgba(255, 255, 255, 0.3)';

const ListSuggestShopComponent: React.FC<IListSuggestShopProps> = (props) => {
  const { data, navigation } = props;

  if (!data || !Array.isArray(data)) return null;

  const onPressBanner = (item: any) => {
    navigation.navigate(SCREENS.SHOPE_PAGE, { id: item?.shop_id });
  };

  return (
    <Block>
      <Swiper
        autoplay
        height={vScale(146)}
        dotColor={DOT_COLOR}
        paginationStyle={styles.dot}
        activeDotColor={Colors.COLOR_4}>
        {data.map((e: any, i: number) => {
          return (
            <Touchable key={i} onPress={() => onPressBanner(e)}>
              <ImageRemote source={e?.avatar} style={styles.bannerImg} resizeMode={'cover'} />
            </Touchable>
          );
        })}
      </Swiper>
    </Block>
  );
};

export const ListSuggestShop = memo(ListSuggestShopComponent, isEqual);

const styles = StyleSheet.create({
  bannerImg: {
    height: vScale(143),
    marginHorizontal: scale(15),
    borderRadius: moderateScale(8),
  },
  dot: {
    bottom: vScale(2),
  },
});
