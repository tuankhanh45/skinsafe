import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';

import { Colors } from '@themes';
import { SCREENS } from '@navigation';
import { IconQr, IconSearch, translate } from '@assets';
import { moderateScale, scale, showToastSuccess, vScale } from '@utils';
import { Block, NotificationBadgeBtn, Text, Touchable } from '@components';

interface IHeaderHomeProps {
  navigation: any;
}

const HeaderHomeComponent: React.FC<IHeaderHomeProps> = (props) => {
  const { navigation } = props;

  const onPressSearch = () => navigation.navigate(SCREENS.SEARCH);
  const onPressQr = () => showToastSuccess('Tính năng đang được phát triển');

  return (
    <Block style={styles.wrapper}>
      <Touchable w={33} center onPress={onPressQr}>
        <IconQr color={Colors.WHITE} />
      </Touchable>

      <Touchable style={styles.input} onPress={onPressSearch}>
        <Block w={17} />
        <Text size={'size12'} color={'COLOR_9'}>
          {translate('cm.search')}
        </Text>
        <IconSearch />
      </Touchable>

      <NotificationBadgeBtn />
    </Block>
  );
};

export const HeaderHome = memo(HeaderHomeComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    height: vScale(54),
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: vScale(10),
    paddingBottom: vScale(5),
    paddingHorizontal: scale(12),
    backgroundColor: Colors.COLOR_1,
  },
  input: {
    flex: 1,
    height: vScale(32),
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: scale(6),
    paddingHorizontal: scale(12),
    backgroundColor: Colors.COLOR_4,
    justifyContent: 'space-between',
    borderRadius: moderateScale(100),
  },
});
