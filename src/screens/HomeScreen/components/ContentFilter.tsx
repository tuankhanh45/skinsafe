import React, { memo } from 'react';
import isEqual from 'react-fast-compare';
import { StyleSheet } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { Colors } from '@themes';
import { IconSuccess } from '@assets';
import { Block, Text, Touchable } from '@components';
import { moderateScale, scale, vScale } from '@utils';

interface IContentFilterProps {
  listFilter: any[];
  selectedFilter: any;
  onPressItemFilter: (e: any) => void;
}

const ContentFilterComponent: React.FC<IContentFilterProps> = (props) => {
  const { listFilter, selectedFilter, onPressItemFilter } = props;
  const insets = useSafeAreaInsets();

  return (
    <Block style={[styles.wrapper, { paddingBottom: vScale(10) + insets.bottom }]}>
      {Array.isArray(listFilter) &&
        listFilter.map((e, i) => {
          const isSelected = e === selectedFilter;
          return (
            <Touchable key={i} style={styles.filterItem} onPress={() => onPressItemFilter(e)}>
              <Text>{e}</Text>
              {isSelected && <IconSuccess />}
            </Touchable>
          );
        })}
    </Block>
  );
};

export const ContentFilter = memo(ContentFilterComponent, isEqual);

const styles = StyleSheet.create({
  wrapper: {
    paddingTop: vScale(16),
    paddingHorizontal: scale(12),
    borderRadius: moderateScale(8),
    backgroundColor: Colors.COLOR_4,
  },
  filterItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: vScale(5),
    justifyContent: 'space-between',
  },
});
