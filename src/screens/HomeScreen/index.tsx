import { useRequest } from 'ahooks';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'react-fast-compare';
import { ImageBackground } from 'react-native';
import { Portal } from 'react-native-portalize';
import { Modalize } from 'react-native-modalize';
import { useScrollToTop } from '@react-navigation/native';
import Geolocation from '@react-native-community/geolocation';
import React, { memo, useCallback, useEffect, useRef, useState } from 'react';

import {
  Category,
  BannerBox,
  HeaderHome,
  BestSeller,
  TrendingBox,
  ListSuggestShop,
  ContentPermissionNotification,
} from './components';
import {
  handleResponse,
  RenderItemProps,
  requestGetListPet,
  requestGetSlideHome,
  requestGetListAddress,
  requestGetListProduct,
  requestGetSlideNearByShop,
} from '@configs';
import styles from './styles';
import { images } from '@assets';
import { Colors } from '@themes';
import * as Com from '@components';
import { useAppStore } from '@store';
import { checkPermissionLocation, fakeItemInOddArray, moderateScale, useCheckNotificationPermission } from '@utils';

const { Block, CustomFlatlist, Image, ItemProduct, LabelCate, ListEmpty, Loading, ModalLocation, SafeWrapper, Text } =
  Com;

interface IHomeProps {
  route: any;
  navigation: any;
}

const HomeComponent: React.FC<IHomeProps> = (props) => {
  const { navigation } = props;
  const refScroll = useRef(null);
  const refModalLocation = useRef<any>(null);
  const refPermissionsNotification = useRef<Modalize>(null);

  const token = useAppStore((state) => state?.account?.token);
  const slideHome = useAppStore((state) => state?.slide?.slideHome);
  const actSaveListPet = useAppStore((state) => state.actSaveListPet);
  const actSaveSlideHome = useAppStore((state) => state.actSaveSlideHome);
  const actSaveListAddress = useAppStore((state) => state.actSaveListAddress);

  const [page, setPage] = useState<number>(1);
  const [loadMore, setLoadMore] = useState<boolean>(true);
  const [listProduct, setListProduct] = useState<any[]>([]);
  const [listQtySell, setListQtySell] = useState<any[]>([]);
  const [refreshing, setRefreshing] = useState<boolean>(false);
  const [slideNearByShop, setSlideNearByShop] = useState<any[]>([]);

  useScrollToTop(refScroll);

  useEffect(() => {
    runGetSlideHome();
    getSlideNearByShop();
    runGetListQtySell({ page: 1, per_page: 6, sort: 'qty_sell' });
    if (token) {
      runGetListPet({ page: 1, size: 20 });
      runGetListAddress();
    }
  }, []);
  useEffect(() => {
    runGetListProduct({ page });
  }, [page]);

  const getSlideNearByShop = async () => {
    const isApprove = await checkPermissionLocation();
    if (!isApprove) {
      refModalLocation.current?.open();
    } else {
      Geolocation.getCurrentPosition((res) => {
        //TODO: use user 's location
        runGetSlideNearByShop({
          distance: 5,
          lat: 21.0879154, // res.coords.latitude,
          lng: 105.7866969, //res.coords.longitude,
        });
      });
    }
  };
  const { run: runGetListAddress } = useRequest(requestGetListAddress, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      Array.isArray(res) && actSaveListAddress(res);
    },
  });
  const { run: runGetListPet } = useRequest(requestGetListPet, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      Array.isArray(res?.data) && actSaveListPet(res?.data);
    },
  });
  const { run: runGetSlideHome } = useRequest(requestGetSlideHome, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (!isEmpty(res)) {
        actSaveSlideHome(res);
      }
    },
  });
  const { run: runGetSlideNearByShop } = useRequest(requestGetSlideNearByShop, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (Array.isArray(res)) {
        setSlideNearByShop(res);
      }
    },
  });
  const { run: runGetListProduct, loading: loadingGetListProduct } = useRequest(requestGetListProduct, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (!res?.next_page_url) {
        setLoadMore(false);
      }
      if (Array.isArray(res?.data)) {
        if (res?.current_page === 1) {
          setListProduct(res?.data);
        } else {
          setListProduct((prev) => [...prev, ...res.data]);
        }
      }
    },
    onFinally: () => refreshing && setRefreshing(false),
  });
  const { run: runGetListQtySell, loading: loadingGetListQtySell } = useRequest(requestGetListProduct, {
    manual: true,
    onSuccess: (data) => {
      const { res } = handleResponse(data);
      if (Array.isArray(res?.data)) {
        setListQtySell(res.data);
      }
    },
  });

  //check permissions notification & show request permission if not authorized
  const isAllowed = useCheckNotificationPermission();
  useEffect(() => {
    if (isAllowed !== null && !isAllowed) {
      refPermissionsNotification.current?.open();
    }
  }, [isAllowed]);

  const renderItem = useCallback(({ item }: RenderItemProps) => {
    if (item.stt === 'empty') return <Block w={106} />;
    return <ItemProduct data={item} />;
  }, []);

  const onEndReached = () => {
    if (loadMore && !loadingGetListProduct) {
      setPage((prev) => prev + 1);
    }
  };
  const onRefresh = () => {
    setLoadMore(true);
    setRefreshing(true);
    runGetSlideHome();
    getSlideNearByShop();
    runGetListQtySell({ page: 1, per_page: 6, sort: 'qty_sell' });
    if (token) {
      runGetListPet({ page: 1, size: 20 });
      runGetListAddress();
    }
    if (page === 1) {
      runGetListProduct({ page: 1 });
    } else {
      setPage(1);
    }
  };

  const ListFooterComponent = () => {
    return (
      <Text center>
        {!loadMore ? 'Hết sản phẩm phù hợp' : loadingGetListProduct ? 'Đang tìm sản phẩm phù hợp' : ''}
      </Text>
    );
  };

  const onClosePermissionsNotification = () => refPermissionsNotification.current?.close();

  return (
    <SafeWrapper bgColor={'COLOR_16'}>
      <HeaderHome navigation={navigation} />
      <ImageBackground source={images.background_triangular} style={styles.bgImg}>
        {loadingGetListProduct && !listProduct.length && <Loading />}
        <CustomFlatlist
          ref={refScroll}
          contentContainerStyle={styles.container}
          ListHeaderComponent={
            <>
              <BannerBox slideHome={slideHome} navigation={navigation} />
              <Block
                bgColor={Colors.WHITE}
                style={{ borderTopLeftRadius: moderateScale(8), borderTopRightRadius: moderateScale(8) }}>
                {listQtySell.length > 0 ? <TrendingBox data={listQtySell} /> : <Block h={20} />}
                <ListSuggestShop data={slideHome?.['2']} navigation={navigation} />
                <Category navigation={navigation} isLogged={Boolean(token)} />

                <Image source="shadow_line" style={styles.line} resizeMode="cover" />
                <LabelCate
                  mgBottom={16}
                  pdHorizontal={15}
                  labelMore="Săn ngay"
                  label="Sản phẩm bán chạy"
                  onPressMore={() => {}}
                />
                <BestSeller data={[]} />

                <Image source="shadow_line" style={styles.line} resizeMode="cover" />
                <LabelCate label="Chủ đề cho bạn" pdHorizontal={15} mgBottom={16} />

                {slideNearByShop.length > 0 && (
                  <>
                    <Image source="shadow_line" style={styles.line} resizeMode="cover" />
                    <LabelCate label="Shop gần bạn" pdHorizontal={15} mgBottom={16} />
                    <ListSuggestShop data={slideNearByShop} navigation={navigation} />
                  </>
                )}

                <Image source="shadow_line" style={styles.line} resizeMode="cover" />
                <LabelCate
                  mgBottom={16}
                  pdHorizontal={15}
                  onPressMore={() => {}}
                  labelMore="Xem tất cả"
                  label="Xu hướng mua hàng"
                />

                <ListSuggestShop data={slideHome?.['3']} navigation={navigation} />

                <Image source="shadow_line" style={styles.line} resizeMode="cover" />
                <LabelCate label="Gợi ý hôm nay" pdHorizontal={15} mgBottom={16} />
              </Block>
            </>
          }
          numColumns={3}
          onRefresh={onRefresh}
          refreshing={refreshing}
          renderItem={renderItem}
          onEndReached={onEndReached}
          data={fakeItemInOddArray(listProduct)}
          columnWrapperStyle={styles.columnWrapper}
          ListFooterComponent={ListFooterComponent}
          ListEmptyComponent={
            <Block bgColor={Colors.WHITE}>
              <ListEmpty />
            </Block>
          }
        />
      </ImageBackground>

      <Portal>
        <Modalize adjustToContentHeight ref={refPermissionsNotification} withHandle={false}>
          <ContentPermissionNotification onClose={onClosePermissionsNotification} />
        </Modalize>
      </Portal>
      <ModalLocation ref={refModalLocation} />
    </SafeWrapper>
  );
};

export const HomeScreen = memo(HomeComponent, isEqual);
