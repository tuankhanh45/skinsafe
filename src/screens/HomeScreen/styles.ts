import { Colors } from '@themes';
import { scale, vScale } from '@utils';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  bgImg: {
    flex: 1,
    width: '100%',
    height: vScale(409),
  },
  container: {
    paddingBottom: vScale(40),
  },
  line: {
    width: '100%',
    height: vScale(16),
  },
  columnWrapper: {
    paddingBottom: vScale(28),
    paddingHorizontal: scale(15),
    backgroundColor: Colors.WHITE,
    justifyContent: 'space-between',
  },
});

export default styles;
