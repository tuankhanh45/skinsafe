import React, { useEffect, useState } from 'react';
import Geolocation, { GeolocationConfiguration } from '@react-native-community/geolocation';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';

import { MainStack } from './Main';
import { AuthStack } from './Auth';
import * as Screens from '@screens';
import { SCREENS } from './screens.name';
import { Block, Text } from '@components';
import * as Progress from 'react-native-progress';
import { vScale } from '@utils';
import { Colors } from '@themes';
import { CareProviderStack } from './Care_Provider';
export * from './screens.name';

const Stack = createStackNavigator();
const screenOptions = {
  headerShown: false,
  gestureEnabled: true,
  ...TransitionPresets.SlideFromRightIOS,
};

const config: GeolocationConfiguration = {
  skipPermissionRequests: true,
  authorizationLevel: 'auto',
  locationProvider: 'android',
};
const AppContainer = () => {
  const [isDownload, setDownload] = useState(false);
  const [progress, setProgress] = useState(0);
  const [status, setStatus] = useState('');
  useEffect(() => {
    Geolocation.setRNConfiguration(config);
    // getUpdateCodePush();
  }, []);
  // const getUpdateCodePush = () => {
  //   CodePush.checkForUpdate().then((update) => {
  //     console.log('KIEM TRA TRANG THAI CODEPUSH', update);
  //     if (!update) {
  //     } else {
  //       setDownload(true);
  //       console.log('An update is available! Should we download it?');
  //       CodePush.disallowRestart();
  //       CodePush.sync(
  //         {
  //           installMode: CodePush.InstallMode.ON_NEXT_RESTART,
  //         },
  //         (syncStatus) => {
  //           console.log('syncStatus==', syncStatus);
  //           codePushStatusDidChange(syncStatus);
  //         },
  //         (progress) => {
  //           codePushDownloadDidProgress(progress);
  //         },
  //       );
  //     }
  //   });
  // };

  // const codePushDownloadDidProgress = (progress: any) => {
  //   const {receivedBytes, totalBytes} = progress;
  //   const temp = (receivedBytes / totalBytes) * 100;
  //   setProgress(temp);
  // };

  // const codePushStatusDidChange = (status: any) => {
  //   switch (status) {
  //     case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
  //       console.log('Checking for updates.');
  //       setStatus('Cập nhật phiên bản mới');
  //       break;
  //     case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
  //       console.log('Downloading package.');
  //       setStatus('Hệ thống đang cập nhật');
  //       break;
  //     case CodePush.SyncStatus.INSTALLING_UPDATE:
  //       console.log('Installing update.');
  //       setStatus('Đang cài đặt bản cập nhật');
  //       break;
  //     case CodePush.SyncStatus.UP_TO_DATE:
  //       console.log('Up-to-date.');
  //       setStatus('Hệ thống cập nhật hoàn tất');
  //       onReStartApp();
  //       break;
  //     case CodePush.SyncStatus.UPDATE_INSTALLED:
  //       console.log('Update installed.');
  //       setStatus('Hệ thống cập nhật hoàn tất');
  //       onReStartApp();
  //       break;
  //   }
  // };

  // const onReStartApp = () => {
  //   CodePush.notifyAppReady();
  //   CodePush.allowRestart();
  //   CodePush.restartApp();
  // };
  return (
    <Block flex>
      <NavigationContainer>
        <Stack.Navigator screenOptions={screenOptions}>
          <Stack.Screen name={SCREENS.CARE_STACK} component={CareProviderStack} />

          <Stack.Screen name={SCREENS.MAIN_STACK} component={MainStack} />
          <Stack.Screen name={SCREENS.AUTH_STACK} component={AuthStack} />
          <Stack.Screen name={SCREENS.BUY_COMBO} component={Screens.BuyComboScreen} />
          <Stack.Screen name={SCREENS.NOTIFICATION} component={Screens.NotificationScreen} />
          <Stack.Screen name={SCREENS.DETAIL_PRODUCT} component={Screens.DetailProductScreen} />
          <Stack.Screen name={SCREENS.SELECT_ADDRESS} component={Screens.SelectAddressScreen} />
          <Stack.Screen name={SCREENS.CREATE_NEW_ADDRESS} component={Screens.CreateNewAddressScreen} />
          <Stack.Screen name={SCREENS.DETAIL_NOTIFICATION} component={Screens.DetailNotificationScreen} />
          <Stack.Screen name={SCREENS.FIND_NEAR_SHOP} component={Screens.FindNearShopScreen} />
          <Stack.Screen name={SCREENS.ORDER_PRODUCT} component={Screens.OrderProductScreen} />
          <Stack.Screen name={SCREENS.ORDER_SERVICE} component={Screens.OrderServiceScreen} />
          <Stack.Screen name={SCREENS.SEARCH} component={Screens.SearchScreen} />
          <Stack.Screen name={SCREENS.REMOTE_ADVISE} component={Screens.RemoteAdviseScreen} />
          <Stack.Screen name={SCREENS.REGISTER_REMOTE_ADVISE} component={Screens.RegisterRemoteAdviseScreen} />
          <Stack.Screen name={SCREENS.HISTORY_ADVISE} component={Screens.HistoryAdviseScreen} />
          <Stack.Screen name={SCREENS.MAIN_PET} component={Screens.MainPetScreen} />
          <Stack.Screen name={SCREENS.CREATE_PET} component={Screens.CreatePetScreen} />
          <Stack.Screen name={SCREENS.EDIT_PET} component={Screens.EditPetScreen} />
          <Stack.Screen name={SCREENS.LIST_PET} component={Screens.ListPetScreen} />
          <Stack.Screen name={SCREENS.DETAILS_PET} component={Screens.DetailPetScreen} />
          <Stack.Screen name={SCREENS.SUCCESS} component={Screens.SuccessScreen} options={{ gestureEnabled: false }} />
          <Stack.Screen name={SCREENS.INJECTION_HISTORY} component={Screens.InjectionHistoryScreen} />
          <Stack.Screen name={SCREENS.INJECTION_CERTIFICATE} component={Screens.InjectionCertificateScreen} />
          <Stack.Screen name={SCREENS.HEALTH_RECORDS} component={Screens.HealthRecordsScreen} />
          <Stack.Screen name={SCREENS.BOOKING_EXAMINATION} component={Screens.BookingExaminationScreen} />
          <Stack.Screen name={SCREENS.BOOKING_INJECTION} component={Screens.BookingInjectionScreen} />
          <Stack.Screen name={SCREENS.DETAIL_NEAR_SHOP} component={Screens.DetailNearShopScreen} />
          <Stack.Screen name={SCREENS.DETAIL_HOTEL_AND_SPA} component={Screens.DetailHotelAndSpaScreen} />
          <Stack.Screen name={SCREENS.SUPPORT} component={Screens.SupportScreen} />
          <Stack.Screen name={SCREENS.REQUEST_SUPPORT} component={Screens.RequestSupportScreen} />
          <Stack.Screen name={SCREENS.DETAIL_ORDER_PRODUCT} component={Screens.DetailOrderProductScreen} />
          <Stack.Screen name={SCREENS.TAKE_CARE} component={Screens.TakeCareScreen} />
          <Stack.Screen name={SCREENS.SHOPE_PAGE} component={Screens.ShopPageScreen} />
          <Stack.Screen name={SCREENS.MY_WALLET} component={Screens.MyWalletScreen} />
          <Stack.Screen name={SCREENS.VOUCHER} component={Screens.VoucherScreenScreen} />
          <Stack.Screen name={SCREENS.LOGIN} component={Screens.LoginScreen} />
          <Stack.Screen name={SCREENS.REGISTER} component={Screens.RegisterScreen} />
          <Stack.Screen name={SCREENS.OTP_CONFIRM} component={Screens.ConfirmOTPScreen} />
          <Stack.Screen name={SCREENS.CREATE_PASSWORD} component={Screens.CreatePasswordScreen} />
          <Stack.Screen name={SCREENS.SUCCESS_ACTION_AUTH} component={Screens.SuccessActionAuthScreen} options={{ gestureEnabled: false }} />
          <Stack.Screen name={SCREENS.FORGOT_PASSWORD} component={Screens.ForgotPasswordScreen} />
          <Stack.Screen name={SCREENS.CHANGE_PASSWORD} component={Screens.ChangePasswordScreen} />
          <Stack.Screen name={SCREENS.DETAIL_SERVICE} component={Screens.DetailServiceScreen} />
          <Stack.Screen name={SCREENS.DETAIL_REVIEW_SERVICE} component={Screens.DetailReviewServiceScreen} />
          <Stack.Screen name={SCREENS.REVIEW} component={Screens.ReviewScreen} />
          <Stack.Screen name={SCREENS.TAB_REVIEW} component={Screens.TabReviewScreen} />
          <Stack.Screen name={SCREENS.SHOPE_PAGE_CATEGORY} component={Screens.ShopPageCategoryScreen} />
          <Stack.Screen name={SCREENS.SHOPE_PAGE_CATEGORY_GENERAL} component={Screens.ShopPageCategoryGeneralScreen} />
          <Stack.Screen name={SCREENS.SHOPE_PAGE_CATEGORY_DETAIL} component={Screens.ShopPageCategoryDetailScreen} />
          {/* new */}
          <Stack.Screen name={SCREENS.PARTNER_REGISTER} component={Screens.PartnerRegisterScreen} />
          <Stack.Screen name={SCREENS.INCOME} component={Screens.IncomeScreen} />
          {/* <Stack.Screen name={SCREENS.QR} component={Screens.QRScanScreen} /> */}
          <Stack.Screen name={SCREENS.CONTACT} component={Screens.ContactScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Block>
  );
};

export default AppContainer;
