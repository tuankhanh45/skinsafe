import React from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';

import { SCREENS } from '@navigation';
import {
  LoginScreen,
  RegisterScreen,
  ConfirmOTPScreen,
  ChangePasswordScreen,
  CreatePasswordScreen,
  ForgotPasswordScreen,
  SuccessActionAuthScreen,
} from '@screens';

const Stack = createStackNavigator();
const screenOptions = { headerShown: false, gestureEnabled: true, ...TransitionPresets.SlideFromRightIOS };

export const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen name={SCREENS.LOGIN} component={LoginScreen} />
      <Stack.Screen name={SCREENS.REGISTER} component={RegisterScreen} />
      <Stack.Screen name={SCREENS.OTP_CONFIRM} component={ConfirmOTPScreen} />
      <Stack.Screen name={SCREENS.CREATE_PASSWORD} component={CreatePasswordScreen} />
      <Stack.Screen
        name={SCREENS.SUCCESS_ACTION_AUTH}
        component={SuccessActionAuthScreen}
        options={{ gestureEnabled: false }}
      />
      <Stack.Screen name={SCREENS.FORGOT_PASSWORD} component={ForgotPasswordScreen} />
      <Stack.Screen name={SCREENS.CHANGE_PASSWORD} component={ChangePasswordScreen} />
    </Stack.Navigator>
  );
};
